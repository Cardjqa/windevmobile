/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_APropos
 * Date : 11/09/2017 09:51:45
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.card.Annuaire.wdgen;


import com.card.Annuaire.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.zml.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_APropos extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_APropos
////////////////////////////////////////////////////////////////////////////

/**
 * ZM_MENU
 */
class GWDZM_MENU extends WDZoneMultiligne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_APropos.ZM_MENU
////////////////////////////////////////////////////////////////////////////

/**
 * ZM_MENU_Ligne1
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_APropos.ZM_MENU
////////////////////////////////////////////////////////////////////////////

/**
 * IMG_PICTO_VERSION
 */
class GWDIMG_PICTO_VERSION extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_APropos.ZM_MENU.IMG_PICTO_VERSION
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2954868754619826903l);

super.setChecksum("906354321");

super.setNom("IMG_PICTO_VERSION");

super.setType(8);

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(15, 13);

super.setTailleInitiale(36, 36);

super.setValeurInitiale("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\about_version@dpi160.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setTransparence(1);

super.setParamImage(2097162, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(false);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_PICTO_VERSION mWD_IMG_PICTO_VERSION = new GWDIMG_PICTO_VERSION();

/**
 * LIB_VERSION
 */
class GWDLIB_VERSION extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_APropos.ZM_MENU.LIB_VERSION
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2954868754619892439l);

super.setChecksum("906417577");

super.setNom("LIB_VERSION");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("0.0.0.9");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(71, 33);

super.setTailleInitiale(216, 20);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x949494, 0xFFFFFFFF, creerPolice_GEN("Arial", -7.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_VERSION mWD_LIB_VERSION = new GWDLIB_VERSION();

/**
 * LIB_LIB_VERSION
 */
class GWDLIB_LIB_VERSION extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_APropos.ZM_MENU.LIB_LIB_VERSION
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2954868754619957975l);

super.setChecksum("906483113");

super.setNom("LIB_LIB_VERSION");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Version de l'application");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(71, 11);

super.setTailleInitiale(216, 21);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xFFFFFFFF, creerPolice_GEN("Arial", -8.000000, 1), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_LIB_VERSION mWD_LIB_LIB_VERSION = new GWDLIB_LIB_VERSION();
class GWDZM_MENU_Ligne1 extends LigneZMLStatique
{
/**
 * Initialise tous les champs de FEN_APropos.ZM_MENU
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_APropos.ZM_MENU
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_IMG_PICTO_VERSION.initialiserObjet();
super.ajouterChamp(mWD_IMG_PICTO_VERSION);
mWD_LIB_VERSION.initialiserObjet();
super.ajouterChamp(mWD_LIB_VERSION);
mWD_LIB_LIB_VERSION.initialiserObjet();
super.ajouterChamp(mWD_LIB_LIB_VERSION);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(65);

super.setVisibleInitial(true);

super.setModeSelection(0);

initialiserSousObjets();
super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_MENU_Ligne1 mWD_ZM_MENU_Ligne1 = new GWDZM_MENU_Ligne1();

/**
 * ZM_MENU_Ligne2
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_APropos.ZM_MENU
////////////////////////////////////////////////////////////////////////////

/**
 * IMG_PICTO_NOTER
 */
class GWDIMG_PICTO_NOTER extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_APropos.ZM_MENU.IMG_PICTO_NOTER
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2954868754620089047l);

super.setChecksum("906616465");

super.setNom("IMG_PICTO_NOTER");

super.setType(8);

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(15, 12);

super.setTailleInitiale(36, 36);

super.setValeurInitiale("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\about_noter@dpi160.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setTransparence(1);

super.setParamImage(2097162, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(false);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_PICTO_NOTER mWD_IMG_PICTO_NOTER = new GWDIMG_PICTO_NOTER();

/**
 * LIB_LIB_VOTER
 */
class GWDLIB_LIB_VOTER extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_APropos.ZM_MENU.LIB_LIB_VOTER
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2954868754620154583l);

super.setChecksum("906679721");

super.setNom("LIB_LIB_VOTER");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Noter l'application");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(71, 10);

super.setTailleInitiale(216, 21);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xFFFFFFFF, creerPolice_GEN("Arial", -8.000000, 1), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_LIB_VOTER mWD_LIB_LIB_VOTER = new GWDLIB_LIB_VOTER();

/**
 * LIB_VOTER
 */
class GWDLIB_VOTER extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_APropos.ZM_MENU.LIB_VOTER
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2954868754620220119l);

super.setChecksum("906745257");

super.setNom("LIB_VOTER");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Notez cette application");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(71, 32);

super.setTailleInitiale(216, 31);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x949494, 0xFFFFFFFF, creerPolice_GEN("Arial", -7.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_VOTER mWD_LIB_VOTER = new GWDLIB_VOTER();
class GWDZM_MENU_Ligne2 extends LigneZMLStatique
{
/**
 * Initialise tous les champs de FEN_APropos.ZM_MENU
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_APropos.ZM_MENU
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_IMG_PICTO_NOTER.initialiserObjet();
super.ajouterChamp(mWD_IMG_PICTO_NOTER);
mWD_LIB_LIB_VOTER.initialiserObjet();
super.ajouterChamp(mWD_LIB_LIB_VOTER);
mWD_LIB_VOTER.initialiserObjet();
super.ajouterChamp(mWD_LIB_VOTER);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(65);

super.setVisibleInitial(true);

super.setModeSelection(1);

initialiserSousObjets();
super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_MENU_Ligne2 mWD_ZM_MENU_Ligne2 = new GWDZM_MENU_Ligne2();

/**
 * ZM_MENU_Ligne3
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_APropos.ZM_MENU
////////////////////////////////////////////////////////////////////////////

/**
 * IMG_PICTO_WM
 */
class GWDIMG_PICTO_WM extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_APropos.ZM_MENU.IMG_PICTO_WM
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2954868754620351191l);

super.setChecksum("906878609");

super.setNom("IMG_PICTO_WM");

super.setType(8);

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(15, 25);

super.setTailleInitiale(36, 36);

super.setValeurInitiale("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\about@DPI160.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setTransparence(1);

super.setParamImage(2097162, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(false);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_PICTO_WM mWD_IMG_PICTO_WM = new GWDIMG_PICTO_WM();

/**
 * LIB_LIB_WM
 */
class GWDLIB_LIB_WM extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_APropos.ZM_MENU.LIB_LIB_WM
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2954868754620416727l);

super.setChecksum("906941865");

super.setNom("LIB_LIB_WM");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Card");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(71, 22);

super.setTailleInitiale(216, 21);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xFFFFFFFF, creerPolice_GEN("Arial", -8.000000, 1), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_LIB_WM mWD_LIB_LIB_WM = new GWDLIB_LIB_WM();

/**
 * LIB_WM
 */
class GWDLIB_WM extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_APropos.ZM_MENU.LIB_WM
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2954868754620482263l);

super.setChecksum("907007401");

super.setNom("LIB_WM");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("En savoir plus sur Card");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(71, 44);

super.setTailleInitiale(216, 20);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x949494, 0xFFFFFFFF, creerPolice_GEN("Arial", -7.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_WM mWD_LIB_WM = new GWDLIB_WM();
class GWDZM_MENU_Ligne3 extends LigneZMLStatique
{
/**
 * Initialise tous les champs de FEN_APropos.ZM_MENU
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_APropos.ZM_MENU
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_IMG_PICTO_WM.initialiserObjet();
super.ajouterChamp(mWD_IMG_PICTO_WM);
mWD_LIB_LIB_WM.initialiserObjet();
super.ajouterChamp(mWD_LIB_LIB_WM);
mWD_LIB_WM.initialiserObjet();
super.ajouterChamp(mWD_LIB_WM);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(91);

super.setVisibleInitial(true);

super.setModeSelection(1);

initialiserSousObjets();
super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_MENU_Ligne3 mWD_ZM_MENU_Ligne3 = new GWDZM_MENU_Ligne3();

/**
 * ZM_MENU_Ligne4
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_APropos.ZM_MENU
////////////////////////////////////////////////////////////////////////////

/**
 * IMG_PICTO_AUTRESAPPS
 */
class GWDIMG_PICTO_AUTRESAPPS extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_APropos.ZM_MENU.IMG_PICTO_AUTRESAPPS
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2954868754620613335l);

super.setChecksum("907140753");

super.setNom("IMG_PICTO_AUTRESAPPS");

super.setType(8);

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(15, 19);

super.setTailleInitiale(36, 48);

super.setValeurInitiale("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\about_app@dpi160.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setTransparence(1);

super.setParamImage(2097162, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(false);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_PICTO_AUTRESAPPS mWD_IMG_PICTO_AUTRESAPPS = new GWDIMG_PICTO_AUTRESAPPS();

/**
 * LIB_LIB_AUTRESAPPS
 */
class GWDLIB_LIB_AUTRESAPPS extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_APropos.ZM_MENU.LIB_LIB_AUTRESAPPS
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2954868754620678871l);

super.setChecksum("907204009");

super.setNom("LIB_LIB_AUTRESAPPS");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Autres applications");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(71, 5);

super.setTailleInitiale(216, 24);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xFFFFFFFF, creerPolice_GEN("Arial", -8.000000, 1), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_LIB_AUTRESAPPS mWD_LIB_LIB_AUTRESAPPS = new GWDLIB_LIB_AUTRESAPPS();

/**
 * LIB_AUTRESAPPS
 */
class GWDLIB_AUTRESAPPS extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_APropos.ZM_MENU.LIB_AUTRESAPPS
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2954868754620744407l);

super.setChecksum("907269545");

super.setNom("LIB_AUTRESAPPS");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Découvrez d'autres applications dédiées.");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(71, 31);

super.setTailleInitiale(216, 50);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x949494, 0xFFFFFFFF, creerPolice_GEN("Arial", -7.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_AUTRESAPPS mWD_LIB_AUTRESAPPS = new GWDLIB_AUTRESAPPS();
class GWDZM_MENU_Ligne4 extends LigneZMLStatique
{
/**
 * Initialise tous les champs de FEN_APropos.ZM_MENU
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_APropos.ZM_MENU
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_IMG_PICTO_AUTRESAPPS.initialiserObjet();
super.ajouterChamp(mWD_IMG_PICTO_AUTRESAPPS);
mWD_LIB_LIB_AUTRESAPPS.initialiserObjet();
super.ajouterChamp(mWD_LIB_LIB_AUTRESAPPS);
mWD_LIB_AUTRESAPPS.initialiserObjet();
super.ajouterChamp(mWD_LIB_AUTRESAPPS);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(92);

super.setVisibleInitial(true);

super.setModeSelection(1);

initialiserSousObjets();
super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_MENU_Ligne4 mWD_ZM_MENU_Ligne4 = new GWDZM_MENU_Ligne4();
/**
 * Initialise tous les champs de FEN_APropos.ZM_MENU
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_APropos.ZM_MENU
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_ZM_MENU_Ligne1.initialiserObjet();
super.ajouterLigne(mWD_ZM_MENU_Ligne1);
mWD_ZM_MENU_Ligne2.initialiserObjet();
super.ajouterLigne(mWD_ZM_MENU_Ligne2);
mWD_ZM_MENU_Ligne3.initialiserObjet();
super.ajouterLigne(mWD_ZM_MENU_Ligne3);
mWD_ZM_MENU_Ligne4.initialiserObjet();
super.ajouterLigne(mWD_ZM_MENU_Ligne4);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setPresenceLibelle(false);

super.setQuid(2954868754619695831l);

super.setChecksum("906263833");

super.setNom("ZM_MENU");

super.setType(97);

super.setLibelle("1.0.0.0");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(10, 124);

super.setTailleInitiale(299, 316);

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setNumTab(1);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setIndiceModeleLigneDynamique(0);

super.setTauxParallaxe(500, 0);

super.setCouleurTexteAutomatique(0xFF000001);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCouleurSeparateur(0xD1D1D1);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE5E5E5, 0x656565, 0xF2000000, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0xCCCCCC, 0x4C4C4C, 0xF2000000, 9, 9, 1, 1));

super.setStyleSelection(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xD9D9D9, 4, 4, 1, 1));

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de ZM_MENU
 */
public void init()
{
super.init();

// LIB_VERSION = ExeInfo(exeVersion)
mWD_LIB_VERSION.setValeur(WDAPIExe.exeInfo("VERSION"));

}




/**
 * Traitement: Sélection (clic) d'une ligne dans ZM_MENU
 */
public void selectionLigne()
{
super.selectionLigne();


////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_sURL = new WDChaineA();



// sURL est une chaine ansi


// SELON ZM_MENU
// Délimiteur de visibilité pour ne pas étendre la visibilité de la variable temporaire _WDExpSelon
{
WDObjet _WDExpSelon0 = this;
if(_WDExpSelon0.opEgal(1))
{
}
else if(_WDExpSelon0.opEgal(2))
{
// 		<COMPILE SI TypeConfiguration=Android>
{
// 			AppliOuvreFiche()
WDAPIDivers.appliOuvreFiche();

}

}
else if(_WDExpSelon0.opEgal(3))
{
// 		sURL = "http://www.card.fr"
vWD_sURL.setValeur("http://www.card.fr");

// 		LanceAppliAssociée(sURL)
WDAPIDiversSTD.lanceAppliAssociee(vWD_sURL.getString());

}
else if(_WDExpSelon0.opEgal(4))
{
// 		<COMPILE SI TypeConfiguration=Android>
{
// 			sURL = "market://search?q=pub:..."
vWD_sURL.setValeur("market://search?q=pub:...");

}

// 		<COMPILE SI TypeConfiguration=iOS>

// 		si sURL<>"" alors
if(vWD_sURL.opDiff(""))
{
// 			LanceAppliAssociée(sURL)	
WDAPIDiversSTD.lanceAppliAssociee(vWD_sURL.getString());

}

}

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurSelection();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_MENU mWD_ZM_MENU;

/**
 * LIB_Titre_de_fenêtre
 */
class GWDLIB_Titre_de_fenetre extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_APropos.LIB_Titre_de_fenêtre
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2961941874177512836l);

super.setChecksum("818907469");

super.setNom("LIB_Titre_de_fenêtre");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("A propos");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(8, 61);

super.setTailleInitiale(304, 27);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xC0C0C0, creerPolice_GEN("Arial", -8.000000, 1), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0x30303, 0x0, 0xC0C0C0, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Titre_de_fenetre mWD_LIB_Titre_de_fenetre;

/**
 * LIB_Nom_Utilisateur
 */
class GWDLIB_Nom_Utilisateur extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_APropos.LIB_Nom_Utilisateur
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2978662720079930889l);

super.setChecksum("-2123988649");

super.setNom("LIB_Nom_Utilisateur");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Annuaire");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(8, 8);

super.setTailleInitiale(304, 44);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0xFF000001, 0xFFFFFFFF, creerPolice_GEN("Arial", -16.000000, 1), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(15, 0xF3000000, 0xF1000000, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Nom_Utilisateur mWD_LIB_Nom_Utilisateur;

/**
 * Traitement: Déclarations globales de FEN_APropos
 */
//  Cette fenêtre est un exemple d'interface personnalisable.
//  Vous pouvez la modifier selon vos besoins.
//  Les traitements à personnaliser sont identifiés par des "// A faire" que
//  vous pouvez retrouver en utilisant la recherche ou le volet "Liste des tâches"
public void declarerGlobale(WDObjet[] WD_tabParam)
{
super.declarerGlobale(WD_tabParam);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_APropos
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_ZM_MENU = new GWDZM_MENU();
mWD_LIB_Titre_de_fenetre = new GWDLIB_Titre_de_fenetre();
mWD_LIB_Nom_Utilisateur = new GWDLIB_Nom_Utilisateur();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_APropos
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(2954868754619630295l);

super.setChecksum("911010074");

super.setNom("FEN_APropos");

super.setType(1);

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xC0C0C0);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 458);

super.setTitre("A  propos");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 8, 0, 1);

super.setCouleurTexteAutomatique(0xF4000000);

super.setCouleurBarreSysteme(0xFF000001);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_APropos
////////////////////////////////////////////////////////////////////////////
mWD_ZM_MENU.initialiserObjet();
super.ajouter("ZM_MENU", mWD_ZM_MENU);
mWD_LIB_Titre_de_fenetre.initialiserObjet();
super.ajouter("LIB_Titre_de_fenêtre", mWD_LIB_Titre_de_fenetre);
mWD_LIB_Nom_Utilisateur.initialiserObjet();
super.ajouter("LIB_Nom_Utilisateur", mWD_LIB_Nom_Utilisateur);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return false;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 1;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return true;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPMon_Projet_an.ms_Project.mWD_FEN_APropos;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "190 ACTIVPHONE 7#WM";
}
}

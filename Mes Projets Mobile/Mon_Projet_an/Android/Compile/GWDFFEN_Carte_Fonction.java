/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_Carte_Fonction
 * Date : 11/09/2017 11:24:37
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.card.Annuaire.wdgen;


import com.card.Annuaire.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.carte.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.geo.*;
import fr.pcsoft.wdjava.core.poo.*;
import fr.pcsoft.wdjava.core.parcours.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.geo.map.*;
import fr.pcsoft.wdjava.core.allocation.*;
import fr.pcsoft.wdjava.core.types.collection.tableau.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_Carte_Fonction extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_Carte_Fonction
////////////////////////////////////////////////////////////////////////////

/**
 * CARTE_Fonctions
 */
class GWDCARTE_Fonctions extends WDChampCarteV2
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Carte_Fonction.CARTE_Fonctions
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2981188064927820187l);

super.setChecksum("738729019");

super.setNom("CARTE_Fonctions");

super.setType(30001);

super.setLibelle("Carte");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 458);

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(10, 1000, 1000, 1000, 1000);

super.setPersistant(true);

super.setImageMarqueur("");

super.setParamZoom(true, true);

super.setModeCarte(1);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(2, 0xCCCCCC, 0x4C4C4C, 0xE0E0E0, 4, 4, 1, 1), 0, 0, 0, 0);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de CARTE_Fonctions
 */
public void init()
// <COMPILE SI Configuration="Universal Windows 10 App">
// CarteLicenceBing("AIzaSyDR7D0LDu1kWIuEqfileKMM7An8biGCnMs")
// <FIN>
{
super.init();

// SI tabMarqueurGéoposition <> Null _ET_ tabMarqueurGéoposition..Occurrence > 0 ALORS
if((vWD_tabMarqueurGeoposition.isNotNull() && vWD_tabMarqueurGeoposition.getOccurrence().opSup(0)))
{
// 	TableauSupprimeTout(tabMarqueurGéoposition)
WDAPITableau.tableauSupprimeTout(vWD_tabMarqueurGeoposition);

}

// SI tGeoadresse <> Null _ET_ tGeoadresse..Occurrence > 0 ALORS
if((vWD_tGeoadresse.isNotNull() && vWD_tGeoadresse.getOccurrence().opSup(0)))
{
// 	TableauSupprimeTout(tGeoadresse)	
WDAPITableau.tableauSupprimeTout(vWD_tGeoadresse);

}

// CarteSupprimeTout(CARTE_Fonctions)
WDAPICarte.carteSupprimeTout(this);

// SI ChargeParamètre("GeoPrécis") = Vrai ALORS
if(WDAPIPersist.chargeParametre("GeoPrécis").opEgal(true))
{
// 	GPSInitParamètre(gpsAuto, gpsPrécisionElevée + gpsVitesse + gpsEnergieMoyenne)
WDAPIGPS.gpsInitParametre(4,70);

}
else
{
// 	GPSInitParamètre(gpsAuto,  gpsVitesse + gpsEnergieMoyenne)	
WDAPIGPS.gpsInitParametre(4,68);

}

// aIci = GPSDernièrePosition()
vWD_aIci.setValeur(WDAPIGPS.gpsDernierePosition());

// SI ErreurDétectée = Faux   ALORS
if(WDObjet.ErreurDetectee.opEgal(false))
{
// 	sGeoIci = ChaîneConstruit("%1,%2",aIci..Latitude,aIci..Longitude)
vWD_sGeoIci.setValeur(WDAPIChaine.chaineConstruit(new WDChaineU("%1,%2"),new String[] {vWD_aIci.getProp(EWDPropriete.PROP_LATITUDE).getString(),vWD_aIci.getProp(EWDPropriete.PROP_LONGITUDE).getString()} ));

// 	géoRécupèreAdresse(sGeoIci, tGeoadresse,1)
WDAPIGeo.geoRecupereAdresse(vWD_sGeoIci.getString(),vWD_tGeoadresse,1);

// 	POUR TOUT sEGeoAdresse,nCompteurGeoAd de tGeoadresse
IWDParcours parcours1 = null;
try
{
WDObjet vWD_sEGeoAdresse = WDParcoursFactory.creerVariableParcours(vWD_tGeoadresse);
WDEntier vWD_nCompteurGeoAd = new WDEntier();
parcours1 = WDParcoursFactory.pourTout(vWD_tGeoadresse, vWD_sEGeoAdresse, vWD_nCompteurGeoAd, null, null, 0x0, 0x2);
while(parcours1.testParcours())
{
// 		<COMPILE SI Configuration="Application Android" _OU_ Configuration="Application iOS" >
{
// 			mMarqueur..Nom = nCompteurGeoAd*17
vWD_mMarqueur.setProp(EWDPropriete.PROP_NOM,vWD_nCompteurGeoAd.opMult(17));

}

// 		pPosition est une géoPosition=sEGeoAdresse..Position
WDObjet vWD_pPosition = WDVarNonAllouee.ref;
vWD_pPosition = new WDInstance( new WDGeoPosition() );

vWD_pPosition.setValeur(parcours1.getVariableParcours().getProp(EWDPropriete.PROP_POSITION));


// 		mMarqueur..Position = pPosition
vWD_mMarqueur.setProp(EWDPropriete.PROP_POSITION,vWD_pPosition);

// 		mMarqueur..Description =mMarqueur..Description+sEGeoAdresse..Pays+RC+sEGeoAdresse..Région+RC+sEGeoAdresse..Rue+RC+sEGeoAdresse..CodePostal+","+sEGeoAdresse..Ville
vWD_mMarqueur.setProp(EWDPropriete.PROP_DESCRIPTION,vWD_mMarqueur.getProp(EWDPropriete.PROP_DESCRIPTION).opPlus(parcours1.getVariableParcours().getProp(EWDPropriete.PROP_PAYS)).opPlus("\r\n").opPlus(parcours1.getVariableParcours().getProp(EWDPropriete.PROP_REGION)).opPlus("\r\n").opPlus(parcours1.getVariableParcours().getProp(EWDPropriete.PROP_RUE)).opPlus("\r\n").opPlus(parcours1.getVariableParcours().getProp(EWDPropriete.PROP_CODEPOSTAL)).opPlus(",").opPlus(parcours1.getVariableParcours().getProp(EWDPropriete.PROP_VILLE)));

// 		mMarqueur..ActionClic=ProcClicMarqueur
vWD_mMarqueur.setProp(EWDPropriete.PROP_ACTIONCLIC,(new WDChaineU("FEN_Carte_Fonction.ProcClicMarqueur")));

// 		CarteAjouteMarqueur(CARTE_Fonctions,mMarqueur)
WDAPICarte.carteAjouteMarqueur(this,vWD_mMarqueur);

// 		TableauAjoute(tabMarqueurGéoposition,mMarqueur)
WDAPITableau.tableauAjoute(vWD_tabMarqueurGeoposition,vWD_mMarqueur);

}

}
finally
{
if(parcours1 != null)
{
parcours1.finParcours();
}
}


}
else
{
// 	VariableRAZ(aIci)
WDAPIVM.variableRAZ(vWD_aIci);

// 	aIci=GPSRécupèrePosition()
vWD_aIci.setValeur(WDAPIGPS.gpsRecuperePosition());

// 	SI ErreurDétectée = Faux  ALORS
if(WDObjet.ErreurDetectee.opEgal(false))
{
// 		sGeoIci = ChaîneConstruit("%1,%2",aIci..Latitude,aIci..Longitude)
vWD_sGeoIci.setValeur(WDAPIChaine.chaineConstruit(new WDChaineU("%1,%2"),new String[] {vWD_aIci.getProp(EWDPropriete.PROP_LATITUDE).getString(),vWD_aIci.getProp(EWDPropriete.PROP_LONGITUDE).getString()} ));

// 		géoRécupèreAdresse(sGeoIci, tGeoadresse,1)
WDAPIGeo.geoRecupereAdresse(vWD_sGeoIci.getString(),vWD_tGeoadresse,1);

// 		SI ErreurDétectée ALORS
if(WDObjet.ErreurDetectee.getBoolean())
{
// 				ToastAffiche("Impossible d'avoir votre position.")
WDAPIToast.toastAffiche("Impossible d'avoir votre position.");

}
else
{
// 			POUR TOUT sEGeoAdresse,nCompteurGeoAd de tGeoadresse
IWDParcours parcours2 = null;
try
{
WDObjet vWD_sEGeoAdresse = WDParcoursFactory.creerVariableParcours(vWD_tGeoadresse);
WDEntier vWD_nCompteurGeoAd = new WDEntier();
parcours2 = WDParcoursFactory.pourTout(vWD_tGeoadresse, vWD_sEGeoAdresse, vWD_nCompteurGeoAd, null, null, 0x0, 0x2);
while(parcours2.testParcours())
{
// 				<COMPILE SI Configuration="Application Android" _OU_ Configuration="Application iOS" >
{
// 					mMarqueur..Nom = nCompteurGeoAd*17
vWD_mMarqueur.setProp(EWDPropriete.PROP_NOM,vWD_nCompteurGeoAd.opMult(17));

}

// 				pPosition est une géoPosition=sEGeoAdresse..Position
WDObjet vWD_pPosition = WDVarNonAllouee.ref;
vWD_pPosition = new WDInstance( new WDGeoPosition() );

vWD_pPosition.setValeur(parcours2.getVariableParcours().getProp(EWDPropriete.PROP_POSITION));


// 				mMarqueur..Position = pPosition
vWD_mMarqueur.setProp(EWDPropriete.PROP_POSITION,vWD_pPosition);

// 				mMarqueur..Description =mMarqueur..Description+sEGeoAdresse..Pays+RC+sEGeoAdresse..Région+RC+sEGeoAdresse..Rue+RC+sEGeoAdresse..CodePostal+","+sEGeoAdresse..Ville
vWD_mMarqueur.setProp(EWDPropriete.PROP_DESCRIPTION,vWD_mMarqueur.getProp(EWDPropriete.PROP_DESCRIPTION).opPlus(parcours2.getVariableParcours().getProp(EWDPropriete.PROP_PAYS)).opPlus("\r\n").opPlus(parcours2.getVariableParcours().getProp(EWDPropriete.PROP_REGION)).opPlus("\r\n").opPlus(parcours2.getVariableParcours().getProp(EWDPropriete.PROP_RUE)).opPlus("\r\n").opPlus(parcours2.getVariableParcours().getProp(EWDPropriete.PROP_CODEPOSTAL)).opPlus(",").opPlus(parcours2.getVariableParcours().getProp(EWDPropriete.PROP_VILLE)));

// 				mMarqueur..ActionClic=ProcClicMarqueur
vWD_mMarqueur.setProp(EWDPropriete.PROP_ACTIONCLIC,(new WDChaineU("FEN_Carte_Fonction.ProcClicMarqueur")));

// 				CarteAjouteMarqueur(CARTE_Fonctions,mMarqueur)
WDAPICarte.carteAjouteMarqueur(this,vWD_mMarqueur);

// 				TableauAjoute(tabMarqueurGéoposition,mMarqueur)
WDAPITableau.tableauAjoute(vWD_tabMarqueurGeoposition,vWD_mMarqueur);

}

}
finally
{
if(parcours2 != null)
{
parcours2.finParcours();
}
}


}

}

}

// sListeFonction = ChargeParamètre("ListeFonctions","")
vWD_sListeFonction.setValeur(WDAPIPersist.chargeParametre("ListeFonctions",""));

// SI Taille(sListeFonction) > 0 ALORS
if(WDAPIChaine.taille(vWD_sListeFonction).opSup(0))
{
// 	tabListeFonction = ChaîneDécoupe(sListeFonction,";")
vWD_tabListeFonction.setValeur(WDAPIChaine.chaineDecoupe(vWD_sListeFonction,new WDObjet[] {new WDChaineU(";")} ));

// 	POUR TOUT  sElément , nCompteur de tabListeFonction
IWDParcours parcours3 = null;
try
{
WDObjet vWD_sElement = WDParcoursFactory.creerVariableParcours(vWD_tabListeFonction);
WDEntier vWD_nCompteur = new WDEntier();
parcours3 = WDParcoursFactory.pourTout(vWD_tabListeFonction, vWD_sElement, vWD_nCompteur, null, null, 0x0, 0x2);
while(parcours3.testParcours())
{
// 		FEN_Recherche.myTableau1 = ChaîneDécoupe(sElément,",")
GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.setValeur(WDAPIChaine.chaineDecoupe(parcours3.getVariableParcours(),new WDObjet[] {new WDChaineU(",")} ));

// 		POUR TOUT sElément1 ,nCompteur1 de FEN_Recherche.myTableau1
IWDParcours parcours4 = null;
try
{
WDObjet vWD_sElement1 = WDParcoursFactory.creerVariableParcours(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1);
WDEntier vWD_nCompteur1 = new WDEntier();
parcours4 = WDParcoursFactory.pourTout(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1, vWD_sElement1, vWD_nCompteur1, null, null, 0x0, 0x2);
while(parcours4.testParcours())
{
// 			SI nCompteur1 = 1 ALORS
if(vWD_nCompteur1.opEgal(1))
{
// 				SI Taille(sElément1) > 0 ALORS
if(WDAPIChaine.taille(parcours4.getVariableParcours()).opSup(0))
{
// 					mMarqueur..Description=sElément1+RC
vWD_mMarqueur.setProp(EWDPropriete.PROP_DESCRIPTION,parcours4.getVariableParcours().opPlus("\r\n"));

}

}

// 			SI nCompteur1 = 2 ALORS 
if(vWD_nCompteur1.opEgal(2))
{
// 				SI Taille(sElément1) > 0 ALORS
if(WDAPIChaine.taille(parcours4.getVariableParcours()).opSup(0))
{
// 						TableauSupprimeTout(FEN_Carte_Fonction.tGeoadresse)
WDAPITableau.tableauSupprimeTout(vWD_tGeoadresse);

// 						géoRécupèreAdresse(sElément1,tGeoadresse,1)
WDAPIGeo.geoRecupereAdresse(parcours4.getVariableParcours().getString(),vWD_tGeoadresse,1);

// 						SI ErreurDétectée ALORS
if(WDObjet.ErreurDetectee.getBoolean())
{
// 							sElément1 = ExtraitChaîne(sElément1,2,",")
parcours4.getVariableParcours().setValeur(WDAPIChaine.extraitChaine(parcours4.getVariableParcours(),2,new WDChaineU(",")));

// 							géoRécupèreAdresse(sElément1,tGeoadresse,1)
WDAPIGeo.geoRecupereAdresse(parcours4.getVariableParcours().getString(),vWD_tGeoadresse,1);

}

// 					SI ErreurDétectée = Vrai ALORS
if(WDObjet.ErreurDetectee.opEgal(true))
{
}
else
{
// 					POUR TOUT sEGeoAdresse,nCompteurGeoAd de tGeoadresse
IWDParcours parcours5 = null;
try
{
WDObjet vWD_sEGeoAdresse = WDParcoursFactory.creerVariableParcours(vWD_tGeoadresse);
WDEntier vWD_nCompteurGeoAd = new WDEntier();
parcours5 = WDParcoursFactory.pourTout(vWD_tGeoadresse, vWD_sEGeoAdresse, vWD_nCompteurGeoAd, null, null, 0x0, 0x2);
while(parcours5.testParcours())
{
// 							<COMPILE SI Configuration="Application Android" _OU_ Configuration="Application iOS" >
{
// 								mMarqueur..Nom = (nCompteur+nCompteur1+nCompteurGeoAd)*17
vWD_mMarqueur.setProp(EWDPropriete.PROP_NOM,vWD_nCompteur.opPlus(vWD_nCompteur1).opPlus(vWD_nCompteurGeoAd).opMult(17));

}

// 							pPosition est une géoPosition=sEGeoAdresse..Position
WDObjet vWD_pPosition = WDVarNonAllouee.ref;
vWD_pPosition = new WDInstance( new WDGeoPosition() );

vWD_pPosition.setValeur(parcours5.getVariableParcours().getProp(EWDPropriete.PROP_POSITION));


// 							mMarqueur..Position = pPosition
vWD_mMarqueur.setProp(EWDPropriete.PROP_POSITION,vWD_pPosition);

// 							mMarqueur..Description =mMarqueur..Description+sEGeoAdresse..Pays+RC+sEGeoAdresse..Région+RC+sEGeoAdresse..Rue+RC+sEGeoAdresse..CodePostal+","+sEGeoAdresse..Ville
vWD_mMarqueur.setProp(EWDPropriete.PROP_DESCRIPTION,vWD_mMarqueur.getProp(EWDPropriete.PROP_DESCRIPTION).opPlus(parcours5.getVariableParcours().getProp(EWDPropriete.PROP_PAYS)).opPlus("\r\n").opPlus(parcours5.getVariableParcours().getProp(EWDPropriete.PROP_REGION)).opPlus("\r\n").opPlus(parcours5.getVariableParcours().getProp(EWDPropriete.PROP_RUE)).opPlus("\r\n").opPlus(parcours5.getVariableParcours().getProp(EWDPropriete.PROP_CODEPOSTAL)).opPlus(",").opPlus(parcours5.getVariableParcours().getProp(EWDPropriete.PROP_VILLE)));

// 							mMarqueur..ActionClic=ProcClicMarqueur
vWD_mMarqueur.setProp(EWDPropriete.PROP_ACTIONCLIC,(new WDChaineU("FEN_Carte_Fonction.ProcClicMarqueur")));

// 							CarteAjouteMarqueur(CARTE_Fonctions,mMarqueur)
WDAPICarte.carteAjouteMarqueur(this,vWD_mMarqueur);

// 							TableauAjoute(tabMarqueurGéoposition,mMarqueur)
WDAPITableau.tableauAjoute(vWD_tabMarqueurGeoposition,vWD_mMarqueur);

// 							SI ErreurDétectée = Vrai ALORS
if(WDObjet.ErreurDetectee.opEgal(true))
{
}

}

}
finally
{
if(parcours5 != null)
{
parcours5.finParcours();
}
}


}

}

}

}

}
finally
{
if(parcours4 != null)
{
parcours4.finParcours();
}
}


}

}
finally
{
if(parcours3 != null)
{
parcours3.finParcours();
}
}


}
else
{
// 	TableauSupprimeTout(tabMarqueurGéoposition)	
WDAPITableau.tableauSupprimeTout(vWD_tabMarqueurGeoposition);

}

// SI tabMarqueurGéoposition..Occurrence > 1 ALORS
if(vWD_tabMarqueurGeoposition.getOccurrence().opSup(1))
{
// 	CarteAjouteItinéraire(CARTE_Fonctions,tabMarqueurGéoposition)
WDAPICarte.carteAjouteItineraire(this,vWD_tabMarqueurGeoposition);

// 	CARTE_Fonctions..Zoom = zoomAdapteTaille
this.setZoom(-1);

}
else
{
// 	géoRécupèreAdresse("34, Cours Sextius, 13100 Aix en Provence",tGeoadresse,1)
WDAPIGeo.geoRecupereAdresse("34, Cours Sextius, 13100 Aix en Provence",vWD_tGeoadresse,1);

// 	SI ErreurDétectée = Faux ALORS
if(WDObjet.ErreurDetectee.opEgal(false))
{
// 		POUR TOUT sEGeoAdresse,nCompteurGeoAd de tGeoadresse
IWDParcours parcours6 = null;
try
{
WDObjet vWD_sEGeoAdresse = WDParcoursFactory.creerVariableParcours(vWD_tGeoadresse);
WDEntier vWD_nCompteurGeoAd = new WDEntier();
parcours6 = WDParcoursFactory.pourTout(vWD_tGeoadresse, vWD_sEGeoAdresse, vWD_nCompteurGeoAd, null, null, 0x0, 0x2);
while(parcours6.testParcours())
{
// 			mIci..Position..Latitude=sEGeoAdresse..Position..Latitude
vWD_mIci.getProp(EWDPropriete.PROP_POSITION).setProp(EWDPropriete.PROP_LATITUDE,parcours6.getVariableParcours().getProp(EWDPropriete.PROP_POSITION).getProp(EWDPropriete.PROP_LATITUDE));

// 			mIci..Position..Longitude=sEGeoAdresse..Position..Longitude
vWD_mIci.getProp(EWDPropriete.PROP_POSITION).setProp(EWDPropriete.PROP_LONGITUDE,parcours6.getVariableParcours().getProp(EWDPropriete.PROP_POSITION).getProp(EWDPropriete.PROP_LONGITUDE));

// 			mIci..Nom = "Ici"
vWD_mIci.setProp(EWDPropriete.PROP_NOM,"Ici");

// 			mIci..Description = sEGeoAdresse..Pays+RC+sEGeoAdresse..Région+RC+sEGeoAdresse..Rue+RC+sEGeoAdresse..CodePostal+","+sEGeoAdresse..Ville
vWD_mIci.setProp(EWDPropriete.PROP_DESCRIPTION,parcours6.getVariableParcours().getProp(EWDPropriete.PROP_PAYS).opPlus("\r\n").opPlus(parcours6.getVariableParcours().getProp(EWDPropriete.PROP_REGION)).opPlus("\r\n").opPlus(parcours6.getVariableParcours().getProp(EWDPropriete.PROP_RUE)).opPlus("\r\n").opPlus(parcours6.getVariableParcours().getProp(EWDPropriete.PROP_CODEPOSTAL)).opPlus(",").opPlus(parcours6.getVariableParcours().getProp(EWDPropriete.PROP_VILLE)));

// 			mIci..ActionClic=ProcClicMarqueur
vWD_mIci.setProp(EWDPropriete.PROP_ACTIONCLIC,(new WDChaineU("FEN_Carte_Fonction.ProcClicMarqueur")));

// 			CarteAjouteMarqueur(CARTE_Fonctions,mIci)
WDAPICarte.carteAjouteMarqueur(this,vWD_mIci);

}

}
finally
{
if(parcours6 != null)
{
parcours6.finParcours();
}
}


}

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCARTE_Fonctions mWD_CARTE_Fonctions;


////////////////////////////////////////////////////////////////////////////
// Procédures utilisateur de FEN_Carte_Fonction
////////////////////////////////////////////////////////////////////////////
//  Résumé : <indiquez ici ce que fait la procédure>
//  Syntaxe :
// ProcClicMarqueur (<m> est Marqueur)
// 
//  Paramètres :
// 	m (Marqueur) : <indiquez ici le rôle de m>
//  Valeur de retour :
//  	Aucune
// 
//  Exemple :
//  Indiquez ici un exemple d'utilisation.
// 
public void fWD_procClicMarqueur( WDObjet vWD_m )
{
initExecProcLocale("ProcClicMarqueur");


try
{
vWD_m = WDParametre.traiterParametreClasse(vWD_m, 1, false, WDMarqueur.class, 111);


// ToastAffiche(m..Description,toastLong)
WDAPIToast.toastAffiche(vWD_m.getProp(EWDPropriete.PROP_DESCRIPTION).getString(),1);

}
finally
{
finExecProcLocale();
}

}




/**
 * Traitement: Déclarations globales de FEN_Carte_Fonction
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
super.declarerGlobale(WD_tabParam);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


// sListeFonction est une chaîne UNICODE
vWD_sListeFonction = new WDChaineU();

super.ajouterVariableGlobale("sListeFonction",vWD_sListeFonction);



// sGeoIci est une chaîne
vWD_sGeoIci = new WDChaineU();

super.ajouterVariableGlobale("sGeoIci",vWD_sGeoIci);



// mIci est un Marqueur
vWD_mIci = new WDInstance( new WDMarqueur() );

super.ajouterVariableGlobale("mIci",vWD_mIci);



// mMarqueur est un Marqueur
vWD_mMarqueur = new WDInstance( new WDMarqueur() );

super.ajouterVariableGlobale("mMarqueur",vWD_mMarqueur);



// aIci est une géoPosition
vWD_aIci = new WDInstance( new WDGeoPosition() );

super.ajouterVariableGlobale("aIci",vWD_aIci);



// tabMarqueurGéoposition est un tableau de Marqueur
vWD_tabMarqueurGeoposition = new WDTableauSimple(1, new int[]{0}, 0, new IWDAllocateur(){public WDObjet creerInstance(){return new WDMarqueur();}	public int getTypeWL() {return 111;}		public boolean isDynamique(){return false;}	public Class getClasseWD(){return WDMarqueur.class;}		});
super.ajouterVariableGlobale("tabMarqueurGéoposition",vWD_tabMarqueurGeoposition);



// tabListeFonction est un tableau de chaînes
vWD_tabListeFonction = new WDTableauSimple(1, new int[]{0}, 0, 16);
super.ajouterVariableGlobale("tabListeFonction",vWD_tabListeFonction);



// tGeoadresse est un tableau de Adresse
vWD_tGeoadresse = new WDTableauSimple(1, new int[]{0}, 0, new IWDAllocateur(){public WDObjet creerInstance(){return new WDAdresse();}	public int getTypeWL() {return 111;}		public boolean isDynamique(){return false;}	public Class getClasseWD(){return WDAdresse.class;}		});
super.ajouterVariableGlobale("tGeoadresse",vWD_tGeoadresse);



}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_sListeFonction = WDVarNonAllouee.ref;
 public WDObjet vWD_sGeoIci = WDVarNonAllouee.ref;
 public WDObjet vWD_mIci = WDVarNonAllouee.ref;
 public WDObjet vWD_mMarqueur = WDVarNonAllouee.ref;
 public WDObjet vWD_aIci = WDVarNonAllouee.ref;
 public WDObjet vWD_tabMarqueurGeoposition = WDVarNonAllouee.ref;
 public WDObjet vWD_tabListeFonction = WDVarNonAllouee.ref;
 public WDObjet vWD_tGeoadresse = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_Carte_Fonction
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_CARTE_Fonctions = new GWDCARTE_Fonctions();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_Carte_Fonction
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(2981187936077919948l);

super.setChecksum("742705687");

super.setNom("FEN_Carte_Fonction");

super.setType(1);

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xC0C0C0);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 458);

super.setTitre("Carte_Fonction");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 1, 0, 1);

super.setCouleurTexteAutomatique(0xF4000000);

super.setCouleurBarreSysteme(0xFF000001);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Carte_Fonction
////////////////////////////////////////////////////////////////////////////
mWD_CARTE_Fonctions.initialiserObjet();
super.ajouter("CARTE_Fonctions", mWD_CARTE_Fonctions);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 1;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return false;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si la fenêtre contient un champ mapView, faux sinon.
*/
public boolean isAvecChampCarteV2()
{
return true;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPMon_Projet_an.ms_Project.mWD_FEN_Carte_Fonction;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "190 ACTIVPHONE 7#WM";
}
}

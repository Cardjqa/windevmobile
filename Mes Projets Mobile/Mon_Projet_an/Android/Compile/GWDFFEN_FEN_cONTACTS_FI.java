/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_FEN_cONTACTS_FI
 * Date : 11/09/2017 09:51:45
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.card.Annuaire.wdgen;


import com.card.Annuaire.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.fenetreinterne.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_FEN_cONTACTS_FI extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_FEN_cONTACTS_FI
////////////////////////////////////////////////////////////////////////////

/**
 * CFI_SansNom1
 */
class GWDCFI_SansNom1 extends WDChampFenetreInterne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_FEN_cONTACTS_FI.CFI_SansNom1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2972296052626715979l);

super.setChecksum("962356230");

super.setNom("CFI_SansNom1");

super.setType(31);

super.setLibelle("");

super.setNote("", "");

super.setPositionInitiale(6, 5);

super.setTailleInitiale(309, 428);

super.setValeurInitiale("");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setNumTab(1);

super.setAltitude(1);

super.setAncrageInitial(10, 1000, 1000, 500, 500);

super.setPersistant(false);

super.setFenetreInterne("FI_Contacts");

super.setTauxParallaxe(0, 0);

super.setCouleurTexteAutomatique(0xFF000001);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xE0E0E0, 4, 4, 1, 1), 0, 0, 0, 0);

super.setParamAnimationChamp(25, 26, 300);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
protected boolean isAvecAscenseurAuto()
{
return false;
}

protected boolean isBalayageVertical()
{
return false;
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCFI_SansNom1 mWD_CFI_SansNom1;

/**
 * Traitement: Déclarations globales de FEN_FEN_cONTACTS_FI
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
super.declarerGlobale(WD_tabParam);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_FEN_cONTACTS_FI
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_CFI_SansNom1 = new GWDCFI_SansNom1();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_FEN_cONTACTS_FI
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(2972295541524964842l);

super.setChecksum("966409071");

super.setNom("FEN_FEN_cONTACTS_FI");

super.setType(1);

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xC0C0C0);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 439);

super.setTitre("");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 2097160, 0, 3);

super.setCouleurTexteAutomatique(0xF4000000);

super.setCouleurBarreSysteme(0xFF000001);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_FEN_cONTACTS_FI
////////////////////////////////////////////////////////////////////////////
mWD_CFI_SansNom1.initialiserObjet();
super.ajouter("CFI_SansNom1", mWD_CFI_SansNom1);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 1;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return true;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPMon_Projet_an.ms_Project.mWD_FEN_FEN_cONTACTS_FI;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "190 ACTIVPHONE 7#WM";
}
}

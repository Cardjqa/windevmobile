/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_FenLoginParams
 * Date : 12/09/2017 11:20:38
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.card.Annuaire.wdgen;


import com.card.Annuaire.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.fenetreinterne.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.ui.champs.slidingmenu.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.core.types.collection.tableau.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_FenLoginParams extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_FenLoginParams
////////////////////////////////////////////////////////////////////////////

/**
 * CFI_SansNom1
 */
class GWDCFI_SansNom1 extends WDChampFenetreInterne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_FenLoginParams.CFI_SansNom1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2972288484735685290l);

super.setChecksum("803699331");

super.setNom("CFI_SansNom1");

super.setType(31);

super.setLibelle("");

super.setNote("", "");

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 456);

super.setValeurInitiale("");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setNumTab(1);

super.setAltitude(1);

super.setAncrageInitial(10, 1000, 1000, 1000, 1000);

super.setPersistant(false);

super.setFenetreInterne("FI_Log");

super.setTauxParallaxe(0, 0);

super.setCouleurTexteAutomatique(0xFF000001);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xF1000000, 0xF3000000, 0xE0E0E0, 4, 4, 1, 1), 0, 0, 0, 0);

super.setParamAnimationChamp(25, 26, 0);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
protected boolean isAvecAscenseurAuto()
{
return true;
}

protected boolean isBalayageVertical()
{
return false;
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCFI_SansNom1 mWD_CFI_SansNom1;

/**
 * FEN_FenLoginParams_LeftSliding
 */
class GWDFEN_FenLoginParams_LeftSliding extends WDSlidingMenu
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_FenLoginParams.FEN_FenLoginParams_LeftSliding
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setFenetreInterne("FI_Params");

super.setParamSlidingMenu(100, true, 0);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDFEN_FenLoginParams_LeftSliding mWD_FEN_FenLoginParams_LeftSliding;

/**
 * FEN_FenLoginParams_RightSliding
 */
class GWDFEN_FenLoginParams_RightSliding extends WDSlidingMenu
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_FenLoginParams.FEN_FenLoginParams_RightSliding
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setFenetreInterne("FI_Log");

super.setParamSlidingMenu(0, true, 1);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDFEN_FenLoginParams_RightSliding mWD_FEN_FenLoginParams_RightSliding;

/**
 * Traitement: Déclarations globales de FEN_FenLoginParams
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
super.declarerGlobale(WD_tabParam);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


// gsIp est une chaîne ="192.168.1.248:8086"
vWD_gsIp = new WDChaineU();

vWD_gsIp.setValeur("192.168.1.248:8086");

super.ajouterVariableGlobale("gsIp",vWD_gsIp);



}




/**
 * Traitement: Fin d'initialisation de FEN_FenLoginParams
 */
public void init()
{
super.init();


////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_cMaRequete = new WDChaineU();

WDObjet vWD_CMaReponse = new WDChaineU();



// cMaRequète est une chaîne


// CMaRéponse est une chaîne


// <COMPILE SI Configuration="Application Android" _ET_ Configuration="Application iOS">

// SI RéseauMobileInfoConnexion(réseauSignalGSM) <= 10 _ET_ PAS InternetConnecté() ALORS
if((WDAPIReseau.reseauMobileInfoConnexion(4).opInfEgal(10) && (!WDAPINet.internetConnecte().getBoolean())))
{
// 	SI OuiNon("L'intensité du signal GSM est insuffisante. Activer le Wi-Fi ?")=1 ALORS
if(WDAPIDialogue.ouiNon("L'intensité du signal GSM est insuffisante. Activer le Wi-Fi ?").opEgal(1))
{
// 		<COMPILE SI Configuration="Application Android">
{
// 			SI WiFiEtat()<>wifiActif   ALORS
if(WDAPIWiFi.wifiEtat().opDiff(1))
{
// 				WiFiActive()
WDAPIWiFi.wifiActive();

}

}

// 		FEN_FenLoginParams.gsIp=ChargeParamètre("IpWifi")
vWD_gsIp.setValeur(WDAPIPersist.chargeParametre("IpWifi"));

}
else
{
// 		FEN_FenLoginParams.gsIp=ChargeParamètre("IpLienExt")
vWD_gsIp.setValeur(WDAPIPersist.chargeParametre("IpLienExt"));

}

}
else
{
// 	FEN_FenLoginParams.gsIp=ChargeParamètre("IpLienExt")
vWD_gsIp.setValeur(WDAPIPersist.chargeParametre("IpLienExt"));

}

// cMaRequète = "http://"+FEN_FenLoginParams.gsIp+"/anpro/phpAnAuth.php?us="+URLEncode(ChargeParamètre("SAI_Email"))+"&pa="+URLEncode(ChargeParamètre("SAI_Mot_de_passe"))
vWD_cMaRequete.setValeur(new WDChaineU("http://").opPlus(vWD_gsIp).opPlus("/anpro/phpAnAuth.php?us=").opPlus(WDAPIDiversSTD.urlEncode(WDAPIPersist.chargeParametre("SAI_Email"))).opPlus("&pa=").opPlus(WDAPIDiversSTD.urlEncode(WDAPIPersist.chargeParametre("SAI_Mot_de_passe"))));

// SI HTTPRequête(cMaRequète) ALORS
if(WDAPIHttp.HTTPRequete(vWD_cMaRequete.getString()).getBoolean())
{
// 	CMaRéponse = UTF8VersUnicode(HTTPDonneRésultat(httpRésultat))
vWD_CMaReponse.setValeur(WDAPIChaine.UTF8VersUnicode(WDAPIHttp.HTTPDonneResultat(2)));

}
else
{
// 	CMaRéponse = "Erreur connexion"
vWD_CMaReponse.setValeur("Erreur connexion");

}

// SI ErreurDétectée ALORS
if(WDObjet.ErreurDetectee.getBoolean())
{
// 	Erreur(CMaRéponse)
WDAPIDialogue.erreur(vWD_CMaReponse.getString());

}
else
{
// 	tRéponse est un tableau de   chaîne
WDObjet vWD_tReponse = WDVarNonAllouee.ref;
vWD_tReponse = new WDTableauSimple(1, new int[]{0}, 0, 16);

// 	tRéponse = ChaîneDécoupe(CMaRéponse," ")
vWD_tReponse.setValeur(WDAPIChaine.chaineDecoupe(vWD_CMaReponse,new WDObjet[] {new WDChaineU(" ")} ));

// 	IF ((tRéponse..Vide<>Vrai) _ET_ tRéponse[1]="Bonjour") ALORS
if((vWD_tReponse.getVide().opDiff(true) && vWD_tReponse.get(1).opEgal("Bonjour")))
{
// 		IF(tRéponse..Occurrence>1 _ET_ tRéponse[2]<>"0") ALORS
if((vWD_tReponse.getOccurrence().opSup(1) && vWD_tReponse.get(2).opDiff("0")))
{
// 			SauveParamètre("GROUPE",tRéponse[2])
WDAPIPersist.sauveParametre("GROUPE",vWD_tReponse.get(2).getString());

// 			OuvreFenêtreMobile(FEN_Menu_1)
WDAPIFenetre.ouvreFille(GWDPMon_Projet_an.ms_Project.mWD_FEN_Menu_1);

}

}
else
{
// 		Info("Vous n'avez pas accès a cette application")
WDAPIDialogue.info("Vous n'avez pas accès a cette application");

}

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_gsIp = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_FenLoginParams
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_CFI_SansNom1 = new GWDCFI_SansNom1();
mWD_FEN_FenLoginParams_LeftSliding = new GWDFEN_FenLoginParams_LeftSliding();
mWD_FEN_FenLoginParams_RightSliding = new GWDFEN_FenLoginParams_RightSliding();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_FenLoginParams
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(2972280070940109825l);

super.setChecksum("853920644");

super.setNom("FEN_FenLoginParams");

super.setType(1);

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xC0C0C0);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 456);

super.setTitre("FenLoginParams");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 1, 0, 1);

super.setCouleurTexteAutomatique(0xF4000000);

super.setCouleurBarreSysteme(0xFF000001);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_FenLoginParams
////////////////////////////////////////////////////////////////////////////
mWD_CFI_SansNom1.initialiserObjet();
super.ajouter("CFI_SansNom1", mWD_CFI_SansNom1);
mWD_FEN_FenLoginParams_LeftSliding.initialiserObjet();
super.ajouterSlidingMenu(mWD_FEN_FenLoginParams_LeftSliding, 1);
mWD_FEN_FenLoginParams_RightSliding.initialiserObjet();
super.ajouterSlidingMenu(mWD_FEN_FenLoginParams_RightSliding, 1);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 1;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return true;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPMon_Projet_an.ms_Project.mWD_FEN_FenLoginParams;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "190 ACTIVPHONE 7#WM";
}
}

/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_Recherche
 * Date : 11/09/2017 09:51:45
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.card.Annuaire.wdgen;


import com.card.Annuaire.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.fenetreinterne.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.core.types.collection.tableau.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_Recherche extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_Recherche
////////////////////////////////////////////////////////////////////////////

/**
 * CFI_SansNom1
 */
class GWDCFI_SansNom1 extends WDChampFenetreInterne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Recherche.CFI_SansNom1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2968215554487274508l);

super.setChecksum("926038934");

super.setNom("CFI_SansNom1");

super.setType(31);

super.setLibelle("");

super.setNote("", "");

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 458);

super.setValeurInitiale("");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setNumTab(1);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setPersistant(false);

super.setFenetreInterne("FI_Rech");

super.setTauxParallaxe(0, 0);

super.setCouleurTexteAutomatique(0xFF000001);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xF1000000, 0xF3000000, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setParamAnimationChamp(25, 26, 300);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
protected boolean isAvecAscenseurAuto()
{
return false;
}

protected boolean isBalayageVertical()
{
return false;
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCFI_SansNom1 mWD_CFI_SansNom1;

/**
 * Traitement: Déclarations globales de FEN_Recherche
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
super.declarerGlobale(WD_tabParam);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


// myTableau1 est un tableau de chaînes
vWD_myTableau1 = new WDTableauSimple(1, new int[]{0}, 0, 16);
super.ajouterVariableGlobale("myTableau1",vWD_myTableau1);



// myTableau0 est un tableau de chaînes
vWD_myTableau0 = new WDTableauSimple(1, new int[]{0}, 0, 16);
super.ajouterVariableGlobale("myTableau0",vWD_myTableau0);



// sCMaRéponse est une chaîne
vWD_sCMaReponse = new WDChaineU();

super.ajouterVariableGlobale("sCMaRéponse",vWD_sCMaReponse);



}




/**
 * Traitement: Fin d'initialisation de FEN_Recherche
 */
public void init()
{
super.init();

// FI_Rech.SAI_RECHERCHER..Libellé=ChargeParamètre("Libellég")
GWDPMon_Projet_an.ms_Project.getFI_Rech().mWD_CMOD_SansNom1.mWD_SAI_RECHERCHER.setLibelle(WDAPIPersist.chargeParametre("Libellég").getString());

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_myTableau1 = WDVarNonAllouee.ref;
 public WDObjet vWD_myTableau0 = WDVarNonAllouee.ref;
 public WDObjet vWD_sCMaReponse = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_Recherche
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_CFI_SansNom1 = new GWDCFI_SansNom1();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_Recherche
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(2968214970368309733l);

super.setChecksum("927512152");

super.setNom("FEN_Recherche");

super.setType(1);

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xC0C0C0);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 458);

super.setTitre("");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 8, 0, 1);

super.setCouleurTexteAutomatique(0xF4000000);

super.setCouleurBarreSysteme(0xFF000001);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Recherche
////////////////////////////////////////////////////////////////////////////
mWD_CFI_SansNom1.initialiserObjet();
super.ajouter("CFI_SansNom1", mWD_CFI_SansNom1);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 1;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return true;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPMon_Projet_an.ms_Project.mWD_FEN_Recherche;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "190 ACTIVPHONE 7#WM";
}
}

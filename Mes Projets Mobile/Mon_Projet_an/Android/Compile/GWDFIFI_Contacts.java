/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FI_Contacts
 * Date : 12/09/2017 14:31:57
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.card.Annuaire.wdgen;


import com.card.Annuaire.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetreinterne.*;
import fr.pcsoft.wdjava.ui.champs.combo.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.core.context.*;
import fr.pcsoft.wdjava.ui.champs.saisie.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.core.types.collection.tableau.*;
import fr.pcsoft.wdjava.geo.*;
import fr.pcsoft.wdjava.core.poo.*;
import fr.pcsoft.wdjava.core.allocation.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFIFI_Contacts extends WDFenetreInterne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FI_Contacts
////////////////////////////////////////////////////////////////////////////

/**
 * COMBO_Combo_Saisie
 */
class GWDCOMBO_Combo_Saisie extends WDCombo
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Contacts.FI_Contacts.COMBO_Combo_Saisie
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectCompPrincipal(1,2,299,36);
super.setNom("COMBO_Combo_Saisie");

super.setType(10002);

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(9, 136);

super.setTailleInitiale(300, 40);

super.setValeurInitiale("Choisir un élément dans la liste");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setContenuInitial("");

super.setTriee(false);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setNumTab(1);

super.setLettreAppel(65535);

super.setRetourneValeurProgrammation(false);

super.setPersistant(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(27, 0xC0C0C0, 0x404040, 0xFFFFFF, 4, 4, 1, 1));

super.setStyleElement(0x30303, 0xFFFFFF, creerPolice_GEN("Arial", -8.000000, 0), 48);

super.setStyleSelection(0xFFFFFF, 0xD9D9D9, creerPolice_GEN("Arial", -8.000000, 0));

super.setStyleBouton(WDCadreFactory.creerCadre_GEN(27, 0xF1000000, 0xF3000000, 0xE0E0E0, 4, 4, 1, 1), 0xF4000000);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de COMBO_Combo_Saisie
 */
public void init()
{
super.init();


////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_i = new WDEntier();



// i est un entier


// ListeInsère(COMBO_Combo_Saisie,"Choisir un élément dans la liste",1)
WDAPIListe.listeInsere(this,"Choisir un élément dans la liste",new WDEntier(1));

// SI FEN_Recherche.MyTableau1..Occurrence > 1 ALORS
if(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.getOccurrence().opSup(1))
{
// 	<COMPILE SI Configuration="Application iOS" _et_ Configuration="Universal Windows 10 App">

// 	POUR i = 2 _A_ FEN_Recherche.myTableau1..Occurrence
// Délimiteur de visibilité pour ne pas étendre la visibilité des variables temporaires _WDExpBorneMax et _WDExpPas
{
WDObjet _WDExpBorneMax0 = new WDEntier(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.getOccurrence());
for(vWD_i.setValeur(2);vWD_i.opInfEgal(_WDExpBorneMax0);vWD_i.opInc())
{
// 		gstabComboPost = Remplace(FEN_Recherche.myTableau1[i],"$$",",")
vWD_gstabComboPost.setValeur(WDAPIChaine.remplace(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.get(vWD_i),new WDChaineU("$$"),new WDChaineU(",")));

// 	 SI gstabComboPost <> FEN_Recherche.myTableau1[i] _ET_ Taille(FEN_Recherche.myTableau1[i]) <> 0  alors
if((vWD_gstabComboPost.opDiff(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.get(vWD_i)) && WDAPIChaine.taille(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.get(vWD_i)).opDiff(0)))
{
// 			Si gstabComboPost<>"," ALORS
if(vWD_gstabComboPost.opDiff(","))
{
// 				ListeAjoute(COMBO_Combo_Saisie, gstabComboPost)
WDAPIListe.listeAjoute(this,vWD_gstabComboPost.getString());

}
else if(WDAPIChaine.chaineCommencePar(vWD_gstabComboPost,new WDChaineU(",")).getBoolean())
{
// 				ListeAjoute(COMBO_Combo_Saisie,  Remplace(gstabComboPost,",",""))
WDAPIListe.listeAjoute(this,WDAPIChaine.remplace(vWD_gstabComboPost,new WDChaineU(","),new WDChaineU("")).getString());

}
else if(WDAPIChaine.chaineFinitPar(vWD_gstabComboPost,new WDChaineU(",")).getBoolean())
{
// 				ListeAjoute(COMBO_Combo_Saisie,  Remplace(gstabComboPost,",",""))
WDAPIListe.listeAjoute(this,WDAPIChaine.remplace(vWD_gstabComboPost,new WDChaineU(","),new WDChaineU("")).getString());

}

}
else if((((vWD_i.opDiff(2) && vWD_i.opDiff(3)) && vWD_i.opDiff(10)) && WDAPIChaine.taille(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.get(vWD_i)).opDiff(0)))
{
// 			 ListeAjoute(COMBO_Combo_Saisie, FEN_Recherche.myTableau1[i])      		
WDAPIListe.listeAjoute(this,GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.get(vWD_i).getString());

}

}
}

}

// ListeAjoute(COMBO_Combo_Saisie, "Afficher le(s) lieu(x) des fonctions")
WDAPIListe.listeAjoute(this,"Afficher le(s) lieu(x) des fonctions");

}




/**
 * Traitement: Sélection d'une ligne de COMBO_Combo_Saisie
 */
public void selectionLigne()
{
super.selectionLigne();


////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_stel = new WDChaineU();



// stél est une chaîne


// sI VérifieExpressionRégulière(COMBO_Combo_Saisie[MoiMême],"[-+ ./)(0-9]{10,}") _et_ Taille(SAI_Texte2)=0 ALORS
if((WDAPIChaine.verifieExpressionReguliere(this.get(WDContexte.getMoiMeme()),"[-+ ./)(0-9]{10,}").getBoolean() && WDAPIChaine.taille(mWD_SAI_Texte2).opEgal(0)))
{
// 	stél=Remplace(COMBO_Combo_Saisie[MoiMême]," ",stél)
vWD_stel.setValeur(WDAPIChaine.remplace(this.get(WDContexte.getMoiMeme()),new WDChaineU(" "),vWD_stel));

// 	stél=Remplace(stél,"-",stél)
vWD_stel.setValeur(WDAPIChaine.remplace(vWD_stel,new WDChaineU("-"),vWD_stel));

// 	stél=Remplace(stél,"+",stél)
vWD_stel.setValeur(WDAPIChaine.remplace(vWD_stel,new WDChaineU("+"),vWD_stel));

// 	stél=Remplace(stél,"(33)",stél)	
vWD_stel.setValeur(WDAPIChaine.remplace(vWD_stel,new WDChaineU("(33)"),vWD_stel));

// 	telDialerCompose(stél)	
WDAPITel.telDialerCompose(vWD_stel.getString());

}
else if(((WDAPIChaine.verifieExpressionReguliere(this.get(WDContexte.getMoiMeme()),"[+]{0,1}[(]{0,1}[3]{0,2}[)]{0,1}[/ -]{0,5}0[-/ ]{0,5}[6-7]{1,1}[/ -0-9]{8,}").getBoolean() || WDAPIChaine.verifieExpressionReguliere(this.get(WDContexte.getMoiMeme()),"[-+ ./)(0-9]{10,}").getBoolean()) && WDAPIChaine.taille(mWD_SAI_Texte2).opSup(0)))
{
// 	stél=Remplace(COMBO_Combo_Saisie[MoiMême]," ",stél)
vWD_stel.setValeur(WDAPIChaine.remplace(this.get(WDContexte.getMoiMeme()),new WDChaineU(" "),vWD_stel));

// 	stél=Remplace(stél,"-",stél)
vWD_stel.setValeur(WDAPIChaine.remplace(vWD_stel,new WDChaineU("-"),vWD_stel));

// 	stél=Remplace(stél,"+",stél)
vWD_stel.setValeur(WDAPIChaine.remplace(vWD_stel,new WDChaineU("+"),vWD_stel));

// 	stél=Remplace(stél,"(33)",stél)
vWD_stel.setValeur(WDAPIChaine.remplace(vWD_stel,new WDChaineU("(33)"),vWD_stel));

// 	SMS.Numéro=stél
WDAPISMS.Numero.setValeur(vWD_stel);

// 	SMS.Message=SAI_Texte2
WDAPISMS.Message.setValeur(mWD_SAI_Texte2);

// 	SI SAI_Texte2<>"" _ET_ OuiNon(1,"Voulez-vous vider le champs méssage")  alors
if((mWD_SAI_Texte2.opDiff("") && WDAPIDialogue.ouiNon(new WDEntier(1),new String[] {"Voulez-vous vider le champs méssage"} ).getBoolean()))
{
// 		SAI_Texte2=""
mWD_SAI_Texte2.setValeur("");

}

// 	SMSLanceAppli()	
WDAPISMS.smsLanceAppli();

}
else if(WDAPIChaine.verifieExpressionReguliere(this.get(WDContexte.getMoiMeme()),"[-_.a-z0-9]+[@][-.a-z0-9]+[.][a-z]{2,4}").getBoolean())
{
// 	<COMPILE SI Configuration="Application Android">
{
// 		LanceAppliAssociée("mailto:"+COMBO_Combo_Saisie[MoiMême]+"?body="+URLEncode(SAI_Texte2))
WDAPIDiversSTD.lanceAppliAssociee(new WDChaineU("mailto:").opPlus(this.get(WDContexte.getMoiMeme())).opPlus("?body=").opPlus(WDAPIDiversSTD.urlEncode(mWD_SAI_Texte2)).getString());

// 		SI SAI_Texte2<>"" _ET_ OuiNon(1,"Voulez-vous vider le champs méssage")  ALORS
if((mWD_SAI_Texte2.opDiff("") && WDAPIDialogue.ouiNon(new WDEntier(1),new String[] {"Voulez-vous vider le champs méssage"} ).getBoolean()))
{
// 			SAI_Texte2=""
mWD_SAI_Texte2.setValeur("");

}

}

}
else if(WDContexte.getMoiMeme().opEgal(1))
{
// 	SI SAI_Texte2<>"" _et_ OuiNon(1,"Voulez-vous vider le champs méssage")   ALORS
if((mWD_SAI_Texte2.opDiff("") && WDAPIDialogue.ouiNon(new WDEntier(1),new String[] {"Voulez-vous vider le champs méssage"} ).getBoolean()))
{
// 		SAI_Texte2=""
mWD_SAI_Texte2.setValeur("");

}

// 	retour
return;

}
else if(this.get(WDContexte.getMoiMeme()).opEgal("Afficher le(s) lieu(x) des fonctions"))
{
// 	SI TAILLE(ChargeParamètre("NumContact"))>0 alors
if(WDAPIChaine.taille(WDAPIPersist.chargeParametre("NumContact")).opSup(0))
{
// 		cMaRequête = "http://"+FEN_FenLoginParams.gsIp+"/anpro/phpFonctions.php?"+"params="+ChargeParamètre("NumContact")
vWD_cMaRequete.setValeur(new WDChaineU("http://").opPlus(GWDPMon_Projet_an.ms_Project.getFEN_FenLoginParams().vWD_gsIp).opPlus("/anpro/phpFonctions.php?").opPlus("params=").opPlus(WDAPIPersist.chargeParametre("NumContact")));

// 		SI HTTPRequête(cMaRequête) = Vrai ALORS
if(WDAPIHttp.HTTPRequete(vWD_cMaRequete.getString()).opEgal(true))
{
// 			FEN_Recherche.sCMaRéponse = UTF8VersUnicode(HTTPDonneRésultat(httpRésultat))
GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_sCMaReponse.setValeur(WDAPIChaine.UTF8VersUnicode(WDAPIHttp.HTTPDonneResultat(2)));

}

// 		SauveParamètre("ListeFonctions",FEN_Recherche.sCMaRéponse)
WDAPIPersist.sauveParametre("ListeFonctions",GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_sCMaReponse.getString());

// 		OuvreFenêtreMobile(FEN_Carte_Fonction)
WDAPIFenetre.ouvreFille(GWDPMon_Projet_an.ms_Project.mWD_FEN_Carte_Fonction);

}

}
else
{
// 	SI ChargeParamètre("GeoPrécis") = Vrai ALORS
if(WDAPIPersist.chargeParametre("GeoPrécis").opEgal(true))
{
// 		GPSInitParamètre(gpsAuto, gpsPrécisionElevée + gpsVitesse + gpsEnergieMoyenne)
WDAPIGPS.gpsInitParametre(4,70);

}
else
{
// 		GPSInitParamètre(gpsAuto,  gpsVitesse + gpsEnergieMoyenne)	
WDAPIGPS.gpsInitParametre(4,68);

}

// 	TableauSupprimeTout(gtabAdresse)
WDAPITableau.tableauSupprimeTout(vWD_gtabAdresse);

// 	sEstMAposition est une chaîne=""
WDObjet vWD_sEstMAposition = new WDChaineU();


vWD_sEstMAposition.setValeur("");


// 	géoRécupèreAdresse(ChaîneFormate(ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],1,","),ccMinuscule)+" FRANCE",gtabAdresse,1)
WDAPIGeo.geoRecupereAdresse(WDAPIChaine.chaineFormate(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),1,new WDChaineU(",")),64).opPlus(" FRANCE").getString(),vWD_gtabAdresse,1);

// 	si ErreurDétectée = Faux   alors
if(WDObjet.ErreurDetectee.opEgal(false))
{
// 	géoRécupèreAdresse("FRANCE "+" "+ExtraitChaîne(ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],2,","),2," ")+" "+ExtraitChaîne(ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],2,","),1," ") +" "+ ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],1,","),NULL,1)
WDAPIGeo.geoRecupereAdresse(new WDChaineU("FRANCE  ").opPlus(WDAPIChaine.extraitChaine(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),2,new WDChaineU(",")),2,new WDChaineU(" "))).opPlus(" ").opPlus(WDAPIChaine.extraitChaine(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),2,new WDChaineU(",")),1,new WDChaineU(" "))).opPlus(" ").opPlus(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),1,new WDChaineU(","))).getString(),WDObjet.NULL,1);

// 		SI ErreurDétectée = Faux ALORS
if(WDObjet.ErreurDetectee.opEgal(false))
{
// 			sEstMAposition = "https://www.google.fr/maps/dir/Ma position/"+ExtraitChaîne(ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],2,","),2," ")+"+"+ExtraitChaîne(ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],2,","),1," ")+"+"+ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],1,",")
vWD_sEstMAposition.setValeur(new WDChaineU("https://www.google.fr/maps/dir/Ma position/").opPlus(WDAPIChaine.extraitChaine(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),2,new WDChaineU(",")),2,new WDChaineU(" "))).opPlus("+").opPlus(WDAPIChaine.extraitChaine(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),2,new WDChaineU(",")),1,new WDChaineU(" "))).opPlus("+").opPlus(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),1,new WDChaineU(","))));

// 			LanceAppliAssociée(sEstMAposition)
WDAPIDiversSTD.lanceAppliAssociee(vWD_sEstMAposition.getString());

}
else
{
// 			sEstMAposition = "https://www.google.fr/maps/dir/Ma position/"+ExtraitChaîne(ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],2,","),2," ")+"+"+ExtraitChaîne(ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],2,","),1," ")
vWD_sEstMAposition.setValeur(new WDChaineU("https://www.google.fr/maps/dir/Ma position/").opPlus(WDAPIChaine.extraitChaine(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),2,new WDChaineU(",")),2,new WDChaineU(" "))).opPlus("+").opPlus(WDAPIChaine.extraitChaine(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),2,new WDChaineU(",")),1,new WDChaineU(" "))));

// 			LanceAppliAssociée(sEstMAposition)
WDAPIDiversSTD.lanceAppliAssociee(vWD_sEstMAposition.getString());

}

}
else
{
// 		géoRécupèreAdresse("FRANCE "+" "+ExtraitChaîne(ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],2,","),2," ")+" "+ExtraitChaîne(ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],2,","),1," "),NULL,1)
WDAPIGeo.geoRecupereAdresse(new WDChaineU("FRANCE  ").opPlus(WDAPIChaine.extraitChaine(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),2,new WDChaineU(",")),2,new WDChaineU(" "))).opPlus(" ").opPlus(WDAPIChaine.extraitChaine(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),2,new WDChaineU(",")),1,new WDChaineU(" "))).getString(),WDObjet.NULL,1);

// 		SI ErreurDétectée = Faux alors
if(WDObjet.ErreurDetectee.opEgal(false))
{
// 			sEstMAposition = "https://www.google.fr/maps/dir/Ma position/"+ExtraitChaîne(ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],2,","),2," ")+"+"+ExtraitChaîne(ExtraitChaîne(COMBO_Combo_Saisie[MoiMême],2,","),1," ")
vWD_sEstMAposition.setValeur(new WDChaineU("https://www.google.fr/maps/dir/Ma position/").opPlus(WDAPIChaine.extraitChaine(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),2,new WDChaineU(",")),2,new WDChaineU(" "))).opPlus("+").opPlus(WDAPIChaine.extraitChaine(WDAPIChaine.extraitChaine(this.get(WDContexte.getMoiMeme()),2,new WDChaineU(",")),1,new WDChaineU(" "))));

// 			LanceAppliAssociée(sEstMAposition)
WDAPIDiversSTD.lanceAppliAssociee(vWD_sEstMAposition.getString());

}
else
{
// 			Info("Aucune correspondance n'existe!")
WDAPIDialogue.info("Aucune correspondance n'existe!");

}

}

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurSelection();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCOMBO_Combo_Saisie mWD_COMBO_Combo_Saisie;

/**
 * SAI_Texte2
 */
class GWDSAI_Texte2 extends WDChampSaisieMultiLigne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Contacts.FI_Contacts.SAI_Texte2
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectCompPrincipal(0,2,298,216);
super.setQuid(2968193761678090375l);

super.setChecksum("780907722");

super.setNom("SAI_Texte2");

super.setType(20001);

super.setLibelle("Messagerie et SMS");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(9, 212);

super.setTailleInitiale(300, 220);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(2);

super.setModeAscenseur(2, 1);

super.setEffacementAutomatique(false);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(0);

super.setMiseABlancSiZero(false);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFF, 4, 4, 1, 1));

super.setStyleSaisie(0x30303, creerPolice_GEN("Arial", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Sortie de SAI_Texte2
 */
public void sortieChamp()
//  Désactiver le clavier en cours d'utilisation
{
super.sortieChamp();

// ClavierVisible(Faux)
WDAPIDivers.clavierVisible(false);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurFocus();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Texte2 mWD_SAI_Texte2;

/**
 * LIB_Nom_Utilisateur
 */
class GWDLIB_Nom_Utilisateur extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Contacts.FI_Contacts.LIB_Nom_Utilisateur
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2978662617000233751l);

super.setChecksum("-2124470707");

super.setNom("LIB_Nom_Utilisateur");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Annuaire");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(6, 11);

super.setTailleInitiale(306, 46);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0xFF000001, 0xFFFFFFFF, creerPolice_GEN("Arial", -16.000000, 1), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(15, 0xE0E0E0, 0xFFFFFF, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Nom_Utilisateur mWD_LIB_Nom_Utilisateur;

/**
 * LIB_SansNom1
 */
class GWDLIB_SansNom1 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FI_Contacts.FI_Contacts.LIB_SansNom1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2978860278777906554l);

super.setChecksum("963318902");

super.setNom("LIB_SansNom1");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Contact(s)");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(9, 108);

super.setTailleInitiale(149, 20);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xE0E0E0, creerPolice_GEN("Arial", -8.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xF3000000, 0xF1000000, 0xE0E0E0, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_SansNom1 mWD_LIB_SansNom1;

/**
 * LIB_SansNom2
 */
class GWDLIB_SansNom2 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FI_Contacts.FI_Contacts.LIB_SansNom2
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2978862091257411269l);

super.setChecksum("966625127");

super.setNom("LIB_SansNom2");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Texte");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(10, 184);

super.setTailleInitiale(148, 20);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xE0E0E0, creerPolice_GEN("Arial", -8.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0x808080, 0x0, 0xE0E0E0, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_SansNom2 mWD_LIB_SansNom2;

/**
 * LIB_SansNom3
 */
class GWDLIB_SansNom3 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°6 de FI_Contacts.FI_Contacts.LIB_SansNom3
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2980899159668473077l);

super.setChecksum("749406795");

super.setNom("LIB_SansNom3");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Contacter");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(10, 65);

super.setTailleInitiale(298, 27);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(6);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xE0E0E0, creerPolice_GEN("Arial", -8.000000, 1), 4, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0x30303, 0x0, 0xE0E0E0, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_SansNom3 mWD_LIB_SansNom3;

/**
 * Traitement: Déclarations globales de FI_Contacts
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
super.declarerGlobale(WD_tabParam);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


// gstabComboPost est une chaine
vWD_gstabComboPost = new WDChaineU();

super.ajouterVariableGlobale("gstabComboPost",vWD_gstabComboPost);



// gsTabSComboPost est un tableau de chaînes
vWD_gsTabSComboPost = new WDTableauSimple(1, new int[]{0}, 0, 16);
super.ajouterVariableGlobale("gsTabSComboPost",vWD_gsTabSComboPost);



// cMaRequête est une chaine
vWD_cMaRequete = new WDChaineU();

super.ajouterVariableGlobale("cMaRequête",vWD_cMaRequete);



// gtabAdresse est un tableau de Adresse
vWD_gtabAdresse = new WDTableauSimple(1, new int[]{0}, 0, new IWDAllocateur(){public WDObjet creerInstance(){return new WDAdresse();}	public int getTypeWL() {return 111;}		public boolean isDynamique(){return false;}	public Class getClasseWD(){return WDAdresse.class;}		});
super.ajouterVariableGlobale("gtabAdresse",vWD_gtabAdresse);



}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_gstabComboPost = WDVarNonAllouee.ref;
 public WDObjet vWD_gsTabSComboPost = WDVarNonAllouee.ref;
 public WDObjet vWD_cMaRequete = WDVarNonAllouee.ref;
 public WDObjet vWD_gtabAdresse = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FI_Contacts
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_COMBO_Combo_Saisie = new GWDCOMBO_Combo_Saisie();
mWD_SAI_Texte2 = new GWDSAI_Texte2();
mWD_LIB_Nom_Utilisateur = new GWDLIB_Nom_Utilisateur();
mWD_LIB_SansNom1 = new GWDLIB_SansNom1();
mWD_LIB_SansNom2 = new GWDLIB_SansNom2();
mWD_LIB_SansNom3 = new GWDLIB_SansNom3();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FI_Contacts
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setQuid(2968193761677959303l);

super.setChecksum("780779015");

super.setNom("FI_Contacts");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTailleInitiale(318, 450);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setCouleurFond(0xE0E0E0);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Contacts
////////////////////////////////////////////////////////////////////////////
mWD_COMBO_Combo_Saisie.initialiserObjet();
super.ajouter("COMBO_Combo_Saisie", mWD_COMBO_Combo_Saisie);
mWD_SAI_Texte2.initialiserObjet();
super.ajouter("SAI_Texte2", mWD_SAI_Texte2);
mWD_LIB_Nom_Utilisateur.initialiserObjet();
super.ajouter("LIB_Nom_Utilisateur", mWD_LIB_Nom_Utilisateur);
mWD_LIB_SansNom1.initialiserObjet();
super.ajouter("LIB_SansNom1", mWD_LIB_SansNom1);
mWD_LIB_SansNom2.initialiserObjet();
super.ajouter("LIB_SansNom2", mWD_LIB_SansNom2);
mWD_LIB_SansNom3.initialiserObjet();
super.ajouter("LIB_SansNom3", mWD_LIB_SansNom3);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
}

/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FI_Log
 * Date : 13/09/2017 09:16:39
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.card.Annuaire.wdgen;


import com.card.Annuaire.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetreinterne.*;
import fr.pcsoft.wdjava.ui.champs.saisie.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.champs.bouton.*;
import fr.pcsoft.wdjava.core.types.collection.tableau.*;
import fr.pcsoft.wdjava.ui.champs.groupeoptions.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFIFI_Log extends WDFenetreInterne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FI_Log
////////////////////////////////////////////////////////////////////////////

/**
 * SAI_Email
 */
class GWDSAI_Email extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Log.FI_Log.SAI_Email
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectCompPrincipal(0,2,300,41);
super.setQuid(2971665873509683839l);

super.setChecksum("921669741");

super.setNom("SAI_Email");

super.setType(20001);

super.setLibelle("LOGIN");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(20);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(9, 137);

super.setTailleInitiale(300, 45);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setLiaisonFichier("fenlogin", "email");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setIndication("Code utilisateurs");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(false);

super.setPersistant(true);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(9, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(2);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -2, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_Edt.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x30303, creerPolice_GEN("Arial", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de SAI_Email
 */
public void init()
{
super.init();

// SAI_Email = ChargeParamètre("SAI_Email","")
this.setValeur(WDAPIPersist.chargeParametre("SAI_Email",""));

}




/**
 * Traitement: Sortie de SAI_Email
 */
public void sortieChamp()
{
super.sortieChamp();

// SI INT_Interrupteur = 1 alors
if(mWD_INT_Interrupteur.opEgal(1))
{
// 	SauveParamètre("SAI_Email",SAI_Email)
WDAPIPersist.sauveParametre("SAI_Email",this.getString());

}

// 	ClavierVisible(Faux)
WDAPIDivers.clavierVisible(false);

}




/**
 * Traitement: Validation par le clavier de SAI_Email
 */
public WDObjet clicBtnActionClavier()
{
super.clicBtnActionClavier();

// SI INT_Interrupteur = 1 ALORS
if(mWD_INT_Interrupteur.opEgal(1))
{
// 	SauveParamètre("SAI_Email",SAI_Email)
WDAPIPersist.sauveParametre("SAI_Email",this.getString());

}

return new WDVoid("clicBtnActionClavier");
}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurFocus();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Email mWD_SAI_Email;

/**
 * SAI_Mot_de_passe
 */
class GWDSAI_Mot_de_passe extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Log.FI_Log.SAI_Mot_de_passe
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectCompPrincipal(0,2,300,41);
super.setQuid(2971665873509814911l);

super.setChecksum("921800900");

super.setNom("SAI_Mot_de_passe");

super.setType(20001);

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(20);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(9, 203);

super.setTailleInitiale(300, 45);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(true);

super.setLiaisonFichier("fenlogin", "mot_de_passe");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setIndication("Mot de passe");

super.setNumTab(2);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(false);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(9, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -2, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_Edt.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xF1800000, 1, 5));

super.setStyleSaisie(0x30303, creerPolice_GEN("Arial", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de SAI_Mot_de_passe
 */
public void init()
{
super.init();

// SAI_Mot_de_passe = ChargeParamètre("SAI_Mot_de_passe" , "")
this.setValeur(WDAPIPersist.chargeParametre("SAI_Mot_de_passe",""));

}




/**
 * Traitement: Sortie de SAI_Mot_de_passe
 */
public void sortieChamp()
{
super.sortieChamp();

// SI INT_Interrupteur = 1 ALORS
if(mWD_INT_Interrupteur.opEgal(1))
{
// 	SauveParamètre("SAI_Mot_de_passe",SAI_Mot_de_passe)
WDAPIPersist.sauveParametre("SAI_Mot_de_passe",this.getString());

}

// ClavierVisible(Faux)
WDAPIDivers.clavierVisible(false);

}




/**
 * Traitement: Validation par le clavier de SAI_Mot_de_passe
 */
public WDObjet clicBtnActionClavier()
{
super.clicBtnActionClavier();

// SI INT_Interrupteur = 1 ALORS
if(mWD_INT_Interrupteur.opEgal(1))
{
// 	SauveParamètre("SAI_Mot_de_passe",SAI_Mot_de_passe)
WDAPIPersist.sauveParametre("SAI_Mot_de_passe",this.getString());

}

return new WDVoid("clicBtnActionClavier");
}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurFocus();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Mot_de_passe mWD_SAI_Mot_de_passe;

/**
 * BTN_Se_connecter
 */
class GWDBTN_Se_connecter extends WDBouton
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Log.FI_Log.BTN_Se_connecter
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2971665873509945983l);

super.setChecksum("921932797");

super.setNom("BTN_Se_connecter");

super.setType(4);

super.setLibelle("Se connecter");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(9, 379);

super.setTailleInitiale(300, 50);

super.setPlan(0);

super.setImageEtat(5);

super.setImageFondEtat(5);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setNumTab(4);

super.setLettreAppel(65535);

super.setTypeBouton(1);

super.setTypeActionPredefinie(0);

super.setBoutonOnOff(false);

super.setTauxParallaxe(0, 0);

super.setLibelleVAlign(1);

super.setLibelleHAlign(5);

super.setPresenceLibelle(true);

super.setImage("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_Pict_ok_16_5.png?E5", 0, 1, 5, null, null, null);

super.setStyleLibelleRepos(0x30303, creerPolice_GEN("Arial", -8.000000, 1), 0, 0x808080);

super.setStyleLibelleSurvol(0x30303, creerPolice_GEN("Arial", -8.000000, 1), 0, 0x808080);

super.setStyleLibelleEnfonce(0x30303, creerPolice_GEN("Arial", -8.000000, 1), 0, 0x808080);

super.setCadreRepos(WDCadreFactory.creerCadre_GEN(27, 0xC0C0C0, 0x404040, 0xE0E0E0, 4, 4, 1, 1));

super.setCadreSurvol(WDCadreFactory.creerCadre_GEN(27, 0xC0C0C0, 0x404040, 0xE0E0E0, 4, 4, 1, 1));

super.setCadreEnfonce(WDCadreFactory.creerCadre_GEN(27, 0xC0C0C0, 0x404040, 0xE0E0E0, 4, 4, 1, 1));

super.setImageFond9Images(new int[] {1,2,1,2,2,2,1,2,1}, 8, 8, 10, 10);

super.setImageFond("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_Btn_Std.png?E5_3NP_10_10_8_8", 1, 0, 1, 1);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur BTN_Se_connecter
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// ClavierVisible(Faux)
WDAPIDivers.clavierVisible(false);

// SI INT_Interrupteur = 1 ALORS
if(mWD_INT_Interrupteur.opEgal(1))
{
// 	SauveParamètre("SAI_Mot_de_passe",SAI_Mot_de_passe)
WDAPIPersist.sauveParametre("SAI_Mot_de_passe",mWD_SAI_Mot_de_passe.getString());

// 	SauveParamètre("SAI_Email",SAI_Email)
WDAPIPersist.sauveParametre("SAI_Email",mWD_SAI_Email.getString());

}

// SI RéseauMobileInfoConnexion(réseauSignalGSM) <= 10 _ET_ PAS InternetConnecté() ALORS
if((WDAPIReseau.reseauMobileInfoConnexion(4).opInfEgal(10) && (!WDAPINet.internetConnecte().getBoolean())))
{
// 	SI OuiNon("L'intensité du signal GSM est insuffisante. Activer le Wi-Fi ?")=1 ALORS
if(WDAPIDialogue.ouiNon("L'intensité du signal GSM est insuffisante. Activer le Wi-Fi ?").opEgal(1))
{
// 		<COMPILE SI Configuration="Application Android">
{
// 			SI WiFiEtat()<>wifiActif   ALORS
if(WDAPIWiFi.wifiEtat().opDiff(1))
{
// 				WiFiActive()
WDAPIWiFi.wifiActive();

}

}

// 		FEN_FenLoginParams.gsIp=ChargeParamètre("IpWifi")
GWDPMon_Projet_an.ms_Project.getFEN_FenLoginParams().vWD_gsIp.setValeur(WDAPIPersist.chargeParametre("IpWifi"));

}
else
{
// 		FEN_FenLoginParams.gsIp=ChargeParamètre("IpLienExt")
GWDPMon_Projet_an.ms_Project.getFEN_FenLoginParams().vWD_gsIp.setValeur(WDAPIPersist.chargeParametre("IpLienExt"));

}

}
else
{
// 	FEN_FenLoginParams.gsIp=ChargeParamètre("IpLienExt")
GWDPMon_Projet_an.ms_Project.getFEN_FenLoginParams().vWD_gsIp.setValeur(WDAPIPersist.chargeParametre("IpLienExt"));

}

// 		cMaRequète = "http://"+FEN_FenLoginParams.gsIp+"/anpro/phpAnAuth.php?us="+FI_Log.SAI_Email+"&pa="+FI_Log.SAI_Mot_de_passe
vWD_cMaRequete.setValeur(new WDChaineU("http://").opPlus(GWDPMon_Projet_an.ms_Project.getFEN_FenLoginParams().vWD_gsIp).opPlus("/anpro/phpAnAuth.php?us=").opPlus(mWD_SAI_Email).opPlus("&pa=").opPlus(mWD_SAI_Mot_de_passe));

// 		SI HTTPRequête(cMaRequète) ALORS
if(WDAPIHttp.HTTPRequete(vWD_cMaRequete.getString()).getBoolean())
{
// 			cMaRéponse = UTF8VersUnicode(HTTPDonneRésultat(httpRésultat))
vWD_cMaReponse.setValeur(WDAPIChaine.UTF8VersUnicode(WDAPIHttp.HTTPDonneResultat(2)));

}
else
{
// 			cMaRéponse = "Erreur connexion"
vWD_cMaReponse.setValeur("Erreur connexion");

}

// SI ErreurDétectée ALORS
if(WDObjet.ErreurDetectee.getBoolean())
{
// 	Erreur(cMaRéponse)
WDAPIDialogue.erreur(vWD_cMaReponse.getString());

}
else
{
// 	tRéponse est un tableau de chaîne
WDObjet vWD_tReponse = WDVarNonAllouee.ref;
vWD_tReponse = new WDTableauSimple(1, new int[]{0}, 0, 16);

// 	tRéponse = ChaîneDécoupe(cMaRéponse," ")
vWD_tReponse.setValeur(WDAPIChaine.chaineDecoupe(vWD_cMaReponse,new WDObjet[] {new WDChaineU(" ")} ));

// 	IF ((tRéponse..Vide<>Vrai) _ET_ tRéponse[1]="Bonjour") ALORS
if((vWD_tReponse.getVide().opDiff(true) && vWD_tReponse.get(1).opEgal("Bonjour")))
{
// 	IF(tRéponse..Occurrence>1 _ET_ tRéponse[2]<>"0") ALORS
if((vWD_tReponse.getOccurrence().opSup(1) && vWD_tReponse.get(2).opDiff("0")))
{
// 		SauveParamètre("GROUPE",tRéponse[2])
WDAPIPersist.sauveParametre("GROUPE",vWD_tReponse.get(2).getString());

// 		OuvreFenêtreMobile(FEN_Menu_1)
WDAPIFenetre.ouvreFille(GWDPMon_Projet_an.ms_Project.mWD_FEN_Menu_1);

}

}
else
{
// 		Info("Vous n'avez pas accès a cette application")
WDAPIDialogue.info("Vous n'avez pas accès a cette application");

}

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDBTN_Se_connecter mWD_BTN_Se_connecter;

/**
 * INT_Interrupteur
 */
class GWDINT_Interrupteur extends WDInterrupteur
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FI_Log.FI_Log.INT_Interrupteur
////////////////////////////////////////////////////////////////////////////

/**
 * INT_Interrupteur_Option_0
 */
class GWDINT_Interrupteur_Option_0 extends WDCaseACocherNatif
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Log.FI_Log.INT_Interrupteur.INT_Interrupteur_Option_0
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setLibelle("Se souvenir de moi");

super.setHauteurOption(0);

super.setStyleLibelleOption(0x30303, creerPolice_GEN("Arial", -8.000000, 0));

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDINT_Interrupteur_Option_0 mWD_INT_Interrupteur_Option_0 = new GWDINT_Interrupteur_Option_0();
/**
 * Initialise tous les champs de FI_Log.FI_Log.INT_Interrupteur
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Log.FI_Log.INT_Interrupteur
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
super.ajouterOption(mWD_INT_Interrupteur_Option_0);
positionnerOptions();
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectLibelle(0,0,300,1);
super.setRectCompPrincipal(1,1,298,57);
super.setQuid(2971665873510273678l);

super.setChecksum("922260948");

super.setNom("INT_Interrupteur");

super.setType(5);

super.setLibelle("&Interrupteur");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(9, 278);

super.setTailleInitiale(300, 59);

super.setValeurInitiale("0");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setNumTab(3);

super.setLettreAppel(65535);

super.setPersistant(true);

super.setParamOptions(false, 1, true, true, true);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(27, 0xF3000000, 0xF1000000, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 4, 4, 1, 1));

super.setParamAnimationChamp(18, 19, 300);
super.setParamAnimationChamp(19, 20, 300);

super.setImageCoche("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_CBox.png?E12_A1A6A1A6A1A6A1A6A1A6A1A6_Radio", 6);

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

/**
 * Traitement: A chaque modification de INT_Interrupteur
 */
public void modification()
{
super.modification();

// SI INT_Interrupteur = 0 ALORS
if(this.opEgal(0))
{
// 	SauveParamètre("SAI_Mot_de_passe" , "")
WDAPIPersist.sauveParametre("SAI_Mot_de_passe","");

// 	SauveParamètre("SAI-Email","")
WDAPIPersist.sauveParametre("SAI-Email","");

}
else
{
// 	SauveParamètre("SAI_Mot_de_passe" , SAI_Mot_de_passe)
WDAPIPersist.sauveParametre("SAI_Mot_de_passe",mWD_SAI_Mot_de_passe.getString());

// 	SauveParamètre("SAI_Email",SAI_Email)
WDAPIPersist.sauveParametre("SAI_Email",mWD_SAI_Email.getString());

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurModification();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDINT_Interrupteur mWD_INT_Interrupteur;

/**
 * LIB_Nom_Utilisateur
 */
class GWDLIB_Nom_Utilisateur extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FI_Log.FI_Log.LIB_Nom_Utilisateur
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2978662526804719278l);

super.setChecksum("-2125671985");

super.setNom("LIB_Nom_Utilisateur");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Annuaire");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(9, 14);

super.setTailleInitiale(300, 43);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0xFF000001, 0xFFFFFFFF, creerPolice_GEN("Arial", -16.000000, 1), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(15, 0xE0E0E0, 0xFFFFFF, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Nom_Utilisateur mWD_LIB_Nom_Utilisateur;

/**
 * LIB_Indentification
 */
class GWDLIB_Indentification extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°6 de FI_Log.FI_Log.LIB_Indentification
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2978662526804850350l);

super.setChecksum("-2125540913");

super.setNom("LIB_Indentification");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Identification");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(10, 65);

super.setTailleInitiale(298, 27);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(6);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xE0E0E0, creerPolice_GEN("Arial", -8.000000, 1), 4, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0x30303, 0x0, 0xE0E0E0, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Indentification mWD_LIB_Indentification;

/**
 * Traitement: Déclarations globales de FI_Log
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
super.declarerGlobale(WD_tabParam);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


//  	INT_Interrupteur = ChargeParamètre("INT_Interrupteur" , 0)
mWD_INT_Interrupteur.setValeur(WDAPIPersist.chargeParametre("INT_Interrupteur",new WDEntier(0).getString()));

// cMaRéponse est une chaîne
vWD_cMaReponse = new WDChaineU();

super.ajouterVariableGlobale("cMaRéponse",vWD_cMaReponse);



// cMaRequète est une chaîne
vWD_cMaRequete = new WDChaineU();

super.ajouterVariableGlobale("cMaRequète",vWD_cMaRequete);



// SI ClavierVisible(Vrai) ALORS
if(WDAPIDivers.clavierVisible(true).getBoolean())
{
// 	ClavierVisible(Faux)
WDAPIDivers.clavierVisible(false);

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_cMaReponse = WDVarNonAllouee.ref;
 public WDObjet vWD_cMaRequete = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FI_Log
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_SAI_Email = new GWDSAI_Email();
mWD_SAI_Mot_de_passe = new GWDSAI_Mot_de_passe();
mWD_BTN_Se_connecter = new GWDBTN_Se_connecter();
mWD_INT_Interrupteur = new GWDINT_Interrupteur();
mWD_LIB_Nom_Utilisateur = new GWDLIB_Nom_Utilisateur();
mWD_LIB_Indentification = new GWDLIB_Indentification();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FI_Log
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setQuid(2971665710298790970l);

super.setChecksum("919547762");

super.setNom("FI_Log");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTailleInitiale(318, 450);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setCouleurFond(0xE0E0E0);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Log
////////////////////////////////////////////////////////////////////////////
mWD_SAI_Email.initialiserObjet();
super.ajouter("SAI_Email", mWD_SAI_Email);
mWD_SAI_Mot_de_passe.initialiserObjet();
super.ajouter("SAI_Mot_de_passe", mWD_SAI_Mot_de_passe);
mWD_BTN_Se_connecter.initialiserObjet();
super.ajouter("BTN_Se_connecter", mWD_BTN_Se_connecter);
mWD_INT_Interrupteur.initialiserObjet();
super.ajouter("INT_Interrupteur", mWD_INT_Interrupteur);
mWD_LIB_Nom_Utilisateur.initialiserObjet();
super.ajouter("LIB_Nom_Utilisateur", mWD_LIB_Nom_Utilisateur);
mWD_LIB_Indentification.initialiserObjet();
super.ajouter("LIB_Indentification", mWD_LIB_Indentification);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
}

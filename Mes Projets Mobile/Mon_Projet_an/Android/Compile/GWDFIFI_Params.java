/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FI_Params
 * Date : 13/09/2017 09:16:39
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.card.Annuaire.wdgen;


import com.card.Annuaire.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetreinterne.*;
import fr.pcsoft.wdjava.ui.champs.*;
import fr.pcsoft.wdjava.ui.champs.bouton.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.ui.champs.zml.*;
import fr.pcsoft.wdjava.ui.champs.saisie.*;
import fr.pcsoft.wdjava.ui.champs.groupeoptions.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFIFI_Params extends WDFenetreInterne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des groupes de champs de FI_Params
////////////////////////////////////////////////////////////////////////////
 public WDGroupe mWD_GR_Groupe1;

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FI_Params
////////////////////////////////////////////////////////////////////////////

/**
 * BTN_Supprimer
 */
class GWDBTN_Supprimer extends WDBouton
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Params.FI_Params.BTN_Supprimer
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2962320917976594880l);

super.setChecksum("869304334");

super.setNom("BTN_Supprimer");

super.setType(4);

super.setLibelle("Supprimer");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(12, 95);

super.setTailleInitiale(295, 41);

super.setPlan(0);

super.setImageEtat(5);

super.setImageFondEtat(5);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setNumTab(1);

super.setLettreAppel(65535);

super.setTypeBouton(0);

super.setTypeActionPredefinie(0);

super.setBoutonOnOff(false);

super.setTauxParallaxe(0, 0);

super.setLibelleVAlign(1);

super.setLibelleHAlign(5);

super.setPresenceLibelle(true);

super.setImage("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_Pict_Delete_16_5.png?E5", 0, 1, 5, null, null, null);

super.setStyleLibelleRepos(0xFFFFFF, creerPolice_GEN("Arial", -8.000000, 0), 0, 0x808080);

super.setStyleLibelleSurvol(0xFFFFFF, creerPolice_GEN("Arial", -8.000000, 0), 0, 0x808080);

super.setStyleLibelleEnfonce(0xFFFFFF, creerPolice_GEN("Arial", -8.000000, 0), 0, 0x808080);

super.setCadreRepos(WDCadreFactory.creerCadre_GEN(2, 0xFFFFFFFF, 0xBFBFBF, 0xFFFFFFFF, 4, 4, 1, 1));

super.setCadreSurvol(WDCadreFactory.creerCadre_GEN(2, 0xFFFFFFFF, 0xBFBFBF, 0xE8C6B0, 4, 4, 1, 1));

super.setCadreEnfonce(WDCadreFactory.creerCadre_GEN(2, 0xFFFFFFFF, 0xBFBFBF, 0xD9D9D9, 4, 4, 1, 1));

super.setImageFond9Images(new int[] {1,4,1,2,2,2,1,4,1}, 10, 10, 8, 8);

super.setImageFond("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_Btn_Remove.png?E5_3NP_8_8_10_10", 1, 0, 1, 1);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur BTN_Supprimer
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// ClavierVisible(Faux)
WDAPIDivers.clavierVisible(false);

// SupprimeParamètre("IpWifi")
WDAPIPersist.supprimeParametre("IpWifi");

// SupprimeParamètre("INT_SavAuto")
WDAPIPersist.supprimeParametre("INT_SavAuto");

// SupprimeParamètre("IpLienExt")
WDAPIPersist.supprimeParametre("IpLienExt");

// SAI_Lien_Externe_IP_1=""
mWD_ZM_Parametres_1.mWD_SAI_Lien_Externe_IP_1.setValeur("");

// SAI_Lien_Wifi_IP_2=""
mWD_ZM_Parametres_1.mWD_SAI_Lien_Wifi_IP_2.setValeur("");

// INT_SavAuto=0
mWD_ZM_Parametres_1.mWD_INT_SavAuto.setValeur(0);

// INT_GeoPrécis=0
mWD_ZM_Parametres_1.mWD_INT_GeoPrecis.setValeur(0);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDBTN_Supprimer mWD_BTN_Supprimer;

/**
 * LIB_SansNom1
 */
class GWDLIB_SansNom1 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Params.FI_Params.LIB_SansNom1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2962320917976725952l);

super.setChecksum("869434950");

super.setNom("LIB_SansNom1");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Paramètres");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(13, 64);

super.setTailleInitiale(295, 27);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xE0E0E0, creerPolice_GEN("Arial", -8.000000, 0), 4, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0x30303, 0x0, 0xE0E0E0, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_SansNom1 mWD_LIB_SansNom1;

/**
 * ZM_Paramètres_1
 */
class GWDZM_Parametres_1 extends WDZoneMultiligne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Params.FI_Params.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////

/**
 * ZM_Paramètres_1_Ligne1
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Params.FI_Params.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////

/**
 * SAI_Lien_Externe_IP_1
 */
class GWDSAI_Lien_Externe_IP_1 extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Params.FI_Params.ZM_Paramètres_1.SAI_Lien_Externe_IP_1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectLibelle(0,2,93,40);
super.setRectCompPrincipal(93,2,182,40);
super.setQuid(2962320917977840080l);

super.setChecksum("870548622");

super.setNom("SAI_Lien_Externe_IP_1");

super.setType(20001);

super.setLibelle("Lien externe ");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(12, 9);

super.setTailleInitiale(275, 44);

super.setValeurInitiale("192.168.1.248:8086");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("64"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(true);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(3);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -7.000000, 0), -2, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_Edt.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x30303, creerPolice_GEN("Arial", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Sortie de SAI_Lien_Externe_IP_1
 */
public void sortieChamp()
{
super.sortieChamp();

// SauveParamètre("IpLienExt",SAI_Lien_Externe_IP_1)
WDAPIPersist.sauveParametre("IpLienExt",this.getString());

// ClavierVisible(Faux)
WDAPIDivers.clavierVisible(false);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurFocus();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Lien_Externe_IP_1 mWD_SAI_Lien_Externe_IP_1 = new GWDSAI_Lien_Externe_IP_1();
class GWDZM_Parametres_1_Ligne1 extends LigneZMLStatique
{
/**
 * Initialise tous les champs de FI_Params.FI_Params.ZM_Paramètres_1
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Params.FI_Params.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_SAI_Lien_Externe_IP_1.initialiserObjet();
super.ajouterChamp(mWD_SAI_Lien_Externe_IP_1);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(63);

super.setVisibleInitial(true);

super.setModeSelection(1);

initialiserSousObjets();
super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_1_Ligne1 mWD_ZM_Parametres_1_Ligne1 = new GWDZM_Parametres_1_Ligne1();

/**
 * ZM_Paramètres_1_Ligne2
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Params.FI_Params.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////

/**
 * SAI_Lien_Wifi_IP_2
 */
class GWDSAI_Lien_Wifi_IP_2 extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Params.FI_Params.ZM_Paramètres_1.SAI_Lien_Wifi_IP_2
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectLibelle(0,2,95,40);
super.setRectCompPrincipal(95,2,180,40);
super.setQuid(2962320917977971152l);

super.setChecksum("870679694");

super.setNom("SAI_Lien_Wifi_IP_2");

super.setType(20001);

super.setLibelle("Lien WIFI");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(10, 8);

super.setTailleInitiale(275, 44);

super.setValeurInitiale("192.168.1.248:8086");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("64"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(true);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(3);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -7.000000, 0), -2, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_Edt.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x30303, creerPolice_GEN("Arial", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Sortie de SAI_Lien_Wifi_IP_2
 */
public void sortieChamp()
{
super.sortieChamp();

// SauveParamètre("IpWifi",SAI_Lien_Wifi_IP_2)
WDAPIPersist.sauveParametre("IpWifi",this.getString());

// ClavierVisible(Faux)
WDAPIDivers.clavierVisible(false);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurFocus();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Lien_Wifi_IP_2 mWD_SAI_Lien_Wifi_IP_2 = new GWDSAI_Lien_Wifi_IP_2();
class GWDZM_Parametres_1_Ligne2 extends LigneZMLStatique
{
/**
 * Initialise tous les champs de FI_Params.FI_Params.ZM_Paramètres_1
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Params.FI_Params.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_SAI_Lien_Wifi_IP_2.initialiserObjet();
super.ajouterChamp(mWD_SAI_Lien_Wifi_IP_2);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(61);

super.setVisibleInitial(true);

super.setModeSelection(-1);

initialiserSousObjets();
super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_1_Ligne2 mWD_ZM_Parametres_1_Ligne2 = new GWDZM_Parametres_1_Ligne2();

/**
 * ZM_Paramètres_1_Ligne3
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Params.FI_Params.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////

/**
 * LIB_Ajouter_les_données
 */
class GWDLIB_Ajouter_les_donnees extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Params.FI_Params.ZM_Paramètres_1.LIB_Ajouter_les_données
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2962320917978102224l);

super.setChecksum("870811222");

super.setNom("LIB_Ajouter_les_données");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Sauvegarder la connexion");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(8, 8);

super.setTailleInitiale(212, 28);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xFFFFFFFF, creerPolice_GEN("Arial", -8.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Ajouter_les_donnees mWD_LIB_Ajouter_les_donnees = new GWDLIB_Ajouter_les_donnees();

/**
 * INT_SavAuto
 */
class GWDINT_SavAuto extends WDInterrupteur
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Params.FI_Params.ZM_Paramètres_1.INT_SavAuto
////////////////////////////////////////////////////////////////////////////

/**
 * INT_SavAuto_Option_0
 */
class GWDINT_SavAuto_Option_0 extends WDCaseACocherNatif
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Params.FI_Params.ZM_Paramètres_1.INT_SavAuto.INT_SavAuto_Option_0
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setLibelle("");

super.setHauteurOption(0);

super.setStyleLibelleOption(0x30303, creerPolice_GEN("Arial", -8.000000, 0));

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDINT_SavAuto_Option_0 mWD_INT_SavAuto_Option_0 = new GWDINT_SavAuto_Option_0();
/**
 * Initialise tous les champs de FI_Params.FI_Params.ZM_Paramètres_1.INT_SavAuto
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Params.FI_Params.ZM_Paramètres_1.INT_SavAuto
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
super.ajouterOption(mWD_INT_SavAuto_Option_0);
positionnerOptions();
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setGroupe(mWD_GR_Groupe1);
super.setRectCompPrincipal(0,0,51,28);
super.setQuid(2962320917978167760l);

super.setChecksum("870877670");

super.setNom("INT_SavAuto");

super.setType(5);

super.setLibelle("Interrupteur");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(236, 8);

super.setTailleInitiale(51, 28);

super.setValeurInitiale("0");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setNumTab(1);

super.setLettreAppel(65535);

super.setPersistant(true);

super.setParamOptions(false, 1, true, true, false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1));

super.setParamAnimationChamp(18, 19, 300);
super.setParamAnimationChamp(19, 20, 300);

super.setImageCoche("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_CBox.png?E12_A1A6A1A6A1A6A1A6A1A6A1A6_Radio", 6);

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

/**
 * Traitement: A chaque modification de INT_SavAuto
 */
public void modification()
{
super.modification();

// SI INT_SavAuto=Vrai ALORS
if(this.opEgal(true))
{
// 	SauveParamètre("IntSavAuto",INT_SavAuto)
WDAPIPersist.sauveParametre("IntSavAuto",this.getString());

// 	SauveParamètre("IpWifi",SAI_Lien_Wifi_IP_2)
WDAPIPersist.sauveParametre("IpWifi",mWD_SAI_Lien_Wifi_IP_2.getString());

// 	SauveParamètre("IpLienExt",SAI_Lien_Externe_IP_1)
WDAPIPersist.sauveParametre("IpLienExt",mWD_SAI_Lien_Externe_IP_1.getString());

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurModification();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDINT_SavAuto mWD_INT_SavAuto = new GWDINT_SavAuto();

/**
 * INT_GeoPrécis
 */
class GWDINT_GeoPrecis extends WDInterrupteur
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Params.FI_Params.ZM_Paramètres_1.INT_GeoPrécis
////////////////////////////////////////////////////////////////////////////

/**
 * INT_GeoPrécis_Option_0
 */
class GWDINT_GeoPrecis_Option_0 extends WDCaseACocherNatif
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Params.FI_Params.ZM_Paramètres_1.INT_GeoPrécis.INT_GeoPrécis_Option_0
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setLibelle("");

super.setHauteurOption(29);

super.setStyleLibelleOption(0x30303, creerPolice_GEN("Arial", -8.000000, 0));

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDINT_GeoPrecis_Option_0 mWD_INT_GeoPrecis_Option_0 = new GWDINT_GeoPrecis_Option_0();
/**
 * Initialise tous les champs de FI_Params.FI_Params.ZM_Paramètres_1.INT_GeoPrécis
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Params.FI_Params.ZM_Paramètres_1.INT_GeoPrécis
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
super.ajouterOption(mWD_INT_GeoPrecis_Option_0);
positionnerOptions();
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectCompPrincipal(0,0,51,23);
super.setQuid(2962320917978298832l);

super.setChecksum("871008742");

super.setNom("INT_GeoPrécis");

super.setType(5);

super.setLibelle("Interrupteur");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(242, 105);

super.setTailleInitiale(51, 23);

super.setValeurInitiale("0");

super.setPlan(0);

super.setLiaisonFichier("fenparametres", "interrupteur1");

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setNumTab(2);

super.setLettreAppel(65535);

super.setPersistant(true);

super.setParamOptions(false, 1, true, true, false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1));

super.setParamAnimationChamp(18, 19, 300);
super.setParamAnimationChamp(19, 20, 300);

super.setImageCoche("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_CBox.png?E12_A1A6A1A6A1A6A1A6A1A6A1A6_Radio", 6);

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de INT_GeoPrécis
 */
public void init()
{
super.init();

// INT_GeoPrécis = ChargeParamètre("GeoPrécis")
this.setValeur(WDAPIPersist.chargeParametre("GeoPrécis"));

}




/**
 * Traitement: A chaque modification de INT_GeoPrécis
 */
public void modification()
{
super.modification();

// SauveParamètre("GeoPrécis",INT_GeoPrécis)
WDAPIPersist.sauveParametre("GeoPrécis",this.getString());

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurModification();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDINT_GeoPrecis mWD_INT_GeoPrecis = new GWDINT_GeoPrecis();

/**
 * LIB_Géolocalisation
 */
class GWDLIB_Geolocalisation extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FI_Params.FI_Params.ZM_Paramètres_1.LIB_Géolocalisation
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2962320917978429904l);

super.setChecksum("871138902");

super.setNom("LIB_Géolocalisation");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Géolocalisation");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(8, 72);

super.setTailleInitiale(193, 21);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xFFFFFFFF, creerPolice_GEN("Arial", -8.000000, 1), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0x30303, 0x0, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Geolocalisation mWD_LIB_Geolocalisation = new GWDLIB_Geolocalisation();

/**
 * LIB_Précision_de_la_localisation
 */
class GWDLIB_Precision_de_la_localisation extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FI_Params.FI_Params.ZM_Paramètres_1.LIB_Précision_de_la_localisation
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2962320917978495440l);

super.setChecksum("871204438");

super.setNom("LIB_Précision_de_la_localisation");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Précision de la localisation");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(8, 106);

super.setTailleInitiale(220, 22);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xFFFFFFFF, creerPolice_GEN("Arial", -8.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Precision_de_la_localisation mWD_LIB_Precision_de_la_localisation = new GWDLIB_Precision_de_la_localisation();

/**
 * LIB_Utiliser_le_GPS_pour
 */
class GWDLIB_Utiliser_le_GPS_pour extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°6 de FI_Params.FI_Params.ZM_Paramètres_1.LIB_Utiliser_le_GPS_pour
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2962320917978560976l);

super.setChecksum("871269974");

super.setNom("LIB_Utiliser_le_GPS_pour");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Utiliser le GPS pour plus de précision");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(8, 44);

super.setTailleInitiale(279, 20);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(6);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0xF4000000, 0xFFFFFFFF, creerPolice_GEN("Arial", -8.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(15, 0xC0C0C0, 0x404040, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Utiliser_le_GPS_pour mWD_LIB_Utiliser_le_GPS_pour = new GWDLIB_Utiliser_le_GPS_pour();
class GWDZM_Parametres_1_Ligne3 extends LigneZMLStatique
{
/**
 * Initialise tous les champs de FI_Params.FI_Params.ZM_Paramètres_1
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Params.FI_Params.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_LIB_Ajouter_les_donnees.initialiserObjet();
super.ajouterChamp(mWD_LIB_Ajouter_les_donnees);
mWD_INT_SavAuto.initialiserObjet();
super.ajouterChamp(mWD_INT_SavAuto);
mWD_INT_GeoPrecis.initialiserObjet();
super.ajouterChamp(mWD_INT_GeoPrecis);
mWD_LIB_Geolocalisation.initialiserObjet();
super.ajouterChamp(mWD_LIB_Geolocalisation);
mWD_LIB_Precision_de_la_localisation.initialiserObjet();
super.ajouterChamp(mWD_LIB_Precision_de_la_localisation);
mWD_LIB_Utiliser_le_GPS_pour.initialiserObjet();
super.ajouterChamp(mWD_LIB_Utiliser_le_GPS_pour);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(136);

super.setVisibleInitial(true);

super.setModeSelection(-1);

initialiserSousObjets();
super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_1_Ligne3 mWD_ZM_Parametres_1_Ligne3 = new GWDZM_Parametres_1_Ligne3();
/**
 * Initialise tous les champs de FI_Params.FI_Params.ZM_Paramètres_1
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Params.FI_Params.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_ZM_Parametres_1_Ligne1.initialiserObjet();
super.ajouterLigne(mWD_ZM_Parametres_1_Ligne1);
mWD_ZM_Parametres_1_Ligne2.initialiserObjet();
super.ajouterLigne(mWD_ZM_Parametres_1_Ligne2);
mWD_ZM_Parametres_1_Ligne3.initialiserObjet();
super.ajouterLigne(mWD_ZM_Parametres_1_Ligne3);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setPresenceLibelle(false);

super.setQuid(2962320917977709008l);

super.setChecksum("870460870");

super.setNom("ZM_Paramètres_1");

super.setType(97);

super.setLibelle("Zone multiligne");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(13, 155);

super.setTailleInitiale(293, 262);

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setNumTab(2);

super.setAltitude(3);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setIndiceModeleLigneDynamique(0);

super.setTauxParallaxe(0, 0);

super.setCouleurTexteAutomatique(0xFF000001);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCouleurSeparateur(0xCCCCCC);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE5E5E5, 0x656565, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(27, 0xC0C0C0, 0x404040, 0xFFFFFFFF, 9, 9, 1, 1));

super.setStyleSelection(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xD9D9D9, 4, 4, 1, 1));

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_1 mWD_ZM_Parametres_1;

/**
 * LIB_Nom_Utilisateur
 */
class GWDLIB_Nom_Utilisateur extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FI_Params.FI_Params.LIB_Nom_Utilisateur
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2978662299169761721l);

super.setChecksum("-2127362907");

super.setNom("LIB_Nom_Utilisateur");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Annuaire");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(10, 13);

super.setTailleInitiale(298, 43);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0xFF000001, 0xFFFFFFFF, creerPolice_GEN("Arial", -16.000000, 1), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(15, 0xE0E0E0, 0xFFFFFF, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Nom_Utilisateur mWD_LIB_Nom_Utilisateur;

/**
 * Traitement: Déclarations globales de FI_Params
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
super.declarerGlobale(WD_tabParam);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


// Si Taille(ChargeParamètre("IpLienExt" , "")) > 0 ALORS
if(WDAPIChaine.taille(WDAPIPersist.chargeParametre("IpLienExt","")).opSup(0))
{
// 	SAI_Lien_Externe_IP_1 = ChargeParamètre("IpLienExt")
mWD_ZM_Parametres_1.mWD_SAI_Lien_Externe_IP_1.setValeur(WDAPIPersist.chargeParametre("IpLienExt"));

}

// SI Taille(ChargeParamètre("IpWifi" , "" )) > 0 alors
if(WDAPIChaine.taille(WDAPIPersist.chargeParametre("IpWifi","")).opSup(0))
{
// 	SAI_Lien_Wifi_IP_2 = ChargeParamètre("IpWifi")
mWD_ZM_Parametres_1.mWD_SAI_Lien_Wifi_IP_2.setValeur(WDAPIPersist.chargeParametre("IpWifi"));

}

// SI Taille(ChargeParamètre("IntSavAuto" , "" )) > 0 ALORS
if(WDAPIChaine.taille(WDAPIPersist.chargeParametre("IntSavAuto","")).opSup(0))
{
// 	INT_SavAuto = ChargeParamètre("IntSavAuto")
mWD_ZM_Parametres_1.mWD_INT_SavAuto.setValeur(WDAPIPersist.chargeParametre("IntSavAuto"));

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FI_Params
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_GR_Groupe1 = new WDGroupe();
mWD_BTN_Supprimer = new GWDBTN_Supprimer();
mWD_LIB_SansNom1 = new GWDLIB_SansNom1();
mWD_ZM_Parametres_1 = new GWDZM_Parametres_1();
mWD_LIB_Nom_Utilisateur = new GWDLIB_Nom_Utilisateur();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FI_Params
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setQuid(2962320793421924566l);

super.setChecksum("868698343");

super.setNom("FI_Params");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTailleInitiale(318, 450);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setCouleurFond(0xE0E0E0);


activerEcoute();


////////////////////////////////////////////////////////////////////////////
// Initialisation des groupes de champs de FI_Params
////////////////////////////////////////////////////////////////////////////
mWD_GR_Groupe1.init("GR_Groupe1");
ajouter("GR_Groupe1", mWD_GR_Groupe1);


////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Params
////////////////////////////////////////////////////////////////////////////
mWD_BTN_Supprimer.initialiserObjet();
super.ajouter("BTN_Supprimer", mWD_BTN_Supprimer);
mWD_LIB_SansNom1.initialiserObjet();
super.ajouter("LIB_SansNom1", mWD_LIB_SansNom1);
mWD_ZM_Parametres_1.initialiserObjet();
super.ajouter("ZM_Paramètres_1", mWD_ZM_Parametres_1);
mWD_LIB_Nom_Utilisateur.initialiserObjet();
super.ajouter("LIB_Nom_Utilisateur", mWD_LIB_Nom_Utilisateur);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
}

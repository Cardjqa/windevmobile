/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FI_Rech
 * Date : 13/09/2017 09:16:39
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.card.Annuaire.wdgen;


import com.card.Annuaire.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetreinterne.*;
import fr.pcsoft.wdjava.ui.champs.modelechamp.*;
import fr.pcsoft.wdjava.ui.champs.zr.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.core.context.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.champs.bouton.*;
import fr.pcsoft.wdjava.core.types.collection.tableau.*;
import fr.pcsoft.wdjava.core.parcours.*;
import fr.pcsoft.wdjava.ui.champs.saisie.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFIFI_Rech extends WDFenetreInterne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FI_Rech
////////////////////////////////////////////////////////////////////////////

/**
 * CMOD_SansNom1
 */
class GWDCMOD_SansNom1 extends WDModeleChamp
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Rech.FI_Rech.CMOD_SansNom1
////////////////////////////////////////////////////////////////////////////

/**
 * ZR_Tableau
 */
class GWDZR_Tableau extends WDZoneRepetee
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Rech.FI_Rech.CMOD_SansNom1.ZR_Tableau
////////////////////////////////////////////////////////////////////////////

/**
 * IMG_Vignette
 */
class GWDIMG_Vignette extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Rech.FI_Rech.CMOD_SansNom1.ZR_Tableau.IMG_Vignette
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2968186404384488454l);

super.setChecksum("766273557");

super.setNom("IMG_Vignette");

super.setType(8);

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(11, 8);

super.setTailleInitiale(62, 80);

super.setValeurInitiale("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\PERS.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setTransparence(1);

super.setParamImage(327681, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(false);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(true);

super.setOrientationExif(true);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0xFF000001, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(2, 0xCCCCCC, 0x4C4C4C, 0xF1000000, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Vignette mWD_IMG_Vignette = new GWDIMG_Vignette();

/**
 * LIB_Libellé5
 */
class GWDLIB_Libelle5 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Rech.FI_Rech.CMOD_SansNom1.ZR_Tableau.LIB_Libellé5
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2968186404384553990l);

super.setChecksum("766336813");

super.setNom("LIB_Libellé5");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(85, 8);

super.setTailleInitiale(182, 22);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xFFFFFFFF, creerPolice_GEN("Arial", -5.000000, 1), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Libelle5 mWD_LIB_Libelle5 = new GWDLIB_Libelle5();

/**
 * LIB_SousLibellé1
 */
class GWDLIB_SousLibelle1 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Rech.FI_Rech.CMOD_SansNom1.ZR_Tableau.LIB_SousLibellé1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2968186404384619526l);

super.setChecksum("766402349");

super.setNom("LIB_SousLibellé1");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(85, 38);

super.setTailleInitiale(182, 50);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x303030, 0xFFFFFFFF, creerPolice_GEN("Arial", -5.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_SousLibelle1 mWD_LIB_SousLibelle1 = new GWDLIB_SousLibelle1();

/**
 * IMG_Puce
 */
class GWDIMG_Puce extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FI_Rech.FI_Rech.CMOD_SansNom1.ZR_Tableau.IMG_Puce
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2968186404384685062l);

super.setChecksum("766470165");

super.setNom("IMG_Puce");

super.setType(8);

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(275, 8);

super.setTailleInitiale(16, 80);

super.setValeurInitiale("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\iOS_ZR_Arrow.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setTransparence(1);

super.setParamImage(327682, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(false);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Puce mWD_IMG_Puce = new GWDIMG_Puce();
/**
 * Initialise tous les champs de FI_Rech.FI_Rech.CMOD_SansNom1.ZR_Tableau
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Rech.FI_Rech.CMOD_SansNom1.ZR_Tableau
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_IMG_Vignette.initialiserObjet();
super.ajouterChamp("IMG_Vignette",mWD_IMG_Vignette);
mWD_LIB_Libelle5.initialiserObjet();
super.ajouterChamp("LIB_Libellé5",mWD_LIB_Libelle5);
mWD_LIB_SousLibelle1.initialiserObjet();
super.ajouterChamp("LIB_SousLibellé1",mWD_LIB_SousLibelle1);
mWD_IMG_Puce.initialiserObjet();
super.ajouterChamp("IMG_Puce",mWD_IMG_Puce);
creerAttributAuto();
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectCompPrincipal(0,0,300,96);
super.setQuid(2968186404384422918l);

super.setChecksum("766218053");

super.setNom("ZR_Tableau");

super.setType(30);

super.setLibelle("Zone répétée");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(9, 221);

super.setTailleInitiale(300, 220);

super.setValeurInitiale("");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setNumTab(3);

super.setModeAscenseur(1, 1);

super.setModeSelection(99);

super.setSaisieEnCascade(false);

super.setLettreAppel(65535);

super.setEnregistrementSortieLigne(true);

super.setPersistant(false);

super.setParamAffichage(0, 1, 297, 96);

super.setBtnEnrouleDeroule(true);

super.setScrollRapide(false, null);

super.setDeplacementParDnd(0);

super.setSwipe(0, "", false, false, "", false, false);

super.setRecyclageChamp(true);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -5.000000, 1), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setStyleSeparateurVerticaux(false, 0xF2000000);

super.setStyleSeparateurHorizontaux(1, 0x4FD7);

super.setDessinerLigneVide(false);

super.setCouleurCellule(0xFFFFFF, 0xE0E0E0, 0xFFFFFF, 0x80C0FF);

super.setCadreFondLigne(WDCadreFactory.creerCadre_GEN("", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {4, 4, 4, 4}, 0xFFFFFFFF, 0, 5));

super.setImagePlusMoins("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_Break_Pict.png?E2_");

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

/**
 * Traitement: Sélection d'une ligne de ZR_Tableau (modèle MDLC_Recherche)
 */
public void selectionLigne()
{
super.selectionLigne();

// SauveParamètre("ContactSel",FEN_Recherche.myTableau0[MoiMême])
WDAPIPersist.sauveParametre("ContactSel",GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau0.get(WDContexte.getMoiMeme()).getString());

// FEN_Recherche.myTableau1 = ChaîneDécoupe(ZR_Tableau[MoiMême].LIB_Libellé5..Libellé,",")
GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.setValeur(WDAPIChaine.chaineDecoupe(this.get(WDContexte.getMoiMeme()).get("LIB_Libellé5").getLibelle(),new WDObjet[] {new WDChaineU(",")} ));

// SauveParamètre("NumContact",FEN_Recherche.myTableau1[1])
WDAPIPersist.sauveParametre("NumContact",GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.get(1).getString());

// FEN_Recherche.myTableau1 = ChaîneDécoupe( ChargeParamètre("ContactSel",""),",")
GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.setValeur(WDAPIChaine.chaineDecoupe(WDAPIPersist.chargeParametre("ContactSel",""),new WDObjet[] {new WDChaineU(",")} ));

// OuvreFenêtreMobile(FEN_FEN_cONTACTS_FI)
WDAPIFenetre.ouvreFille(GWDPMon_Projet_an.ms_Project.mWD_FEN_FEN_cONTACTS_FI);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurSelection();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZR_Tableau mWD_ZR_Tableau = new GWDZR_Tableau();

/**
 * BTN_SansNom1
 */
class GWDBTN_SansNom1 extends WDBouton
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Rech.FI_Rech.CMOD_SansNom1.BTN_SansNom1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2968186404385209397l);

super.setChecksum("766992676");

super.setNom("BTN_SansNom1");

super.setType(4);

super.setLibelle("Chercher");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(8, 161);

super.setTailleInitiale(298, 39);

super.setPlan(0);

super.setImageEtat(5);

super.setImageFondEtat(5);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setNumTab(2);

super.setLettreAppel(65535);

super.setTypeBouton(1);

super.setTypeActionPredefinie(0);

super.setBoutonOnOff(false);

super.setTauxParallaxe(0, 0);

super.setLibelleVAlign(1);

super.setLibelleHAlign(5);

super.setPresenceLibelle(true);

super.setImage("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_Pict_Search_16_5.png?E5", 0, 1, 5, null, null, null);

super.setStyleLibelleRepos(0xF4000000, creerPolice_GEN("Arial", -8.000000, 0), 0, 0x808080);

super.setStyleLibelleSurvol(0xF4000000, creerPolice_GEN("Arial", -8.000000, 0), 0, 0x808080);

super.setStyleLibelleEnfonce(0xF4000000, creerPolice_GEN("Arial", -8.000000, 0), 0, 0x808080);

super.setCadreRepos(WDCadreFactory.creerCadre_GEN(27, 0x0, 0xC0C0C0, 0xC0C0C0, 4, 4, 1, 1));

super.setCadreSurvol(WDCadreFactory.creerCadre_GEN(27, 0x0, 0xC0C0C0, 0xC0C0C0, 4, 4, 1, 1));

super.setCadreEnfonce(WDCadreFactory.creerCadre_GEN(27, 0x0, 0xC0C0C0, 0xC0C0C0, 4, 4, 1, 1));

super.setImageFond9Images(new int[] {1,2,1,2,2,2,1,2,1}, 8, 8, 10, 10);

super.setImageFond("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_Btn_Std.png?E5_3NP_10_10_8_8", 1, 0, 1, 1);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur BTN_SansNom1 ( CMOD_SansNom1 ) (modèle MDLC_Recherche)
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();


////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_gTabNumContact = WDVarNonAllouee.ref;


// gTabNumContact est un tableau de  chaînes
vWD_gTabNumContact = new WDTableauSimple(1, new int[]{0}, 0, 16);

// ClavierVisible(Faux)
WDAPIDivers.clavierVisible(false);

// si SAI_RECHERCHER..Libellé = "PERSONNES" ALORS
if(mWD_SAI_RECHERCHER.getLibelle().opEgal("PERSONNES"))
{
// 	cMaRequête est une chaine UNICODE
WDObjet vWD_cMaRequete = new WDChaineU();



// 	si ZR_Tableau..Occurrence > 0 alors
if(mWD_ZR_Tableau.getOccurrence().opSup(0))
{
// 		ZoneRépétéeSupprimeTout(ZR_Tableau)
WDAPIZoneRepetee.zoneRepeteeSupprimeTout(mWD_ZR_Tableau);

}

// 	cMaRequête ="http://"+FEN_FenLoginParams.gsIp+"/anpro/phpAn.php?"+"params="+URLEncode(SAI_RECHERCHER)+"&GROUPE="+ChargeParamètre("GROUPE")
vWD_cMaRequete.setValeur(new WDChaineU("http://").opPlus(GWDPMon_Projet_an.ms_Project.getFEN_FenLoginParams().vWD_gsIp).opPlus("/anpro/phpAn.php?").opPlus("params=").opPlus(WDAPIDiversSTD.urlEncode(mWD_SAI_RECHERCHER)).opPlus("&GROUPE=").opPlus(WDAPIPersist.chargeParametre("GROUPE")));

// 	SI HTTPRequête(cMaRequête) = Vrai ALORS
if(WDAPIHttp.HTTPRequete(vWD_cMaRequete.getString()).opEgal(true))
{
// 		FEN_Recherche.sCMaRéponse = UTF8VersUnicode(HTTPDonneRésultat(httpRésultat))
GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_sCMaReponse.setValeur(WDAPIChaine.UTF8VersUnicode(WDAPIHttp.HTTPDonneResultat(2)));

}

// 	si FEN_Recherche.myTableau0..Occurrence > 0 ALORS
if(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau0.getOccurrence().opSup(0))
{
// 		TableauSupprimeTout(FEN_Recherche.myTableau0)
WDAPITableau.tableauSupprimeTout(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau0);

}

// 	FEN_Recherche.MyTableau0 = ChaîneDécoupe(FEN_Recherche.sCMaRéponse,";")
GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau0.setValeur(WDAPIChaine.chaineDecoupe(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_sCMaReponse,new WDObjet[] {new WDChaineU(";")} ));

// 	nLigne est un entier = 1
WDObjet vWD_nLigne = new WDEntier();


vWD_nLigne.setValeur(1);


// 	POUR TOUT sNident DE  FEN_Recherche.MyTableau0
IWDParcours parcours1 = null;
try
{
WDObjet vWD_sNident = WDParcoursFactory.creerVariableParcours(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau0);
parcours1 = WDParcoursFactory.pourTout(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau0, vWD_sNident, null, null, null, 0x0, 0x2);
while(parcours1.testParcours())
{
// 		SI FEN_Recherche.myTableau1..Occurrence > 0 alors
if(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.getOccurrence().opSup(0))
{
// 			TableauSupprimeTout(FEN_Recherche.myTableau1)
WDAPITableau.tableauSupprimeTout(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1);

}

// 		FEN_Recherche.MyTableau1 = ChaîneDécoupe(sNident,",")
GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.setValeur(WDAPIChaine.chaineDecoupe(parcours1.getVariableParcours(),new WDObjet[] {new WDChaineU(",")} ));

// 		SI nLigne <= FEN_Recherche.MyTableau0..Occurrence alors
if(vWD_nLigne.opInfEgal(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau0.getOccurrence()))
{
// 			nLigne = ZoneRépétéeAjouteLigne(ZR_Tableau)
vWD_nLigne.setValeur(WDAPIZoneRepetee.zoneRepeteeAjouteLigne(mWD_ZR_Tableau));

}
else
{
// 			SORTIR
break;

//////////////////////////////////////////////////////////
// Code Inaccessible
// 
}

// 		Si FEN_Recherche.myTableau1..Occurrence > 0 ou FEN_Recherche.myTableau1..Occurrence > 1 ou FEN_Recherche.myTableau1..Occurrence > 2
if(((GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.getOccurrence().opSup(0) | GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.getOccurrence().opSup(1)) | GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.getOccurrence().opSup(2)))
{
// 			nCompteur est un entier
WDObjet vWD_nCompteur = new WDEntier();



// 			nNumContact est un entier
WDObjet vWD_nNumContact = new WDEntier();



// 			POUR indice = 1 _A_ 3 
// Délimiteur de visibilité pour ne pas étendre la visibilité des variables temporaires _WDExpBorneMax et _WDExpPas
{
WDObjet _WDExpBorneMax0 = new WDEntier(3);
for(WDObjet vWD_indice = new WDEntier(1);vWD_indice.opInfEgal(_WDExpBorneMax0);vWD_indice.opInc())
{
// 				nCompteur=1			
vWD_nCompteur.setValeur(1);

// 				SI FEN_Recherche.MyTableau1..Occurrence > 0 _et_ indice = 1 ALORS
if((GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.getOccurrence().opSup(0) && vWD_indice.opEgal(1)))
{
// 					ZR_Tableau[nLigne].LIB_Libellé5..Libellé = FEN_Recherche.MyTableau1[indice]
mWD_ZR_Tableau.get(vWD_nLigne).get("LIB_Libellé5").setLibelle(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.get(vWD_indice).getString());

// 					gTabNumContact = ChaîneDécoupe(FEN_Recherche.myTableau1[indice]," ")
vWD_gTabNumContact.setValeur(WDAPIChaine.chaineDecoupe(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.get(vWD_indice),new WDObjet[] {new WDChaineU(" ")} ));

// 					nNumContact = gTabNumContact[1]
vWD_nNumContact.setValeur(vWD_gTabNumContact.get(1));

}

// 				SI FEN_Recherche.MyTableau1..Occurrence > 1 _et_ indice = 2 ALORS
if((GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.getOccurrence().opSup(1) && vWD_indice.opEgal(2)))
{
// 					tabAddresse est un tableau de chaînes
WDObjet vWD_tabAddresse = WDVarNonAllouee.ref;
vWD_tabAddresse = new WDTableauSimple(1, new int[]{0}, 0, 16);

// 					tabAddresse = ChaîneDécoupe(FEN_Recherche.myTableau1[indice],"$$")			
vWD_tabAddresse.setValeur(WDAPIChaine.chaineDecoupe(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.get(vWD_indice),new WDObjet[] {new WDChaineU("$$")} ));

// 					POUR TOUT nElément  ,nCompteur DE tabAddresse
IWDParcours parcours2 = null;
try
{
WDObjet vWD_nElement = WDParcoursFactory.creerVariableParcours(vWD_tabAddresse);
parcours2 = WDParcoursFactory.pourTout(vWD_tabAddresse, vWD_nElement, vWD_nCompteur, null, null, 0x0, 0x2);
while(parcours2.testParcours())
{
// 						SI nCompteur = 1 ALORS
if(vWD_nCompteur.opEgal(1))
{
// 							ZR_Tableau[nLigne].LIB_SousLibellé1..Libellé = nElément+RC
mWD_ZR_Tableau.get(vWD_nLigne).get("LIB_SousLibellé1").setLibelle(parcours2.getVariableParcours().opPlus("\r\n").getString());

}
else
{
// 							ZR_Tableau[nLigne].LIB_SousLibellé1..Libellé =ZR_Tableau[nLigne].LIB_SousLibellé1..Libellé+" "+nElément
mWD_ZR_Tableau.get(vWD_nLigne).get("LIB_SousLibellé1").setLibelle(mWD_ZR_Tableau.get(vWD_nLigne).get("LIB_SousLibellé1").getLibelle().opPlus(" ").opPlus(parcours2.getVariableParcours()).getString());

}

// 						nCompteur = nCompteur+1
vWD_nCompteur.setValeur(vWD_nCompteur.opPlus(1));

}

}
finally
{
if(parcours2 != null)
{
parcours2.finParcours();
}
}


}

// 				SI FEN_Recherche.myTableau1..Occurrence > 2 _ET_ indice = 3 _et_ ChaîneCommencePar(FEN_Recherche.myTableau1[indice],"fiche") ALORS
if(((GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.getOccurrence().opSup(2) && vWD_indice.opEgal(3)) && WDAPIChaine.chaineCommencePar(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.get(vWD_indice),new WDChaineU("fiche")).getBoolean()))
{
// 					ZR_Tableau[nLigne].IMG_Vignette = "http://"+FEN_FenLoginParams.gsIp+"/annuaire/photos_min/"+FEN_Recherche.myTableau1[indice]
mWD_ZR_Tableau.get(vWD_nLigne).get("IMG_Vignette").setValeur(new WDChaineU("http://").opPlus(GWDPMon_Projet_an.ms_Project.getFEN_FenLoginParams().vWD_gsIp).opPlus("/annuaire/photos_min/").opPlus(GWDPMon_Projet_an.ms_Project.getFEN_Recherche().vWD_myTableau1.get(vWD_indice)));

}

}
}

}

}

}
finally
{
if(parcours1 != null)
{
parcours1.finParcours();
}
}


// 	ZoneRépétéeSupprime(ZR_Tableau,ZR_Tableau..Occurrence)
WDAPIZoneRepetee.zoneRepeteeSupprime(mWD_ZR_Tableau,mWD_ZR_Tableau.getOccurrence().getInt());

// 	SI ErreurDétectée ALORS
if(WDObjet.ErreurDetectee.getBoolean())
{
// 		Erreur(ErreurInfo(errComplet))	
WDAPIDialogue.erreur(WDAPIVM.erreurInfo(19).getString());

}

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDBTN_SansNom1 mWD_BTN_SansNom1 = new GWDBTN_SansNom1();

/**
 * SAI_RECHERCHER
 */
class GWDSAI_RECHERCHER extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Rech.FI_Rech.CMOD_SansNom1.SAI_RECHERCHER
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectLibelle(0,0,300,23);
super.setRectCompPrincipal(0,23,300,39);
super.setQuid(2968186404385340469l);

super.setChecksum("767122836");

super.setNom("SAI_RECHERCHER");

super.setType(20001);

super.setLibelle("PERSONNES");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(9, 89);

super.setTailleInitiale(300, 64);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(9, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, creerPolice_GEN("Arial", -8.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xCCCCCC, 0x4C4C4C, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\Gabarits\\WM\\190 ActivPhone 7\\ActivPhone 7_Edt.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x30303, creerPolice_GEN("Arial", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Sortie de SAI_RECHERCHER ( CMOD_SansNom1 ) (modèle MDLC_Recherche)
 */
public void sortieChamp()
//  Désactiver le clavier en cours d'utilisation
{
super.sortieChamp();

// ClavierVisible(Faux)
WDAPIDivers.clavierVisible(false);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurFocus();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_RECHERCHER mWD_SAI_RECHERCHER = new GWDSAI_RECHERCHER();

/**
 * LIB_ARCHEVECHE
 */
class GWDLIB_ARCHEVECHE extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FI_Rech.FI_Rech.CMOD_SansNom1.LIB_ARCHEVECHE
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2978303814155440294l);

super.setChecksum("893527432");

super.setNom("LIB_ARCHEVECHE");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Rechercher");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(8, 60);

super.setTailleInitiale(302, 21);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x30303, 0xE0E0E0, creerPolice_GEN("Arial", -8.000000, 1), 4, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0x0, 0x0, 0xE0E0E0, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_ARCHEVECHE mWD_LIB_ARCHEVECHE = new GWDLIB_ARCHEVECHE();

/**
 * LIB_Nom_Utilisateur
 */
class GWDLIB_Nom_Utilisateur extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FI_Rech.FI_Rech.CMOD_SansNom1.LIB_Nom_Utilisateur
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2978663089467586837l);

super.setChecksum("-2103520071");

super.setNom("LIB_Nom_Utilisateur");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Annuaire");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(6, 9);

super.setTailleInitiale(306, 43);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0xFF000001, 0xFFFFFFFF, creerPolice_GEN("Arial", -16.000000, 1), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(15, 0xE0E0E0, 0xFFFFFF, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Nom_Utilisateur mWD_LIB_Nom_Utilisateur = new GWDLIB_Nom_Utilisateur();
/**
 * Initialise tous les champs de FI_Rech.FI_Rech.CMOD_SansNom1
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Rech.FI_Rech.CMOD_SansNom1
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_ZR_Tableau.initialiserObjet();
super.ajouter("ZR_Tableau", mWD_ZR_Tableau);
mWD_BTN_SansNom1.initialiserObjet();
super.ajouter("BTN_SansNom1", mWD_BTN_SansNom1);
mWD_SAI_RECHERCHER.initialiserObjet();
super.ajouter("SAI_RECHERCHER", mWD_SAI_RECHERCHER);
mWD_LIB_ARCHEVECHE.initialiserObjet();
super.ajouter("LIB_ARCHEVECHE", mWD_LIB_ARCHEVECHE);
mWD_LIB_Nom_Utilisateur.initialiserObjet();
super.ajouter("LIB_Nom_Utilisateur", mWD_LIB_Nom_Utilisateur);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2968186400087620192l);

super.setChecksum("764388550");

super.setNom("CMOD_SansNom1");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNumTab(1);

super.setEtatInitial(0);

super.setPositionInitiale(8, 8);

super.setTailleInitiale(302, 434);

super.setValeurInitiale("");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500);

super.setPersistant(false);

super.setTauxParallaxe(0, 0);

super.setCouleurTexteAutomatique(0xFF000001);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xE0E0E0, 4, 4, 1, 1), 0, 0, 0, 0);

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}








// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCMOD_SansNom1 mWD_CMOD_SansNom1;

/**
 * Traitement: Déclarations globales de FI_Rech
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
super.declarerGlobale(WD_tabParam);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FI_Rech
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_CMOD_SansNom1 = new GWDCMOD_SansNom1();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FI_Rech
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setQuid(2968186022129033638l);

super.setChecksum("762918940");

super.setNom("FI_Rech");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTailleInitiale(318, 450);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setCouleurFond(0xC0C0C0);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Rech
////////////////////////////////////////////////////////////////////////////
mWD_CMOD_SansNom1.initialiserObjet();
super.ajouter("CMOD_SansNom1", mWD_CMOD_SansNom1);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
}

/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Projet
 * Classe Android : Mon_Projet_an
 * Date : 15/09/2017 16:36:00
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.card.Annuaire.wdgen;


import com.card.Annuaire.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.core.context.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/





public class GWDPMon_Projet_an extends WDProjet
{
/**
 * Accès au projet: Mon_Projet_an
 * Pour accéder au projet à partir de n'importe où: 
 * GWDPMon_Projet_an.ms_Project
 */
public static GWDPMon_Projet_an ms_Project;

 // FEN_Menu_1
public GWDFFEN_Menu_1 mWD_FEN_Menu_1 = new GWDFFEN_Menu_1();
 // accesseur de FEN_Menu_1
public GWDFFEN_Menu_1 getFEN_Menu_1()
{
mWD_FEN_Menu_1.verifierOuverte();
return mWD_FEN_Menu_1;
}

 // FEN_FEN_cONTACTS_FI
public GWDFFEN_FEN_cONTACTS_FI mWD_FEN_FEN_cONTACTS_FI = new GWDFFEN_FEN_cONTACTS_FI();
 // accesseur de FEN_FEN_cONTACTS_FI
public GWDFFEN_FEN_cONTACTS_FI getFEN_FEN_cONTACTS_FI()
{
mWD_FEN_FEN_cONTACTS_FI.verifierOuverte();
return mWD_FEN_FEN_cONTACTS_FI;
}

 // FEN_APropos
public GWDFFEN_APropos mWD_FEN_APropos = new GWDFFEN_APropos();
 // accesseur de FEN_APropos
public GWDFFEN_APropos getFEN_APropos()
{
mWD_FEN_APropos.verifierOuverte();
return mWD_FEN_APropos;
}

 // FEN_Paramètres
public GWDFFEN_Parametres mWD_FEN_Parametres = new GWDFFEN_Parametres();
 // accesseur de FEN_Paramètres
public GWDFFEN_Parametres getFEN_Parametres()
{
mWD_FEN_Parametres.verifierOuverte();
return mWD_FEN_Parametres;
}

 // FEN_Recherche
public GWDFFEN_Recherche mWD_FEN_Recherche = new GWDFFEN_Recherche();
 // accesseur de FEN_Recherche
public GWDFFEN_Recherche getFEN_Recherche()
{
mWD_FEN_Recherche.verifierOuverte();
return mWD_FEN_Recherche;
}

 // FEN_FenLoginParams
public GWDFFEN_FenLoginParams mWD_FEN_FenLoginParams = new GWDFFEN_FenLoginParams();
 // accesseur de FEN_FenLoginParams
public GWDFFEN_FenLoginParams getFEN_FenLoginParams()
{
mWD_FEN_FenLoginParams.verifierOuverte();
return mWD_FEN_FenLoginParams;
}

 // FEN_Carte_Fonction
public GWDFFEN_Carte_Fonction mWD_FEN_Carte_Fonction = new GWDFFEN_Carte_Fonction();
 // accesseur de FEN_Carte_Fonction
public GWDFFEN_Carte_Fonction getFEN_Carte_Fonction()
{
mWD_FEN_Carte_Fonction.verifierOuverte();
return mWD_FEN_Carte_Fonction;
}


 // FI_Params
public GWDFIFI_Params mWD_FI_Params = new GWDFIFI_Params();
 // accesseur de FI_Params
public GWDFIFI_Params getFI_Params()
{
GWDFIFI_Params fiCtx = (GWDFIFI_Params)WDAppelContexte.getContexte().getFenetreInterne("FI_Params");
return fiCtx != null ? fiCtx  : mWD_FI_Params;
}

 // FI_Contacts
public GWDFIFI_Contacts mWD_FI_Contacts = new GWDFIFI_Contacts();
 // accesseur de FI_Contacts
public GWDFIFI_Contacts getFI_Contacts()
{
GWDFIFI_Contacts fiCtx = (GWDFIFI_Contacts)WDAppelContexte.getContexte().getFenetreInterne("FI_Contacts");
return fiCtx != null ? fiCtx  : mWD_FI_Contacts;
}

 // FI_Rech
public GWDFIFI_Rech mWD_FI_Rech = new GWDFIFI_Rech();
 // accesseur de FI_Rech
public GWDFIFI_Rech getFI_Rech()
{
GWDFIFI_Rech fiCtx = (GWDFIFI_Rech)WDAppelContexte.getContexte().getFenetreInterne("FI_Rech");
return fiCtx != null ? fiCtx  : mWD_FI_Rech;
}

 // FI_Log
public GWDFIFI_Log mWD_FI_Log = new GWDFIFI_Log();
 // accesseur de FI_Log
public GWDFIFI_Log getFI_Log()
{
GWDFIFI_Log fiCtx = (GWDFIFI_Log)WDAppelContexte.getContexte().getFenetreInterne("FI_Log");
return fiCtx != null ? fiCtx  : mWD_FI_Log;
}

 // Constructeur de la classe GWDPMon_Projet_an
public GWDPMon_Projet_an()
{
ajouterFenetre("FEN_Menu_1", mWD_FEN_Menu_1);
ajouterFenetre("FEN_FEN_cONTACTS_FI", mWD_FEN_FEN_cONTACTS_FI);
ajouterFenetre("FEN_APropos", mWD_FEN_APropos);
ajouterFenetre("FEN_Paramètres", mWD_FEN_Parametres);
ajouterFenetre("FEN_Recherche", mWD_FEN_Recherche);
ajouterFenetre("FEN_FenLoginParams", mWD_FEN_FenLoginParams);
ajouterFenetre("FEN_Carte_Fonction", mWD_FEN_Carte_Fonction);
ajouterFenetreInterne("FI_Params");
ajouterFenetreInterne("FI_Contacts");
ajouterFenetreInterne("FI_Rech");
ajouterFenetreInterne("FI_Log");

}


////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
static
{
// Allocation de l'objet global
GWDPMon_Projet_an.ms_Project = new GWDPMon_Projet_an();

// Définition des langues du projet
GWDPMon_Projet_an.ms_Project.setLangueProjet(new int[] {1}, new int[] {0}, 1, true);
GWDPMon_Projet_an.ms_Project.setNomCollectionProcedure(new String[]{"GWDCPCOL_ProceduresGlobales"});
}
public String getVersionApplication(){ return "0.0.312.0";}
public String getNomSociete(){ return "PC SOFT";}
public String getNomAPK(){ return "Annuaire";}
public int getIdNomApplication(){return com.card.Annuaire.R.string.app_name;}
public boolean isModeAnsi(){ return false;}
public boolean isAffectationTableauParCopie(){ return true;}
public boolean isAssistanceAutoHFActive(){ return true;}
public String getPackageRacine(){ return "com.card.Annuaire";}
public int getIdIconeApplication(){ return com.card.Annuaire.R.drawable.card_0;}
public String getCleGoogleMapsApi()
{
return "AIzaSyAsgvS9Eq08BD0PxRIh9yk49j5utYPxcjg";
}
public int getInfoPlateforme(EWDInfoPlateforme info)
{
switch(info)
{
case DPI_ECRAN : return 160;
case HAUTEUR_BARRE_SYSTEME : return 24;
case HAUTEUR_BARRE_TITRE : return 48;
case HAUTEUR_ACTION_BAR : return 48;
case HAUTEUR_BARRE_BAS : return 0;
case HAUTEUR_ECRAN : return 480;
case LARGEUR_ECRAN : return 320;
default : return 0;
}
}
public boolean isActiveThemeMaterialDesign()
{
return true;
}
////////////////////////////////////////////////////////////////////////////
public String getAdresseEmail() 
{
return "jqa@card.fr";
}
public boolean isIgnoreErreurCertificatHTTPS()
{
return false;
}
////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
protected void declarerRessources()
{
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\ICONE-150X150_CONTACT_01.PNG?E5",com.card.Annuaire.R.drawable.icone_150x150_contact_01_21_selector, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\DECO.PNG?E5",com.card.Annuaire.R.drawable.deco_20_selector, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\APROPOS.PNG?E5",com.card.Annuaire.R.drawable.apropos_19_selector, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\ABOUT_VERSION.PNG",com.card.Annuaire.R.drawable.about_version_18, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\ABOUT_NOTER.PNG",com.card.Annuaire.R.drawable.about_noter_17, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\ABOUT_APP.PNG",com.card.Annuaire.R.drawable.about_app_16, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\ABOUT.PNG",com.card.Annuaire.R.drawable.about_15, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\IOS_ZR_ARROW.PNG",com.card.Annuaire.R.drawable.ios_zr_arrow_14, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\GABARITS\\WM\\190 ACTIVPHONE 7\\ACTIVPHONE 7_PICT_SEARCH_16_5.PNG?E5",com.card.Annuaire.R.drawable.activphone_7_pict_search_16_5_13_selector, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\GABARITS\\WM\\190 ACTIVPHONE 7\\ACTIVPHONE 7_BREAK_PICT.PNG?E2_",com.card.Annuaire.R.drawable.activphone_7_break_pict_12_selector, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\GABARITS\\WM\\190 ACTIVPHONE 7\\ACTIVPHONE 7_BREAK.PNG",com.card.Annuaire.R.drawable.activphone_7_break_11, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\GABARITS\\WM\\190 ACTIVPHONE 7\\ACTIVPHONE 7_PICT_DELETE_16_5.PNG?E5",com.card.Annuaire.R.drawable.activphone_7_pict_delete_16_5_10_selector, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\GABARITS\\WM\\190 ACTIVPHONE 7\\ACTIVPHONE 7_BTN_REMOVE.PNG?E5_3NP_8_8_10_10",com.card.Annuaire.R.drawable.activphone_7_btn_remove_9_np3_8_8_10_10_selector, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\GABARITS\\WM\\190 ACTIVPHONE 7\\ACTIVPHONE 7_SELECT.PNG",com.card.Annuaire.R.drawable.activphone_7_select_8, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\GABARITS\\WM\\190 ACTIVPHONE 7\\ACTIVPHONE 7_PICT_OK_16_5.PNG?E5",com.card.Annuaire.R.drawable.activphone_7_pict_ok_16_5_7_selector, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\GABARITS\\WM\\190 ACTIVPHONE 7\\ACTIVPHONE 7_EDT.PNG?E5_3NP_8_8_8_8",com.card.Annuaire.R.drawable.activphone_7_edt_6_np3_8_8_8_8_selector, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\GABARITS\\WM\\190 ACTIVPHONE 7\\ACTIVPHONE 7_CBOX.PNG?E12_A1A6A1A6A1A6A1A6A1A6A1A6_Radio",com.card.Annuaire.R.drawable.activphone_7_cbox_5_selector_anim, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\GABARITS\\WM\\190 ACTIVPHONE 7\\ACTIVPHONE 7_BTN_STD.PNG?E5_3NP_10_10_8_8",com.card.Annuaire.R.drawable.activphone_7_btn_std_4_np3_10_10_8_8_selector, "");
super.ajouterFichierAssocie("PERS.png",com.card.Annuaire.R.drawable.pers_3, "");
super.ajouterFichierAssocie("E:\\Card Git\\windevmobile\\Mes Projets Mobile\\Mon_Projet_an\\PERS.png",com.card.Annuaire.R.drawable.pers_3, "");
super.ajouterFichierAssocie("apropos.wdpic",com.card.Annuaire.R.raw.apropos_2, "");
super.ajouterFichierAssocie("E:\\CARD GIT\\WINDEVMOBILE\\MES PROJETS MOBILE\\MON_PROJET_AN\\PARAM.PNG?E5",com.card.Annuaire.R.drawable.param_22_selector, "");
}

////////////////////////////////////////////////////////////////////////////
// Formats des masques du projet
////////////////////////////////////////////////////////////////////////////


/**
 * Appel des méthodes d'initialisation des classes / collections de procédures / projet
 */
static void GWDPMon_Projet_an_InitProjet( String [] args)
{
GWDPMon_Projet_an.ms_Project.initialiserProjet("Mon_Projet_an", "Application Android", args);
}

/**
 * Appel des méthodes de terminaison des projet / collections de procédures / classes
 */
static protected void GWDPMon_Projet_an_TermineProjet()
{

// Terminaison des collections de procédures et des classes

// Libération de l'objet global
GWDPMon_Projet_an.ms_Project = null;
}

/**
 * Lancer de l'application Android
 */
public static class WDLanceur extends WDAbstractLanceur
{
public void init()
{
// Appel des méthodes d'initialisation
GWDPMon_Projet_an_InitProjet(null);
}
public void run()
{

GWDPMon_Projet_an.ms_Project.lancerProjet();
}
}
}

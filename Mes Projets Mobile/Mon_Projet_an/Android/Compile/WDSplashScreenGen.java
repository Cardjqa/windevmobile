/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Splash Screen
 * Classe Android : Mon_Projet_an
 * Date : 15/09/2017 16:36:00
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.card.Annuaire.wdgen;


import com.card.Annuaire.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.style.degrade.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class WDSplashScreenGen implements IWDSplashScreen
{
public int getIdImageFond(){return com.card.Annuaire.R.drawable.s_p_l_a_s_c_r_e_e_n________1;}
public int getModeAffichageImageFond(){return 8;}
public int getIdNomApplication(){return com.card.Annuaire.R.string.splashscreen_title;}
public int getIdInfoVersion(){return com.card.Annuaire.R.string.splashscreen_version;}
public int getIdLogo(){return 0;}
public boolean isAvecAnimationChargement(){return true;}
public int getIdMessageChargement(){return com.card.Annuaire.R.string.splashscreen_message;}
public int getCouleurLibelle(){return 0xFFFFFF;}
public IWDDegrade getFondDegrade(){return null;}
public int getCouleurFond(){return 0xFFFFFF;}
}

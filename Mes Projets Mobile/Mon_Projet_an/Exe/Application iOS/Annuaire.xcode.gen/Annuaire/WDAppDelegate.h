//
//  WDAppDelegate.h
//
//  Created by JEROME on 04/08/2017.
//  Copyright . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	BOOL bBackground;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@end


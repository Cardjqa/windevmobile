//
//  main.m
//
//  Created by JEROME on 04/08/2017.
//  Copyright . All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"WDAppDelegate");
    [pool release];
    return retVal;
}

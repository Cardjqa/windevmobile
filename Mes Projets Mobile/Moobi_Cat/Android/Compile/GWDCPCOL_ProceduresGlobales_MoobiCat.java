/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Collection
 * Classe Android : COL_ProcéduresGlobales_MoobiCat
 * Date : 25/08/2017 17:31:48
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.masociete.moobi_cat.wdgen;


import com.masociete.moobi_cat.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.core.types.collection.tableau.*;
import fr.pcsoft.wdjava.rdv.*;
import fr.pcsoft.wdjava.core.poo.*;
import fr.pcsoft.wdjava.contact.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.core.parcours.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



class GWDCPCOL_ProceduresGlobales_MoobiCat extends WDCollProcAndroid
{

// Code de déclaration de COL_ProcéduresGlobales_MoobiCat
static public void init()
{
// tabGsCoupePontvir est un tableau de chaînes
vWD_tabGsCoupePontvir = new WDTableauSimple(1, new int[]{0}, 0, 16);
declarerVariableGlobale("COL_ProcéduresGlobales_MoobiCat","tabGsCoupePontvir",vWD_tabGsCoupePontvir);



// tabGsCoupeVirgule est un tableau de chaînes
vWD_tabGsCoupeVirgule = new WDTableauSimple(1, new int[]{0}, 0, 16);
declarerVariableGlobale("COL_ProcéduresGlobales_MoobiCat","tabGsCoupeVirgule",vWD_tabGsCoupeVirgule);



// gsReqPlanning est une chaîne
vWD_gsReqPlanning = new WDChaineU();

declarerVariableGlobale("COL_ProcéduresGlobales_MoobiCat","gsReqPlanning",vWD_gsReqPlanning);



// gsUrl est une chaîne
vWD_gsUrl = new WDChaineU();

declarerVariableGlobale("COL_ProcéduresGlobales_MoobiCat","gsUrl",vWD_gsUrl);



// gsRésultat est une chaîne
vWD_gsResultat = new WDChaineU();

declarerVariableGlobale("COL_ProcéduresGlobales_MoobiCat","gsRésultat",vWD_gsResultat);



// rdvPlanning est un RendezVous
vWD_rdvPlanning = new WDInstance( new WDRendezVous() );

declarerVariableGlobale("COL_ProcéduresGlobales_MoobiCat","rdvPlanning",vWD_rdvPlanning);



// gdhTempDate est une DateHeure
vWD_gdhTempDate = new WDDateHeure();

declarerVariableGlobale("COL_ProcéduresGlobales_MoobiCat","gdhTempDate",vWD_gdhTempDate);



// gRdvContact est un Contact
vWD_gRdvContact = new WDInstance( new WDContact() );

declarerVariableGlobale("COL_ProcéduresGlobales_MoobiCat","gRdvContact",vWD_gRdvContact);



// gsReqPlanning = ""
vWD_gsReqPlanning.setValeur("");

}



// Nombre de Procédures : 5
//  Résumé : <indiquez ici ce que fait la procédure>
//  Syntaxe :
// [ <Résultat> = ] Journal_Rendez_Vous ()
// 
//  Paramètres :
// 	Aucun
//  Valeur de retour :
//  	Type indéterminé : // 	Aucune
// 
//  Exemple :
//  Indiquez ici un exemple d'utilisation.
// 
static public WDObjet fWD_journal_Rendez_Vous()
{
initExecProcGlobale("Journal_Rendez_Vous");


try
{
// <COMPILE SI Configuration="Application Android">
{
// 	gsUrl = URLEncode(RetourneURL()+"/MoobiCat.php?DateHeure="+DateVersChaîne(DateSys() + HeureSys(),"AAAA/MM/JJ HH:SS")+"&collabnum="+ChargeParamètre("1"))
vWD_gsUrl.setValeur(WDAPIDiversSTD.urlEncode(GWDCPCOL_ProceduresGlobales_MoobiCat.fWD_retourneURL().opPlus("/MoobiCat.php?DateHeure=").opPlus(WDAPIDate.dateVersChaine(WDAPIDate.dateSys().opPlus(WDAPIDate.heureSys()),"AAAA/MM/JJ HH:SS")).opPlus("&collabnum=").opPlus(WDAPIPersist.chargeParametre("1"))));

}

// <COMPILE SI Configuration="Universal Windows 10 App">

// RENVOYER Test_Connecti(HTTPRequête(gsUrl),Null)
return GWDCPCOL_ProceduresGlobales_MoobiCat.fWD_test_Connecti(WDAPIHttp.HTTPRequete(vWD_gsUrl.getString()),WDObjet.NULL);

}
finally
{
finExecProcGlobale();
}

}


//  Résumé : <indiquez ici ce que fait la procédure>
//  Syntaxe :
// HttpRequDécoupe (<gsRésultatParam>)
// 
//  Paramètres :
// 	gsRésultatParam : <indiquez ici le rôle de gsRésultat>
//  Valeur de retour :
//  	Aucune
// 
//  Exemple :
//  Indiquez ici un exemple d'utilisation.
// 
static public void fWD_httpRequDecoupe( WDObjet vWD_gsResultatParam )
{
initExecProcGlobale("HttpRequDécoupe");


try
{
// gsRésultat = gsRésultatParam
vWD_gsResultat.setValeur(vWD_gsResultatParam);

// TableauSupprimeTout(tabGsCoupePontvir)
WDAPITableau.tableauSupprimeTout(vWD_tabGsCoupePontvir);

// TableauSupprimeTout(tabGsCoupeVirgule)
WDAPITableau.tableauSupprimeTout(vWD_tabGsCoupeVirgule);

// tabGsCoupePontvir = ChaîneDécoupe(gsRésultat,";")
vWD_tabGsCoupePontvir.setValeur(WDAPIChaine.chaineDecoupe(vWD_gsResultat,new WDObjet[] {new WDChaineU(";")} ));

// POUR TOUT sChainon DE tabGsCoupePontvir
IWDParcours parcours1 = null;
try
{
WDObjet vWD_sChainon = WDParcoursFactory.creerVariableParcours(vWD_tabGsCoupePontvir);
parcours1 = WDParcoursFactory.pourTout(vWD_tabGsCoupePontvir, vWD_sChainon, null, null, null, 0x0, 0x2);
while(parcours1.testParcours())
{
// 	tabGsCoupeVirgule = ChaîneDécoupe(sChainon,",")
vWD_tabGsCoupeVirgule.setValeur(WDAPIChaine.chaineDecoupe(parcours1.getVariableParcours(),new WDObjet[] {new WDChaineU(",")} ));

}

}
finally
{
if(parcours1 != null)
{
parcours1.finParcours();
}
}


}
finally
{
finExecProcGlobale();
}

}


//  Résumé : <indiquez ici ce que fait la procédure>
//  Syntaxe :
//  Login ()
// 
//  Paramètres :
// 	Aucun
//  Valeur de retour :
//  	Aucune
// 
//  Exemple :
//  Indiquez ici un exemple d'utilisation.
// 
static public void fWD_login()
{
initExecProcGlobale("Login");


try
{
// gsUrl = URLEncode(RetourneURL()+"/MoobiCatLogi.php?us="+FI_Présentation_Fin.SAI_Email+"&pa="+FI_Présentation_Fin.SAI_Mot_de_passe)
vWD_gsUrl.setValeur(WDAPIDiversSTD.urlEncode(GWDCPCOL_ProceduresGlobales_MoobiCat.fWD_retourneURL().opPlus("/MoobiCatLogi.php?us=").opPlus(GWDPMoobi_Cat.ms_Project.getFI_Presentation_Fin().mWD_SAI_Email).opPlus("&pa=").opPlus(GWDPMoobi_Cat.ms_Project.getFI_Presentation_Fin().mWD_SAI_Mot_de_passe)));

// Test_Connecti(HTTPRequête(gsUrl),Null)
GWDCPCOL_ProceduresGlobales_MoobiCat.fWD_test_Connecti(WDAPIHttp.HTTPRequete(vWD_gsUrl.getString()),WDObjet.NULL);

// ToastAffiche(gsUrl)
WDAPIToast.toastAffiche(vWD_gsUrl.getString());

// ToastAffiche(tabGsCoupePontvir..Occurrence)
WDAPIToast.toastAffiche(vWD_tabGsCoupePontvir.getOccurrence().getString());

// HttpRequDécoupe(gsRésultat)
GWDCPCOL_ProceduresGlobales_MoobiCat.fWD_httpRequDecoupe(vWD_gsResultat);

// SI tabGsCoupePontvir..Occurrence > 0 ALORS
if(vWD_tabGsCoupePontvir.getOccurrence().opSup(0))
{
// 	nCompteur est un entier
WDObjet vWD_nCompteur = new WDEntier();



// 	nCompteur = 1
vWD_nCompteur.setValeur(1);

// 	POUR TOUT  sChainon ,nCompteur de tabGsCoupeVirgule
IWDParcours parcours2 = null;
try
{
WDObjet vWD_sChainon = WDParcoursFactory.creerVariableParcours(vWD_tabGsCoupeVirgule);
parcours2 = WDParcoursFactory.pourTout(vWD_tabGsCoupeVirgule, vWD_sChainon, vWD_nCompteur, null, null, 0x0, 0x2);
while(parcours2.testParcours())
{
// 		SauveParamètre(NumériqueVersChaîne(nCompteur),sChainon)
WDAPIPersist.sauveParametre(WDAPINum.numeriqueVersChaine(vWD_nCompteur).getString(),parcours2.getVariableParcours().getString());

// 		nCompteur = nCompteur + 1
vWD_nCompteur.setValeur(vWD_nCompteur.opPlus(1));

// 		ToastAffiche(sChainon)
WDAPIToast.toastAffiche(parcours2.getVariableParcours().getString());

}

}
finally
{
if(parcours2 != null)
{
parcours2.finParcours();
}
}


// 	SI FI_Présentation_Milieu.INT_Option_1..Valeur ALORS
if(GWDPMoobi_Cat.ms_Project.getFI_Presentation_Milieu().mWD_ZM_Parametres_1.mWD_INT_Option_1.getValeur().getBoolean())
{
// 		TimerSys("Journal_Rendez_Vous",300000,1)
WDAPITimer.timerSys(new WDChaineU("Journal_Rendez_Vous"),300000,(long)1);

// 		ToastAffiche("Ok")
WDAPIToast.toastAffiche("Ok");

}
else
{
// 		FinTimerSys(1)
WDAPITimer.finTimerSys(1);

// 		ToastAffiche("Terminer")
WDAPIToast.toastAffiche("Terminer");

}

}

}
finally
{
finExecProcGlobale();
}

}


//  Résumé : <indiquez ici ce que fait la procédure>
//  Syntaxe :
// [ <Résultat> = ] Test_Connecti (<htpRequete>, <Temps>)
// 
//  Paramètres :
// 	htpRequete : <indiquez ici le rôle de htpRequete>
// 	Temps : <indiquez ici le rôle de Temps>
//  Valeur de retour :
//  	booléen : // 	Aucune
// 
//  Exemple :
//  Indiquez ici un exemple d'utilisation.
// 
//  	parschainon : <indiquez ici le rôle de parschainon>
static public WDObjet fWD_test_Connecti( WDObjet vWD_htpRequete , WDObjet vWD_Temps )
{
initExecProcGlobale("Test_Connecti");


try
{
// ToastAffiche(RetourneURL())
WDAPIToast.toastAffiche(GWDCPCOL_ProceduresGlobales_MoobiCat.fWD_retourneURL().getString());

// <compile si Configuration="Application Android">
{
// SI RéseauMobileInfoConnexion(réseauType) = réseau3G _OU_ RéseauMobileInfoConnexion(réseauType) = réseau4G _OU_ WiFiEtat = wifiActif ALORS
if(((WDAPIReseau.reseauMobileInfoConnexion(1).opEgal(3) || WDAPIReseau.reseauMobileInfoConnexion(1).opEgal(4)) || WDAPIWiFi.wifiEtat().opEgal(1)))
{
// 	SI htpRequete ALORS
if(vWD_htpRequete.getBoolean())
{
// 		gsRésultat = UTF8VersUnicode(HTTPDonneRésultat(httpRésultat))
vWD_gsResultat.setValeur(WDAPIChaine.UTF8VersUnicode(WDAPIHttp.HTTPDonneResultat(2)));

// 		gdhTempDate = Temps
vWD_gdhTempDate.setValeur(vWD_Temps);

// 		ToastAffiche(gsRésultat,toastLong)
WDAPIToast.toastAffiche(vWD_gsResultat.getString(),1);

// 		Renvoyer Vrai
return new WDBooleen(true);

}
else
{
// 		Erreur(ErreurInfo())
WDAPIDialogue.erreur(WDAPIVM.erreurInfo().getString());

// 		Renvoyer Faux
return new WDBooleen(false);

}

}

}

// <compile si Configuration="Universal Windows 10 App">

return new WDVoid("fWD_test_Connecti");
}
finally
{
finExecProcGlobale();
}

}


//  Résumé : <indiquez ici ce que fait la procédure>
//  Syntaxe :
// [ <Résultat> = ] RetourneURL ()
// 
//  Paramètres :
// 	Aucun
//  Valeur de retour :
//  	Type indéterminé : // 	Aucune
// 
//  Exemple :
//  Indiquez ici un exemple d'utilisation.
// 
static public WDObjet fWD_retourneURL()
{
initExecProcGlobale("RetourneURL");


try
{
// <compile si TypeConfiguration=Android>
{
// 	SI WiFiEtat() = wifiActif ALORS
if(WDAPIWiFi.wifiEtat().opEgal(1))
{
// 		renvoyer  "http://"+ChargeParamètre("IpWifi")
return new WDChaineU("http://").opPlus(WDAPIPersist.chargeParametre("IpWifi"));

}
else
{
// 		Renvoyer "http://"+ChargeParamètre("IpExt")
return new WDChaineU("http://").opPlus(WDAPIPersist.chargeParametre("IpExt"));

}

}

// <compile si TypeConfiguration=UniversalWindowsApp>

//////////////////////////////////////////////////////////
// Code Inaccessible
// 
}
finally
{
finExecProcGlobale();
}

}



////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
static public WDObjet vWD_tabGsCoupePontvir = WDVarNonAllouee.ref;
static public WDObjet vWD_tabGsCoupeVirgule = WDVarNonAllouee.ref;
static public WDObjet vWD_gsReqPlanning = WDVarNonAllouee.ref;
static public WDObjet vWD_gsUrl = WDVarNonAllouee.ref;
static public WDObjet vWD_gsResultat = WDVarNonAllouee.ref;
static public WDObjet vWD_rdvPlanning = WDVarNonAllouee.ref;
static public WDObjet vWD_gdhTempDate = WDVarNonAllouee.ref;
static public WDObjet vWD_gRdvContact = WDVarNonAllouee.ref;
}

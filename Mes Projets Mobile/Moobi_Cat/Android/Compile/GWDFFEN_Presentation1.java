/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_Présentation1
 * Date : 25/08/2017 11:51:27
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.masociete.moobi_cat.wdgen;


import com.masociete.moobi_cat.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.*;
import fr.pcsoft.wdjava.ui.champs.fenetreinterne.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.core.context.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.actionbar.*;
import fr.pcsoft.wdjava.ui.menu.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_Presentation1 extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des groupes de champs de FEN_Présentation1
////////////////////////////////////////////////////////////////////////////
 public WDGroupe mWD_GR_Bulles;

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_Présentation1
////////////////////////////////////////////////////////////////////////////

/**
 * CFI_Présentation
 */
class GWDCFI_Presentation extends WDChampFenetreInterneExt
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Présentation1.CFI_Présentation
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2998639174701586778l);

super.setChecksum("1262886659");

super.setNom("CFI_Présentation");

super.setType(31);

super.setLibelle("");

super.setNote("", "");

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 480);

super.setValeurInitiale("");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setNumTab(1);

super.setAltitude(1);

super.setAncrageInitial(10, 1000, 1000, 1000, 1000);

super.setPersistant(false);

super.setFenetreInterne("");

super.setNbFenetreAvantRecyclage(3);

super.setTauxParallaxe(0, 0);

super.setCouleurTexteAutomatique(0xFF000001);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xF1000000, 0xF3000000, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setParamAnimationChamp(25, 23, 0);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Sélection par balayage de CFI_Présentation
 */
public void selectionLigne()
{
super.selectionLigne();

// RafraîchitAffichagePosition(moimeme)
fWD_rafraichitAffichagePosition(WDContexte.getMoiMeme());

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurSelection();
}

////////////////////////////////////////////////////////////////////////////
protected boolean isAvecAscenseurAuto()
{
return false;
}

protected boolean isBalayageVertical()
{
return false;
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCFI_Presentation mWD_CFI_Presentation;

/**
 * IMG_Bulle_Base
 */
class GWDIMG_Bulle_Base extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Présentation1.IMG_Bulle_Base
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setGroupe(mWD_GR_Bulles);
super.setQuid(2998639174701652314l);

super.setChecksum("1262941707");

super.setNom("IMG_Bulle_Base");

super.setType(8);

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(0, 507);

super.setTailleInitiale(12, 12);

super.setValeurInitiale("");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 500, 1000);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(false);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(31, 0x222222, 0x0, 0xFFFFFF, 4, 4, 2, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Bulle_Base mWD_IMG_Bulle_Base;


////////////////////////////////////////////////////////////////////////////
// Procédures utilisateur de FEN_Présentation1
////////////////////////////////////////////////////////////////////////////
//  Résumé : Rafraîchit les bulles de position
// 
public void fWD_rafraichitAffichagePosition( WDObjet vWD_sNomChampFenetreInterne )
{
initExecProcLocale("RafraîchitAffichagePosition");


try
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_nNbFI = new WDEntier();

WDObjet vWD_nPosition = new WDEntier();



// VerifiePucePresenteSeonFI(sNomChampFenetreInterne)
fWD_verifiePucePresenteSeonFI(vWD_sNomChampFenetreInterne);

// nNbFI est un entier = FIListeOccurrence(sNomChampFenetreInterne) 

vWD_nNbFI.setValeur(WDAPIFenetreInterne.fiListeOccurrence(vWD_sNomChampFenetreInterne));


// nPosition est un entier = FIListePosition(sNomChampFenetreInterne)

vWD_nPosition.setValeur(WDAPIFenetreInterne.fiListePosition(vWD_sNomChampFenetreInterne));


// si nNbFI>0 alors
if(vWD_nNbFI.opSup(0))
{
// 	GR_Bulles..Opacité=50
mWD_GR_Bulles.setOpacite(50);

// 	si nPosition>0 alors
if(vWD_nPosition.opSup(0))
{
// 		{_sNomBulle(nPosition), indChamp}..Opacité=100	
WDIndirection.get(fWD__sNomBulle(vWD_nPosition).getString(),4).setProp(EWDPropriete.PROP_OPACITE,100);

}

}

// GR_Bulles..Visible = (nNbFI>0) _et_ (nPosition <> nNbFI)
mWD_GR_Bulles.setVisible((vWD_nNbFI.opSup(0) && vWD_nPosition.opDiff(vWD_nNbFI)));

// OPT_PASSER..Visible = (nNbFI=0) _ou_ (nPosition <> nNbFI)
mWD__Menu.mWD_OPT_PASSER.setVisible((vWD_nNbFI.opEgal(0) || vWD_nPosition.opDiff(vWD_nNbFI)));

}
finally
{
finExecProcLocale();
}

}



//  Résumé : <indiquez ici ce que fait la procédure>
//  Syntaxe :
// VerifiePucePresenteSeonFI (<sNomChampFenetreInterne>)
// 
//  Paramètres :
//  	sNomChampFenetreInterne : <indiquez ici le rôle de sNomChampFenetreInterne>
//  Valeur de retour :
//  	Aucune
// 
//  Exemple :
//  Indiquez ici un exemple d'utilisation.
// 
public void fWD_verifiePucePresenteSeonFI( WDObjet vWD_sNomChampFenetreInterne )
{
initExecProcLocale("VerifiePucePresenteSeonFI");


try
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_bDoitReplacerPuce = new WDBooleen();

WDObjet vWD_nNbFI = new WDEntier();

WDObjet vWD_nIndASupprimer = new WDEntier();



// bDoitReplacerPuce est un booléen=faux

vWD_bDoitReplacerPuce.setValeur(false);


// nNbFI est un entier = FIListeOccurrence(sNomChampFenetreInterne)

vWD_nNbFI.setValeur(WDAPIFenetreInterne.fiListeOccurrence(vWD_sNomChampFenetreInterne));


// pour i=1 a nNbFI
for(WDObjet vWD_i = new WDEntier(1);vWD_i.opInfEgal(vWD_nNbFI);vWD_i.opInc())
{
// 	sNomBulle est une chaine = _sNomBulle(i)
WDObjet vWD_sNomBulle = new WDChaineU();


vWD_sNomBulle.setValeur(fWD__sNomBulle(vWD_i));


// 	si pas ChampExiste(sNomBulle) alors
if((!WDAPIDivers.champExiste(vWD_sNomBulle.getString()).getBoolean()))
{
// 		champclone(IMG_Bulle_Base,sNomBulle)
WDAPIDivers.champClone(mWD_IMG_Bulle_Base,vWD_sNomBulle.getString());

// 		bDoitReplacerPuce=vrai
vWD_bDoitReplacerPuce.setValeur(true);

}

}

// nIndASupprimer est un entier = nNbFI+1

vWD_nIndASupprimer.setValeur(vWD_nNbFI.opPlus(1));


// tantque ChampExiste(_sNomBulle(nIndASupprimer))
while(WDAPIDivers.champExiste(fWD__sNomBulle(vWD_nIndASupprimer).getString()).getBoolean())
{
// 	si {_sNomBulle(nIndASupprimer), indChamp}..Y<>-1000 alors
if(WDIndirection.get(fWD__sNomBulle(vWD_nIndASupprimer).getString(),4).getProp(EWDPropriete.PROP_LIGNE).opDiff(-1000))
{
// 		{_sNomBulle(nIndASupprimer), indChamp}..Y=-1000
WDIndirection.get(fWD__sNomBulle(vWD_nIndASupprimer).getString(),4).setProp(EWDPropriete.PROP_LIGNE,-1000);

// 		bDoitReplacerPuce=Vrai
vWD_bDoitReplacerPuce.setValeur(true);

}

}

// si bDoitReplacerPuce alors
if(vWD_bDoitReplacerPuce.getBoolean())
{
// 	RepositionnePuce(nNbFI, sNomChampFenetreInterne)
fWD_repositionnePuce(vWD_nNbFI,vWD_sNomChampFenetreInterne);

}

}
finally
{
finExecProcLocale();
}

}



public WDObjet fWD__sNomBulle( WDObjet vWD_nInd )
{
initExecProcLocale("_sNomBulle");


try
{
vWD_nInd = WDParametre.traiterParametre(vWD_nInd, 1, false, 8);


// renvoyer "IMG_Bulle_"+nInd
return new WDChaineU("IMG_Bulle_").opPlus(vWD_nInd);

}
finally
{
finExecProcLocale();
}

}



public void fWD_repositionnePuce( WDObjet vWD_nNbFI , WDObjet vWD_sNomChampFenetreInterne )
{
initExecProcLocale("RepositionnePuce");


try
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_nLargeurEntrePuce = new WDEntier();

WDObjet vWD_nLargeurDesPuces = new WDEntier();

WDObjet vWD_nLargeurMax = new WDEntier();

WDObjet vWD_nPosXDepart = new WDEntier();

WDObjet vWD_nPosY = new WDEntier();



vWD_nNbFI = WDParametre.traiterParametre(vWD_nNbFI, 1, false, 8);


// si nNbFI=0 alors retour
if(vWD_nNbFI.opEgal(0))
{
// si nNbFI=0 alors retour
return;

}

// nLargeurEntrePuce est un entier = IMG_Bulle_Base..Largeur

vWD_nLargeurEntrePuce.setValeur(mWD_IMG_Bulle_Base.getLargeur());


// nLargeurDesPuces est un entier = nNbFI*IMG_Bulle_Base..Largeur + (nNbFI-1)*nLargeurEntrePuce

vWD_nLargeurDesPuces.setValeur(vWD_nNbFI.opMult(mWD_IMG_Bulle_Base.getLargeur()).opPlus(vWD_nNbFI.opMoins(1).opMult(vWD_nLargeurEntrePuce)));


// nLargeurMax est un entier = (sNomChampFenetreInterne..Largeur*0.8)

vWD_nLargeurMax.setValeur(vWD_sNomChampFenetreInterne.getProp(EWDPropriete.PROP_LARGEUR).opMult(0.8));


// si nLargeurDesPuces>nLargeurMax alors
if(vWD_nLargeurDesPuces.opSup(vWD_nLargeurMax))
{
// 	nLargeurEntrePuce = (nLargeurMax-(nNbFI*IMG_Bulle_Base..Largeur))/nNbFI
vWD_nLargeurEntrePuce.setValeur(vWD_nLargeurMax.opMoins(vWD_nNbFI.opMult(mWD_IMG_Bulle_Base.getLargeur())).opDiv(vWD_nNbFI));

// 	nLargeurDesPuces=(nNbFI*IMG_Bulle_Base..Largeur)+((nNbFI-1)*nLargeurEntrePuce)
vWD_nLargeurDesPuces.setValeur(vWD_nNbFI.opMult(mWD_IMG_Bulle_Base.getLargeur()).opPlus(vWD_nNbFI.opMoins(1).opMult(vWD_nLargeurEntrePuce)));

}

// nPosXDepart est un entier = (sNomChampFenetreInterne..Largeur-nLargeurDesPuces)/2

vWD_nPosXDepart.setValeur(vWD_sNomChampFenetreInterne.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nLargeurDesPuces).opDiv(2));


// nPosY est un entier = sNomChampFenetreInterne..Y+sNomChampFenetreInterne..Hauteur-(0.1*sNomChampFenetreInterne..Hauteur)-IMG_Bulle_Base..Hauteur

vWD_nPosY.setValeur(vWD_sNomChampFenetreInterne.getProp(EWDPropriete.PROP_LIGNE).opPlus(vWD_sNomChampFenetreInterne.getProp(EWDPropriete.PROP_HAUTEUR)).opMoins(new WDReel(0.1).opMult(vWD_sNomChampFenetreInterne.getProp(EWDPropriete.PROP_HAUTEUR))).opMoins(mWD_IMG_Bulle_Base.getHauteur()));


// pour i=1 _a_ nNbFI
// Délimiteur de visibilité pour ne pas étendre la visibilité des variables temporaires _WDExpBorneMax et _WDExpPas
{
WDObjet _WDExpBorneMax0 = new WDEntier(vWD_nNbFI);
for(WDObjet vWD_i = new WDEntier(1);vWD_i.opInfEgal(_WDExpBorneMax0);vWD_i.opInc())
{
// 	sNomBulle est une chaine = _sNomBulle(i)
WDObjet vWD_sNomBulle = new WDChaineU();


vWD_sNomBulle.setValeur(fWD__sNomBulle(vWD_i));


// 	{sNomBulle, indChamp}..X=nPosXDepart
WDIndirection.get(vWD_sNomBulle.getString(),4).setProp(EWDPropriete.PROP_COLONNE,vWD_nPosXDepart);

// 	{sNomBulle, indChamp}..Y=nPosY
WDIndirection.get(vWD_sNomBulle.getString(),4).setProp(EWDPropriete.PROP_LIGNE,vWD_nPosY);

// 	nPosXDepart+=IMG_Bulle_Base..largeur+nLargeurEntrePuce
vWD_nPosXDepart.setValeur(vWD_nPosXDepart.opPlus(mWD_IMG_Bulle_Base.getLargeur().opPlus(vWD_nLargeurEntrePuce)));

}
}

}
finally
{
finExecProcLocale();
}

}




/**
 * ACTB_ActionBar
 */
class GWDACTB_ActionBar extends WDActionBar
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Présentation1.ACTB_ActionBar
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setNom("ACTB_ActionBar");

super.setNote("", "");

super.setParamBoutonGauche(true, 1, "", "");

super.setParamBoutonDroit(false, 0, "", "");

super.setStyleActionBar(0xFFFFFF, 0x383226, true);

super.setImageFond("");

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDACTB_ActionBar mWD_ACTB_ActionBar;

class GWDM_Menu extends WDMenuPrincipal
{

class GWDMOPT_PASSER extends WDOptionMenu
{
public GWDMOPT_PASSER(boolean b)
{
super(b, true);
}
public void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setConteneurMenu(GWDM_Menu.this);
super.setQuid(2998639174701783386l);

super.setNom("OPT_PASSER");

super.setType(40001);

super.setLibelle("PASSER");

super.setEtat(0);

super.setImage("");

super.setVisible(true);

super.setCochee(false);

super.setNumero(1);

super.setAffichageDansActionBar(true);

super.setIconePredefinie(-1);

activerEcoute();
}
}
public GWDMOPT_PASSER mWD_OPT_PASSER = new GWDMOPT_PASSER(true);

public void initialiserSousObjets()
{
mWD_OPT_PASSER.initialiserObjet();
super.ajouterMenu(mWD_OPT_PASSER);
}
public void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2998639174701717850l);

super.setNom("_Menu");

super.setType(40001);

super.setStyleOptionRepos(0xF7000000, 0xFF000001, creerPolice_GEN("Tahoma", -11.000000, 0));
super.setStyleOptionSurvol(0xF7000000, 0xFF000001, creerPolice_GEN("Tahoma", -11.000000, 0));
activerEcoute();
initialiserSousObjets();
}
}
public GWDM_Menu mWD__Menu;



/**
 * Traitement: Déclarations globales de FEN_Présentation1
 */
//  Cette fenêtre est un exemple d'interface personnalisable.
//  Vous pouvez la modifier selon vos besoins.
//  Les traitements à personnaliser sont identifiés par des "// A faire" que
//  vous pouvez retrouver en utilisant la recherche ou le volet "Liste des tâches"
public void declarerGlobale(WDObjet[] WD_tabParam)
{
super.declarerGlobale(WD_tabParam);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


// FIListeAjoute(CFI_Présentation,FI_Présentation_Debut)
WDAPIFenetreInterne.fiListeAjoute(mWD_CFI_Presentation,GWDPMoobi_Cat.ms_Project.mWD_FI_Presentation_Debut);

// FIListeAjoute(CFI_Présentation,FI_Présentation_Milieu)
WDAPIFenetreInterne.fiListeAjoute(mWD_CFI_Presentation,GWDPMoobi_Cat.ms_Project.mWD_FI_Presentation_Milieu);

// FIListeAjoute(CFI_Présentation,FI_Présentation_Fin)
WDAPIFenetreInterne.fiListeAjoute(mWD_CFI_Presentation,GWDPMoobi_Cat.ms_Project.mWD_FI_Presentation_Fin);

// ACTB_ActionBar..couleur = noir
mWD_ACTB_ActionBar.setCouleur(0);

}




/**
 * Traitement: Fin d'initialisation de FEN_Présentation1
 */
public void init()
//  Rafraîchit l'affichage en fonction de la position dans la fenêtre interne
{
super.init();

// RafraîchitAffichagePosition(CFI_Présentation)
fWD_rafraichitAffichagePosition(mWD_CFI_Presentation);

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_Présentation1
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_GR_Bulles = new WDGroupe();
mWD_CFI_Presentation = new GWDCFI_Presentation();
mWD_IMG_Bulle_Base = new GWDIMG_Bulle_Base();
mWD_ACTB_ActionBar = new GWDACTB_ActionBar();
mWD__Menu = new GWDM_Menu();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_Présentation1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(2998639174701390155l);

super.setChecksum("1267795909");

super.setNom("FEN_Présentation1");

super.setType(1);

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xFFFFFF);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 480);

super.setTitre("Présentation");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 1, 0, 1);

super.setCouleurTexteAutomatique(0xF4000000);

super.setCouleurBarreSysteme(0xFF000001);


activerEcoute();


////////////////////////////////////////////////////////////////////////////
// Initialisation des groupes de champs de FEN_Présentation1
////////////////////////////////////////////////////////////////////////////
mWD_GR_Bulles.init("GR_Bulles");
ajouter("GR_Bulles", mWD_GR_Bulles);


////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Présentation1
////////////////////////////////////////////////////////////////////////////
mWD_CFI_Presentation.initialiserObjet();
super.ajouter("CFI_Présentation", mWD_CFI_Presentation);
mWD_IMG_Bulle_Base.initialiserObjet();
super.ajouter("IMG_Bulle_Base", mWD_IMG_Bulle_Base);
mWD_ACTB_ActionBar.initialiserObjet();
super.ajouterActionBar(mWD_ACTB_ActionBar);
mWD__Menu.initialiserObjet();
ajouterMenuPrincipal(mWD__Menu);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 2;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return true;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 2;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return true;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPMoobi_Cat.ms_Project.mWD_FEN_Presentation1;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "210 MATERIAL DESIGN BLUE GREY#WM";
}
}

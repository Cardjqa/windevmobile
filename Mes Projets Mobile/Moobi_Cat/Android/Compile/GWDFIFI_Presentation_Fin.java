/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FI_Présentation_Fin
 * Date : 25/08/2017 11:51:27
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.masociete.moobi_cat.wdgen;


import com.masociete.moobi_cat.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetreinterne.*;
import fr.pcsoft.wdjava.ui.champs.saisie.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.ui.champs.bouton.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.ui.champs.groupeoptions.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFIFI_Presentation_Fin extends WDFenetreInterne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FI_Présentation_Fin
////////////////////////////////////////////////////////////////////////////

/**
 * SAI_Email
 */
class GWDSAI_Email extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Présentation_Fin.FI_Présentation_Fin.SAI_Email
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectCompPrincipal(2,2,313,40);
super.setQuid(2998636339840583685l);

super.setChecksum("1080285042");

super.setNom("SAI_Email");

super.setType(20001);

super.setLibelle("Login");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(0, 102);

super.setTailleInitiale(315, 44);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(12, 400, 1000, 300, 1000);

super.setEllipse(0);

super.setIndication("Code expert");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(false);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Edt@dpi160.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Email mWD_SAI_Email;

/**
 * SAI_Mot_de_passe
 */
class GWDSAI_Mot_de_passe extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Présentation_Fin.FI_Présentation_Fin.SAI_Mot_de_passe
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectLibelle(0,2,2,40);
super.setRectCompPrincipal(2,2,313,40);
super.setQuid(2998636339840714757l);

super.setChecksum("1080416201");

super.setNom("SAI_Mot_de_passe");

super.setType(20001);

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(0, 154);

super.setTailleInitiale(315, 44);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(true);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(12, 400, 1000, 300, 1000);

super.setEllipse(0);

super.setIndication("Mot de passe");

super.setNumTab(2);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Edt@dpi160.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Mot_de_passe mWD_SAI_Mot_de_passe;

/**
 * BTN_Se_connecter
 */
class GWDBTN_Se_connecter extends WDBouton
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Présentation_Fin.FI_Présentation_Fin.BTN_Se_connecter
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2998636339840845845l);

super.setChecksum("1080548114");

super.setNom("BTN_Se_connecter");

super.setType(4);

super.setLibelle("Se connecter");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(0, 256);

super.setTailleInitiale(315, 48);

super.setPlan(0);

super.setImageEtat(1);

super.setImageFondEtat(5);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(12, 400, 1000, 300, 1000);

super.setNumTab(4);

super.setLettreAppel(65535);

super.setTypeBouton(0);

super.setTypeActionPredefinie(0);

super.setBoutonOnOff(false);

super.setTauxParallaxe(0, 0);

super.setLibelleVAlign(1);

super.setLibelleHAlign(5);

super.setPresenceLibelle(true);

super.setImage("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Pict_Save_16_5@dpi160.png", 0, 1, 1, null, null, null);

super.setStyleLibelleRepos(0xFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0x808080);

super.setStyleLibelleSurvol(0xFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0x808080);

super.setStyleLibelleEnfonce(0xFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0x808080);

super.setCadreRepos(WDCadreFactory.creerCadre_GEN(2, 0x859300, 0x51300, 0x859300, 4, 4, 1, 1));

super.setCadreSurvol(WDCadreFactory.creerCadre_GEN(2, 0x859300, 0x51300, 0xE8C6B0, 4, 4, 1, 1));

super.setCadreEnfonce(WDCadreFactory.creerCadre_GEN(2, 0x859300, 0x51300, 0xE2E2E2, 4, 4, 1, 1));

super.setImageFond9Images(new int[] {1,2,1,2,2,2,1,2,1}, 10, 10, 10, 10);

super.setImageFond("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Btn_Std@dpi160.png?E5_3NP_10_10_10_10", 1, 0, 1, 1);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur BTN_Se_connecter
 */
public void clicSurBoutonGauche()
//  vérification basique
{
super.clicSurBoutonGauche();

// si SAI_Email="" alors
if(mWD_SAI_Email.opEgal(""))
{
// 	ToastAffiche("Veuillez saisir un code expert")
WDAPIToast.toastAffiche("Veuillez saisir un code expert");

// 	<COMPILE SI Configuration="Application Android">
{
// 		DonneFocus(SAI_Email)
WDAPIDivers.donneFocus(mWD_SAI_Email);

}

// 	retour
return;

}

// si SAI_Mot_de_passe = "" alors
if(mWD_SAI_Mot_de_passe.opEgal(""))
{
// 	ToastAffiche("Veuillez saisir un mot de passe")
WDAPIToast.toastAffiche("Veuillez saisir un mot de passe");

// 	<compile si Configuration="Application Android">
{
// 		DonneFocus(SAI_Mot_de_passe)
WDAPIDivers.donneFocus(mWD_SAI_Mot_de_passe);

}

// 	RETOUR
return;

}

// Login()
GWDCPCOL_ProceduresGlobales_MoobiCat.fWD_login();

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDBTN_Se_connecter mWD_BTN_Se_connecter;

/**
 * LIB_Identification
 */
class GWDLIB_Identification extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FI_Présentation_Fin.FI_Présentation_Fin.LIB_Identification
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2998636339840976917l);

super.setChecksum("1080678730");

super.setNom("LIB_Identification");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Identification");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(0, 72);

super.setTailleInitiale(315, 22);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(12, 400, 1000, 300, 1000);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Identification mWD_LIB_Identification;

/**
 * INT_Interrupteur
 */
class GWDINT_Interrupteur extends WDInterrupteur
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FI_Présentation_Fin.FI_Présentation_Fin.INT_Interrupteur
////////////////////////////////////////////////////////////////////////////

/**
 * INT_Interrupteur_Option_0
 */
class GWDINT_Interrupteur_Option_0 extends WDCaseACocher
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Présentation_Fin.FI_Présentation_Fin.INT_Interrupteur.INT_Interrupteur_Option_0
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setLibelle("Se souvenir de moi");

super.setHauteurOption(0);

super.setStyleLibelleOption(0x212121, creerPolice_GEN("Roboto", -8.000000, 0));

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDINT_Interrupteur_Option_0 mWD_INT_Interrupteur_Option_0 = new GWDINT_Interrupteur_Option_0();
/**
 * Initialise tous les champs de FI_Présentation_Fin.FI_Présentation_Fin.INT_Interrupteur
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Présentation_Fin.FI_Présentation_Fin.INT_Interrupteur
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
super.ajouterOption(mWD_INT_Interrupteur_Option_0);
positionnerOptions();
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectCompPrincipal(0,0,315,42);
super.setQuid(2998636339841173525l);

super.setChecksum("1080876250");

super.setNom("INT_Interrupteur");

super.setType(5);

super.setLibelle("&Interrupteur");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(0, 206);

super.setTailleInitiale(315, 42);

super.setValeurInitiale("0");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(12, 400, 1000, 300, 1000);

super.setNumTab(3);

super.setLettreAppel(65535);

super.setPersistant(false);

super.setParamOptions(false, 1, true, true, false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x212121, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xF1000000, 0xF3000000, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 4, 4, 1, 1));


super.setImageCoche("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_CBox@dpi160.png?E12_Radio", 1);

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDINT_Interrupteur mWD_INT_Interrupteur;

/**
 * Traitement: Déclarations globales de FI_Présentation_Fin
 */
//  Cette fenêtre est un exemple d'interface personnalisable.
//  Vous pouvez la modifier selon vos besoins.
//  Les traitements à personnaliser sont identifiés par des "// A faire" que
//  vous pouvez retrouver en utilisant la recherche ou le volet "Liste des tâches"
//  Cette fenêtre interne est a utiliser dans la fenêtre Présentation
//  comme dernière fenêtre du défilement de la présentation de l'application
public void declarerGlobale(WDObjet[] WD_tabParam)
{
super.declarerGlobale(WD_tabParam);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FI_Présentation_Fin
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_SAI_Email = new GWDSAI_Email();
mWD_SAI_Mot_de_passe = new GWDSAI_Mot_de_passe();
mWD_BTN_Se_connecter = new GWDBTN_Se_connecter();
mWD_LIB_Identification = new GWDLIB_Identification();
mWD_INT_Interrupteur = new GWDINT_Interrupteur();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FI_Présentation_Fin
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setQuid(2998630605706831783l);

super.setChecksum("727885645");

super.setNom("FI_Présentation_Fin");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTailleInitiale(320, 480);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setCouleurFond(0xFFFFFFFF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Présentation_Fin
////////////////////////////////////////////////////////////////////////////
mWD_SAI_Email.initialiserObjet();
super.ajouter("SAI_Email", mWD_SAI_Email);
mWD_SAI_Mot_de_passe.initialiserObjet();
super.ajouter("SAI_Mot_de_passe", mWD_SAI_Mot_de_passe);
mWD_BTN_Se_connecter.initialiserObjet();
super.ajouter("BTN_Se_connecter", mWD_BTN_Se_connecter);
mWD_LIB_Identification.initialiserObjet();
super.ajouter("LIB_Identification", mWD_LIB_Identification);
mWD_INT_Interrupteur.initialiserObjet();
super.ajouter("INT_Interrupteur", mWD_INT_Interrupteur);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
}

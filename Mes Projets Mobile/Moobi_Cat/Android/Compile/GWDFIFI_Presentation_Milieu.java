/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FI_Présentation_Milieu
 * Date : 25/08/2017 11:51:27
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.masociete.moobi_cat.wdgen;


import com.masociete.moobi_cat.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetreinterne.*;
import fr.pcsoft.wdjava.ui.champs.zml.*;
import fr.pcsoft.wdjava.ui.champs.saisie.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.ui.champs.groupeoptions.*;
import fr.pcsoft.wdjava.core.context.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFIFI_Presentation_Milieu extends WDFenetreInterne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FI_Présentation_Milieu
////////////////////////////////////////////////////////////////////////////

/**
 * ZM_Paramètres_1
 */
class GWDZM_Parametres_1 extends WDZoneMultiligne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////

/**
 * ZM_Paramètres_1_Ligne1
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////

/**
 * SAI_Code_expert
 */
class GWDSAI_Code_expert extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1.SAI_Code_expert
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectLibelle(0,2,123,29);
super.setRectCompPrincipal(123,2,181,29);
super.setQuid(2998634698828395056l);

super.setChecksum("745603103");

super.setNom("SAI_Code_expert");

super.setType(20001);

super.setLibelle("Code expert");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(9, 8);

super.setTailleInitiale(304, 33);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Edt@dpi160.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de SAI_Code_expert
 */
public void init()
{
super.init();

// ChargeParamètre("1")
WDAPIPersist.chargeParametre("1");

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Code_expert mWD_SAI_Code_expert = new GWDSAI_Code_expert();
class GWDZM_Parametres_1_Ligne1 extends LigneZMLStatique
{
/**
 * Initialise tous les champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_SAI_Code_expert.initialiserObjet();
super.ajouterChamp(mWD_SAI_Code_expert);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(86);

super.setVisibleInitial(true);

super.setModeSelection(1);

initialiserSousObjets();
super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_1_Ligne1 mWD_ZM_Parametres_1_Ligne1 = new GWDZM_Parametres_1_Ligne1();

/**
 * ZM_Paramètres_1_Ligne2
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////

/**
 * SAI_Numéro_expert
 */
class GWDSAI_Numero_expert extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1.SAI_Numéro_expert
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectLibelle(0,2,123,29);
super.setRectCompPrincipal(123,2,181,29);
super.setQuid(2998634698828526128l);

super.setChecksum("745734175");

super.setNom("SAI_Numéro_expert");

super.setType(20001);

super.setLibelle("Numéro expert");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(9, -26);

super.setTailleInitiale(304, 33);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Edt@dpi160.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de SAI_Numéro_expert
 */
public void init()
{
super.init();

// ChargeParamètre("2")
WDAPIPersist.chargeParametre("2");

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Numero_expert mWD_SAI_Numero_expert = new GWDSAI_Numero_expert();

/**
 * SAI_Email_expert
 */
class GWDSAI_Email_expert extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1.SAI_Email_expert
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectLibelle(0,2,123,29);
super.setRectCompPrincipal(123,2,181,29);
super.setQuid(2998634698828591664l);

super.setChecksum("745799711");

super.setNom("SAI_Email_expert");

super.setType(20001);

super.setLibelle("Email expert");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(9, 26);

super.setTailleInitiale(304, 33);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(2);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Edt@dpi160.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de SAI_Email_expert
 */
public void init()
{
super.init();

// ChargeParamètre("3")
WDAPIPersist.chargeParametre("3");

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Email_expert mWD_SAI_Email_expert = new GWDSAI_Email_expert();

/**
 * LIB_Synchronisation_des_rendez_vous
 */
class GWDLIB_Synchronisation_des_rendez_vous extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1.LIB_Synchronisation_des_rendez_vous
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2998634698828722736l);

super.setChecksum("745931239");

super.setNom("LIB_Synchronisation_des_rendez_vous");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Synchronisation des rendez-vous");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(9, 67);

super.setTailleInitiale(257, 28);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Synchronisation_des_rendez_vous mWD_LIB_Synchronisation_des_rendez_vous = new GWDLIB_Synchronisation_des_rendez_vous();

/**
 * INT_Option_1
 */
class GWDINT_Option_1 extends WDInterrupteur
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1.INT_Option_1
////////////////////////////////////////////////////////////////////////////

/**
 * INT_Option_1_Option_0
 */
class GWDINT_Option_1_Option_0 extends WDCaseACocher
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1.INT_Option_1.INT_Option_1_Option_0
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setLibelle("");

super.setHauteurOption(0);

super.setStyleLibelleOption(0x212121, creerPolice_GEN("Roboto", -8.000000, 0));

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDINT_Option_1_Option_0 mWD_INT_Option_1_Option_0 = new GWDINT_Option_1_Option_0();
/**
 * Initialise tous les champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1.INT_Option_1
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1.INT_Option_1
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
super.ajouterOption(mWD_INT_Option_1_Option_0);
positionnerOptions();
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectCompPrincipal(0,0,42,33);
super.setQuid(2998634698828788272l);

super.setChecksum("745997687");

super.setNom("INT_Option_1");

super.setType(5);

super.setLibelle("Interrupteur");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(271, 67);

super.setTailleInitiale(42, 33);

super.setValeurInitiale("0");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(4, 1000, 1000, 1000, 1000);

super.setNumTab(3);

super.setLettreAppel(65535);

super.setPersistant(false);

super.setParamOptions(false, 1, true, true, false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x212121, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xF1000000, 0xF3000000, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 4, 4, 1, 1));


super.setImageCoche("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_CBox@dpi160.png?E12_Radio", 1);

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDINT_Option_1 mWD_INT_Option_1 = new GWDINT_Option_1();
class GWDZM_Parametres_1_Ligne2 extends LigneZMLStatique
{
/**
 * Initialise tous les champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_SAI_Numero_expert.initialiserObjet();
super.ajouterChamp(mWD_SAI_Numero_expert);
mWD_SAI_Email_expert.initialiserObjet();
super.ajouterChamp(mWD_SAI_Email_expert);
mWD_LIB_Synchronisation_des_rendez_vous.initialiserObjet();
super.ajouterChamp(mWD_LIB_Synchronisation_des_rendez_vous);
mWD_INT_Option_1.initialiserObjet();
super.ajouterChamp(mWD_INT_Option_1);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(86);

super.setVisibleInitial(true);

super.setModeSelection(1);

initialiserSousObjets();
super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_1_Ligne2 mWD_ZM_Parametres_1_Ligne2 = new GWDZM_Parametres_1_Ligne2();

/**
 * ZM_Paramètres_1_Ligne3
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////
class GWDZM_Parametres_1_Ligne3 extends LigneZMLStatique
{
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(21);

super.setVisibleInitial(true);

super.setModeSelection(1);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_1_Ligne3 mWD_ZM_Parametres_1_Ligne3 = new GWDZM_Parametres_1_Ligne3();
/**
 * Initialise tous les champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_1
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_ZM_Parametres_1_Ligne1.initialiserObjet();
super.ajouterLigne(mWD_ZM_Parametres_1_Ligne1);
mWD_ZM_Parametres_1_Ligne2.initialiserObjet();
super.ajouterLigne(mWD_ZM_Parametres_1_Ligne2);
mWD_ZM_Parametres_1_Ligne3.initialiserObjet();
super.ajouterLigne(mWD_ZM_Parametres_1_Ligne3);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setPresenceLibelle(false);

super.setQuid(2998634698828263984l);

super.setChecksum("745515351");

super.setNom("ZM_Paramètres_1");

super.setType(97);

super.setLibelle("Zone multiligne");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(1, 72);

super.setTailleInitiale(319, 194);

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setNumTab(1);

super.setAltitude(1);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000);

super.setIndiceModeleLigneDynamique(0);

super.setTauxParallaxe(0, 0);

super.setCouleurTexteAutomatique(0xFF000001);

super.setStyleLibelle(0x212121, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x808080);

super.setCouleurSeparateur(0xFFFFFFFF);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Bg_Sheet@dpi160.png?_3NP_10_10_10_10", new int[] {1,2,1,2,2,2,1,2,1}, new int[] {10, 10, 10, 10}, 0xFFFFFFFF, 0, 1));

super.setStyleSelection(WDCadreFactory.creerCadre_GEN("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Bg_Sheet_Select@dpi160.png?_3NP_10_10_10_10", new int[] {1,2,1,2,2,2,1,2,1}, new int[] {10, 10, 10, 10}, 0xFFFFFFFF, 0, 1));

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

/**
 * Traitement: Sélection (clic) d'une ligne dans ZM_Paramètres_1
 */
public void selectionLigne()
//  change l'état de l'interrupteur selon le clic sur la ligne
{
super.selectionLigne();

// si moimeme=3 alors 
if(WDContexte.getMoiMeme().opEgal(3))
{
// 	INT_Option_1=pas INT_Option_1
mWD_INT_Option_1.setValeur((!mWD_INT_Option_1.getBoolean()));

// 	ExécuteTraitement(INT_Option_1,trtModification)
WDAPIVM.executeTraitement(mWD_INT_Option_1,17);

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurSelection();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_1 mWD_ZM_Parametres_1;

/**
 * ZM_Paramètres_2
 */
class GWDZM_Parametres_2 extends WDZoneMultiligne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
////////////////////////////////////////////////////////////////////////////

/**
 * ZM_Paramètres_2_Ligne1
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
////////////////////////////////////////////////////////////////////////////

/**
 * SAI_Adresse_Ip
 */
class GWDSAI_Adresse_Ip extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2.SAI_Adresse_Ip
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectLibelle(0,2,123,29);
super.setRectCompPrincipal(123,2,181,29);
super.setQuid(2998634698829705791l);

super.setChecksum("746913838");

super.setNom("SAI_Adresse_Ip");

super.setType(20001);

super.setLibelle("Adresse Ip");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(8, 8);

super.setTailleInitiale(304, 33);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Edt@dpi160.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de SAI_Adresse_Ip
 */
public void init()
{
super.init();

// SAI_Adresse_Ip = ChargeParamètre("IpExt")
this.setValeur(WDAPIPersist.chargeParametre("IpExt"));

}




/**
 * Traitement: Entrée dans SAI_Adresse_Ip
 */
public void entreeChamp()
{
super.entreeChamp();

// SAI_Adresse_Ip = ""
this.setValeur("");

}




/**
 * Traitement: Sortie de SAI_Adresse_Ip
 */
public void sortieChamp()
{
super.sortieChamp();

// SauveParamètre("IpExt",SAI_Adresse_Ip)
WDAPIPersist.sauveParametre("IpExt",this.getString());

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurFocus();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Adresse_Ip mWD_SAI_Adresse_Ip = new GWDSAI_Adresse_Ip();
class GWDZM_Parametres_2_Ligne1 extends LigneZMLStatique
{
/**
 * Initialise tous les champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_SAI_Adresse_Ip.initialiserObjet();
super.ajouterChamp(mWD_SAI_Adresse_Ip);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(43);

super.setVisibleInitial(true);

super.setModeSelection(0);

initialiserSousObjets();
super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_2_Ligne1 mWD_ZM_Parametres_2_Ligne1 = new GWDZM_Parametres_2_Ligne1();

/**
 * ZM_Paramètres_2_Ligne2
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
////////////////////////////////////////////////////////////////////////////

/**
 * SAI_Adresse_Ip_wifi
 */
class GWDSAI_Adresse_Ip_wifi extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2.SAI_Adresse_Ip_wifi
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectLibelle(0,2,123,29);
super.setRectCompPrincipal(123,2,181,29);
super.setQuid(2998634698829836863l);

super.setChecksum("747044910");

super.setNom("SAI_Adresse_Ip_wifi");

super.setType(20001);

super.setLibelle("Adresse Ip wifi");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(8, 7);

super.setTailleInitiale(304, 33);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Edt@dpi160.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de SAI_Adresse_Ip_wifi
 */
public void init()
{
super.init();

// SAI_Adresse_Ip_wifi = ChargeParamètre("IpWifi")
this.setValeur(WDAPIPersist.chargeParametre("IpWifi"));

}




/**
 * Traitement: Entrée dans SAI_Adresse_Ip_wifi
 */
public void entreeChamp()
{
super.entreeChamp();

// SAI_Adresse_Ip_wifi = ""
this.setValeur("");

}




/**
 * Traitement: Sortie de SAI_Adresse_Ip_wifi
 */
public void sortieChamp()
{
super.sortieChamp();

// SauveParamètre("IpWifi",SAI_Adresse_Ip_wifi)
WDAPIPersist.sauveParametre("IpWifi",this.getString());

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurFocus();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Adresse_Ip_wifi mWD_SAI_Adresse_Ip_wifi = new GWDSAI_Adresse_Ip_wifi();
class GWDZM_Parametres_2_Ligne2 extends LigneZMLStatique
{
/**
 * Initialise tous les champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_SAI_Adresse_Ip_wifi.initialiserObjet();
super.ajouterChamp(mWD_SAI_Adresse_Ip_wifi);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(46);

super.setVisibleInitial(true);

super.setModeSelection(1);

initialiserSousObjets();
super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_2_Ligne2 mWD_ZM_Parametres_2_Ligne2 = new GWDZM_Parametres_2_Ligne2();

/**
 * ZM_Paramètres_2_Ligne3
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
////////////////////////////////////////////////////////////////////////////

/**
 * INT_Option_2
 */
class GWDINT_Option_2 extends WDInterrupteur
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2.INT_Option_2
////////////////////////////////////////////////////////////////////////////

/**
 * INT_Option_2_Option_0
 */
class GWDINT_Option_2_Option_0 extends WDCaseACocher
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2.INT_Option_2.INT_Option_2_Option_0
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setLibelle("");

super.setHauteurOption(0);

super.setStyleLibelleOption(0x212121, creerPolice_GEN("Roboto", -8.000000, 0));

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDINT_Option_2_Option_0 mWD_INT_Option_2_Option_0 = new GWDINT_Option_2_Option_0();
/**
 * Initialise tous les champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2.INT_Option_2
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2.INT_Option_2
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
super.ajouterOption(mWD_INT_Option_2_Option_0);
positionnerOptions();
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setRectCompPrincipal(0,0,42,33);
super.setQuid(2998641730216111600l);

super.setChecksum("1271859100");

super.setNom("INT_Option_2");

super.setType(5);

super.setLibelle("Interrupteur");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(269, 8);

super.setTailleInitiale(42, 33);

super.setValeurInitiale("0");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(4, 1000, 1000, 1000, 1000);

super.setNumTab(1);

super.setLettreAppel(65535);

super.setPersistant(false);

super.setParamOptions(false, 1, true, true, false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x212121, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xF1000000, 0xF3000000, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 4, 4, 1, 1));


super.setImageCoche("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_CBox@dpi160.png?E12_Radio", 1);

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDINT_Option_2 mWD_INT_Option_2 = new GWDINT_Option_2();

/**
 * LIB_Utiliser_le_GPS_pour
 */
class GWDLIB_Utiliser_le_GPS_pour extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2.LIB_Utiliser_le_GPS_pour
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(2998641730215914976l);

super.setChecksum("1271661564");

super.setNom("LIB_Utiliser_le_GPS_pour");

super.setType(3);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Utiliser le GPS pour plus de précision");

super.setNote("", "");

super.setEtatInitial(0);

super.setPositionInitiale(8, 8);

super.setTailleInitiale(257, 20);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x8E8E8F, 0xFFFFFFFF, creerPolice_GEN("Roboto", -7.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Utiliser_le_GPS_pour mWD_LIB_Utiliser_le_GPS_pour = new GWDLIB_Utiliser_le_GPS_pour();
class GWDZM_Parametres_2_Ligne3 extends LigneZMLStatique
{
/**
 * Initialise tous les champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_INT_Option_2.initialiserObjet();
super.ajouterChamp(mWD_INT_Option_2);
mWD_LIB_Utiliser_le_GPS_pour.initialiserObjet();
super.ajouterChamp(mWD_LIB_Utiliser_le_GPS_pour);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(46);

super.setVisibleInitial(true);

super.setModeSelection(1);

initialiserSousObjets();
super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_2_Ligne3 mWD_ZM_Parametres_2_Ligne3 = new GWDZM_Parametres_2_Ligne3();

/**
 * ZM_Paramètres_2_Ligne4
 */

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
////////////////////////////////////////////////////////////////////////////
class GWDZM_Parametres_2_Ligne4 extends LigneZMLStatique
{
public  void initialiserObjet()
{
super.initialiserObjet();
super.setHauteurInitiale(8);

super.setVisibleInitial(true);

super.setModeSelection(-1);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_2_Ligne4 mWD_ZM_Parametres_2_Ligne4 = new GWDZM_Parametres_2_Ligne4();
/**
 * Initialise tous les champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Présentation_Milieu.FI_Présentation_Milieu.ZM_Paramètres_2
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_ZM_Parametres_2_Ligne1.initialiserObjet();
super.ajouterLigne(mWD_ZM_Parametres_2_Ligne1);
mWD_ZM_Parametres_2_Ligne2.initialiserObjet();
super.ajouterLigne(mWD_ZM_Parametres_2_Ligne2);
mWD_ZM_Parametres_2_Ligne3.initialiserObjet();
super.ajouterLigne(mWD_ZM_Parametres_2_Ligne3);
mWD_ZM_Parametres_2_Ligne4.initialiserObjet();
super.ajouterLigne(mWD_ZM_Parametres_2_Ligne4);
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setPresenceLibelle(false);

super.setQuid(2998634698829509183l);

super.setChecksum("746760550");

super.setNom("ZM_Paramètres_2");

super.setType(97);

super.setLibelle("Zone multiligne");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(1, 274);

super.setTailleInitiale(319, 144);

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setNumTab(2);

super.setAltitude(2);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000);

super.setIndiceModeleLigneDynamique(0);

super.setTauxParallaxe(0, 0);

super.setCouleurTexteAutomatique(0xFF000001);

super.setStyleLibelle(0x212121, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x808080);

super.setCouleurSeparateur(0xFFFFFFFF);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 4, 4, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Bg_Sheet@dpi160.png?_3NP_10_10_10_10", new int[] {1,2,1,2,2,2,1,2,1}, new int[] {10, 10, 10, 10}, 0xFFFFFFFF, 0, 1));

super.setStyleSelection(WDCadreFactory.creerCadre_GEN("C:\\Mes Projets Mobile\\Moobi_Cat\\Gabarits\\WM\\210 Material Design Blue Grey\\Material Design Blue Grey_Bg_Sheet_Select@dpi160.png?_3NP_10_10_10_10", new int[] {1,2,1,2,2,2,1,2,1}, new int[] {10, 10, 10, 10}, 0xFFFFFFFF, 0, 1));

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

/**
 * Traitement: Sélection (clic) d'une ligne dans ZM_Paramètres_2
 */
public void selectionLigne()
//  change l'état de l'interrupteur selon le clic sur la ligne
{
super.selectionLigne();

// si moimeme=2 alors 
if(WDContexte.getMoiMeme().opEgal(2))
{
// 	INT_Option_1=pas INT_Option_1
mWD_ZM_Parametres_1.mWD_INT_Option_1.setValeur((!mWD_ZM_Parametres_1.mWD_INT_Option_1.getBoolean()));

// 	ExécuteTraitement(INT_Option_1,trtModification)
WDAPIVM.executeTraitement(mWD_ZM_Parametres_1.mWD_INT_Option_1,17);

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurSelection();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZM_Parametres_2 mWD_ZM_Parametres_2;

/**
 * Traitement: Déclarations globales de FI_Présentation_Milieu
 */
//  Cette fenêtre est un exemple d'interface personnalisable.
//  Vous pouvez la modifier selon vos besoins.
//  Les traitements à personnaliser sont identifiés par des "// A faire" que
//  vous pouvez retrouver en utilisant la recherche ou le volet "Liste des tâches"
//  Cette fenêtre interne est a utiliser dans la fenêtre Présentation
//  au milieu du défilement de la présentation de l'application
public void declarerGlobale(WDObjet[] WD_tabParam)
{
super.declarerGlobale(WD_tabParam);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FI_Présentation_Milieu
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_ZM_Parametres_1 = new GWDZM_Parametres_1();
mWD_ZM_Parametres_2 = new GWDZM_Parametres_2();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FI_Présentation_Milieu
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setQuid(2998630601410356799l);

super.setChecksum("726377956");

super.setNom("FI_Présentation_Milieu");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTailleInitiale(320, 480);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setCouleurFond(0xFFFFFF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Présentation_Milieu
////////////////////////////////////////////////////////////////////////////
mWD_ZM_Parametres_1.initialiserObjet();
super.ajouter("ZM_Paramètres_1", mWD_ZM_Parametres_1);
mWD_ZM_Parametres_2.initialiserObjet();
super.ajouter("ZM_Paramètres_2", mWD_ZM_Parametres_2);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
}

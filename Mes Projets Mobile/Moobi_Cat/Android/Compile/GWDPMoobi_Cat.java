/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Projet
 * Classe Android : Moobi_Cat
 * Date : 25/08/2017 17:43:02
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.masociete.moobi_cat.wdgen;


import com.masociete.moobi_cat.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.core.context.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/





public class GWDPMoobi_Cat extends WDProjet
{
/**
 * Accès au projet: Moobi_Cat
 * Pour accéder au projet à partir de n'importe où: 
 * GWDPMoobi_Cat.ms_Project
 */
public static GWDPMoobi_Cat ms_Project;

 // FEN_Présentation1
public GWDFFEN_Presentation1 mWD_FEN_Presentation1 = new GWDFFEN_Presentation1();
 // accesseur de FEN_Présentation1
public GWDFFEN_Presentation1 getFEN_Presentation1()
{
mWD_FEN_Presentation1.verifierOuverte();
return mWD_FEN_Presentation1;
}


 // FI_Présentation_Debut
public GWDFIFI_Presentation_Debut mWD_FI_Presentation_Debut = new GWDFIFI_Presentation_Debut();
 // accesseur de FI_Présentation_Debut
public GWDFIFI_Presentation_Debut getFI_Presentation_Debut()
{
GWDFIFI_Presentation_Debut fiCtx = (GWDFIFI_Presentation_Debut)WDAppelContexte.getContexte().getFenetreInterne("FI_Présentation_Debut");
return fiCtx != null ? fiCtx  : mWD_FI_Presentation_Debut;
}

 // FI_Présentation_Milieu
public GWDFIFI_Presentation_Milieu mWD_FI_Presentation_Milieu = new GWDFIFI_Presentation_Milieu();
 // accesseur de FI_Présentation_Milieu
public GWDFIFI_Presentation_Milieu getFI_Presentation_Milieu()
{
GWDFIFI_Presentation_Milieu fiCtx = (GWDFIFI_Presentation_Milieu)WDAppelContexte.getContexte().getFenetreInterne("FI_Présentation_Milieu");
return fiCtx != null ? fiCtx  : mWD_FI_Presentation_Milieu;
}

 // FI_Présentation_Fin
public GWDFIFI_Presentation_Fin mWD_FI_Presentation_Fin = new GWDFIFI_Presentation_Fin();
 // accesseur de FI_Présentation_Fin
public GWDFIFI_Presentation_Fin getFI_Presentation_Fin()
{
GWDFIFI_Presentation_Fin fiCtx = (GWDFIFI_Presentation_Fin)WDAppelContexte.getContexte().getFenetreInterne("FI_Présentation_Fin");
return fiCtx != null ? fiCtx  : mWD_FI_Presentation_Fin;
}

 // Constructeur de la classe GWDPMoobi_Cat
public GWDPMoobi_Cat()
{
ajouterFenetre("FEN_Présentation1", mWD_FEN_Presentation1);
ajouterFenetreInterne("FI_Présentation_Debut");
ajouterFenetreInterne("FI_Présentation_Milieu");
ajouterFenetreInterne("FI_Présentation_Fin");

}


////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
static
{
// Allocation de l'objet global
GWDPMoobi_Cat.ms_Project = new GWDPMoobi_Cat();

// Définition des langues du projet
GWDPMoobi_Cat.ms_Project.setLangueProjet(new int[] {1}, new int[] {0}, 1, false);
GWDPMoobi_Cat.ms_Project.setNomCollectionProcedure(new String[]{"GWDCPCOL_ProceduresGlobales_MoobiCat"});
}
public String getVersionApplication(){ return "0.0.37.0";}
public String getNomSociete(){ return "PC SOFT";}
public String getNomAPK(){ return "Moobi_Cat";}
public int getIdNomApplication(){return com.masociete.moobi_cat.R.string.app_name;}
public boolean isModeAnsi(){ return false;}
public boolean isAffectationTableauParCopie(){ return true;}
public boolean isAssistanceAutoHFActive(){ return true;}
public String getPackageRacine(){ return "com.masociete.moobi_cat";}
public int getIdIconeApplication(){ return com.masociete.moobi_cat.R.drawable.i_c_o_n_e________0;}
public int getInfoPlateforme(EWDInfoPlateforme info)
{
switch(info)
{
case DPI_ECRAN : return 160;
case HAUTEUR_BARRE_SYSTEME : return 24;
case HAUTEUR_BARRE_TITRE : return 48;
case HAUTEUR_ACTION_BAR : return 48;
case HAUTEUR_BARRE_BAS : return 0;
case HAUTEUR_ECRAN : return 480;
case LARGEUR_ECRAN : return 320;
default : return 0;
}
}
public boolean isActiveThemeMaterialDesign()
{
return true;
}
////////////////////////////////////////////////////////////////////////////
public String getAdresseEmail() 
{
return "JQA@CARD.FR";
}
public boolean isIgnoreErreurCertificatHTTPS()
{
return false;
}
////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
protected void declarerRessources()
{
super.ajouterFichierAssocie("C:\\MES PROJETS MOBILE\\MOOBI_CAT\\GABARITS\\WM\\210 MATERIAL DESIGN BLUE GREY\\MATERIAL DESIGN BLUE GREY_BG_SHEET_SELECT.PNG?_3NP_10_10_10_10",com.masociete.moobi_cat.R.drawable.material_design_blue_grey_bg_sheet_select_8_np3_10_10_10_10, "");
super.ajouterFichierAssocie("C:\\MES PROJETS MOBILE\\MOOBI_CAT\\GABARITS\\WM\\210 MATERIAL DESIGN BLUE GREY\\MATERIAL DESIGN BLUE GREY_BG_SHEET.PNG?_3NP_10_10_10_10",com.masociete.moobi_cat.R.drawable.material_design_blue_grey_bg_sheet_7_np3_10_10_10_10, "");
super.ajouterFichierAssocie("C:\\MES PROJETS MOBILE\\MOOBI_CAT\\GABARITS\\WM\\210 MATERIAL DESIGN BLUE GREY\\MATERIAL DESIGN BLUE GREY_PICT_SAVE_16_5.PNG",com.masociete.moobi_cat.R.drawable.material_design_blue_grey_pict_save_16_5_6, "");
super.ajouterFichierAssocie("C:\\MES PROJETS MOBILE\\MOOBI_CAT\\GABARITS\\WM\\210 MATERIAL DESIGN BLUE GREY\\MATERIAL DESIGN BLUE GREY_EDT.PNG?E5_3NP_8_8_8_8",com.masociete.moobi_cat.R.drawable.material_design_blue_grey_edt_5_np3_8_8_8_8_selector, "");
super.ajouterFichierAssocie("C:\\MES PROJETS MOBILE\\MOOBI_CAT\\GABARITS\\WM\\210 MATERIAL DESIGN BLUE GREY\\MATERIAL DESIGN BLUE GREY_CBOX.PNG?E12_Radio",com.masociete.moobi_cat.R.drawable.material_design_blue_grey_cbox_4_selector, "");
super.ajouterFichierAssocie("C:\\MES PROJETS MOBILE\\MOOBI_CAT\\GABARITS\\WM\\210 MATERIAL DESIGN BLUE GREY\\MATERIAL DESIGN BLUE GREY_BTN_STD.PNG?E5_3NP_10_10_10_10",com.masociete.moobi_cat.R.drawable.material_design_blue_grey_btn_std_3_np3_10_10_10_10_selector, "");
super.ajouterFichierAssocie("C:\\MES PROJETS MOBILE\\MOOBI_CAT\\BCKGRD-PRESENTATION-3.JPG",com.masociete.moobi_cat.R.drawable.bckgrd_presentation_3_2, "");
}

////////////////////////////////////////////////////////////////////////////
// Formats des masques du projet
////////////////////////////////////////////////////////////////////////////
// Initialisation des collections de procédures
public void initCollections()
{
GWDCPCOL_ProceduresGlobales_MoobiCat.init();

}


/**
 * Appel des méthodes d'initialisation des classes / collections de procédures / projet
 */
static void GWDPMoobi_Cat_InitProjet( String [] args)
{
GWDPMoobi_Cat.ms_Project.initialiserProjet("Moobi_Cat", "Application Android", args);
}

/**
 * Appel des méthodes de terminaison des projet / collections de procédures / classes
 */
static protected void GWDPMoobi_Cat_TermineProjet()
{

// Terminaison des collections de procédures et des classes

// Libération de l'objet global
GWDPMoobi_Cat.ms_Project = null;
}

/**
 * Lancer de l'application Android
 */
public static class WDLanceur extends WDAbstractLanceur
{
public void init()
{
// Appel des méthodes d'initialisation
GWDPMoobi_Cat_InitProjet(null);
}
public void run()
{

GWDPMoobi_Cat.ms_Project.lancerProjet();
}
}
}

/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Splash Screen
 * Classe Android : Moobi_Cat
 * Date : 25/08/2017 17:43:02
 * Version de wdjava64.dll  : 22.0.104.0
 */


package com.masociete.moobi_cat.wdgen;


import com.masociete.moobi_cat.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.style.degrade.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class WDSplashScreenGen implements IWDSplashScreen
{
public int getIdImageFond(){return com.masociete.moobi_cat.R.drawable.s_p_l_a_s_c_r_e_e_n________1;}
public int getModeAffichageImageFond(){return 8;}
public int getIdNomApplication(){return com.masociete.moobi_cat.R.string.splashscreen_title;}
public int getIdInfoVersion(){return com.masociete.moobi_cat.R.string.splashscreen_version;}
public int getIdLogo(){return 0;}
public boolean isAvecAnimationChargement(){return true;}
public int getIdMessageChargement(){return com.masociete.moobi_cat.R.string.splashscreen_message;}
public int getCouleurLibelle(){return 0xFFFFFF;}
public IWDDegrade getFondDegrade(){return null;}
public int getCouleurFond(){return 0xFFFFFF;}
}

/* accueilardi */
#pragma pack(1)
typedef struct _stACCUEILARDI
{
	char	cabinet[20+1];	/*cabinet*/
	char	cabinettype[2+1];	/*cabinettype*/
	char	agence[20+1];	/*agence*/
	char	agencetype[2+1];	/*agencetype*/
	char	dossnum[20+1];	/*dossnum*/
	__int64	collabnum;	/*collabnum*/
	char	collabcode[10+1];	/*collabcode*/
	__int64	secretnum;	/*secretnum*/
	char	secretcode[10+1];	/*secretcode*/
	char	sinistrecode[38+1];	/*sinistrecode*/
	__int64	sinistreadminnum;	/*sinistreadminnum*/
	char	sinistre[38+1];	/*sinistre*/
	__int64	datesinistre;	/*datesinistre*/
	__int64	assureadminnum;	/*assureadminnum*/
	char	assure[38+1];	/*assure*/
	__int64	mandantadminnum;	/*mandantadminnum*/
	char	mandant[38+1];	/*mandant*/
	__int64	compagnieadminnum;	/*compagnieadminnum*/
	char	compagnie[38+1];	/*compagnie*/
	__int64	datecloture;	/*datecloture*/
	__int64	datemission;	/*datemission*/
	__int64	datereception;	/*datereception*/
	char	typereceptcode[10+1];	/*typereceptcode*/
	char	lese;	/*lese*/
	char	responsable;	/*responsable*/
	__int64	commentairesinistre;	/*commentairesinistre*/
	char	cbtcodelie[32+1];	/*cbtcodelie*/
	char	groupement[32+1];	/*groupement*/
	char	color[7+1];	/*color*/
} stACCUEILARDI, *pstACCUEILARDI;
#pragma pack()

/* accueilardi.PRIMARY */
#pragma pack(1)
typedef struct _st_ACCUEILARDI_PRIMARY
{
	char	cabinet[20];	/*cabinet*/
	char	cabinettype[2];	/*cabinettype*/
	char	agence[20];	/*agence*/
	char	agencetype[2];	/*agencetype*/
	char	dossnum[20];	/*dossnum*/
} st_ACCUEILARDI_PRIMARY, *pst_ACCUEILARDI_PRIMARY;
#pragma pack()

/* admin */
#pragma pack(1)
typedef struct _stADMIN
{
	__int64	oldpersnum;	/*oldpersnum*/
	char	code[38+1];	/*code*/
	__int64	adminnum;	/*adminnum*/
	__int64	civilitenum;	/*civilitenum*/
	char	codepostal[10+1];	/*codepostal*/
	char	ville[28+1];	/*ville*/
	char	payscode[4+1];	/*payscode*/
	char	nom[38+1];	/*nom*/
	char	prenom[38+1];	/*prenom*/
	char	surnom[38+1];	/*surnom*/
	char	adresse1[38+1];	/*adresse1*/
	char	adresse2[38+1];	/*adresse2*/
	char	adresse3[38+1];	/*adresse3*/
	char	commune[38+1];	/*commune*/
	char	email[60+1];	/*email*/
	char	web[60+1];	/*web*/
	__int64	admindatecree;	/*admindatecree*/
	__int64	admindatemaj;	/*admindatemaj*/
	__int64	admindatesuppr;	/*admindatesuppr*/
	__int64	datenom;	/*datenom*/
	__int64	datesurnom;	/*datesurnom*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stADMIN, *pstADMIN;
#pragma pack()

/* admin.FK_admincp */
#pragma pack(1)
typedef struct _st_ADMIN_FK_ADMINCP
{
	char	codepostal[10];	/*codepostal*/
	char	ville[28];	/*ville*/
} st_ADMIN_FK_ADMINCP, *pst_ADMIN_FK_ADMINCP;
#pragma pack()

/* adrcabinet */
#pragma pack(1)
typedef struct _stADRCABINET
{
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcodelie[20+1];	/*cbtcodelie*/
	char	cbttypelie[2+1];	/*cbttypelie*/
} stADRCABINET, *pstADRCABINET;
#pragma pack()

/* adrcabinet.PRIMARY */
#pragma pack(1)
typedef struct _st_ADRCABINET_PRIMARY
{
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcodelie[20];	/*cbtcodelie*/
	char	cbttypelie[2];	/*cbttypelie*/
} st_ADRCABINET_PRIMARY, *pst_ADRCABINET_PRIMARY;
#pragma pack()

/* adrcabinet.FK_adrcabinet2 */
#pragma pack(1)
typedef struct _st_ADRCABINET_FK_ADRCABINET2
{
	char	cbtcodelie[20];	/*cbtcodelie*/
	char	cbttypelie[2];	/*cbttypelie*/
} st_ADRCABINET_FK_ADRCABINET2, *pst_ADRCABINET_FK_ADRCABINET2;
#pragma pack()

/* adrdossier */
#pragma pack(1)
typedef struct _stADRDOSSIER
{
	__int64	adrdossnum;	/*adrdossnum*/
	__int64	adrdossnumtiers;	/*adrdossnumtiers*/
	char	cbtcode3[20+1];	/*cbtcode3*/
	char	cbttype3[2+1];	/*cbttype3*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	persnum;	/*persnum*/
	char	typeperscode[10+1];	/*typeperscode*/
	char	collabcode[10+1];	/*collabcode*/
	char	collabcode2[10+1];	/*collabcode2*/
	char	secretcode[10+1];	/*secretcode*/
	char	qualitecode[2+1];	/*qualitecode*/
	char	statutcode[2+1];	/*statutcode*/
	char	adrdossreference1[50+1];	/*adrdossreference1*/
	char	adrdossreference2[50+1];	/*adrdossreference2*/
	char	adrdossreference3[50+1];	/*adrdossreference3*/
	char	adrdossnumpolice[50+1];	/*adrdossnumpolice*/
	__int64	adrdosscommentaire;	/*adrdosscommentaire*/
	float	adrdossmttravaux;	/*adrdossmttravaux*/
	__int64	adrdossdatedebut;	/*adrdossdatedebut*/
	__int64	adrdossdatecree;	/*adrdossdatecree*/
	__int64	adrdossdatemaj;	/*adrdossdatemaj*/
	__int64	adrdossdatefin;	/*adrdossdatefin*/
	__int64	adrdossdatesuppr;	/*adrdossdatesuppr*/
	char	adrdosslibelle[100+1];	/*adrdosslibelle*/
	__int64	adrdossposition;	/*adrdossposition*/
	float	adrdossmtdevis;	/*adrdossmtdevis*/
	__int64	adrdossdatedevis;	/*adrdossdatedevis*/
	char	adrdossmiseencause;	/*adrdossmiseencause*/
	float	adrdossmtfranchise;	/*adrdossmtfranchise*/
	float	adrdossmtclause;	/*adrdossmtclause*/
	char	adrdosstypetiers[10+1];	/*adrdosstypetiers*/
	char	adrdosslese;	/*adrdosslese*/
	char	adrdossresponsable;	/*adrdossresponsable*/
	char	adrdossrar;	/*adrdossrar*/
	char	adrdossnumsinistre[50+1];	/*adrdossnumsinistre*/
	__int64	persnumsoustraite;	/*persnumsoustraite*/
} stADRDOSSIER, *pstADRDOSSIER;
#pragma pack()

/* adrdossier.FK_adrdossier */
#pragma pack(1)
typedef struct _st_ADRDOSSIER_FK_ADRDOSSIER
{
	char	cbtcode3[20];	/*cbtcode3*/
	char	cbttype3[2];	/*cbttype3*/
} st_ADRDOSSIER_FK_ADRDOSSIER, *pst_ADRDOSSIER_FK_ADRDOSSIER;
#pragma pack()

/* adrdossier.FK_adrdossier2 */
#pragma pack(1)
typedef struct _st_ADRDOSSIER_FK_ADRDOSSIER2
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_ADRDOSSIER_FK_ADRDOSSIER2, *pst_ADRDOSSIER_FK_ADRDOSSIER2;
#pragma pack()

/* adrpersonne */
#pragma pack(1)
typedef struct _stADRPERSONNE
{
	__int64	adrpersonne_id;	/*adrpersonne_id*/
	__int64	persnum;	/*persnum*/
	__int64	persnumlie;	/*persnumlie*/
	char	typeperscode[10+1];	/*typeperscode*/
	char	typeperscodelie[10+1];	/*typeperscodelie*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	char	adrpersreference[50+1];	/*adrpersreference*/
	__int64	datenom;	/*datenom*/
} stADRPERSONNE, *pstADRPERSONNE;
#pragma pack()

/* adrpersonne.PRIMARY */
#pragma pack(1)
typedef struct _st_ADRPERSONNE_PRIMARY
{
	__int64	adrpersonne_id;	/*adrpersonne_id*/
	__int64	persnum;	/*persnum*/
	__int64	persnumlie;	/*persnumlie*/
} st_ADRPERSONNE_PRIMARY, *pst_ADRPERSONNE_PRIMARY;
#pragma pack()

/* adrpersonne.FK_adrpersonne5 */
#pragma pack(1)
typedef struct _st_ADRPERSONNE_FK_ADRPERSONNE5
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_ADRPERSONNE_FK_ADRPERSONNE5, *pst_ADRPERSONNE_FK_ADRPERSONNE5;
#pragma pack()

/* annexe */
#pragma pack(1)
typedef struct _stANNEXE
{
	__int64	annexeligne;	/*annexeligne*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	char	annexetype[20+1];	/*annexetype*/
	char	annexecheminfichier[250+1];	/*annexecheminfichier*/
	char	annexenomfichier[50+1];	/*annexenomfichier*/
	char	annexelibellefichier[50+1];	/*annexelibellefichier*/
	__int64	annexedatecree;	/*annexedatecree*/
	__int64	annexedatemaj;	/*annexedatemaj*/
	__int64	annextedatesuppr;	/*annextedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stANNEXE, *pstANNEXE;
#pragma pack()

/* annexe.PRIMARY */
#pragma pack(1)
typedef struct _st_ANNEXE_PRIMARY
{
	__int64	annexeligne;	/*annexeligne*/
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_ANNEXE_PRIMARY, *pst_ANNEXE_PRIMARY;
#pragma pack()

/* annexe.FK_ajoutpiece */
#pragma pack(1)
typedef struct _st_ANNEXE_FK_AJOUTPIECE
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_ANNEXE_FK_AJOUTPIECE, *pst_ANNEXE_FK_AJOUTPIECE;
#pragma pack()

/* cabinet */
#pragma pack(1)
typedef struct _stCABINET
{
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	tvacode[10+1];	/*tvacode*/
	char	procode[32+1];	/*procode*/
	__int64	prochrono;	/*prochrono*/
	char	juridiquestatut[2+1];	/*juridiquestatut*/
	char	natinscription[1+1];	/*natinscription*/
	__int64	adminnum;	/*adminnum*/
	char	cbtemail[60+1];	/*cbtemail*/
	char	cbtemail2[60+1];	/*cbtemail2*/
	char	cbtweb[60+1];	/*cbtweb*/
	__int64	cbtcomment;	/*cbtcomment*/
	__int64	cbtdatecree;	/*cbtdatecree*/
	__int64	cbtdatemaj;	/*cbtdatemaj*/
	__int64	cbtdatesuppr;	/*cbtdatesuppr*/
	char	cbtchemincompta[80+1];	/*cbtchemincompta*/
	char	cbtirdwebom;	/*cbtirdwebom*/
	char	cbtirdwebnh;	/*cbtirdwebnh*/
	char	cbtirdwebre;	/*cbtirdwebre*/
	char	cbtirdwebsda;	/*cbtirdwebsda*/
	char	cbtirdwebsde;	/*cbtirdwebsde*/
	char	cbtirdwebsdr;	/*cbtirdwebsdr*/
	char	cbtirdwebah;	/*cbtirdwebah*/
	char	cbtirdwebdt;	/*cbtirdwebdt*/
	char	cbtirdwebpdf;	/*cbtirdwebpdf*/
	char	cbtirdwebcoderecup[60+1];	/*cbtirdwebcoderecup*/
	char	cbtirdwebmdprecup[60+1];	/*cbtirdwebmdprecup*/
	char	cbtirdwebcodepdf[60+1];	/*cbtirdwebcodepdf*/
	char	cbtirdwebmdppdf[60+1];	/*cbtirdwebmdppdf*/
	char	cbtirdwebcodemodif[60+1];	/*cbtirdwebcodemodif*/
	char	cbtirdwebmdpmodif[60+1];	/*cbtirdwebmdpmodif*/
	char	cbtirdwebcodeabonne[60+1];	/*cbtirdwebcodeabonne*/
	char	cbtcheminmultimedia[80+1];	/*cbtcheminmultimedia*/
	char	cbtcheminirdwebbase[80+1];	/*cbtcheminirdwebbase*/
	char	cbtcheminirdwebrecup[80+1];	/*cbtcheminirdwebrecup*/
	char	cbtcheminmodeldoc[80+1];	/*cbtcheminmodeldoc*/
	char	cbtchemindoctype[80+1];	/*cbtchemindoctype*/
	char	cbtcomptevente[15+1];	/*cbtcomptevente*/
	char	cbtcodeanalytique[20+1];	/*cbtcodeanalytique*/
	char	cbtjournalvente[2+1];	/*cbtjournalvente*/
	char	cbtnumfolio[3+1];	/*cbtnumfolio*/
	char	cbtplateforme;	/*cbtplateforme*/
	long	cbtordremaif;	/*cbtordremaif*/
	long	cbtordregenerali;	/*cbtordregenerali*/
	char	cbtcheminfichierfusion[80+1];	/*cbtcheminfichierfusion*/
	char	cbtcheminbase[80+1];	/*cbtcheminbase*/
	char	cbtcheminbaseinter[80+1];	/*cbtcheminbaseinter*/
	char	cbtchemincompactinter[80+1];	/*cbtchemincompactinter*/
	char	cbtcheminbasearchive[80+1];	/*cbtcheminbasearchive*/
	char	cbtchemindocarchive[80+1];	/*cbtchemindocarchive*/
	char	cbtcheminmultiarchive[80+1];	/*cbtcheminmultiarchive*/
	char	cbtcodeape[5+1];	/*cbtcodeape*/
	char	cbtnumsiret[17+1];	/*cbtnumsiret*/
	char	cbtrc[10+1];	/*cbtrc*/
	float	cbtmtcapital;	/*cbtmtcapital*/
	char	cbtnumintra[13+1];	/*cbtnumintra*/
	char	cbtlocgerant[1+1];	/*cbtlocgerant*/
	char	cbtcheminetat[80+1];	/*cbtcheminetat*/
	char	cbtirdwebnumcertif[14+1];	/*cbtirdwebnumcertif*/
	char	cbtcalculhono[8+1];	/*cbtcalculhono*/
	__int64	cbtdelairelance;	/*cbtdelairelance*/
	char	cbtarchivage[8+1];	/*cbtarchivage*/
	__int64	cbtdelaiarchivage;	/*cbtdelaiarchivage*/
	char	cbttypepointage[8+1];	/*cbttypepointage*/
	char	cbtversionbase[5+1];	/*cbtversionbase*/
	char	cbtnumparagence;	/*cbtnumparagence*/
	long	cbttypenumdossier;	/*cbttypenumdossier*/
	long	cbttypenumhono;	/*cbttypenumhono*/
	long	cbttypenumrgl;	/*cbttypenumrgl*/
	char	cbtdevise[10+1];	/*cbtdevise*/
	char	cbtcompourcentmt[1+1];	/*cbtcompourcentmt*/
	char	cbtcombasecalcul[2+1];	/*cbtcombasecalcul*/
	char	cbtcheminlogo[80+1];	/*cbtcheminlogo*/
	char	cbttypecompta[1+1];	/*cbttypecompta*/
	char	cbtnumcollectif[30+1];	/*cbtnumcollectif*/
	long	cbtrelance;	/*cbtrelance*/
	__int64	cbtdateenvoistat;	/*cbtdateenvoistat*/
	char	cbtcheminsysteme[80+1];	/*cbtcheminsysteme*/
	char	cbtirdwebcodehono[15+1];	/*cbtirdwebcodehono*/
	char	cbtirdwebcodefrais[15+1];	/*cbtirdwebcodefrais*/
	char	cbtstatemail[60+1];	/*cbtstatemail*/
	char	cbtstatcc[60+1];	/*cbtstatcc*/
	char	cbtirdweberreur[60+1];	/*cbtirdweberreur*/
	char	cbtregtel;	/*cbtregtel*/
	char	cbttabletpc;	/*cbttabletpc*/
	char	cbttypeafficheplanning[1+1];	/*cbttypeafficheplanning*/
	char	cbttypeaffichedoc[1+1];	/*cbttypeaffichedoc*/
	char	cbtentreprise;	/*cbtentreprise*/
	char	cbtchemintabletpc[80+1];	/*cbtchemintabletpc*/
	char	cbtcrenhauto;	/*cbtcrenhauto*/
	char	cbtdatenhdefaut[5+1];	/*cbtdatenhdefaut*/
	char	cbtsoumisetva;	/*cbtsoumisetva*/
	char	cbtrepatnh[3+1];	/*cbtrepatnh*/
	char	cbtcopieinter;	/*cbtcopieinter*/
	char	cbtcopiedoc;	/*cbtcopiedoc*/
	char	cbtpjinter;	/*cbtpjinter*/
	char	cbtpjdoc;	/*cbtpjdoc*/
	char	cbtreceptdossier;	/*cbtreceptdossier*/
	char	cbtajoutprof;	/*cbtajoutprof*/
	char	cbtajoutsecu;	/*cbtajoutsecu*/
	char	cbtajoutcp;	/*cbtajoutcp*/
	char	cbtajoutprejudice;	/*cbtajoutprejudice*/
	char	cbtajoutspecialite;	/*cbtajoutspecialite*/
	char	cbtajouttypepiece;	/*cbtajouttypepiece*/
	char	cbtajoutlieux;	/*cbtajoutlieux*/
	__int64	cbtnbepuretablet;	/*cbtnbepuretablet*/
	char	cbtenvoistatdelai;	/*cbtenvoistatdelai*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	char	cbtnumauto;	/*cbtnumauto*/
	__int64	cbtinfosecu;	/*cbtinfosecu*/
	char	groupement[38+1];	/*groupement*/
	char	cbtsmtplogin[50+1];	/*cbtsmtplogin*/
	char	cbtsmtppwd[50+1];	/*cbtsmtppwd*/
	char	cbtsmtphost[50+1];	/*cbtsmtphost*/
	char	cbtword;	/*cbtword*/
} stCABINET, *pstCABINET;
#pragma pack()

/* cabinet.PRIMARY */
#pragma pack(1)
typedef struct _st_CABINET_PRIMARY
{
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
} st_CABINET_PRIMARY, *pst_CABINET_PRIMARY;
#pragma pack()

/* cabinet.FK_chartecabinet */
#pragma pack(1)
typedef struct _st_CABINET_FK_CHARTECABINET
{
	char	procode[32];	/*procode*/
	__int64	prochrono;	/*prochrono*/
} st_CABINET_FK_CHARTECABINET, *pst_CABINET_FK_CHARTECABINET;
#pragma pack()

/* cbtcompetence */
#pragma pack(1)
typedef struct _stCBTCOMPETENCE
{
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	__int64	secteurnum;	/*secteurnum*/
	char	secteurcode[5+1];	/*secteurcode*/
	char	sinistrecode[38+1];	/*sinistrecode*/
	__int64	persnum;	/*persnum*/
	__int64	cbtcompetencenbdossier;	/*cbtcompetencenbdossier*/
} stCBTCOMPETENCE, *pstCBTCOMPETENCE;
#pragma pack()

/* cbtcompetence.PRIMARY */
#pragma pack(1)
typedef struct _st_CBTCOMPETENCE_PRIMARY
{
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	__int64	secteurnum;	/*secteurnum*/
	char	secteurcode[5];	/*secteurcode*/
	char	sinistrecode[38];	/*sinistrecode*/
	__int64	persnum;	/*persnum*/
} st_CBTCOMPETENCE_PRIMARY, *pst_CBTCOMPETENCE_PRIMARY;
#pragma pack()

/* chantier */
#pragma pack(1)
typedef struct _stCHANTIER
{
	char	chantiernum[20+1];	/*chantiernum*/
	__int64	adminnum;	/*adminnum*/
	char	usageconstcode[8+1];	/*usageconstcode*/
	char	chantierref[15+1];	/*chantierref*/
	__int64	chantierdroc;	/*chantierdroc*/
	__int64	chantierdaterecept1;	/*chantierdaterecept1*/
	__int64	chantierdaterecept2;	/*chantierdaterecept2*/
	__int64	chantierdaterecept3;	/*chantierdaterecept3*/
	__int64	chantierdaterecept4;	/*chantierdaterecept4*/
	__int64	chantierdaterecept5;	/*chantierdaterecept5*/
	__int64	chantierdaterecept6;	/*chantierdaterecept6*/
	char	chantierrecptlibelle1[64+1];	/*chantierrecptlibelle1*/
	char	chantierrecptlibelle2[64+1];	/*chantierrecptlibelle2*/
	char	chantierrecptlibelle3[64+1];	/*chantierrecptlibelle3*/
	char	chantierrecptlibelle4[64+1];	/*chantierrecptlibelle4*/
	char	chantierrecptlibelle5[64+1];	/*chantierrecptlibelle5*/
	char	chantierrecptlibelle6[64+1];	/*chantierrecptlibelle6*/
	__int64	chantierdateoccup;	/*chantierdateoccup*/
	float	chantiermtprev;	/*chantiermtprev*/
	float	chantiermtdecl;	/*chantiermtdecl*/
	float	chantiermtreel;	/*chantiermtreel*/
	__int64	chantierdescriptgene;	/*chantierdescriptgene*/
	__int64	chantierdescriptpart;	/*chantierdescriptpart*/
	__int64	chantiernumpolice;	/*chantiernumpolice*/
	__int64	chantierdatemaj;	/*chantierdatemaj*/
	__int64	chantierdatecree;	/*chantierdatecree*/
	__int64	chantierdatesuppr;	/*chantierdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stCHANTIER, *pstCHANTIER;
#pragma pack()

/* charte */
#pragma pack(1)
typedef struct _stCHARTE
{
	char	procode[32+1];	/*procode*/
	__int64	prochrono;	/*prochrono*/
	__int64	datedossiernum;	/*datedossiernum*/
	char	typemisisoncode[10+1];	/*typemisisoncode*/
	char	docnom[30+1];	/*docnom*/
	char	sinistrecode[38+1];	/*sinistrecode*/
	char	propolice[50+1];	/*propolice*/
	long	proordre;	/*proordre*/
	char	protype[1+1];	/*protype*/
	char	proparticipant[38+1];	/*proparticipant*/
	__int64	prodatecree;	/*prodatecree*/
	__int64	prodatemaj;	/*prodatemaj*/
	__int64	prodatesuppr;	/*prodatesuppr*/
	char	prolibelledoc[50+1];	/*prolibelledoc*/
	char	prodestinataire[40+1];	/*prodestinataire*/
	__int64	prodelairelance;	/*prodelairelance*/
	char	proapreslibelle[20+1];	/*proapreslibelle*/
	__int64	pronbexemplaire;	/*pronbexemplaire*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	__int64	prodelaiconserve;	/*prodelaiconserve*/
} stCHARTE, *pstCHARTE;
#pragma pack()

/* charte.PRIMARY */
#pragma pack(1)
typedef struct _st_CHARTE_PRIMARY
{
	char	procode[32];	/*procode*/
	__int64	prochrono;	/*prochrono*/
} st_CHARTE_PRIMARY, *pst_CHARTE_PRIMARY;
#pragma pack()

/* chrono */
#pragma pack(1)
typedef struct _stCHRONO
{
	char	chronotype[5+1];	/*chronotype*/
	char	chronoanneemois[10+1];	/*chronoanneemois*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	chronodernier[20+1];	/*chronodernier*/
	__int64	chronodatecree;	/*chronodatecree*/
	__int64	chronodatemaj;	/*chronodatemaj*/
	__int64	chronodatesuppr;	/*chronodatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stCHRONO, *pstCHRONO;
#pragma pack()

/* chrono.PRIMARY */
#pragma pack(1)
typedef struct _st_CHRONO_PRIMARY
{
	char	chronotype[5];	/*chronotype*/
	char	chronoanneemois[10];	/*chronoanneemois*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
} st_CHRONO_PRIMARY, *pst_CHRONO_PRIMARY;
#pragma pack()

/* chrono.FK_cbtchrono */
#pragma pack(1)
typedef struct _st_CHRONO_FK_CBTCHRONO
{
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
} st_CHRONO_FK_CBTCHRONO, *pst_CHRONO_FK_CBTCHRONO;
#pragma pack()

/* civilite */
#pragma pack(1)
typedef struct _stCIVILITE
{
	char	cbtcode[20+1];	/*cbtcode*/
	__int64	civilitenum;	/*civilitenum*/
	char	civilitetitre[30+1];	/*civilitetitre*/
	char	civilitelibelle[40+1];	/*civilitelibelle*/
	char	civiliteformule[40+1];	/*civiliteformule*/
	__int64	civilitedatecree;	/*civilitedatecree*/
	__int64	civilitedatemaj;	/*civilitedatemaj*/
	__int64	civilitedatesuppr;	/*civilitedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stCIVILITE, *pstCIVILITE;
#pragma pack()

/* clause */
#pragma pack(1)
typedef struct _stCLAUSE
{
	__int64	clausenum;	/*clausenum*/
	char	clausetype[1+1];	/*clausetype*/
	char	clauselibelle[30+1];	/*clauselibelle*/
	__int64	clausedatecree;	/*clausedatecree*/
	__int64	clausedatemaj;	/*clausedatemaj*/
	__int64	clausedatesuppr;	/*clausedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stCLAUSE, *pstCLAUSE;
#pragma pack()

/* clausechantier */
#pragma pack(1)
typedef struct _stCLAUSECHANTIER
{
	char	chantiernum[20+1];	/*chantiernum*/
	__int64	clausenum;	/*clausenum*/
	float	clausechantiermt;	/*clausechantiermt*/
	__int64	clausechantierdatecree;	/*clausechantierdatecree*/
	__int64	clausechantierdatemaj;	/*clausechantierdatemaj*/
	__int64	clausechantierdatesuppr;	/*clausechantierdatesuppr*/
} stCLAUSECHANTIER, *pstCLAUSECHANTIER;
#pragma pack()

/* clausechantier.PRIMARY */
#pragma pack(1)
typedef struct _st_CLAUSECHANTIER_PRIMARY
{
	char	chantiernum[20];	/*chantiernum*/
	__int64	clausenum;	/*clausenum*/
} st_CLAUSECHANTIER_PRIMARY, *pst_CLAUSECHANTIER_PRIMARY;
#pragma pack()

/* clausecontratsouscr */
#pragma pack(1)
typedef struct _stCLAUSECONTRATSOUSCR
{
	__int64	clausenum;	/*clausenum*/
	__int64	persnum;	/*persnum*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	float	clausemt;	/*clausemt*/
} stCLAUSECONTRATSOUSCR, *pstCLAUSECONTRATSOUSCR;
#pragma pack()

/* clausecontratsouscr.PRIMARY */
#pragma pack(1)
typedef struct _st_CLAUSECONTRATSOUSCR_PRIMARY
{
	__int64	clausenum;	/*clausenum*/
	__int64	persnum;	/*persnum*/
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_CLAUSECONTRATSOUSCR_PRIMARY, *pst_CLAUSECONTRATSOUSCR_PRIMARY;
#pragma pack()

/* clausecontratsouscr.FK_clausecontratsouscr3 */
#pragma pack(1)
typedef struct _st_CLAUSECONTRATSOUSCR_FK_CLAUSECONTRATSOUSCR3
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_CLAUSECONTRATSOUSCR_FK_CLAUSECONTRATSOUSCR3, *pst_CLAUSECONTRATSOUSCR_FK_CLAUSECONTRATSOUSCR3;
#pragma pack()

/* codepostal */
#pragma pack(1)
typedef struct _stCODEPOSTAL
{
	char	codepostal[10+1];	/*codepostal*/
	char	ville[28+1];	/*ville*/
	char	cpdureetrajet[6+1];	/*cpdureetrajet*/
	__int64	cpdistance;	/*cpdistance*/
	char	cpabscisse[3+1];	/*cpabscisse*/
	char	cpordonnee[3+1];	/*cpordonnee*/
	__int64	cpdatecree;	/*cpdatecree*/
	__int64	cpdatemaj;	/*cpdatemaj*/
	__int64	cpdatesuppr;	/*cpdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stCODEPOSTAL, *pstCODEPOSTAL;
#pragma pack()

/* codepostal.PRIMARY */
#pragma pack(1)
typedef struct _st_CODEPOSTAL_PRIMARY
{
	char	codepostal[10];	/*codepostal*/
	char	ville[28];	/*ville*/
} st_CODEPOSTAL_PRIMARY, *pst_CODEPOSTAL_PRIMARY;
#pragma pack()

/* collabcommission */
#pragma pack(1)
typedef struct _stCOLLABCOMMISSION
{
	__int64	collabcom_id;	/*collabcom_id*/
	__int64	collabnum;	/*collabnum*/
	char	hononumfacture[20+1];	/*hononumfacture*/
	float	compourcent;	/*compourcent*/
	float	commontant;	/*commontant*/
} stCOLLABCOMMISSION, *pstCOLLABCOMMISSION;
#pragma pack()

/* collabcompetence */
#pragma pack(1)
typedef struct _stCOLLABCOMPETENCE
{
	char	sinistrecode[38+1];	/*sinistrecode*/
	__int64	collabnum;	/*collabnum*/
} stCOLLABCOMPETENCE, *pstCOLLABCOMPETENCE;
#pragma pack()

/* collabcompetence.PRIMARY */
#pragma pack(1)
typedef struct _st_COLLABCOMPETENCE_PRIMARY
{
	char	sinistrecode[38];	/*sinistrecode*/
	__int64	collabnum;	/*collabnum*/
} st_COLLABCOMPETENCE_PRIMARY, *pst_COLLABCOMPETENCE_PRIMARY;
#pragma pack()

/* collabcompte */
#pragma pack(1)
typedef struct _stCOLLABCOMPTE
{
	__int64	collabnum;	/*collabnum*/
	__int64	compteid;	/*compteid*/
} stCOLLABCOMPTE, *pstCOLLABCOMPTE;
#pragma pack()

/* collabcompte.PRIMARY */
#pragma pack(1)
typedef struct _st_COLLABCOMPTE_PRIMARY
{
	__int64	collabnum;	/*collabnum*/
	__int64	compteid;	/*compteid*/
} st_COLLABCOMPTE_PRIMARY, *pst_COLLABCOMPTE_PRIMARY;
#pragma pack()

/* collabmultibase */
#pragma pack(1)
typedef struct _stCOLLABMULTIBASE
{
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	__int64	collabnum;	/*collabnum*/
	char	collabcheminbase[80+1];	/*collabcheminbase*/
	char	collabcorres[10+1];	/*collabcorres*/
	char	collabactif;	/*collabactif*/
	char	collabchemindoctype[80+1];	/*collabchemindoctype*/
	char	collabchemindocword[80+1];	/*collabchemindocword*/
	char	collabchemindocdivers[80+1];	/*collabchemindocdivers*/
	char	collabchemintabletemis[80+1];	/*collabchemintabletemis*/
	char	collabchemintabletrecept[80+1];	/*collabchemintabletrecept*/
	char	collabchemintabletsav[80+1];	/*collabchemintabletsav*/
	char	collabplanningmulti;	/*collabplanningmulti*/
	char	collabtypetablet[10+1];	/*collabtypetablet*/
} stCOLLABMULTIBASE, *pstCOLLABMULTIBASE;
#pragma pack()

/* collabmultibase.PRIMARY */
#pragma pack(1)
typedef struct _st_COLLABMULTIBASE_PRIMARY
{
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	__int64	collabnum;	/*collabnum*/
} st_COLLABMULTIBASE_PRIMARY, *pst_COLLABMULTIBASE_PRIMARY;
#pragma pack()

/* collaborateur */
#pragma pack(1)
typedef struct _stCOLLABORATEUR
{
	__int64	collabnum;	/*collabnum*/
	char	procode[32+1];	/*procode*/
	__int64	prochrono;	/*prochrono*/
	__int64	idparametreutilisateur;	/*idparametreutilisateur*/
	char	collabcode[10+1];	/*collabcode*/
	char	tvacode[10+1];	/*tvacode*/
	__int64	adminnum;	/*adminnum*/
	char	collabemail[60+1];	/*collabemail*/
	char	collabsommeil;	/*collabsommeil*/
	char	collabtype[3+1];	/*collabtype*/
	__int64	collabafaire;	/*collabafaire*/
	__int64	collabfait;	/*collabfait*/
	__int64	collabordre;	/*collabordre*/
	char	collabchemintablet[80+1];	/*collabchemintablet*/
	__int64	collabdatecree;	/*collabdatecree*/
	__int64	collabdatemaj;	/*collabdatemaj*/
	__int64	collabdatesuppr;	/*collabdatesuppr*/
	__int64	collabdateentree;	/*collabdateentree*/
	__int64	collabdatesortie;	/*collabdatesortie*/
	char	collabdureeminirdv[6+1];	/*collabdureeminirdv*/
	char	collabdureedefrdv[6+1];	/*collabdureedefrdv*/
	char	collabindentationrdv[6+1];	/*collabindentationrdv*/
	float	collabtauxhoraire;	/*collabtauxhoraire*/
	char	collabtvaintra[15+1];	/*collabtvaintra*/
	float	collabpourcent1;	/*collabpourcent1*/
	float	collabpourcent2;	/*collabpourcent2*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	__int64	collabinfosecu;	/*collabinfosecu*/
	char	couleuraccueil[7+1];	/*couleuraccueil*/
	__int64	coordmaj;	/*coordmaj*/
	char	collablongi[31+1];	/*collablongi*/
	char	collablatt[31+1];	/*collablatt*/
} stCOLLABORATEUR, *pstCOLLABORATEUR;
#pragma pack()

/* collaborateur.FK_par_defaut */
#pragma pack(1)
typedef struct _st_COLLABORATEUR_FK_PAR_DEFAUT
{
	char	procode[32];	/*procode*/
	__int64	prochrono;	/*prochrono*/
} st_COLLABORATEUR_FK_PAR_DEFAUT, *pst_COLLABORATEUR_FK_PAR_DEFAUT;
#pragma pack()

/* collabpersonne */
#pragma pack(1)
typedef struct _stCOLLABPERSONNE
{
	__int64	collabnum;	/*collabnum*/
	__int64	persnum;	/*persnum*/
} stCOLLABPERSONNE, *pstCOLLABPERSONNE;
#pragma pack()

/* collabpersonne.PRIMARY */
#pragma pack(1)
typedef struct _st_COLLABPERSONNE_PRIMARY
{
	__int64	collabnum;	/*collabnum*/
	__int64	persnum;	/*persnum*/
} st_COLLABPERSONNE_PRIMARY, *pst_COLLABPERSONNE_PRIMARY;
#pragma pack()

/* collabsecteur */
#pragma pack(1)
typedef struct _stCOLLABSECTEUR
{
	__int64	collabnum;	/*collabnum*/
	__int64	secteurnum;	/*secteurnum*/
	char	secteurcode[5+1];	/*secteurcode*/
} stCOLLABSECTEUR, *pstCOLLABSECTEUR;
#pragma pack()

/* collabsecteur.PRIMARY */
#pragma pack(1)
typedef struct _st_COLLABSECTEUR_PRIMARY
{
	__int64	collabnum;	/*collabnum*/
	__int64	secteurnum;	/*secteurnum*/
	char	secteurcode[5];	/*secteurcode*/
} st_COLLABSECTEUR_PRIMARY, *pst_COLLABSECTEUR_PRIMARY;
#pragma pack()

/* compta */
#pragma pack(1)
typedef struct _stCOMPTA
{
	long	numero;	/*numero*/
	char	journal[4+1];	/*journal*/
	char	dateecriture[8+1];	/*dateecriture*/
	char	dateecheance[8+1];	/*dateecheance*/
	char	numpiece[12+1];	/*numpiece*/
	char	compte[15+1];	/*compte*/
	char	libelle[40+1];	/*libelle*/
	double	debit;	/*debit*/
	double	credit;	/*credit*/
	char	pointage[22+1];	/*pointage*/
} stCOMPTA, *pstCOMPTA;
#pragma pack()

/* compte */
#pragma pack(1)
typedef struct _stCOMPTE
{
	__int64	compteid;	/*compteid*/
	char	comptenum[15+1];	/*comptenum*/
	__int64	adminnum;	/*adminnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	__int64	ribnum;	/*ribnum*/
	char	comptetype[2+1];	/*comptetype*/
	char	compteintitule[38+1];	/*compteintitule*/
	__int64	comptedatemvt;	/*comptedatemvt*/
	__int64	comptedatecree;	/*comptedatecree*/
	__int64	comptedatemaj;	/*comptedatemaj*/
	__int64	comptedatesuppr;	/*comptedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	__int64	comptedaterelance;	/*comptedaterelance*/
	char	comptetypelet[1+1];	/*comptetypelet*/
	__int64	comptedatefacture;	/*comptedatefacture*/
	char	comptedernlettre[2+1];	/*comptedernlettre*/
	char	comptemodereglt[20+1];	/*comptemodereglt*/
	__int64	comptedelai;	/*comptedelai*/
} stCOMPTE, *pstCOMPTE;
#pragma pack()

/* compte.comptenum2 */
#pragma pack(1)
typedef struct _st_COMPTE_COMPTENUM2
{
	char	comptenum[15];	/*comptenum*/
	__int64	adminnum;	/*adminnum*/
} st_COMPTE_COMPTENUM2, *pst_COMPTE_COMPTENUM2;
#pragma pack()

/* compte.FK_cabinetcompte */
#pragma pack(1)
typedef struct _st_COMPTE_FK_CABINETCOMPTE
{
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
} st_COMPTE_FK_CABINETCOMPTE, *pst_COMPTE_FK_CABINETCOMPTE;
#pragma pack()

/* consultationutil */
#pragma pack(1)
typedef struct _stCONSULTATIONUTIL
{
	__int64	idparametreutilisateur;	/*idparametreutilisateur*/
	__int64	persnum;	/*persnum*/
	char	typeadherent[10+1];	/*typeadherent*/
	__int64	dateheuredebutconnection;	/*dateheuredebutconnection*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	histoligne;	/*histoligne*/
	char	reglementnum[20+1];	/*reglementnum*/
	__int64	compteid;	/*compteid*/
	char	type_piece[2+1];	/*type_piece*/
	char	hononumfacture[20+1];	/*hononumfacture*/
	__int64	dateheurefinconnection;	/*dateheurefinconnection*/
} stCONSULTATIONUTIL, *pstCONSULTATIONUTIL;
#pragma pack()

/* consultationutil.PRIMARY */
#pragma pack(1)
typedef struct _st_CONSULTATIONUTIL_PRIMARY
{
	__int64	idparametreutilisateur;	/*idparametreutilisateur*/
	char	typeadherent[10];	/*typeadherent*/
	__int64	dateheuredebutconnection;	/*dateheuredebutconnection*/
	__int64	persnum;	/*persnum*/
} st_CONSULTATIONUTIL_PRIMARY, *pst_CONSULTATIONUTIL_PRIMARY;
#pragma pack()

/* consultationutil.FK_consultationutil */
#pragma pack(1)
typedef struct _st_CONSULTATIONUTIL_FK_CONSULTATIONUTIL
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
	__int64	histoligne;	/*histoligne*/
} st_CONSULTATIONUTIL_FK_CONSULTATIONUTIL, *pst_CONSULTATIONUTIL_FK_CONSULTATIONUTIL;
#pragma pack()

/* consultationutil.FK_consultationutil2 */
#pragma pack(1)
typedef struct _st_CONSULTATIONUTIL_FK_CONSULTATIONUTIL2
{
	char	reglementnum[20];	/*reglementnum*/
	__int64	compteid;	/*compteid*/
	char	type_piece[2];	/*type_piece*/
} st_CONSULTATIONUTIL_FK_CONSULTATIONUTIL2, *pst_CONSULTATIONUTIL_FK_CONSULTATIONUTIL2;
#pragma pack()

/* consultationutil.FK_consultationutil5 */
#pragma pack(1)
typedef struct _st_CONSULTATIONUTIL_FK_CONSULTATIONUTIL5
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_CONSULTATIONUTIL_FK_CONSULTATIONUTIL5, *pst_CONSULTATIONUTIL_FK_CONSULTATIONUTIL5;
#pragma pack()

/* contactcbt */
#pragma pack(1)
typedef struct _stCONTACTCBT
{
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	__int64	adminnum;	/*adminnum*/
	char	champvide[5+1];	/*champvide*/
} stCONTACTCBT, *pstCONTACTCBT;
#pragma pack()

/* contactcbt.PRIMARY */
#pragma pack(1)
typedef struct _st_CONTACTCBT_PRIMARY
{
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	__int64	adminnum;	/*adminnum*/
} st_CONTACTCBT_PRIMARY, *pst_CONTACTCBT_PRIMARY;
#pragma pack()

/* contactcbt.FK_contactcbt */
#pragma pack(1)
typedef struct _st_CONTACTCBT_FK_CONTACTCBT
{
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
} st_CONTACTCBT_FK_CONTACTCBT, *pst_CONTACTCBT_FK_CONTACTCBT;
#pragma pack()

/* contratpersonne */
#pragma pack(1)
typedef struct _stCONTRATPERSONNE
{
	__int64	persnuminter;	/*persnuminter*/
	__int64	contratligne;	/*contratligne*/
	__int64	persnumassur;	/*persnumassur*/
	char	contratservsp[30+1];	/*contratservsp*/
	char	contratnumpolice[50+1];	/*contratnumpolice*/
	__int64	contratdatedebut;	/*contratdatedebut*/
	__int64	contratdatefin;	/*contratdatefin*/
	float	contratmtfranchise;	/*contratmtfranchise*/
	__int64	contratdatecree;	/*contratdatecree*/
	__int64	contratdatemaj;	/*contratdatemaj*/
	__int64	contratdatesuppr;	/*contratdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stCONTRATPERSONNE, *pstCONTRATPERSONNE;
#pragma pack()

/* contratpersonne.PRIMARY */
#pragma pack(1)
typedef struct _st_CONTRATPERSONNE_PRIMARY
{
	__int64	contratligne;	/*contratligne*/
	__int64	persnuminter;	/*persnuminter*/
} st_CONTRATPERSONNE_PRIMARY, *pst_CONTRATPERSONNE_PRIMARY;
#pragma pack()

/* convention */
#pragma pack(1)
typedef struct _stCONVENTION
{
	char	conventioncode[6+1];	/*conventioncode*/
	char	conventionlibelle[50+1];	/*conventionlibelle*/
	__int64	conventiondatecree;	/*conventiondatecree*/
	__int64	conventiondatemaj;	/*conventiondatemaj*/
	__int64	conventiondatesuppr;	/*conventiondatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stCONVENTION, *pstCONVENTION;
#pragma pack()

/* corresdarvaardi */
#pragma pack(1)
typedef struct _stCORRESDARVAARDI
{
	char	codedarva[2+1];	/*codedarva*/
	char	libelle[164+1];	/*libelle*/
	char	corresardi[32+1];	/*corresardi*/
	__int64	darvadatecree;	/*darvadatecree*/
	__int64	darvadatemaj;	/*darvadatemaj*/
	__int64	paramsuppr;	/*paramsuppr*/
} stCORRESDARVAARDI, *pstCORRESDARVAARDI;
#pragma pack()

/* darva */
#pragma pack(1)
typedef struct _stDARVA
{
	char	code_darva[12+1];	/*code_darva*/
	char	code_gta[4+1];	/*code_gta*/
	char	sinistre[6+1];	/*sinistre*/
	char	type[8+1];	/*type*/
	char	base[60+1];	/*base*/
	char	login_acces[30+1];	/*login_acces*/
	char	mdp_acces[30+1];	/*mdp_acces*/
	char	login_pdf[30+1];	/*login_pdf*/
	char	mdp_pdf[30+1];	/*mdp_pdf*/
	char	login_recep[30+1];	/*login_recep*/
	char	mdp_recep[30+1];	/*mdp_recep*/
	char	mission[1+1];	/*mission*/
	char	rapport[1+1];	/*rapport*/
	char	nh[1+1];	/*nh*/
	char	entite[1+1];	/*entite*/
	char	assureur[1+1];	/*assureur*/
	char	exp[1+1];	/*exp*/
	char	collab[6+1];	/*collab*/
	char	secret[6+1];	/*secret*/
	char	agence[6+1];	/*agence*/
	char	cabinet[6+1];	/*cabinet*/
	char	utilisateur[6+1];	/*utilisateur*/
	char	nom[32+1];	/*nom*/
	char	mail[32+1];	/*mail*/
	char	actif[1+1];	/*actif*/
	char	rapport_vide[1+1];	/*rapport_vide*/
} stDARVA, *pstDARVA;
#pragma pack()

/* darva.PRIMARY */
#pragma pack(1)
typedef struct _st_DARVA_PRIMARY
{
	char	code_darva[12];	/*code_darva*/
	char	code_gta[4];	/*code_gta*/
	char	sinistre[6];	/*sinistre*/
} st_DARVA_PRIMARY, *pst_DARVA_PRIMARY;
#pragma pack()

/* datedossier */
#pragma pack(1)
typedef struct _stDATEDOSSIER
{
	__int64	datedossiernum;	/*datedossiernum*/
	char	datedossiercode[30+1];	/*datedossiercode*/
} stDATEDOSSIER, *pstDATEDOSSIER;
#pragma pack()

/* decision */
#pragma pack(1)
typedef struct _stDECISION
{
	char	decisioncode[10+1];	/*decisioncode*/
	char	decisionlibelle[50+1];	/*decisionlibelle*/
	__int64	decisiondatecree;	/*decisiondatecree*/
	__int64	decisiondatemaj;	/*decisiondatemaj*/
	__int64	decisiondatesuppr;	/*decisiondatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stDECISION, *pstDECISION;
#pragma pack()

/* decisiondossier */
#pragma pack(1)
typedef struct _stDECISIONDOSSIER
{
	__int64	decisdossnum;	/*decisdossnum*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	char	decisioncode[10+1];	/*decisioncode*/
	__int64	decisdate;	/*decisdate*/
	__int64	decisdatedebut;	/*decisdatedebut*/
	__int64	decisdatefin;	/*decisdatefin*/
	char	decisappel;	/*decisappel*/
	char	decissurcis;	/*decissurcis*/
	char	descispourvoi;	/*descispourvoi*/
	char	descisdepens;	/*descisdepens*/
	__int64	deciscommentaire;	/*deciscommentaire*/
} stDECISIONDOSSIER, *pstDECISIONDOSSIER;
#pragma pack()

/* decisiondossier.FK_decisiondossier */
#pragma pack(1)
typedef struct _st_DECISIONDOSSIER_FK_DECISIONDOSSIER
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_DECISIONDOSSIER_FK_DECISIONDOSSIER, *pst_DECISIONDOSSIER_FK_DECISIONDOSSIER;
#pragma pack()

/* delaicontractuel */
#pragma pack(1)
typedef struct _stDELAICONTRACTUEL
{
	__int64	delaicontnum;	/*delaicontnum*/
	char	delaicontlibelle[30+1];	/*delaicontlibelle*/
	__int64	delaicontlegal;	/*delaicontlegal*/
	__int64	delaicontcbt;	/*delaicontcbt*/
	__int64	delaicontdatemaj;	/*delaicontdatemaj*/
	__int64	delaicontdatecree;	/*delaicontdatecree*/
	__int64	delaicontdatesuppr;	/*delaicontdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stDELAICONTRACTUEL, *pstDELAICONTRACTUEL;
#pragma pack()

/* delaidossier */
#pragma pack(1)
typedef struct _stDELAIDOSSIER
{
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	delaicontnum;	/*delaicontnum*/
	__int64	delaicbt;	/*delaicbt*/
	__int64	delailegal;	/*delailegal*/
	__int64	delaidatecree;	/*delaidatecree*/
	__int64	delaidatemaj;	/*delaidatemaj*/
	__int64	delaidatesuppr;	/*delaidatesuppr*/
	__int64	delaidateecheance;	/*delaidateecheance*/
	__int64	delairetard;	/*delairetard*/
} stDELAIDOSSIER, *pstDELAIDOSSIER;
#pragma pack()

/* delaidossier.PRIMARY */
#pragma pack(1)
typedef struct _st_DELAIDOSSIER_PRIMARY
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
	__int64	delaicontnum;	/*delaicontnum*/
} st_DELAIDOSSIER_PRIMARY, *pst_DELAIDOSSIER_PRIMARY;
#pragma pack()

/* delaidossier.FK_delaidossier */
#pragma pack(1)
typedef struct _st_DELAIDOSSIER_FK_DELAIDOSSIER
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_DELAIDOSSIER_FK_DELAIDOSSIER, *pst_DELAIDOSSIER_FK_DELAIDOSSIER;
#pragma pack()

/* desordre */
#pragma pack(1)
typedef struct _stDESORDRE
{
	long	desordrenum;	/*desordrenum*/
	__int64	adminnum;	/*adminnum*/
	char	famdesordrecode[8+1];	/*famdesordrecode*/
	char	chantiernum[20+1];	/*chantiernum*/
	__int64	desordreligne;	/*desordreligne*/
	char	desordrelibellecourt[100+1];	/*desordrelibellecourt*/
	__int64	desordrelibellelong;	/*desordrelibellelong*/
	char	desordrerefinterne[12+1];	/*desordrerefinterne*/
	char	desordrereserve[30+1];	/*desordrereserve*/
	__int64	desordredatelevee;	/*desordredatelevee*/
	__int64	desordredatedecl;	/*desordredatedecl*/
	__int64	desordredateapparit;	/*desordredateapparit*/
	__int64	desordreconsequence;	/*desordreconsequence*/
	__int64	desordreorigine;	/*desordreorigine*/
	char	desordrelocal[30+1];	/*desordrelocal*/
	char	desordrepartcomm;	/*desordrepartcomm*/
	__int64	desorordreremede;	/*desorordreremede*/
	__int64	desordredatesuppr;	/*desordredatesuppr*/
	__int64	desordredatecree;	/*desordredatecree*/
	__int64	desordredatemaj;	/*desordredatemaj*/
	char	desordreprisecharge;	/*desordreprisecharge*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stDESORDRE, *pstDESORDRE;
#pragma pack()

/* destinataire */
#pragma pack(1)
typedef struct _stDESTINATAIRE
{
	__int64	destiligne;	/*destiligne*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	histoligne;	/*histoligne*/
	__int64	persnum;	/*persnum*/
	char	destitype[15+1];	/*destitype*/
	__int64	destitiersnum;	/*destitiersnum*/
	char	destichoixtype[1+1];	/*destichoixtype*/
	char	destitypelibelle[20+1];	/*destitypelibelle*/
	__int64	destidatecree;	/*destidatecree*/
	__int64	destidatemaj;	/*destidatemaj*/
	__int64	destidatesuppr;	/*destidatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	long	destinbexemplaire;	/*destinbexemplaire*/
	char	destitypeperstiers[15+1];	/*destitypeperstiers*/
	char	destilistdepend[20+1];	/*destilistdepend*/
} stDESTINATAIRE, *pstDESTINATAIRE;
#pragma pack()

/* destinataire.FK_envoiea */
#pragma pack(1)
typedef struct _st_DESTINATAIRE_FK_ENVOIEA
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
	__int64	histoligne;	/*histoligne*/
} st_DESTINATAIRE_FK_ENVOIEA, *pst_DESTINATAIRE_FK_ENVOIEA;
#pragma pack()

/* destinatairereunion */
#pragma pack(1)
typedef struct _stDESTINATAIREREUNION
{
	char	reunionnum[20+1];	/*reunionnum*/
	__int64	persnum;	/*persnum*/
	char	dossnum[20+1];	/*dossnum*/
	char	reunionabsent;	/*reunionabsent*/
	char	reunionmotifabsence[200+1];	/*reunionmotifabsence*/
	char	destireunionconvoque;	/*destireunionconvoque*/
	char	destireuniondestconvo;	/*destireuniondestconvo*/
	char	destireuniondestcr;	/*destireuniondestcr*/
	char	destireunionchoixtypelibelle[20+1];	/*destireunionchoixtypelibelle*/
	char	destireuniontierstype[10+1];	/*destireuniontierstype*/
} stDESTINATAIREREUNION, *pstDESTINATAIREREUNION;
#pragma pack()

/* destinatairereunion.PRIMARY */
#pragma pack(1)
typedef struct _st_DESTINATAIREREUNION_PRIMARY
{
	char	reunionnum[20];	/*reunionnum*/
	__int64	persnum;	/*persnum*/
	char	dossnum[20];	/*dossnum*/
} st_DESTINATAIREREUNION_PRIMARY, *pst_DESTINATAIREREUNION_PRIMARY;
#pragma pack()

/* diversbati */
#pragma pack(1)
typedef struct _stDIVERSBATI
{
	__int64	diversbatinum;	/*diversbatinum*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	char	ticketannee[8+1];	/*ticketannee*/
	char	diverspourfranchise[12+1];	/*diverspourfranchise*/
	char	diversconcernechantier;	/*diversconcernechantier*/
	char	diversnatureforfait[1+1];	/*diversnatureforfait*/
	char	diversavenant;	/*diversavenant*/
	char	diversprolongation;	/*diversprolongation*/
	__int64	diversdatejourj;	/*diversdatejourj*/
	__int64	diversdateenvoi;	/*diversdateenvoi*/
	char	divershorsdelai;	/*divershorsdelai*/
	char	diversreference[50+1];	/*diversreference*/
	char	diversconcerne[160+1];	/*diversconcerne*/
	char	diverspourcfranchise[12+1];	/*diverspourcfranchise*/
	char	diversfrchant;	/*diversfrchant*/
	__int64	diversdateinfdoss;	/*diversdateinfdoss*/
	__int64	diversbatidatecree;	/*diversbatidatecree*/
	__int64	diversbatidatemaj;	/*diversbatidatemaj*/
	__int64	diversbatidatesuppr;	/*diversbatidatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	float	diversmtsinistre;	/*diversmtsinistre*/
} stDIVERSBATI, *pstDIVERSBATI;
#pragma pack()

/* diversbati.FK_diversbaticard */
#pragma pack(1)
typedef struct _st_DIVERSBATI_FK_DIVERSBATICARD
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_DIVERSBATI_FK_DIVERSBATICARD, *pst_DIVERSBATI_FK_DIVERSBATICARD;
#pragma pack()

/* diversird */
#pragma pack(1)
typedef struct _stDIVERSIRD
{
	__int64	diversirdnum;	/*diversirdnum*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	char	typemisisoncode[10+1];	/*typemisisoncode*/
	char	conventioncode[6+1];	/*conventioncode*/
	char	motifcode[6+1];	/*motifcode*/
	char	typeexpertisecode[25+1];	/*typeexpertisecode*/
	char	diverscategoriestat[6+1];	/*diverscategoriestat*/
	char	diverspresomptionfraude;	/*diverspresomptionfraude*/
	char	diversnonconforme;	/*diversnonconforme*/
	char	diversrectifie;	/*diversrectifie*/
	char	diverselec;	/*diverselec*/
	char	diversaspm;	/*diversaspm*/
	char	diversaspi;	/*diversaspi*/
	float	diversmtreclame;	/*diversmtreclame*/
	float	diversmtattribue;	/*diversmtattribue*/
	float	diversmtdontmobilier;	/*diversmtdontmobilier*/
	float	diversmtasubir;	/*diversmtasubir*/
	float	diversmtaexercer;	/*diversmtaexercer*/
	__int64	diversdatesinistre;	/*diversdatesinistre*/
	__int64	diversdatedebsinistre;	/*diversdatedebsinistre*/
	__int64	diversdatefinsinistre;	/*diversdatefinsinistre*/
	__int64	diversdate1ercontact;	/*diversdate1ercontact*/
	__int64	diversdate1erevisite;	/*diversdate1erevisite*/
	__int64	diversdatedervisite;	/*diversdatedervisite*/
	__int64	diversdatemisecause;	/*diversdatemisecause*/
	__int64	diversdatear;	/*diversdatear*/
	__int64	diversdate1ercourrier;	/*diversdate1ercourrier*/
	__int64	diversdatederrelance;	/*diversdatederrelance*/
	__int64	diversdatereceptdoc;	/*diversdatereceptdoc*/
	__int64	diversdateirpv;	/*diversdateirpv*/
	__int64	diversdateremisefrappe;	/*diversdateremisefrappe*/
	__int64	diversdatefrappe;	/*diversdatefrappe*/
	__int64	diversdaterelecture;	/*diversdaterelecture*/
	__int64	diversdaterapport;	/*diversdaterapport*/
	__int64	diversdatesignature;	/*diversdatesignature*/
	__int64	diversdateenvoila;	/*diversdateenvoila*/
	__int64	diversdateretourla;	/*diversdateretourla*/
	__int64	diversdatedevisregtel;	/*diversdatedevisregtel*/
	__int64	diversdatesigneprotocole;	/*diversdatesigneprotocole*/
	char	diverstec;	/*diverstec*/
	char	diversexamcause;	/*diversexamcause*/
	char	diversverifrisque;	/*diversverifrisque*/
	char	diversformerapp;	/*diversformerapp*/
	char	diversmissioncomplete;	/*diversmissioncomplete*/
	char	diversrecours;	/*diversrecours*/
	char	diverssignesurplace;	/*diverssignesurplace*/
	char	diverstypeintervention[5+1];	/*diverstypeintervention*/
	__int64	diversdatedebtravaux;	/*diversdatedebtravaux*/
	__int64	diversdatefintravaux;	/*diversdatefintravaux*/
	__int64	diversdatesignepv;	/*diversdatesignepv*/
	char	diversreparpar[1+1];	/*diversreparpar*/
	char	diversmissionannule;	/*diversmissionannule*/
	float	diversmttravaux;	/*diversmttravaux*/
	char	diverscausesuppr;	/*diverscausesuppr*/
	char	diversdossierpidi;	/*diversdossierpidi*/
	char	diversrespectdelai;	/*diversrespectdelai*/
	char	diversrefevenement[50+1];	/*diversrefevenement*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stDIVERSIRD, *pstDIVERSIRD;
#pragma pack()

/* diversird.FK_diversird */
#pragma pack(1)
typedef struct _st_DIVERSIRD_FK_DIVERSIRD
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_DIVERSIRD_FK_DIVERSIRD, *pst_DIVERSIRD_FK_DIVERSIRD;
#pragma pack()

/* diversmedi */
#pragma pack(1)
typedef struct _stDIVERSMEDI
{
	__int64	diversmedinum;	/*diversmedinum*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	typepiparnum;	/*typepiparnum*/
	__int64	prejuestnum;	/*prejuestnum*/
	__int64	typepinum;	/*typepinum*/
	__int64	secunum;	/*secunum*/
	__int64	quantumnum;	/*quantumnum*/
	__int64	professionnum;	/*professionnum*/
	__int64	typ_professionnum;	/*typ_professionnum*/
	char	typeexpertisecode[25+1];	/*typeexpertisecode*/
	char	typemisisoncode[10+1];	/*typemisisoncode*/
	__int64	diversdateaccident;	/*diversdateaccident*/
	char	diversemailinter[60+1];	/*diversemailinter*/
	__int64	diversdatenaissinter;	/*diversdatenaissinter*/
	__int64	diversdatedeceinter;	/*diversdatedeceinter*/
	char	diversetatcivilinter[25+1];	/*diversetatcivilinter*/
	long	diversnbenfantinter;	/*diversnbenfantinter*/
	char	diversforminter[50+1];	/*diversforminter*/
	char	diverscontratinter[50+1];	/*diverscontratinter*/
	char	diversmodeexerinter[50+1];	/*diversmodeexerinter*/
	char	diverssexeinter[1+1];	/*diverssexeinter*/
	char	diversetiquette[60+1];	/*diversetiquette*/
	__int64	diversdatedepot;	/*diversdatedepot*/
	__int64	diversdatejuge;	/*diversdatejuge*/
	char	diversnumjuge[25+1];	/*diversnumjuge*/
	char	diversnumrg[10+1];	/*diversnumrg*/
	char	diverslieujuge[50+1];	/*diverslieujuge*/
	__int64	diversdateexp;	/*diversdateexp*/
	char	diversippprov1[20+1];	/*diversippprov1*/
	char	diversippprov2[20+1];	/*diversippprov2*/
	long	diversippdef;	/*diversippdef*/
	char	diversprejest[35+1];	/*diversprejest*/
	char	diverscaissecomple[32+1];	/*diverscaissecomple*/
	char	diversprejagre;	/*diversprejagre*/
	__int64	diversprejagreinfo;	/*diversprejagreinfo*/
	char	diversincidprof;	/*diversincidprof*/
	__int64	diversincidinfo;	/*diversincidinfo*/
	__int64	diversprejsexinfo;	/*diversprejsexinfo*/
	char	diversprejmoral;	/*diversprejmoral*/
	__int64	diversprejmoralinfo;	/*diversprejmoralinfo*/
	__int64	diversdatearretdebut;	/*diversdatearretdebut*/
	__int64	diversdatearretfin;	/*diversdatearretfin*/
	__int64	diversdatereprisetravaux;	/*diversdatereprisetravaux*/
	char	diversinfotravaux[150+1];	/*diversinfotravaux*/
	char	diversarevoir;	/*diversarevoir*/
	__int64	diversdateconsolide;	/*diversdateconsolide*/
	char	diversdateconsolideprev[25+1];	/*diversdateconsolideprev*/
	__int64	diversdatesouscrip;	/*diversdatesouscrip*/
	char	diversinfosouscrip[80+1];	/*diversinfosouscrip*/
	__int64	diversittdatedebut;	/*diversittdatedebut*/
	__int64	diversittdatefin;	/*diversittdatefin*/
	char	diversittinfo[150+1];	/*diversittinfo*/
	char	diversitp[2+1];	/*diversitp*/
	__int64	diversitpdatedebut;	/*diversitpdatedebut*/
	__int64	diversitpdatefin;	/*diversitpdatefin*/
	char	diversitpinfo[150+1];	/*diversitpinfo*/
	long	diversgtt;	/*diversgtt*/
	__int64	diversgttdatedebut;	/*diversgttdatedebut*/
	__int64	diversgttdatefin;	/*diversgttdatefin*/
	char	diversgttinfo[150+1];	/*diversgttinfo*/
	long	diversgtp;	/*diversgtp*/
	__int64	diversgtpdatedebut;	/*diversgtpdatedebut*/
	__int64	diversgtpdatefin;	/*diversgtpdatefin*/
	char	diversgtpinfo[150+1];	/*diversgtpinfo*/
	long	diversgt;	/*diversgt*/
	__int64	diversgtdatedebut;	/*diversgtdatedebut*/
	__int64	diversgtdatefin;	/*diversgtdatefin*/
	char	diversgtinfo[150+1];	/*diversgtinfo*/
	__int64	diverssoinsdatedebut;	/*diverssoinsdatedebut*/
	__int64	diverssoinsdatefin;	/*diverssoinsdatefin*/
	__int64	diverssoinsdatedebut2;	/*diverssoinsdatedebut2*/
	__int64	diverssoinsdatefin2;	/*diverssoinsdatefin2*/
	__int64	divershospidatedebut;	/*divershospidatedebut*/
	__int64	divershospidatefin;	/*divershospidatefin*/
	__int64	divershospidatedebut2;	/*divershospidatedebut2*/
	__int64	divershospidatefin2;	/*divershospidatefin2*/
	char	diversfraisfutur;	/*diversfraisfutur*/
	char	diverstiercepersonne;	/*diverstiercepersonne*/
	char	diversdoug[1+1];	/*diversdoug*/
	char	diverstaille[4+1];	/*diverstaille*/
	char	diverspoids[7+1];	/*diverspoids*/
	__int64	diversmascorp;	/*diversmascorp*/
	char	diversmascorplibelle[50+1];	/*diversmascorplibelle*/
	char	diversservicemili[50+1];	/*diversservicemili*/
	__int64	diversantecedentmed;	/*diversantecedentmed*/
	__int64	diversantecedentchir;	/*diversantecedentchir*/
	__int64	diversantecedenttrau;	/*diversantecedenttrau*/
	__int64	diversheureaccident;	/*diversheureaccident*/
	char	diverslieuaccident[50+1];	/*diverslieuaccident*/
	char	diversmoyenloc[15+1];	/*diversmoyenloc*/
	char	diversmoyenlocadverse[15+1];	/*diversmoyenlocadverse*/
	char	diversstatutaccident[15+1];	/*diversstatutaccident*/
	char	diversportceinture;	/*diversportceinture*/
	char	diversusageappuietete;	/*diversusageappuietete*/
	char	diversusagecasque;	/*diversusagecasque*/
	char	diverslieuimpact[25+1];	/*diverslieuimpact*/
	float	diversmtmateriel;	/*diversmtmateriel*/
	char	diversepave;	/*diversepave*/
	char	divershospitalise;	/*divershospitalise*/
	char	diversretourdomicile;	/*diversretourdomicile*/
	char	diversretourtravail;	/*diversretourtravail*/
	__int64	diversdaterevision;	/*diversdaterevision*/
	__int64	diversdatedercontact;	/*diversdatedercontact*/
	__int64	diversdatederrdv;	/*diversdatederrdv*/
	__int64	diversdatederrapport;	/*diversdatederrapport*/
	__int64	diversdatedercourrier;	/*diversdatedercourrier*/
	__int64	diversdatedercourhono;	/*diversdatedercourhono*/
	__int64	diversdatederrgl;	/*diversdatederrgl*/
	__int64	diversdatederrglpartie;	/*diversdatederrglpartie*/
	char	diverssamu;	/*diverssamu*/
	char	diversurgence;	/*diversurgence*/
	char	divershospilibelle1[50+1];	/*divershospilibelle1*/
	char	divershospilibelle2[50+1];	/*divershospilibelle2*/
	char	divershospilibelle3[50+1];	/*divershospilibelle3*/
	__int64	diversdatehospi1debut;	/*diversdatehospi1debut*/
	__int64	diversdatehospi2debut;	/*diversdatehospi2debut*/
	__int64	diversdatehospi3debut;	/*diversdatehospi3debut*/
	__int64	diversdatehospi1ifin;	/*diversdatehospi1ifin*/
	__int64	diversdatehospi3ifin;	/*diversdatehospi3ifin*/
	__int64	diversdatehospi2ifin;	/*diversdatehospi2ifin*/
	long	diversnbconsultext;	/*diversnbconsultext*/
	long	diversnbconsulmed;	/*diversnbconsulmed*/
	char	diversreeduclibelle[50+1];	/*diversreeduclibelle*/
	__int64	diversreeducdatedebut;	/*diversreeducdatedebut*/
	__int64	diversreeducdatefin;	/*diversreeducdatefin*/
	__int64	diversnbreedu;	/*diversnbreedu*/
	char	diverskinelibelle[50+1];	/*diverskinelibelle*/
	__int64	diverskinedatedebut;	/*diverskinedatedebut*/
	__int64	diverskinedatefin;	/*diverskinedatefin*/
	long	diversnbkine;	/*diversnbkine*/
	__int64	diverspharmacie;	/*diverspharmacie*/
	__int64	diversexambio;	/*diversexambio*/
	__int64	diversautresoins;	/*diversautresoins*/
	char	divershabitattype[50+1];	/*divershabitattype*/
	char	divershabitatetage;	/*divershabitatetage*/
	char	divershabitatascenseur;	/*divershabitatascenseur*/
	char	divershabitatsurface[50+1];	/*divershabitatsurface*/
	float	diversmedimtreclame;	/*diversmedimtreclame*/
	float	diversmedimtattribue;	/*diversmedimtattribue*/
	char	diverspitype[50+1];	/*diverspitype*/
	__int64	diverspdidatedelivre;	/*diverspdidatedelivre*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	__int64	diversmedidatecree;	/*diversmedidatecree*/
	__int64	diversmedidatemaj;	/*diversmedidatemaj*/
	__int64	diversmedidatesuppr;	/*diversmedidatesuppr*/
	char	diversreference1[60+1];	/*diversreference1*/
	char	diversreference2[60+1];	/*diversreference2*/
	char	diversreference3[60+1];	/*diversreference3*/
	char	diverslieunaissance[38+1];	/*diverslieunaissance*/
	char	diversprejsex;	/*diversprejsex*/
	char	diversnumsecuinter[31+1];	/*diversnumsecuinter*/
	__int64	diversdateclotinstruct;	/*diversdateclotinstruct*/
} stDIVERSMEDI, *pstDIVERSMEDI;
#pragma pack()

/* diversmedi.FK_diversmedicard */
#pragma pack(1)
typedef struct _st_DIVERSMEDI_FK_DIVERSMEDICARD
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_DIVERSMEDI_FK_DIVERSMEDICARD, *pst_DIVERSMEDI_FK_DIVERSMEDICARD;
#pragma pack()

/* document */
#pragma pack(1)
typedef struct _stDOCUMENT
{
	char	docnom[30+1];	/*docnom*/
	__int64	secretnum;	/*secretnum*/
	char	groupecode[38+1];	/*groupecode*/
	__int64	persnum;	/*persnum*/
	__int64	secretnummailto;	/*secretnummailto*/
	char	groupecodemailto[38+1];	/*groupecodemailto*/
	__int64	collabnum;	/*collabnum*/
	char	editeurcode[20+1];	/*editeurcode*/
	char	typedoccode[6+1];	/*typedoccode*/
	__int64	collabnummailto;	/*collabnummailto*/
	char	doclibelle[50+1];	/*doclibelle*/
	__int64	docdelairelance;	/*docdelairelance*/
	__int64	docdelaiarchivage;	/*docdelaiarchivage*/
	__int64	docnbexemplaire;	/*docnbexemplaire*/
	char	docvisiblepar[2+1];	/*docvisiblepar*/
	char	docexiste;	/*docexiste*/
	char	doctabletpc;	/*doctabletpc*/
	char	docmailtopiece;	/*docmailtopiece*/
	char	docmailto;	/*docmailto*/
	char	dochorsselect;	/*dochorsselect*/
	char	docfamillestat[32+1];	/*docfamillestat*/
	char	docmajdate[32+1];	/*docmajdate*/
	__int64	docdatecree;	/*docdatecree*/
	__int64	docdatemaj;	/*docdatemaj*/
	__int64	docdatesuppr;	/*docdatesuppr*/
	char	docconvoc;	/*docconvoc*/
	char	doccrendu;	/*doccrendu*/
	char	docmailtodestidoc;	/*docmailtodestidoc*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	char	docmailtodesti[32+1];	/*docmailtodesti*/
} stDOCUMENT, *pstDOCUMENT;
#pragma pack()

/* dossier */
#pragma pack(1)
typedef struct _stDOSSIER
{
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	char	famillecode[20+1];	/*famillecode*/
	__int64	collabnum;	/*collabnum*/
	__int64	secretnum;	/*secretnum*/
	__int64	secretnum2;	/*secretnum2*/
	char	typereceptcode[10+1];	/*typereceptcode*/
	char	chantiernum[20+1];	/*chantiernum*/
	char	qualitecode[2+1];	/*qualitecode*/
	char	sinistrecode[38+1];	/*sinistrecode*/
	char	metiercode[20+1];	/*metiercode*/
	char	metierpwd[20+1];	/*metierpwd*/
	char	procode[32+1];	/*procode*/
	__int64	prochrono;	/*prochrono*/
	__int64	collabnum2;	/*collabnum2*/
	__int64	adminnum;	/*adminnum*/
	char	dossnumom[20+1];	/*dossnumom*/
	char	dossnumcbt[20+1];	/*dossnumcbt*/
	char	dossnumsinistrerecu[20+1];	/*dossnumsinistrerecu*/
	char	dossidentifiantrecu[20+1];	/*dossidentifiantrecu*/
	char	dosscodegta[6+1];	/*dosscodegta*/
	char	dossdestiar;	/*dossdestiar*/
	char	dossconnexe[50+1];	/*dossconnexe*/
	char	dossnumpolice[50+1];	/*dossnumpolice*/
	char	dosstypepolice[20+1];	/*dosstypepolice*/
	char	dossnumpoliceregie[50+1];	/*dossnumpoliceregie*/
	char	dossresponsable;	/*dossresponsable*/
	char	dosslese;	/*dosslese*/
	__int64	dosscommentsinistre;	/*dosscommentsinistre*/
	__int64	dossdatemaj;	/*dossdatemaj*/
	char	dossunioucontre[32+1];	/*dossunioucontre*/
	__int64	dosscomment1;	/*dosscomment1*/
	__int64	dosscomment2;	/*dosscomment2*/
	char	dosstransfert;	/*dosstransfert*/
	char	dosshorsdelai;	/*dosshorsdelai*/
	__int64	dossdatereception;	/*dossdatereception*/
	char	dossrefsinistre[50+1];	/*dossrefsinistre*/
	__int64	dossdatearchive;	/*dossdatearchive*/
	char	dossarretprocedure;	/*dossarretprocedure*/
	__int64	dossdatecloture;	/*dossdatecloture*/
	__int64	dossdatedeclsinistre;	/*dossdatedeclsinistre*/
	__int64	dossdatederhono;	/*dossdatederhono*/
	__int64	dossdatemission;	/*dossdatemission*/
	__int64	dossdatetransfert;	/*dossdatetransfert*/
	float	dossmtindemntotal;	/*dossmtindemntotal*/
	__int64	dossdatecree;	/*dossdatecree*/
	__int64	dossdatesuppr;	/*dossdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	char	codeportefeuille[32+1];	/*codeportefeuille*/
	char	utilisateur[32+1];	/*utilisateur*/
} stDOSSIER, *pstDOSSIER;
#pragma pack()

/* dossier.PRIMARY */
#pragma pack(1)
typedef struct _st_DOSSIER_PRIMARY
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_DOSSIER_PRIMARY, *pst_DOSSIER_PRIMARY;
#pragma pack()

/* dossier.FK_agence */
#pragma pack(1)
typedef struct _st_DOSSIER_FK_AGENCE
{
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
} st_DOSSIER_FK_AGENCE, *pst_DOSSIER_FK_AGENCE;
#pragma pack()

/* dossier.FK_cabinet */
#pragma pack(1)
typedef struct _st_DOSSIER_FK_CABINET
{
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_DOSSIER_FK_CABINET, *pst_DOSSIER_FK_CABINET;
#pragma pack()

/* dossier.FK_charte */
#pragma pack(1)
typedef struct _st_DOSSIER_FK_CHARTE
{
	char	procode[32];	/*procode*/
	__int64	prochrono;	/*prochrono*/
} st_DOSSIER_FK_CHARTE, *pst_DOSSIER_FK_CHARTE;
#pragma pack()

/* dossier.FK_metier */
#pragma pack(1)
typedef struct _st_DOSSIER_FK_METIER
{
	char	metiercode[20];	/*metiercode*/
	char	metierpwd[20];	/*metierpwd*/
} st_DOSSIER_FK_METIER, *pst_DOSSIER_FK_METIER;
#pragma pack()

/* dossierlesionsequelle */
#pragma pack(1)
typedef struct _stDOSSIERLESIONSEQUELLE
{
	__int64	lesionsequelledossnum;	/*lesionsequelledossnum*/
	char	lesionsequelletype[1+1];	/*lesionsequelletype*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	char	lesionsequellecode[6+1];	/*lesionsequellecode*/
} stDOSSIERLESIONSEQUELLE, *pstDOSSIERLESIONSEQUELLE;
#pragma pack()

/* dossierlesionsequelle.FK_dossierlesionsequelle */
#pragma pack(1)
typedef struct _st_DOSSIERLESIONSEQUELLE_FK_DOSSIERLESIONSEQUELLE
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_DOSSIERLESIONSEQUELLE_FK_DOSSIERLESIONSEQUELLE, *pst_DOSSIERLESIONSEQUELLE_FK_DOSSIERLESIONSEQUELLE;
#pragma pack()

/* dossiertauxhoraire */
#pragma pack(1)
typedef struct _stDOSSIERTAUXHORAIRE
{
	__int64	tauxhorairenum;	/*tauxhorairenum*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	dosstxtligne;	/*dosstxtligne*/
	char	dosstxttypetravaux[6+1];	/*dosstxttypetravaux*/
	char	dosstxttypeligne[5+1];	/*dosstxttypeligne*/
	double	dosstxthoraire;	/*dosstxthoraire*/
	char	dosstxttemps[6+1];	/*dosstxttemps*/
	char	dosstxtfacturable;	/*dosstxtfacturable*/
	char	dosstxtafacturer;	/*dosstxtafacturer*/
	__int64	dosstxudatecree;	/*dosstxudatecree*/
	__int64	dosstxtdatemaj;	/*dosstxtdatemaj*/
	__int64	dosstxtdatesuppr;	/*dosstxtdatesuppr*/
	__int64	dosstxtdatedeb;	/*dosstxtdatedeb*/
	__int64	dosstxtdatefin;	/*dosstxtdatefin*/
} stDOSSIERTAUXHORAIRE, *pstDOSSIERTAUXHORAIRE;
#pragma pack()

/* dossiertauxhoraire.PRIMARY */
#pragma pack(1)
typedef struct _st_DOSSIERTAUXHORAIRE_PRIMARY
{
	__int64	tauxhorairenum;	/*tauxhorairenum*/
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_DOSSIERTAUXHORAIRE_PRIMARY, *pst_DOSSIERTAUXHORAIRE_PRIMARY;
#pragma pack()

/* dossiertauxhoraire.FK_dossiertauxhoraire2 */
#pragma pack(1)
typedef struct _st_DOSSIERTAUXHORAIRE_FK_DOSSIERTAUXHORAIRE2
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_DOSSIERTAUXHORAIRE_FK_DOSSIERTAUXHORAIRE2, *pst_DOSSIERTAUXHORAIRE_FK_DOSSIERTAUXHORAIRE2;
#pragma pack()

/* droitaccesutilisateur */
#pragma pack(1)
typedef struct _stDROITACCESUTILISATEUR
{
	__int64	droitaccesnum;	/*droitaccesnum*/
	__int64	profilnum;	/*profilnum*/
	__int64	formulairenum;	/*formulairenum*/
	char	droitacces[15+1];	/*droitacces*/
} stDROITACCESUTILISATEUR, *pstDROITACCESUTILISATEUR;
#pragma pack()

/* editeur */
#pragma pack(1)
typedef struct _stEDITEUR
{
	char	editeurcode[20+1];	/*editeurcode*/
	char	editeurlibelle[100+1];	/*editeurlibelle*/
} stEDITEUR, *pstEDITEUR;
#pragma pack()

/* electrodomestique */
#pragma pack(1)
typedef struct _stELECTRODOMESTIQUE
{
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	electroligne;	/*electroligne*/
	char	electrotype[6+1];	/*electrotype*/
	char	electroirreparable;	/*electroirreparable*/
	char	electroavisexpert;	/*electroavisexpert*/
	char	electroavisreparateur;	/*electroavisreparateur*/
	__int64	electrodaterepare;	/*electrodaterepare*/
	float	electromtindemnite;	/*electromtindemnite*/
	char	typeindemncode[6+1];	/*typeindemncode*/
	char	electrolibelle[50+1];	/*electrolibelle*/
	__int64	electrodatecree;	/*electrodatecree*/
	__int64	electrodatemaj;	/*electrodatemaj*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stELECTRODOMESTIQUE, *pstELECTRODOMESTIQUE;
#pragma pack()

/* electrodomestique.PRIMARY */
#pragma pack(1)
typedef struct _st_ELECTRODOMESTIQUE_PRIMARY
{
	__int64	electroligne;	/*electroligne*/
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_ELECTRODOMESTIQUE_PRIMARY, *pst_ELECTRODOMESTIQUE_PRIMARY;
#pragma pack()

/* electrodomestique.FK_electro */
#pragma pack(1)
typedef struct _st_ELECTRODOMESTIQUE_FK_ELECTRO
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_ELECTRODOMESTIQUE_FK_ELECTRO, *pst_ELECTRODOMESTIQUE_FK_ELECTRO;
#pragma pack()

/* etatdb */
#pragma pack(1)
typedef struct _stETATDB
{
	char	codecollab[1+1];	/*CODECOLLAB*/
	long	nbdossierencours;	/*NBDOSSIERENCOURS*/
	long	nbdossierfacture;	/*NBDOSSIERFACTURE*/
	char	nomcollab[1+1];	/*NOMCOLLAB*/
	char	numero[1+1];	/*NUMERO*/
	double	totalht;	/*TOTALHT*/
} stETATDB, *pstETATDB;
#pragma pack()

/* famille */
#pragma pack(1)
typedef struct _stFAMILLE
{
	char	famillecode[20+1];	/*famillecode*/
	char	famillelibelle[50+1];	/*famillelibelle*/
	__int64	familledatecree;	/*familledatecree*/
	__int64	familledatemaj;	/*familledatemaj*/
	__int64	familledatesuppr;	/*familledatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stFAMILLE, *pstFAMILLE;
#pragma pack()

/* familledesordre */
#pragma pack(1)
typedef struct _stFAMILLEDESORDRE
{
	char	famdesordrecode[8+1];	/*famdesordrecode*/
	char	famdesordrelibelle[30+1];	/*famdesordrelibelle*/
	__int64	famdesordredatecree;	/*famdesordredatecree*/
	__int64	famdesordredatemaj;	/*famdesordredatemaj*/
	__int64	famdesordredatesuppr;	/*famdesordredatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stFAMILLEDESORDRE, *pstFAMILLEDESORDRE;
#pragma pack()

/* formulaire */
#pragma pack(1)
typedef struct _stFORMULAIRE
{
	char	cbtcode[20+1];	/*cbtcode*/
	__int64	formulairenum;	/*formulairenum*/
	char	formulairenom[50+1];	/*formulairenom*/
	char	formulairelibelle[100+1];	/*formulairelibelle*/
} stFORMULAIRE, *pstFORMULAIRE;
#pragma pack()

/* fusionsdoc */
#pragma pack(1)
typedef struct _stFUSIONSDOC
{
	__int64	fusionnum;	/*fusionnum*/
	char	docnom[30+1];	/*docnom*/
	__int64	fusionquantite;	/*fusionquantite*/
	char	fusionbloque;	/*fusionbloque*/
	__int64	fusiondatecree;	/*fusiondatecree*/
	__int64	fusiondatemaj;	/*fusiondatemaj*/
	__int64	fusiondatesuppr;	/*fusiondatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stFUSIONSDOC, *pstFUSIONSDOC;
#pragma pack()

/* garantie */
#pragma pack(1)
typedef struct _stGARANTIE
{
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	garantligne;	/*garantligne*/
	char	typecontratcode[20+1];	/*typecontratcode*/
	__int64	garantdate;	/*garantdate*/
	char	garantcode[5+1];	/*garantcode*/
	char	garantlibelle[50+1];	/*garantlibelle*/
	double	garantindicesouscript;	/*garantindicesouscript*/
	double	garantindiceregle;	/*garantindiceregle*/
	char	garantindexation;	/*garantindexation*/
	char	garantrisqueexcep;	/*garantrisqueexcep*/
	float	garantmtimmobilier;	/*garantmtimmobilier*/
	float	garantmtmobilier;	/*garantmtmobilier*/
	float	garantmtmarchandise;	/*garantmtmarchandise*/
	float	garantmtvaleurespece;	/*garantmtvaleurespece*/
	float	garantmtbiensensible;	/*garantmtbiensensible*/
	float	garantmtequipement;	/*garantmtequipement*/
	float	garantmtautre;	/*garantmtautre*/
	float	garantmtembellissement;	/*garantmtembellissement*/
	float	garantmtmateriel;	/*garantmtmateriel*/
	float	garantmtobjetprecieux;	/*garantmtobjetprecieux*/
	float	garantmtrecherchefuite;	/*garantmtrecherchefuite*/
	float	garantmtstock;	/*garantmtstock*/
	float	garantmtvetuste;	/*garantmtvetuste*/
	float	garantmtfranchise;	/*garantmtfranchise*/
	double	garantpourcentfranchise;	/*garantpourcentfranchise*/
	float	garantmtfranchiseabsolue;	/*garantmtfranchiseabsolue*/
	float	garantmtplancher;	/*garantmtplancher*/
	float	garantmtplafond;	/*garantmtplafond*/
	__int64	garantnbpiece;	/*garantnbpiece*/
	char	garantinfomobilier[32+1];	/*garantinfomobilier*/
	char	garantinfoimmo[32+1];	/*garantinfoimmo*/
	char	garantinfomarchand[32+1];	/*garantinfomarchand*/
	char	garantinfovaleurespece[32+1];	/*garantinfovaleurespece*/
	char	garantinfobiensensible[32+1];	/*garantinfobiensensible*/
	char	garantinfoequipement[32+1];	/*garantinfoequipement*/
	char	garantinfoautre[32+1];	/*garantinfoautre*/
	char	garantinfoembellissement[32+1];	/*garantinfoembellissement*/
	char	garantinfomateriel[32+1];	/*garantinfomateriel*/
	char	garantinfoobjet[32+1];	/*garantinfoobjet*/
	char	garantinfofuite[32+1];	/*garantinfofuite*/
	char	garantinfostock[32+1];	/*garantinfostock*/
	char	garantinfovetuste[32+1];	/*garantinfovetuste*/
	__int64	garantcommentaire;	/*garantcommentaire*/
	double	garantsurfacerisque;	/*garantsurfacerisque*/
	double	garantsurfacebatiment;	/*garantsurfacebatiment*/
	double	garantsurfacedespend;	/*garantsurfacedespend*/
	float	garantchiffreaffaire;	/*garantchiffreaffaire*/
	double	garantquantite;	/*garantquantite*/
	__int64	garantdatecree;	/*garantdatecree*/
	__int64	garantdatemaj;	/*garantdatemaj*/
	__int64	garantdatesuppr;	/*garantdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stGARANTIE, *pstGARANTIE;
#pragma pack()

/* garantie.PRIMARY */
#pragma pack(1)
typedef struct _st_GARANTIE_PRIMARY
{
	__int64	garantligne;	/*garantligne*/
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_GARANTIE_PRIMARY, *pst_GARANTIE_PRIMARY;
#pragma pack()

/* garantie.FK_garantie */
#pragma pack(1)
typedef struct _st_GARANTIE_FK_GARANTIE
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_GARANTIE_FK_GARANTIE, *pst_GARANTIE_FK_GARANTIE;
#pragma pack()

/* grillehonoraire */
#pragma pack(1)
typedef struct _stGRILLEHONORAIRE
{
	__int64	persnum;	/*persnum*/
	char	typemisisoncode[10+1];	/*typemisisoncode*/
	float	grillehonomt;	/*grillehonomt*/
} stGRILLEHONORAIRE, *pstGRILLEHONORAIRE;
#pragma pack()

/* grillehonoraire.PRIMARY */
#pragma pack(1)
typedef struct _st_GRILLEHONORAIRE_PRIMARY
{
	__int64	persnum;	/*persnum*/
	char	typemisisoncode[10];	/*typemisisoncode*/
} st_GRILLEHONORAIRE_PRIMARY, *pst_GRILLEHONORAIRE_PRIMARY;
#pragma pack()

/* groupersonne */
#pragma pack(1)
typedef struct _stGROUPERSONNE
{
	char	groupecode[38+1];	/*groupecode*/
	__int64	adminnum;	/*adminnum*/
	__int64	idparametreutilisateur;	/*idparametreutilisateur*/
	__int64	groupedatecree;	/*groupedatecree*/
	__int64	groupedatemaj;	/*groupedatemaj*/
	__int64	groupedatesuppr;	/*groupedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stGROUPERSONNE, *pstGROUPERSONNE;
#pragma pack()

/* historique */
#pragma pack(1)
typedef struct _stHISTORIQUE
{
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	histoligne;	/*histoligne*/
	__int64	destiligne;	/*destiligne*/
	__int64	secretnum;	/*secretnum*/
	char	hononumfacture[20+1];	/*hononumfacture*/
	__int64	planningnum;	/*planningnum*/
	char	procode[32+1];	/*procode*/
	__int64	prochrono;	/*prochrono*/
	char	docnom[30+1];	/*docnom*/
	char	reunionnum[20+1];	/*reunionnum*/
	char	doclibelle[50+1];	/*doclibelle*/
	__int64	histodatecree;	/*histodatecree*/
	__int64	histodatemaj;	/*histodatemaj*/
	__int64	histodatesuppr;	/*histodatesuppr*/
	__int64	histodateecheance;	/*histodateecheance*/
	__int64	histodateeventeffect;	/*histodateeventeffect*/
	__int64	histodelairelance;	/*histodelairelance*/
	char	histoarretrelance;	/*histoarretrelance*/
	char	histoeventeffectue;	/*histoeventeffectue*/
	__int64	histodelaiarchivage;	/*histodelaiarchivage*/
	char	histocollab[10+1];	/*histocollab*/
	char	histocollab2[10+1];	/*histocollab2*/
	char	histosecretgere[10+1];	/*histosecretgere*/
	char	histosecretsecfrappe[10+1];	/*histosecretsecfrappe*/
	char	histomodecreation[6+1];	/*histomodecreation*/
	char	histonomdocfusionne[20+1];	/*histonomdocfusionne*/
	wchar_t	histopourinfo[100+1];	/*histopourinfo*/
	char	histoenvoimail;	/*histoenvoimail*/
	char	historertdoc[10+1];	/*historertdoc*/
	char	histoenvoimaileva;	/*histoenvoimaileva*/
	char	histoenvoimailto;	/*histoenvoimailto*/
	__int64	histodateretourtablet;	/*histodateretourtablet*/
	char	histoidentifiantirdweb[20+1];	/*histoidentifiantirdweb*/
	char	histosigne;	/*histosigne*/
	char	histotabletpc;	/*histotabletpc*/
	__int64	histodatemail;	/*histodatemail*/
	__int64	histodemandemaileva;	/*histodemandemaileva*/
	__int64	histodatetranstabletpc;	/*histodatetranstabletpc*/
	__int64	histodatesignetabletpc;	/*histodatesignetabletpc*/
	char	histotypeirdweb[5+1];	/*histotypeirdweb*/
	char	histosoustypeirdweb[5+1];	/*histosoustypeirdweb*/
	char	historeunionouconvo[2+1];	/*historeunionouconvo*/
	__int64	histodateimpr;	/*histodateimpr*/
	char	histomultidest;	/*histomultidest*/
	__int64	histodatecopie;	/*histodatecopie*/
	char	histotypecopie[1+1];	/*histotypecopie*/
	__int64	histodatefinale;	/*histodatefinale*/
	char	histocheminmatrice[100+1];	/*histocheminmatrice*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	char	histoaediter;	/*histoaediter*/
	char	histovisiblepar[2+1];	/*histovisiblepar*/
	char	histoemisrecu[1+1];	/*histoemisrecu*/
	__int64	histodatear;	/*histodatear*/
	char	histonumar[25+1];	/*histonumar*/
	__int64	datedossiernum;	/*datedossiernum*/
} stHISTORIQUE, *pstHISTORIQUE;
#pragma pack()

/* historique.PRIMARY */
#pragma pack(1)
typedef struct _st_HISTORIQUE_PRIMARY
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
	__int64	histoligne;	/*histoligne*/
} st_HISTORIQUE_PRIMARY, *pst_HISTORIQUE_PRIMARY;
#pragma pack()

/* historique.FK_dossierhisto */
#pragma pack(1)
typedef struct _st_HISTORIQUE_FK_DOSSIERHISTO
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_HISTORIQUE_FK_DOSSIERHISTO, *pst_HISTORIQUE_FK_DOSSIERHISTO;
#pragma pack()

/* historique.FK_provient */
#pragma pack(1)
typedef struct _st_HISTORIQUE_FK_PROVIENT
{
	char	procode[32];	/*procode*/
	__int64	prochrono;	/*prochrono*/
} st_HISTORIQUE_FK_PROVIENT, *pst_HISTORIQUE_FK_PROVIENT;
#pragma pack()

/* histotransfert */
#pragma pack(1)
typedef struct _stHISTOTRANSFERT
{
	__int64	histotransnum;	/*histotransnum*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	histoligne;	/*histoligne*/
	__int64	histotransdate;	/*histotransdate*/
	char	histotranssens[25+1];	/*histotranssens*/
	char	histotransfichier[50+1];	/*histotransfichier*/
	char	histotransresultat[80+1];	/*histotransresultat*/
	__int64	histotransdatecree;	/*histotransdatecree*/
	__int64	histotransdatemaj;	/*histotransdatemaj*/
	__int64	histotransdatesuppr;	/*histotransdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stHISTOTRANSFERT, *pstHISTOTRANSFERT;
#pragma pack()

/* histotransfert.FK_transfert */
#pragma pack(1)
typedef struct _st_HISTOTRANSFERT_FK_TRANSFERT
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
	__int64	histoligne;	/*histoligne*/
} st_HISTOTRANSFERT_FK_TRANSFERT, *pst_HISTOTRANSFERT_FK_TRANSFERT;
#pragma pack()

/* honoraire */
#pragma pack(1)
typedef struct _stHONORAIRE
{
	char	hononumfacture[20+1];	/*hononumfacture*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	histoligne;	/*histoligne*/
	__int64	destiligne;	/*destiligne*/
	char	tresorcode[6+1];	/*tresorcode*/
	char	hononumirdweb[20+1];	/*hononumirdweb*/
	char	honotype[2+1];	/*honotype*/
	char	honoclosdossier;	/*honoclosdossier*/
	char	honoht;	/*honoht*/
	char	honoidentifiantirdweb[20+1];	/*honoidentifiantirdweb*/
	__int64	honodatefacture;	/*honodatefacture*/
	__int64	honodateimpr;	/*honodateimpr*/
	__int64	honodateecheance;	/*honodateecheance*/
	__int64	honodatecompta;	/*honodatecompta*/
	char	honocodetva1[10+1];	/*honocodetva1*/
	float	honototalfrais;	/*honototalfrais*/
	float	honototalhonos;	/*honototalhonos*/
	char	honotyperepart[1+1];	/*honotyperepart*/
	__int64	honodatereglement;	/*honodatereglement*/
	char	honosimpleoudetaille[1+1];	/*honosimpleoudetaille*/
	__int64	honodatemaj;	/*honodatemaj*/
	__int64	honodatesuppr;	/*honodatesuppr*/
	char	honoregle;	/*honoregle*/
	float	honorelicat;	/*honorelicat*/
	long	hononbrelance;	/*hononbrelance*/
	char	honosoumistva;	/*honosoumistva*/
	char	honocomplexe;	/*honocomplexe*/
	float	hononbheure;	/*hononbheure*/
	float	honotauxhoraire;	/*honotauxhoraire*/
	float	honomtheure;	/*honomtheure*/
	char	honoreglementencours;	/*honoreglementencours*/
	float	honomtregle;	/*honomtregle*/
	char	honocodetva2[10+1];	/*honocodetva2*/
	char	honocodetva3[10+1];	/*honocodetva3*/
	double	honotauxtva1;	/*honotauxtva1*/
	double	honotauxtva2;	/*honotauxtva2*/
	double	honotauxtva3;	/*honotauxtva3*/
	float	honomtht1;	/*honomtht1*/
	float	honomtht2;	/*honomtht2*/
	float	honomtht3;	/*honomtht3*/
	float	honomttva1;	/*honomttva1*/
	float	honomttva2;	/*honomttva2*/
	float	honomttva3;	/*honomttva3*/
	float	honomtttc1;	/*honomtttc1*/
	float	honomtttc2;	/*honomtttc2*/
	float	honomtttc3;	/*honomtttc3*/
	float	honototalht;	/*honototalht*/
	float	honototaltva;	/*honototaltva*/
	float	honototalttc;	/*honototalttc*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	char	nomdot[20+1];	/*nomdot*/
	char	factsolde;	/*factsolde*/
	char	cabinet[20+1];	/*cabinet*/
	char	agence[20+1];	/*agence*/
	__int64	regsecrsecond;	/*regsecrsecond*/
	__int64	regsecretaire;	/*regsecretaire*/
	char	collab1[6+1];	/*collab1*/
	char	collab2[6+1];	/*collab2*/
	char	collab3[6+1];	/*collab3*/
	char	collab4[6+1];	/*collab4*/
	char	collab5[6+1];	/*collab5*/
	double	mtcollab1;	/*mtcollab1*/
	double	mtcollab2;	/*mtcollab2*/
	double	mtcollab3;	/*mtcollab3*/
	double	mtcollab4;	/*mtcollab4*/
	double	mtcollab5;	/*mtcollab5*/
	double	pourcentcollab1;	/*pourcentcollab1*/
	double	pourcentcollab2;	/*pourcentcollab2*/
	double	pourcentcollab3;	/*pourcentcollab3*/
	double	pourcentcollab4;	/*pourcentcollab4*/
	double	pourcentcollab5;	/*pourcentcollab5*/
} stHONORAIRE, *pstHONORAIRE;
#pragma pack()

/* honoraire.FK_dossierhonoTampon */
#pragma pack(1)
typedef struct _st_HONORAIRE_FK_DOSSIERHONOTAMPON
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_HONORAIRE_FK_DOSSIERHONOTAMPON, *pst_HONORAIRE_FK_DOSSIERHONOTAMPON;
#pragma pack()

/* honoraire.FK_honoimprimeTampon */
#pragma pack(1)
typedef struct _st_HONORAIRE_FK_HONOIMPRIMETAMPON
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
	__int64	histoligne;	/*histoligne*/
} st_HONORAIRE_FK_HONOIMPRIMETAMPON, *pst_HONORAIRE_FK_HONOIMPRIMETAMPON;
#pragma pack()

/* honorairedefaut */
#pragma pack(1)
typedef struct _stHONORAIREDEFAUT
{
	char	honodefautperscode[38+1];	/*honodefautperscode*/
	char	honodefautcode[15+1];	/*honodefautcode*/
	__int64	honodefautligne;	/*honodefautligne*/
	char	docnom[30+1];	/*docnom*/
	__int64	collabnum;	/*collabnum*/
	__int64	payeurnum;	/*payeurnum*/
	char	tvacode[10+1];	/*tvacode*/
	char	typemisisoncode[10+1];	/*typemisisoncode*/
	char	groupecode[38+1];	/*groupecode*/
	char	honodefauttype[6+1];	/*honodefauttype*/
	char	honodefautlibelle[50+1];	/*honodefautlibelle*/
	float	honodefautpu;	/*honodefautpu*/
	char	honodefautunite[5+1];	/*honodefautunite*/
	char	honodefautcolonne[6+1];	/*honodefautcolonne*/
	__int64	honodfautdatecree;	/*honodfautdatecree*/
	__int64	honodafautdatemaj;	/*honodafautdatemaj*/
	__int64	honodefautdatesuppr;	/*honodefautdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stHONORAIREDEFAUT, *pstHONORAIREDEFAUT;
#pragma pack()

/* honorairedefaut.PRIMARY */
#pragma pack(1)
typedef struct _st_HONORAIREDEFAUT_PRIMARY
{
	__int64	honodefautligne;	/*honodefautligne*/
	char	docnom[30];	/*docnom*/
	char	honodefautcode[15];	/*honodefautcode*/
	char	honodefautperscode[38];	/*honodefautperscode*/
} st_HONORAIREDEFAUT_PRIMARY, *pst_HONORAIREDEFAUT_PRIMARY;
#pragma pack()

/* honoraireligne */
#pragma pack(1)
typedef struct _stHONORAIRELIGNE
{
	__int64	honoligne;	/*honoligne*/
	char	hononumfacture[20+1];	/*hononumfacture*/
	char	tvacode[10+1];	/*tvacode*/
	char	honolignetype[5+1];	/*honolignetype*/
	char	honolignelibelle[80+1];	/*honolignelibelle*/
	char	honoligneunite[5+1];	/*honoligneunite*/
	double	honolignequantite;	/*honolignequantite*/
	float	honolignepu;	/*honolignepu*/
	float	honolignemtht;	/*honolignemtht*/
	float	honolignemttva;	/*honolignemttva*/
	float	honolignemtttc;	/*honolignemtttc*/
	__int64	honolignedatecree;	/*honolignedatecree*/
	__int64	honolignedatemaj;	/*honolignedatemaj*/
	__int64	honolignedatesuppr;	/*honolignedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stHONORAIRELIGNE, *pstHONORAIRELIGNE;
#pragma pack()

/* honoraireligne.PRIMARY */
#pragma pack(1)
typedef struct _st_HONORAIRELIGNE_PRIMARY
{
	__int64	honoligne;	/*honoligne*/
	char	hononumfacture[20];	/*hononumfacture*/
} st_HONORAIRELIGNE_PRIMARY, *pst_HONORAIRELIGNE_PRIMARY;
#pragma pack()

/* impayemayma34 */
#pragma pack(1)
typedef struct _stIMPAYEMAYMA34
{
	char	nompayeur[38+1];	/*nompayeur*/
	char	nomexpert[38+1];	/*nomexpert*/
	char	nomassure[38+1];	/*nomassure*/
	char	honotype[2+1];	/*honotype*/
	char	dossnum[20+1];	/*dossnum*/
	long	annees;	/*annees*/
	char	compa[50+1];	/*compa*/
	char	exper[50+1];	/*EXPER*/
	char	assur[50+1];	/*ASSUR*/
	__int64	datemission;	/*datemission*/
	double	honototalttc;	/*honototalttc*/
	__int64	honodatefacture;	/*honodatefacture*/
	char	factsolde;	/*factsolde*/
	__int64	honodatereglement;	/*honodatereglement*/
	char	nomassurance[38+1];	/*nomassurance*/
	char	reglementnum[20+1];	/*reglementnum*/
	char	numpolice[32+1];	/*numpolice*/
	double	reste;	/*reste*/
	__int64	reglementdatesaisie;	/*reglementdatesaisie*/
	char	hononumfacture[38+1];	/*hononumfacture*/
} stIMPAYEMAYMA34, *pstIMPAYEMAYMA34;
#pragma pack()

/* indemnitedossier */
#pragma pack(1)
typedef struct _stINDEMNITEDOSSIER
{
	__int64	diversirdnum;	/*diversirdnum*/
	char	indemndossiernum[20+1];	/*indemndossiernum*/
	char	typeindemncode[6+1];	/*typeindemncode*/
	float	indemnmtrectifie;	/*indemnmtrectifie*/
	float	indemnmtindemnimmediate;	/*indemnmtindemnimmediate*/
	float	indemnmtindemndiffere;	/*indemnmtindemndiffere*/
	__int64	indemnum;	/*indemnum*/
} stINDEMNITEDOSSIER, *pstINDEMNITEDOSSIER;
#pragma pack()

/* indemnitedossier.PRIMARY */
#pragma pack(1)
typedef struct _st_INDEMNITEDOSSIER_PRIMARY
{
	__int64	diversirdnum;	/*diversirdnum*/
	char	indemndossiernum[20];	/*indemndossiernum*/
	__int64	indemnum;	/*indemnum*/
} st_INDEMNITEDOSSIER_PRIMARY, *pst_INDEMNITEDOSSIER_PRIMARY;
#pragma pack()

/* juridique */
#pragma pack(1)
typedef struct _stJURIDIQUE
{
	char	juridiquestatut[2+1];	/*juridiquestatut*/
	char	juridiquelibcourt[10+1];	/*juridiquelibcourt*/
	char	juridiqueliblong[50+1];	/*juridiqueliblong*/
	__int64	juridiquedatecree;	/*juridiquedatecree*/
	__int64	juridiquedatemaj;	/*juridiquedatemaj*/
	__int64	juridiquedatesuppr;	/*juridiquedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stJURIDIQUE, *pstJURIDIQUE;
#pragma pack()

/* lesionsequelle */
#pragma pack(1)
typedef struct _stLESIONSEQUELLE
{
	char	pathologiecode[2+1];	/*pathologiecode*/
	char	lesionsequelletype[1+1];	/*lesionsequelletype*/
	char	lesionsequellecode[6+1];	/*lesionsequellecode*/
	char	lesionsequellelibelle[80+1];	/*lesionsequellelibelle*/
	__int64	lesionsequelledatecree;	/*lesionsequelledatecree*/
	__int64	lesionsequelledatemaj;	/*lesionsequelledatemaj*/
	__int64	lesionsequelledatesuppr;	/*lesionsequelledatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stLESIONSEQUELLE, *pstLESIONSEQUELLE;
#pragma pack()

/* lesionsequelle.PRIMARY */
#pragma pack(1)
typedef struct _st_LESIONSEQUELLE_PRIMARY
{
	char	lesionsequelletype[1];	/*lesionsequelletype*/
	char	lesionsequellecode[6];	/*lesionsequellecode*/
} st_LESIONSEQUELLE_PRIMARY, *pst_LESIONSEQUELLE_PRIMARY;
#pragma pack()

/* metier */
#pragma pack(1)
typedef struct _stMETIER
{
	char	metiercode[20+1];	/*metiercode*/
	char	metierpwd[20+1];	/*metierpwd*/
	char	metierlibelle[50+1];	/*metierlibelle*/
	__int64	metierdatecree;	/*metierdatecree*/
	__int64	metierdatemaj;	/*metierdatemaj*/
	__int64	metierdatesuppr;	/*metierdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	__int64	profilnum;	/*profilnum*/
} stMETIER, *pstMETIER;
#pragma pack()

/* metier.PRIMARY */
#pragma pack(1)
typedef struct _st_METIER_PRIMARY
{
	char	metiercode[20];	/*metiercode*/
	char	metierpwd[20];	/*metierpwd*/
} st_METIER_PRIMARY, *pst_METIER_PRIMARY;
#pragma pack()

/* motif */
#pragma pack(1)
typedef struct _stMOTIF
{
	char	motifcode[6+1];	/*motifcode*/
	char	motiflibelle[50+1];	/*motiflibelle*/
	__int64	motifdatecree;	/*motifdatecree*/
	__int64	motifdatemaj;	/*motifdatemaj*/
	__int64	motifdatesuppr;	/*motifdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stMOTIF, *pstMOTIF;
#pragma pack()

/* natureinscription */
#pragma pack(1)
typedef struct _stNATUREINSCRIPTION
{
	char	natinscription[1+1];	/*natinscription*/
	char	natinscrlibelle[50+1];	/*natinscrlibelle*/
	__int64	natdatecree;	/*natdatecree*/
	__int64	natdatemaj;	/*natdatemaj*/
	__int64	natdatesuppr;	/*natdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stNATUREINSCRIPTION, *pstNATUREINSCRIPTION;
#pragma pack()

/* objetformulaire */
#pragma pack(1)
typedef struct _stOBJETFORMULAIRE
{
	char	objetcode[20+1];	/*objetcode*/
	__int64	droitaccesnum;	/*droitaccesnum*/
	char	objettype[20+1];	/*objettype*/
	char	objetlibelle[50+1];	/*objetlibelle*/
	char	autorisation[20+1];	/*autorisation*/
} stOBJETFORMULAIRE, *pstOBJETFORMULAIRE;
#pragma pack()

/* objetformulaire.PRIMARY */
#pragma pack(1)
typedef struct _st_OBJETFORMULAIRE_PRIMARY
{
	char	objetcode[20];	/*objetcode*/
	__int64	droitaccesnum;	/*droitaccesnum*/
} st_OBJETFORMULAIRE_PRIMARY, *pst_OBJETFORMULAIRE_PRIMARY;
#pragma pack()

/* parametreformulaire */
#pragma pack(1)
typedef struct _stPARAMETREFORMULAIRE
{
	__int64	idparametreutilisateur;	/*idparametreutilisateur*/
	char	objetcode[20+1];	/*objetcode*/
	__int64	droitaccesnum;	/*droitaccesnum*/
	char	champsvide[38+1];	/*champsvide*/
} stPARAMETREFORMULAIRE, *pstPARAMETREFORMULAIRE;
#pragma pack()

/* parametreformulaire.PRIMARY */
#pragma pack(1)
typedef struct _st_PARAMETREFORMULAIRE_PRIMARY
{
	__int64	idparametreutilisateur;	/*idparametreutilisateur*/
	char	objetcode[20];	/*objetcode*/
	__int64	droitaccesnum;	/*droitaccesnum*/
} st_PARAMETREFORMULAIRE_PRIMARY, *pst_PARAMETREFORMULAIRE_PRIMARY;
#pragma pack()

/* parametreformulaire.FK_parametreformulaire2 */
#pragma pack(1)
typedef struct _st_PARAMETREFORMULAIRE_FK_PARAMETREFORMULAIRE2
{
	char	objetcode[20];	/*objetcode*/
	__int64	droitaccesnum;	/*droitaccesnum*/
} st_PARAMETREFORMULAIRE_FK_PARAMETREFORMULAIRE2, *pst_PARAMETREFORMULAIRE_FK_PARAMETREFORMULAIRE2;
#pragma pack()

/* parametreutilisateur */
#pragma pack(1)
typedef struct _stPARAMETREUTILISATEUR
{
	__int64	idparametreutilisateur;	/*idparametreutilisateur*/
	__int64	profilnum;	/*profilnum*/
	char	codeacces[10+1];	/*codeacces*/
	char	pwd1[32+1];	/*pwd1*/
	__int64	datepwd1;	/*datepwd1*/
	char	pwd2[32+1];	/*pwd2*/
	__int64	datepwd2;	/*datepwd2*/
	char	pwd3[32+1];	/*pwd3*/
	__int64	datepwd3;	/*datepwd3*/
	char	pwd4[32+1];	/*pwd4*/
	__int64	datepwd4;	/*datepwd4*/
	char	agencedefaut[10+1];	/*agencedefaut*/
	char	collabdefaut[10+1];	/*collabdefaut*/
	char	secretdefaut[10+1];	/*secretdefaut*/
	char	dossierencours;	/*dossierencours*/
	char	typeutilisateur[5+1];	/*typeutilisateur*/
	__int64	profilnum2;	/*profilnum2*/
	__int64	profilnum3;	/*profilnum3*/
	char	secretaire[10+1];	/*secretaire*/
	char	collaborateur[10+1];	/*collaborateur*/
	char	agence[10+1];	/*agence*/
	char	relance;	/*relance*/
	char	messirdweb;	/*messirdweb*/
	__int64	utildatecree;	/*utildatecree*/
	__int64	utildatemaj;	/*utildatemaj*/
	__int64	utildatesuppr;	/*utildatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	char	langue[4+1];	/*langue*/
	char	administrateur;	/*administrateur*/
	char	addrmac[50+1];	/*addrmac*/
	char	addip[50+1];	/*addip*/
	long	sessipmac;	/*sessipmac*/
} stPARAMETREUTILISATEUR, *pstPARAMETREUTILISATEUR;
#pragma pack()

/* pathologie */
#pragma pack(1)
typedef struct _stPATHOLOGIE
{
	char	pathologiecode[2+1];	/*pathologiecode*/
	char	pathologielibelle[60+1];	/*pathologielibelle*/
	char	pathologietype[1+1];	/*pathologietype*/
	__int64	pathologiedatecree;	/*pathologiedatecree*/
	__int64	pathologiedatemaj;	/*pathologiedatemaj*/
	__int64	pathologiedatesuppr;	/*pathologiedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stPATHOLOGIE, *pstPATHOLOGIE;
#pragma pack()

/* payeur */
#pragma pack(1)
typedef struct _stPAYEUR
{
	__int64	payeurnum;	/*payeurnum*/
	__int64	compteid;	/*compteid*/
	__int64	persnum;	/*persnum*/
	char	hononumfacture[20+1];	/*hononumfacture*/
	char	payeurtype[5+1];	/*payeurtype*/
	__int64	oldpayeurnum;	/*oldpayeurnum*/
} stPAYEUR, *pstPAYEUR;
#pragma pack()

/* pays */
#pragma pack(1)
typedef struct _stPAYS
{
	char	payscode[4+1];	/*payscode*/
	char	paysnom[50+1];	/*paysnom*/
	__int64	paysdatecree;	/*paysdatecree*/
	__int64	paysdatemaj;	/*paysdatemaj*/
	__int64	paysdatesuppr;	/*paysdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stPAYS, *pstPAYS;
#pragma pack()

/* personne */
#pragma pack(1)
typedef struct _stPERSONNE
{
	__int64	oldpersnum;	/*oldpersnum*/
	char	srcpersnum[5+1];	/*srcpersnum*/
	__int64	persnum;	/*persnum*/
	char	docnom[30+1];	/*docnom*/
	char	groupecode[38+1];	/*groupecode*/
	char	conventioncode[6+1];	/*conventioncode*/
	char	situationjuricode[10+1];	/*situationjuricode*/
	__int64	idparametreutilisateur;	/*idparametreutilisateur*/
	char	famillecode[20+1];	/*famillecode*/
	char	juridiquestatut[2+1];	/*juridiquestatut*/
	__int64	adminnum;	/*adminnum*/
	char	typeperscode[10+1];	/*typeperscode*/
	char	natinscription[1+1];	/*natinscription*/
	__int64	compteid;	/*compteid*/
	__int64	compteidparagence;	/*compteidparagence*/
	char	specialitecode[10+1];	/*specialitecode*/
	char	procode[32+1];	/*procode*/
	__int64	prochrono;	/*prochrono*/
	char	categentrcode[6+1];	/*categentrcode*/
	char	perscode[38+1];	/*perscode*/
	char	perscodegta[5+1];	/*perscodegta*/
	__int64	perscommentaire;	/*perscommentaire*/
	__int64	persdatecree;	/*persdatecree*/
	__int64	persdatemaj;	/*persdatemaj*/
	__int64	persdatesuppr;	/*persdatesuppr*/
	char	persprovenance[10+1];	/*persprovenance*/
	char	persemail[60+1];	/*persemail*/
	char	persemail2[60+1];	/*persemail2*/
	char	persweb[60+1];	/*persweb*/
	__int64	perstauxvetuste;	/*perstauxvetuste*/
	char	persmdpintranet[32+1];	/*persmdpintranet*/
	char	perscategorie[40+1];	/*perscategorie*/
	char	persloginintranet[15+1];	/*persloginintranet*/
	float	persmtplafond1;	/*persmtplafond1*/
	float	persmtplafond2;	/*persmtplafond2*/
	char	perstypefacturation[1+1];	/*perstypefacturation*/
	float	perspourcenttravaux;	/*perspourcenttravaux*/
	char	perstvaintra[32+1];	/*perstvaintra*/
	char	perspartenaire;	/*perspartenaire*/
	__int64	persdistance;	/*persdistance*/
	char	persnumordre[50+1];	/*persnumordre*/
	char	persagree;	/*persagree*/
	char	persaignorer;	/*persaignorer*/
	char	persqualiteresp[32+1];	/*persqualiteresp*/
	char	perscodeape[4+1];	/*perscodeape*/
	char	perssiret[18+1];	/*perssiret*/
	char	persrc[10+1];	/*persrc*/
	float	perscapital;	/*perscapital*/
	__int64	perstrancheff;	/*perstrancheff*/
	__int64	perseffectlocal;	/*perseffectlocal*/
	__int64	persdatesitujuri;	/*persdatesitujuri*/
	char	perscodequalib1[6+1];	/*perscodequalib1*/
	char	perscodequalib2[6+1];	/*perscodequalib2*/
	char	perscodequalib3[6+1];	/*perscodequalib3*/
	char	perscodequalib4[6+1];	/*perscodequalib4*/
	char	perscodequalib5[6+1];	/*perscodequalib5*/
	char	perscodequalib6[6+1];	/*perscodequalib6*/
	char	perscodequalib7[6+1];	/*perscodequalib7*/
	char	perscodequalib8[6+1];	/*perscodequalib8*/
	char	perscodequalib9[6+1];	/*perscodequalib9*/
	char	perscodequalib10[6+1];	/*perscodequalib10*/
	char	persinterv1;	/*persinterv1*/
	char	persinterv2;	/*persinterv2*/
	char	persinterv3;	/*persinterv3*/
	char	persinterv4;	/*persinterv4*/
	char	persinterv5;	/*persinterv5*/
	char	persinterv6;	/*persinterv6*/
	char	persinterv7;	/*persinterv7*/
	char	persinterv8;	/*persinterv8*/
	char	persinterv9;	/*persinterv9*/
	char	persinterv10;	/*persinterv10*/
	char	persqualif1;	/*persqualif1*/
	char	persqualif2;	/*persqualif2*/
	char	persqualif3;	/*persqualif3*/
	char	persqualif4;	/*persqualif4*/
	char	persqualif5;	/*persqualif5*/
	char	persqualif6;	/*persqualif6*/
	char	persqualif7;	/*persqualif7*/
	char	persqualif8;	/*persqualif8*/
	char	persqualif9;	/*persqualif9*/
	char	persqualif10;	/*persqualif10*/
	__int64	persintervsucc;	/*persintervsucc*/
	char	perssiren[12+1];	/*perssiren*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stPERSONNE, *pstPERSONNE;
#pragma pack()

/* personne.FK_chartepersonne */
#pragma pack(1)
typedef struct _st_PERSONNE_FK_CHARTEPERSONNE
{
	char	procode[32];	/*procode*/
	__int64	prochrono;	/*prochrono*/
} st_PERSONNE_FK_CHARTEPERSONNE, *pst_PERSONNE_FK_CHARTEPERSONNE;
#pragma pack()

/* planning */
#pragma pack(1)
typedef struct _stPLANNING
{
	__int64	planningnum;	/*planningnum*/
	char	planningcode[10+1];	/*planningcode*/
	__int64	collabnum;	/*collabnum*/
	char	typetelcode[2+1];	/*typetelcode*/
	char	payscode[4+1];	/*payscode*/
	__int64	collabnum2;	/*collabnum2*/
	char	secteurcode[5+1];	/*secteurcode*/
	__int64	persnum;	/*persnum*/
	char	typetelcode2[2+1];	/*typetelcode2*/
	char	codepostal[10+1];	/*codepostal*/
	char	ville[28+1];	/*ville*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	civilitenum;	/*civilitenum*/
	__int64	planningdatedebut;	/*planningdatedebut*/
	__int64	planningdatefin;	/*planningdatefin*/
	char	planningnature[1+1];	/*planningnature*/
	char	planningvalid[1+1];	/*planningvalid*/
	__int64	planningdatecree;	/*planningdatecree*/
	__int64	planningdatemaj;	/*planningdatemaj*/
	__int64	planningdatesuppr;	/*planningdatesuppr*/
	char	planningnom[38+1];	/*planningnom*/
	char	planningadresse1[38+1];	/*planningadresse1*/
	char	planningadresse2[38+1];	/*planningadresse2*/
	char	planningadresse3[38+1];	/*planningadresse3*/
	__int64	planningcommentaire;	/*planningcommentaire*/
	char	planningtel1[20+1];	/*planningtel1*/
	char	planningtel2[20+1];	/*planningtel2*/
	char	planningabsent;	/*planningabsent*/
	char	planningraisonabsence[100+1];	/*planningraisonabsence*/
	char	planningconfirmepar[8+1];	/*planningconfirmepar*/
	char	planningpriorite[1+1];	/*planningpriorite*/
	char	planninglibelle[60+1];	/*planninglibelle*/
	char	planningtypetel1[10+1];	/*planningtypetel1*/
	char	planningtypetel2[10+1];	/*planningtypetel2*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	char	planningduree[6+1];	/*planningduree*/
	char	planningdureealler[6+1];	/*planningdureealler*/
	char	planningdureeretour[6+1];	/*planningdureeretour*/
	char	planningcommentaire2[50+1];	/*planningcommentaire2*/
	long	planningligne;	/*planningligne*/
	char	planningexchange[255+1];	/*planningexchange*/
} stPLANNING, *pstPLANNING;
#pragma pack()

/* planning.FK_dossierrdv */
#pragma pack(1)
typedef struct _st_PLANNING_FK_DOSSIERRDV
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_PLANNING_FK_DOSSIERRDV, *pst_PLANNING_FK_DOSSIERRDV;
#pragma pack()

/* planning.FK_rdvlocalisation */
#pragma pack(1)
typedef struct _st_PLANNING_FK_RDVLOCALISATION
{
	char	codepostal[10];	/*codepostal*/
	char	ville[28];	/*ville*/
} st_PLANNING_FK_RDVLOCALISATION, *pst_PLANNING_FK_RDVLOCALISATION;
#pragma pack()

/* postit */
#pragma pack(1)
typedef struct _stPOSTIT
{
	__int64	postitnum;	/*postitnum*/
	char	postitexpediteur[3+1];	/*postitexpediteur*/
	char	postittypeexp[1+1];	/*postittypeexp*/
	char	postitdestinataire[3+1];	/*postitdestinataire*/
	char	postittypedest[1+1];	/*postittypedest*/
	char	postitobjet[50+1];	/*postitobjet*/
	__int64	postitmessage;	/*postitmessage*/
	char	postitpiecejointe[100+1];	/*postitpiecejointe*/
	__int64	postitdatesuppr;	/*postitdatesuppr*/
	__int64	postitdatemaj;	/*postitdatemaj*/
	__int64	postitdatecree;	/*postitdatecree*/
	__int64	postitdateactive;	/*postitdateactive*/
	__int64	postitdatefin;	/*postitdatefin*/
	__int64	postitnbactive;	/*postitnbactive*/
	char	postitperiodicite[1+1];	/*postitperiodicite*/
	__int64	postitperioderecur;	/*postitperioderecur*/
	char	postitfini;	/*postitfini*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stPOSTIT, *pstPOSTIT;
#pragma pack()

/* postithisto */
#pragma pack(1)
typedef struct _stPOSTITHISTO
{
	__int64	postitnum;	/*postitnum*/
	__int64	postithistoligne;	/*postithistoligne*/
	__int64	postithistodate;	/*postithistodate*/
	__int64	postithistodaterel;	/*postithistodaterel*/
	char	postithistofini;	/*postithistofini*/
	__int64	postithistodatecree;	/*postithistodatecree*/
	__int64	postithistodatemaj;	/*postithistodatemaj*/
	__int64	postithistodatesuppr;	/*postithistodatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stPOSTITHISTO, *pstPOSTITHISTO;
#pragma pack()

/* postithisto.PRIMARY */
#pragma pack(1)
typedef struct _st_POSTITHISTO_PRIMARY
{
	__int64	postitnum;	/*postitnum*/
	__int64	postithistoligne;	/*postithistoligne*/
} st_POSTITHISTO_PRIMARY, *pst_POSTITHISTO_PRIMARY;
#pragma pack()

/* prejudicedossier */
#pragma pack(1)
typedef struct _stPREJUDICEDOSSIER
{
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	typeprejunum;	/*typeprejunum*/
	__int64	dossprejunum;	/*dossprejunum*/
	__int64	dossprejudatedebut;	/*dossprejudatedebut*/
	__int64	dossprejudatefin;	/*dossprejudatefin*/
	float	dossprejucout;	/*dossprejucout*/
	__int64	dossprejucommentaire;	/*dossprejucommentaire*/
} stPREJUDICEDOSSIER, *pstPREJUDICEDOSSIER;
#pragma pack()

/* prejudicedossier.PRIMARY */
#pragma pack(1)
typedef struct _st_PREJUDICEDOSSIER_PRIMARY
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
	__int64	typeprejunum;	/*typeprejunum*/
} st_PREJUDICEDOSSIER_PRIMARY, *pst_PREJUDICEDOSSIER_PRIMARY;
#pragma pack()

/* prejudicedossier.FK_prejudicedossier */
#pragma pack(1)
typedef struct _st_PREJUDICEDOSSIER_FK_PREJUDICEDOSSIER
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_PREJUDICEDOSSIER_FK_PREJUDICEDOSSIER, *pst_PREJUDICEDOSSIER_FK_PREJUDICEDOSSIER;
#pragma pack()

/* profil */
#pragma pack(1)
typedef struct _stPROFIL
{
	__int64	profilnum;	/*profilnum*/
	char	profilcode[20+1];	/*profilcode*/
	__int64	profildatecree;	/*profildatecree*/
	__int64	profildatemaj;	/*profildatemaj*/
	__int64	profildatesuppr;	/*profildatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stPROFIL, *pstPROFIL;
#pragma pack()

/* qualite */
#pragma pack(1)
typedef struct _stQUALITE
{
	char	qualitecode[2+1];	/*qualitecode*/
	char	qualitelibelle[50+1];	/*qualitelibelle*/
	__int64	qualitedatecree;	/*qualitedatecree*/
	__int64	qualitedatemaj;	/*qualitedatemaj*/
	__int64	qualitedatesuppr;	/*qualitedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stQUALITE, *pstQUALITE;
#pragma pack()

/* reglement */
#pragma pack(1)
typedef struct _stREGLEMENT
{
	char	reglementnum[20+1];	/*reglementnum*/
	__int64	compteid;	/*compteid*/
	char	type_piece[2+1];	/*type_piece*/
	char	tresorcode[6+1];	/*tresorcode*/
	__int64	reglementdatesaisie;	/*reglementdatesaisie*/
	__int64	reglementdatemaj;	/*reglementdatemaj*/
	__int64	reglementdatesuppr;	/*reglementdatesuppr*/
	__int64	reglementdatetransfert;	/*reglementdatetransfert*/
	char	reglementmoderegl[20+1];	/*reglementmoderegl*/
	char	reglementreference[30+1];	/*reglementreference*/
	char	reglementlibelle[60+1];	/*reglementlibelle*/
	float	reglementtotalregle;	/*reglementtotalregle*/
	char	reglementcodeecart[2+1];	/*reglementcodeecart*/
	char	reglementcoderemise[2+1];	/*reglementcoderemise*/
	float	reglementmtecart;	/*reglementmtecart*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	char	agence[6+1];	/*agence*/
	char	cabinet[6+1];	/*cabinet*/
	__int64	payeurnum;	/*payeurnum*/
	char	codepayeur[38+1];	/*codepayeur*/
	__int64	nompayeur;	/*nompayeur*/
	__int64	numpersassur;	/*numpersassur*/
	char	codeassur[38+1];	/*codeassur*/
	__int64	nomassur;	/*nomassur*/
	char	dossier[12+1];	/*dossier*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	char	refcompta[50+1];	/*refcompta*/
	char	police[32+1];	/*police*/
	char	sinistre[50+1];	/*sinistre*/
	char	collab1[6+1];	/*collab1*/
	char	collab2[6+1];	/*collab2*/
	char	collab3[6+1];	/*collab3*/
	char	collab4[6+1];	/*collab4*/
	char	collab5[6+1];	/*collab5*/
	char	secretaire[6+1];	/*secretaire*/
	char	secretaire2[6+1];	/*secretaire2*/
	double	mtcollab1;	/*mtcollab1*/
	double	mtcollab2;	/*mtcollab2*/
	double	mtcollab3;	/*mtcollab3*/
	double	mtcollab4;	/*mtcollab4*/
	double	mtcollab5;	/*mtcollab5*/
	double	mtfraiscollab1;	/*mtfraiscollab1*/
	double	mtfraiscollab2;	/*mtfraiscollab2*/
	double	mtfraiscollab3;	/*mtfraiscollab3*/
	double	mtfraiscollab4;	/*mtfraiscollab4*/
	double	mtfraiscollab5;	/*mtfraiscollab5*/
	double	totalht;	/*totalht*/
	double	totalfraisht;	/*totalfraisht*/
	double	totalhonoht;	/*totalhonoht*/
	double	mtfacture;	/*mtfacture*/
	double	mtreste;	/*mtreste*/
	__int64	datesinistre;	/*datesinistre*/
	__int64	datehono;	/*datehono*/
	__int64	datepiece;	/*datepiece*/
	__int64	dateche;	/*dateche*/
	__int64	datlettrage;	/*datlettrage*/
	__int64	daterelanc;	/*daterelanc*/
	__int64	datecompta;	/*datecompta*/
	char	lettre[2+1];	/*lettre*/
	char	numrelanc[2+1];	/*numrelanc*/
	char	factsolde;	/*factsolde*/
	char	pointtemp;	/*pointtemp*/
	char	flagetat[1+1];	/*flagetat*/
	char	flagtemp[1+1];	/*flagtemp*/
	char	operateur[6+1];	/*operateur*/
} stREGLEMENT, *pstREGLEMENT;
#pragma pack()

/* reglement.PRIMARY */
#pragma pack(1)
typedef struct _st_REGLEMENT_PRIMARY
{
	char	reglementnum[20];	/*reglementnum*/
	__int64	compteid;	/*compteid*/
	char	type_piece[2];	/*type_piece*/
} st_REGLEMENT_PRIMARY, *pst_REGLEMENT_PRIMARY;
#pragma pack()

/* reglement.FK_DOSSIER */
#pragma pack(1)
typedef struct _st_REGLEMENT_FK_DOSSIER
{
	char	dossier[12];	/*dossier*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_REGLEMENT_FK_DOSSIER, *pst_REGLEMENT_FK_DOSSIER;
#pragma pack()

/* reglementligne */
#pragma pack(1)
typedef struct _stREGLEMENTLIGNE
{
	char	reglementnum[20+1];	/*reglementnum*/
	__int64	compteid;	/*compteid*/
	char	type_piece[2+1];	/*type_piece*/
	char	hononumfacture[20+1];	/*hononumfacture*/
	__int64	reglementligne;	/*reglementligne*/
	float	regleentlignemtregle;	/*regleentlignemtregle*/
	float	reglementlignemtreste;	/*reglementlignemtreste*/
	char	reglementlignefactsolde;	/*reglementlignefactsolde*/
	float	reglementlignemtecart;	/*reglementlignemtecart*/
	char	reglementlignefactregle;	/*reglementlignefactregle*/
} stREGLEMENTLIGNE, *pstREGLEMENTLIGNE;
#pragma pack()

/* reglementligne.PRIMARY */
#pragma pack(1)
typedef struct _st_REGLEMENTLIGNE_PRIMARY
{
	char	reglementnum[20];	/*reglementnum*/
	__int64	compteid;	/*compteid*/
	char	type_piece[2];	/*type_piece*/
	char	hononumfacture[20];	/*hononumfacture*/
} st_REGLEMENTLIGNE_PRIMARY, *pst_REGLEMENTLIGNE_PRIMARY;
#pragma pack()

/* reglementligne.FK_reglementligne2 */
#pragma pack(1)
typedef struct _st_REGLEMENTLIGNE_FK_REGLEMENTLIGNE2
{
	char	reglementnum[20];	/*reglementnum*/
	__int64	compteid;	/*compteid*/
	char	type_piece[2];	/*type_piece*/
} st_REGLEMENTLIGNE_FK_REGLEMENTLIGNE2, *pst_REGLEMENTLIGNE_FK_REGLEMENTLIGNE2;
#pragma pack()

/* reglementlignetamp */
#pragma pack(1)
typedef struct _stREGLEMENTLIGNETAMP
{
	char	reglementnum[20+1];	/*reglementnum*/
	__int64	compteid;	/*compteid*/
	char	type_piece[2+1];	/*type_piece*/
	char	hononumfacture[20+1];	/*hononumfacture*/
	__int64	reglementligne;	/*reglementligne*/
	float	regleentlignemtregle;	/*regleentlignemtregle*/
	float	reglementlignemtreste;	/*reglementlignemtreste*/
	char	reglementlignefactsolde;	/*reglementlignefactsolde*/
	float	reglementlignemtecart;	/*reglementlignemtecart*/
	char	reglementlignefactregle;	/*reglementlignefactregle*/
	char	reglementlignedatesaisie[8+1];	/*reglementlignedatesaisie*/
	char	codepayeur[50+1];	/*codepayeur*/
	double	mtfacture;	/*mtfacture*/
} stREGLEMENTLIGNETAMP, *pstREGLEMENTLIGNETAMP;
#pragma pack()

/* reglementlignetamp.PRIMARY */
#pragma pack(1)
typedef struct _st_REGLEMENTLIGNETAMP_PRIMARY
{
	char	reglementnum[20];	/*reglementnum*/
	__int64	compteid;	/*compteid*/
	char	type_piece[2];	/*type_piece*/
	char	hononumfacture[20];	/*hononumfacture*/
} st_REGLEMENTLIGNETAMP_PRIMARY, *pst_REGLEMENTLIGNETAMP_PRIMARY;
#pragma pack()

/* reglementparam */
#pragma pack(1)
typedef struct _stREGLEMENTPARAM
{
	__int64	rglparamnum;	/*rglparamnum*/
	char	tresorcode[6+1];	/*tresorcode*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	rglparamcomptautilisee[20+1];	/*rglparamcomptautilisee*/
	float	rglparammtdiff;	/*rglparammtdiff*/
	char	rglparamcomptedebit[15+1];	/*rglparamcomptedebit*/
	char	rglparamcomptecredit[15+1];	/*rglparamcomptecredit*/
	__int64	rglparamdatedernmvt;	/*rglparamdatedernmvt*/
	__int64	rglparamdatedermaj;	/*rglparamdatedermaj*/
	char	rglparamdernjournal[6+1];	/*rglparamdernjournal*/
	float	rglparamdernmontant;	/*rglparamdernmontant*/
	char	rglparammoderegle[20+1];	/*rglparammoderegle*/
	__int64	rglparamdelairegle;	/*rglparamdelairegle*/
	__int64	rglparamdatecree;	/*rglparamdatecree*/
	__int64	rglparamdatemaj;	/*rglparamdatemaj*/
	__int64	rglparamdatesuppr;	/*rglparamdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	char	rglparamgestdiff[1+1];	/*rglparamgestdiff*/
} stREGLEMENTPARAM, *pstREGLEMENTPARAM;
#pragma pack()

/* reglementparam.FK_par_cabinet */
#pragma pack(1)
typedef struct _st_REGLEMENTPARAM_FK_PAR_CABINET
{
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
} st_REGLEMENTPARAM_FK_PAR_CABINET, *pst_REGLEMENTPARAM_FK_PAR_CABINET;
#pragma pack()

/* relance */
#pragma pack(1)
typedef struct _stRELANCE
{
	char	hononumfacture[20+1];	/*hononumfacture*/
	long	relanceligne;	/*relanceligne*/
	__int64	secretnum;	/*secretnum*/
	char	relancetype[10+1];	/*relancetype*/
	__int64	relancedate;	/*relancedate*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stRELANCE, *pstRELANCE;
#pragma pack()

/* repartitiontemp */
#pragma pack(1)
typedef struct _stREPARTITIONTEMP
{
	long	idutil;	/*IDUTIL*/
	char	codesinistre[50+1];	/*CODESINISTRE*/
	long	numassureur;	/*NUMASSUREUR*/
	char	codepostal[50+1];	/*CODEPOSTAL*/
	char	nomsinistre[50+1];	/*NOMSINISTRE*/
	char	nomassureur[50+1];	/*NOMASSUREUR*/
	char	date[50+1];	/*DATE*/
	double	mthono;	/*MTHONO*/
	double	mtfrais;	/*MTFRAIS*/
	char	dossier[50+1];	/*DOSSIER*/
	char	codeassureur[50+1];	/*CODEASSUREUR*/
} stREPARTITIONTEMP, *pstREPARTITIONTEMP;
#pragma pack()

/* reunion */
#pragma pack(1)
typedef struct _stREUNION
{
	char	reunionnum[20+1];	/*reunionnum*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	char	reuniondate[8+1];	/*reuniondate*/
	char	reunionheure[6+1];	/*reunionheure*/
	__int64	reunionsujet;	/*reunionsujet*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	__int64	reuniondatecree;	/*reuniondatecree*/
	__int64	reuniondatemaj;	/*reuniondatemaj*/
	__int64	reuniondatesuppr;	/*reuniondatesuppr*/
} stREUNION, *pstREUNION;
#pragma pack()

/* reunion.FK_convoque */
#pragma pack(1)
typedef struct _st_REUNION_FK_CONVOQUE
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_REUNION_FK_CONVOQUE, *pst_REUNION_FK_CONVOQUE;
#pragma pack()

/* rib */
#pragma pack(1)
typedef struct _stRIB
{
	char	cbtcode[20+1];	/*cbtcode*/
	__int64	ribnum;	/*ribnum*/
	char	ribbanque[6+1];	/*ribbanque*/
	char	ribindicatif[6+1];	/*ribindicatif*/
	char	ribnumcompte[15+1];	/*ribnumcompte*/
	char	ribcle[2+1];	/*ribcle*/
	char	ribdomiciliation[38+1];	/*ribdomiciliation*/
	char	ribiban[35+1];	/*ribiban*/
	char	ribbic[10+1];	/*ribbic*/
	__int64	ribdatecree;	/*ribdatecree*/
	__int64	ribdatemaj;	/*ribdatemaj*/
	__int64	ribdatesuppr;	/*ribdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	char	tresorcode[6+1];	/*tresorcode*/
} stRIB, *pstRIB;
#pragma pack()

/* secretaire */
#pragma pack(1)
typedef struct _stSECRETAIRE
{
	__int64	secretnum;	/*secretnum*/
	__int64	adminnum;	/*adminnum*/
	__int64	idparametreutilisateur;	/*idparametreutilisateur*/
	char	secretcode[10+1];	/*secretcode*/
	char	secretemail[60+1];	/*secretemail*/
	__int64	secretdateentree;	/*secretdateentree*/
	__int64	secretdatesortie;	/*secretdatesortie*/
	__int64	secretdatecree;	/*secretdatecree*/
	__int64	secretdatemaj;	/*secretdatemaj*/
	__int64	secretdatesuppr;	/*secretdatesuppr*/
	char	secretaffiche;	/*secretaffiche*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	__int64	secretinfosecu;	/*secretinfosecu*/
} stSECRETAIRE, *pstSECRETAIRE;
#pragma pack()

/* secretcommission */
#pragma pack(1)
typedef struct _stSECRETCOMMISSION
{
	__int64	secretcom_id;	/*secretcom_id*/
	__int64	secretnum;	/*secretnum*/
	char	hononumfacture[20+1];	/*hononumfacture*/
	float	compourcent;	/*compourcent*/
	float	commontant;	/*commontant*/
} stSECRETCOMMISSION, *pstSECRETCOMMISSION;
#pragma pack()

/* sinistre */
#pragma pack(1)
typedef struct _stSINISTRE
{
	char	sinistrecode[38+1];	/*sinistrecode*/
	char	metiercode[20+1];	/*metiercode*/
	char	metierpwd[20+1];	/*metierpwd*/
	char	famillecode[20+1];	/*famillecode*/
	char	sinistrelibelle[50+1];	/*sinistrelibelle*/
	char	sinistreplafond[1+1];	/*sinistreplafond*/
	__int64	sinistredatecree;	/*sinistredatecree*/
	__int64	sinistredatemaj;	/*sinistredatemaj*/
	__int64	sinistredatesuppr;	/*sinistredatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stSINISTRE, *pstSINISTRE;
#pragma pack()

/* sinistre.FK_sinistremetier */
#pragma pack(1)
typedef struct _st_SINISTRE_FK_SINISTREMETIER
{
	char	metiercode[20];	/*metiercode*/
	char	metierpwd[20];	/*metierpwd*/
} st_SINISTRE_FK_SINISTREMETIER, *pst_SINISTRE_FK_SINISTREMETIER;
#pragma pack()

/* sinistrepersonne */
#pragma pack(1)
typedef struct _stSINISTREPERSONNE
{
	__int64	persnum;	/*persnum*/
	char	sinistrecode[38+1];	/*sinistrecode*/
	char	sinistrecorres[38+1];	/*sinistrecorres*/
} stSINISTREPERSONNE, *pstSINISTREPERSONNE;
#pragma pack()

/* sinistrepersonne.PRIMARY */
#pragma pack(1)
typedef struct _st_SINISTREPERSONNE_PRIMARY
{
	__int64	persnum;	/*persnum*/
	char	sinistrecode[38];	/*sinistrecode*/
} st_SINISTREPERSONNE_PRIMARY, *pst_SINISTREPERSONNE_PRIMARY;
#pragma pack()

/* situationjuridique */
#pragma pack(1)
typedef struct _stSITUATIONJURIDIQUE
{
	char	situationjuricode[10+1];	/*situationjuricode*/
	char	situationjurilibelle[30+1];	/*situationjurilibelle*/
	__int64	situationdatemaj;	/*situationdatemaj*/
	__int64	situationdatecree;	/*situationdatecree*/
	__int64	situationdatesuppr;	/*situationdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stSITUATIONJURIDIQUE, *pstSITUATIONJURIDIQUE;
#pragma pack()

/* specialite */
#pragma pack(1)
typedef struct _stSPECIALITE
{
	char	specialitecode[10+1];	/*specialitecode*/
	char	specialitelibelle[50+1];	/*specialitelibelle*/
	__int64	specialitedatecree;	/*specialitedatecree*/
	__int64	specialitedatemaj;	/*specialitedatemaj*/
	__int64	specialitedatesuppr;	/*specialitedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stSPECIALITE, *pstSPECIALITE;
#pragma pack()

/* statparammaif */
#pragma pack(1)
typedef struct _stSTATPARAMMAIF
{
	char	statparammaifcode[38+1];	/*statparammaifcode*/
	__int64	statparammaifcolonne;	/*statparammaifcolonne*/
	char	sinistrecode[38+1];	/*sinistrecode*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	__int64	statparammaifdatecree;	/*statparammaifdatecree*/
	__int64	statparammaifdatesuppr;	/*statparammaifdatesuppr*/
} stSTATPARAMMAIF, *pstSTATPARAMMAIF;
#pragma pack()

/* statparammaif.PRIMARY */
#pragma pack(1)
typedef struct _st_STATPARAMMAIF_PRIMARY
{
	char	statparammaifcode[38];	/*statparammaifcode*/
	__int64	statparammaifcolonne;	/*statparammaifcolonne*/
	char	sinistrecode[38];	/*sinistrecode*/
} st_STATPARAMMAIF_PRIMARY, *pst_STATPARAMMAIF_PRIMARY;
#pragma pack()

/* statut */
#pragma pack(1)
typedef struct _stSTATUT
{
	char	statutcode[2+1];	/*statutcode*/
	char	statutlibelle[50+1];	/*statutlibelle*/
	__int64	statutdatecree;	/*statutdatecree*/
	__int64	statutdatemaj;	/*statutdatemaj*/
	__int64	statutdatesuppr;	/*statutdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stSTATUT, *pstSTATUT;
#pragma pack()

/* tableaubord */
#pragma pack(1)
typedef struct _stTABLEAUBORD
{
	char	assureur[50+1];	/*ASSUREUR*/
	char	expert[50+1];	/*EXPERT*/
	char	sinistre[50+1];	/*SINISTRE*/
	long	nbencoursdeb;	/*NBENCOURSDEB*/
	long	nbrecu;	/*NBRECU*/
	long	nbclos;	/*NBCLOS*/
	long	nbencoursfin;	/*NBENCOURSFIN*/
	long	delaimoy;	/*DELAIMOY*/
	long	delaitot;	/*DELAITOT*/
	double	hono;	/*HONO*/
	double	frais;	/*FRAIS*/
	double	dommagedem;	/*DOMMAGEDEM*/
	double	dommageattribuer;	/*DOMMAGEATTRIBUER*/
	double	indemnite;	/*INDEMNITE*/
	long	dommdem;	/*DOMMDEM*/
	long	dommattrib;	/*DOMMATTRIB*/
	long	nbindemnite;	/*NBINDEMNITE*/
	long	idutil;	/*IDUTIL*/
	char	assureurcode[50+1];	/*ASSUREURCODE*/
	char	expertcode[50+1];	/*EXPERTCODE*/
	long	keytable;	/*KEYTABLE*/
} stTABLEAUBORD, *pstTABLEAUBORD;
#pragma pack()

/* tabledossiertableaubord */
#pragma pack(1)
typedef struct _stTABLEDOSSIERTABLEAUBORD
{
	long	idutil;	/*IDUTIL*/
	char	dossier[50+1];	/*DOSSIER*/
} stTABLEDOSSIERTABLEAUBORD, *pstTABLEDOSSIERTABLEAUBORD;
#pragma pack()

/* tauxhoraire */
#pragma pack(1)
typedef struct _stTAUXHORAIRE
{
	__int64	tauxhorairenum;	/*tauxhorairenum*/
	__int64	collabnum;	/*collabnum*/
	__int64	secretnum;	/*secretnum*/
	__int64	tauxhoraireligne;	/*tauxhoraireligne*/
	char	tauxhorairetypetravaux[100+1];	/*tauxhorairetypetravaux*/
	char	tauxhorairetypeligne[5+1];	/*tauxhorairetypeligne*/
	float	tauxhoraire;	/*tauxhoraire*/
	__int64	tauxhorairedatecree;	/*tauxhorairedatecree*/
	__int64	tauxhorairedatemaj;	/*tauxhorairedatemaj*/
	__int64	tauxhorairedatesuppr;	/*tauxhorairedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTAUXHORAIRE, *pstTAUXHORAIRE;
#pragma pack()

/* telbareme */
#pragma pack(1)
typedef struct _stTELBAREME
{
	char	baremecode[15+1];	/*baremecode*/
	char	corpsetatcode[6+1];	/*corpsetatcode*/
	char	tvacode[10+1];	/*tvacode*/
	char	typebaremecode[5+1];	/*typebaremecode*/
	__int64	persnumassur;	/*persnumassur*/
	__int64	persnumentreprise;	/*persnumentreprise*/
	char	baremelibellecourt[32+1];	/*baremelibellecourt*/
	char	baremelibellelong[64+1];	/*baremelibellelong*/
	float	baremepu;	/*baremepu*/
	char	baremeunite[3+1];	/*baremeunite*/
	char	baremeannee[8+1];	/*baremeannee*/
	__int64	baremedatecree;	/*baremedatecree*/
	__int64	baremedatemaj;	/*baremedatemaj*/
	__int64	baremedatesuppr;	/*baremedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTELBAREME, *pstTELBAREME;
#pragma pack()

/* telcategentr */
#pragma pack(1)
typedef struct _stTELCATEGENTR
{
	char	categentrcode[6+1];	/*categentrcode*/
	char	categlibellecourt[32+1];	/*categlibellecourt*/
	char	categlibellelong[64+1];	/*categlibellelong*/
	__int64	categentrdatecree;	/*categentrdatecree*/
	__int64	categentrdatemaj;	/*categentrdatemaj*/
	__int64	categentrdatesuppr;	/*categentrdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTELCATEGENTR, *pstTELCATEGENTR;
#pragma pack()

/* telcorpsetat */
#pragma pack(1)
typedef struct _stTELCORPSETAT
{
	char	corpsetatcode[6+1];	/*corpsetatcode*/
	char	corpsetatlibellecourt[32+1];	/*corpsetatlibellecourt*/
	char	corpsetatlibellelong[64+1];	/*corpsetatlibellelong*/
	__int64	corpsetatdatecree;	/*corpsetatdatecree*/
	__int64	corpsetatdatemaj;	/*corpsetatdatemaj*/
	__int64	corpsetatdatesuppr;	/*corpsetatdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTELCORPSETAT, *pstTELCORPSETAT;
#pragma pack()

/* telcorpsetatentr */
#pragma pack(1)
typedef struct _stTELCORPSETATENTR
{
	__int64	persnum;	/*persnum*/
	char	corpsetatcode[6+1];	/*corpsetatcode*/
} stTELCORPSETATENTR, *pstTELCORPSETATENTR;
#pragma pack()

/* telcorpsetatentr.PRIMARY */
#pragma pack(1)
typedef struct _st_TELCORPSETATENTR_PRIMARY
{
	__int64	persnum;	/*persnum*/
	char	corpsetatcode[6];	/*corpsetatcode*/
} st_TELCORPSETATENTR_PRIMARY, *pst_TELCORPSETATENTR_PRIMARY;
#pragma pack()

/* teldevis */
#pragma pack(1)
typedef struct _stTELDEVIS
{
	char	devisnum[20+1];	/*devisnum*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	histoligne;	/*histoligne*/
	__int64	persnum;	/*persnum*/
	char	docnom[30+1];	/*docnom*/
	char	secteurcode[5+1];	/*secteurcode*/
	char	devistypecloture[1+1];	/*devistypecloture*/
	char	devisreparpar[1+1];	/*devisreparpar*/
	char	deviscidre;	/*deviscidre*/
	char	devisconforme;	/*devisconforme*/
	char	devistypeventil[1+1];	/*devistypeventil*/
	__int64	devisdatecree;	/*devisdatecree*/
	__int64	devisdatemaj;	/*devisdatemaj*/
	__int64	devisdatesuppr;	/*devisdatesuppr*/
	__int64	devisdateimprime;	/*devisdateimprime*/
	__int64	deviscodetva1;	/*deviscodetva1*/
	__int64	deviscodetva2;	/*deviscodetva2*/
	__int64	deviscodetva3;	/*deviscodetva3*/
	double	devistauxtva1;	/*devistauxtva1*/
	double	devistauxtva2;	/*devistauxtva2*/
	double	devistauxtva3;	/*devistauxtva3*/
	float	devismtht1;	/*devismtht1*/
	float	devismtht2;	/*devismtht2*/
	float	devismtht3;	/*devismtht3*/
	float	devismttva1;	/*devismttva1*/
	float	devismttva2;	/*devismttva2*/
	float	devismttva3;	/*devismttva3*/
	float	devismtttc1;	/*devismtttc1*/
	float	devismtttc2;	/*devismtttc2*/
	float	devismtttc3;	/*devismtttc3*/
	float	devistotalht;	/*devistotalht*/
	float	devistotaltva;	/*devistotaltva*/
	float	devistotalttc;	/*devistotalttc*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	float	devistotalmo;	/*devistotalmo*/
	float	devistotalmat;	/*devistotalmat*/
	float	devistotalmob;	/*devistotalmob*/
	float	devistotalembel;	/*devistotalembel*/
	double	devistauxassure;	/*devistauxassure*/
	double	devistauxvetuste;	/*devistauxvetuste*/
} stTELDEVIS, *pstTELDEVIS;
#pragma pack()

/* teldevis.FK_dossierteldevis */
#pragma pack(1)
typedef struct _st_TELDEVIS_FK_DOSSIERTELDEVIS
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_TELDEVIS_FK_DOSSIERTELDEVIS, *pst_TELDEVIS_FK_DOSSIERTELDEVIS;
#pragma pack()

/* teldevis.FK_teldevishisto */
#pragma pack(1)
typedef struct _st_TELDEVIS_FK_TELDEVISHISTO
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
	__int64	histoligne;	/*histoligne*/
} st_TELDEVIS_FK_TELDEVISHISTO, *pst_TELDEVIS_FK_TELDEVISHISTO;
#pragma pack()

/* teldevisligne */
#pragma pack(1)
typedef struct _stTELDEVISLIGNE
{
	char	devisnum[20+1];	/*devisnum*/
	__int64	devisligne;	/*devisligne*/
	char	tvacode[10+1];	/*tvacode*/
	char	baremecode[15+1];	/*baremecode*/
	char	typebaremecode[5+1];	/*typebaremecode*/
	char	localisationcode[5+1];	/*localisationcode*/
	char	corpsetatcode[6+1];	/*corpsetatcode*/
	char	devislignelibellebareme[32+1];	/*devislignelibellebareme*/
	char	devislignelibelle[60+1];	/*devislignelibelle*/
	char	devisligneunite[3+1];	/*devisligneunite*/
	double	devislignelongueur;	/*devislignelongueur*/
	double	devislignehauteur;	/*devislignehauteur*/
	float	devislignepu;	/*devislignepu*/
	char	devislignecondit[25+1];	/*devislignecondit*/
	float	devislignemtht;	/*devislignemtht*/
	float	devislignepupondere;	/*devislignepupondere*/
	double	devislignetauxpondere;	/*devislignetauxpondere*/
	__int64	devislignedatecree;	/*devislignedatecree*/
	__int64	devislignedatemaj;	/*devislignedatemaj*/
	__int64	devislignedatesuppr;	/*devislignedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTELDEVISLIGNE, *pstTELDEVISLIGNE;
#pragma pack()

/* teldevisligne.PRIMARY */
#pragma pack(1)
typedef struct _st_TELDEVISLIGNE_PRIMARY
{
	char	devisnum[20];	/*devisnum*/
	__int64	devisligne;	/*devisligne*/
} st_TELDEVISLIGNE_PRIMARY, *pst_TELDEVISLIGNE_PRIMARY;
#pragma pack()

/* telentrassureur */
#pragma pack(1)
typedef struct _stTELENTRASSUREUR
{
	__int64	persnumentreprise;	/*persnumentreprise*/
	__int64	persnumassur;	/*persnumassur*/
} stTELENTRASSUREUR, *pstTELENTRASSUREUR;
#pragma pack()

/* telentrassureur.PRIMARY */
#pragma pack(1)
typedef struct _st_TELENTRASSUREUR_PRIMARY
{
	__int64	persnumentreprise;	/*persnumentreprise*/
	__int64	persnumassur;	/*persnumassur*/
} st_TELENTRASSUREUR_PRIMARY, *pst_TELENTRASSUREUR_PRIMARY;
#pragma pack()

/* telephone */
#pragma pack(1)
typedef struct _stTELEPHONE
{
	__int64	telnum;	/*telnum*/
	char	typetelcode[2+1];	/*typetelcode*/
	__int64	adminnum;	/*adminnum*/
	char	telnumero[20+1];	/*telnumero*/
	__int64	teldatecree;	/*teldatecree*/
	__int64	teldatemaj;	/*teldatemaj*/
	__int64	teldatesuppr;	/*teldatesuppr*/
} stTELEPHONE, *pstTELEPHONE;
#pragma pack()

/* tellocalisation */
#pragma pack(1)
typedef struct _stTELLOCALISATION
{
	char	localisationcode[5+1];	/*localisationcode*/
	char	localisationlibelle[50+1];	/*localisationlibelle*/
	__int64	localisationdatesuppr;	/*localisationdatesuppr*/
	__int64	localisationdatemaj;	/*localisationdatemaj*/
	__int64	localisationdatecree;	/*localisationdatecree*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTELLOCALISATION, *pstTELLOCALISATION;
#pragma pack()

/* telponderation */
#pragma pack(1)
typedef struct _stTELPONDERATION
{
	char	ponderationannee[8+1];	/*ponderationannee*/
	__int64	secteurnum;	/*secteurnum*/
	char	secteurcode[5+1];	/*secteurcode*/
	char	corpsetatcode[6+1];	/*corpsetatcode*/
	__int64	persnum;	/*persnum*/
	double	ponderationtaux;	/*ponderationtaux*/
	__int64	ponderationdatecree;	/*ponderationdatecree*/
	__int64	ponderationdatemaj;	/*ponderationdatemaj*/
	__int64	ponderationdatesuppr;	/*ponderationdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTELPONDERATION, *pstTELPONDERATION;
#pragma pack()

/* telponderation.PRIMARY */
#pragma pack(1)
typedef struct _st_TELPONDERATION_PRIMARY
{
	char	ponderationannee[8];	/*ponderationannee*/
	__int64	secteurnum;	/*secteurnum*/
	char	secteurcode[5];	/*secteurcode*/
	char	corpsetatcode[6];	/*corpsetatcode*/
	__int64	persnum;	/*persnum*/
} st_TELPONDERATION_PRIMARY, *pst_TELPONDERATION_PRIMARY;
#pragma pack()

/* telsecteurentr */
#pragma pack(1)
typedef struct _stTELSECTEURENTR
{
	__int64	secteurnum;	/*secteurnum*/
	char	secteurcode[5+1];	/*secteurcode*/
	__int64	persnum;	/*persnum*/
} stTELSECTEURENTR, *pstTELSECTEURENTR;
#pragma pack()

/* telsecteurentr.PRIMARY */
#pragma pack(1)
typedef struct _st_TELSECTEURENTR_PRIMARY
{
	__int64	secteurnum;	/*secteurnum*/
	char	secteurcode[5];	/*secteurcode*/
	__int64	persnum;	/*persnum*/
} st_TELSECTEURENTR_PRIMARY, *pst_TELSECTEURENTR_PRIMARY;
#pragma pack()

/* teltypebareme */
#pragma pack(1)
typedef struct _stTELTYPEBAREME
{
	char	typebaremecode[5+1];	/*typebaremecode*/
	char	typebaremelibellecourt[32+1];	/*typebaremelibellecourt*/
	char	typebaremelibellelong[64+1];	/*typebaremelibellelong*/
	__int64	typebaremedatecree;	/*typebaremedatecree*/
	__int64	typebaremedatemaj;	/*typebaremedatemaj*/
	__int64	typebaremedatesuppr;	/*typebaremedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	long	typebaremecumul;	/*typebaremecumul*/
} stTELTYPEBAREME, *pstTELTYPEBAREME;
#pragma pack()

/* temppartage */
#pragma pack(1)
typedef struct _stTEMPPARTAGE
{
	__int64	tempid;	/*tempid*/
	char	dossnum[20+1];	/*dossnum*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	char	cbtcode2[20+1];	/*cbtcode2*/
	char	cbttype2[2+1];	/*cbttype2*/
	__int64	idparametreutilisateur;	/*idparametreutilisateur*/
	__int64	datetimedebut;	/*datetimedebut*/
	__int64	datetimefin;	/*datetimefin*/
} stTEMPPARTAGE, *pstTEMPPARTAGE;
#pragma pack()

/* temppartage.dossier_key */
#pragma pack(1)
typedef struct _st_TEMPPARTAGE_DOSSIER_KEY
{
	char	dossnum[20];	/*dossnum*/
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
	char	cbtcode2[20];	/*cbtcode2*/
	char	cbttype2[2];	/*cbttype2*/
} st_TEMPPARTAGE_DOSSIER_KEY, *pst_TEMPPARTAGE_DOSSIER_KEY;
#pragma pack()

/* ticketmoderateur */
#pragma pack(1)
typedef struct _stTICKETMODERATEUR
{
	char	ticketannee[8+1];	/*ticketannee*/
	float	ticketmt;	/*ticketmt*/
	__int64	ticketdatemaj;	/*ticketdatemaj*/
	__int64	ticketdatecree;	/*ticketdatecree*/
	__int64	ticketdatesuppr;	/*ticketdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTICKETMODERATEUR, *pstTICKETMODERATEUR;
#pragma pack()

/* tresorerie */
#pragma pack(1)
typedef struct _stTRESORERIE
{
	char	tresorcode[6+1];	/*tresorcode*/
	__int64	collabnum;	/*collabnum*/
	char	hononumfacture[20+1];	/*hononumfacture*/
	char	tresorlibelle[50+1];	/*tresorlibelle*/
	__int64	tresordatedermvt;	/*tresordatedermvt*/
	char	tresormodeventil[10+1];	/*tresormodeventil*/
	char	tresornumcompte[15+1];	/*tresornumcompte*/
	__int64	tresordatecree;	/*tresordatecree*/
	__int64	tresordatemaj;	/*tresordatemaj*/
	__int64	tresordatesuppr;	/*tresordatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTRESORERIE, *pstTRESORERIE;
#pragma pack()

/* tva */
#pragma pack(1)
typedef struct _stTVA
{
	char	tvacode[10+1];	/*tvacode*/
	char	cbtcode[20+1];	/*cbtcode*/
	char	cbttype[2+1];	/*cbttype*/
	float	tvataux;	/*tvataux*/
	char	tvalibelle[32+1];	/*tvalibelle*/
	char	tvacompte[15+1];	/*tvacompte*/
	__int64	tvadatecree;	/*tvadatecree*/
	__int64	tvadatemaj;	/*tvadatemaj*/
	__int64	tvadatesuppr;	/*tvadatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTVA, *pstTVA;
#pragma pack()

/* tva.FK_cabinettva */
#pragma pack(1)
typedef struct _st_TVA_FK_CABINETTVA
{
	char	cbtcode[20];	/*cbtcode*/
	char	cbttype[2];	/*cbttype*/
} st_TVA_FK_CABINETTVA, *pst_TVA_FK_CABINETTVA;
#pragma pack()

/* typecentresecu */
#pragma pack(1)
typedef struct _stTYPECENTRESECU
{
	__int64	secunum;	/*secunum*/
	char	seculibelle[32+1];	/*seculibelle*/
	__int64	secudatecree;	/*secudatecree*/
	__int64	secudatemaj;	/*secudatemaj*/
	__int64	secudatesuppr;	/*secudatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPECENTRESECU, *pstTYPECENTRESECU;
#pragma pack()

/* typecontrat */
#pragma pack(1)
typedef struct _stTYPECONTRAT
{
	char	typecontratcode[20+1];	/*typecontratcode*/
	char	typecontratlibelle[80+1];	/*typecontratlibelle*/
	__int64	typecontratdatecree;	/*typecontratdatecree*/
	__int64	typecontratdatemaj;	/*typecontratdatemaj*/
	__int64	typecontratdatesuppr;	/*typecontratdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPECONTRAT, *pstTYPECONTRAT;
#pragma pack()

/* typedocument */
#pragma pack(1)
typedef struct _stTYPEDOCUMENT
{
	char	typedoccode[6+1];	/*typedoccode*/
	char	typedoclibelle[32+1];	/*typedoclibelle*/
	__int64	typedocdatecree;	/*typedocdatecree*/
	__int64	typedocdatemaj;	/*typedocdatemaj*/
	__int64	typedocdatesuppr;	/*typedocdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPEDOCUMENT, *pstTYPEDOCUMENT;
#pragma pack()

/* typeexpertise */
#pragma pack(1)
typedef struct _stTYPEEXPERTISE
{
	char	typeexpertisecode[25+1];	/*typeexpertisecode*/
	char	metiercode[20+1];	/*metiercode*/
	char	metierpwd[20+1];	/*metierpwd*/
	char	typeexpertiselibelle[50+1];	/*typeexpertiselibelle*/
	__int64	typeexpertisedatecree;	/*typeexpertisedatecree*/
	__int64	typeexpertisedatemaj;	/*typeexpertisedatemaj*/
	__int64	typeexpertisedatesuppr;	/*typeexpertisedatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPEEXPERTISE, *pstTYPEEXPERTISE;
#pragma pack()

/* typeexpertise.FK_metiertypeexpertise */
#pragma pack(1)
typedef struct _st_TYPEEXPERTISE_FK_METIERTYPEEXPERTISE
{
	char	metiercode[20];	/*metiercode*/
	char	metierpwd[20];	/*metierpwd*/
} st_TYPEEXPERTISE_FK_METIERTYPEEXPERTISE, *pst_TYPEEXPERTISE_FK_METIERTYPEEXPERTISE;
#pragma pack()

/* typegarantie */
#pragma pack(1)
typedef struct _stTYPEGARANTIE
{
	char	garantcode[5+1];	/*garantcode*/
	char	garantlibelle[50+1];	/*garantlibelle*/
} stTYPEGARANTIE, *pstTYPEGARANTIE;
#pragma pack()

/* typeindemnite */
#pragma pack(1)
typedef struct _stTYPEINDEMNITE
{
	char	typeindemncode[6+1];	/*typeindemncode*/
	char	typeindemnlibelle[32+1];	/*typeindemnlibelle*/
	__int64	typeindemndatecree;	/*typeindemndatecree*/
	__int64	typeindemndatemaj;	/*typeindemndatemaj*/
	__int64	typeindemndatesuppr;	/*typeindemndatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPEINDEMNITE, *pstTYPEINDEMNITE;
#pragma pack()

/* typelesion */
#pragma pack(1)
typedef struct _stTYPELESION
{
	char	lesioncode[4+1];	/*lesioncode*/
	char	lesionlibelle[65+1];	/*lesionlibelle*/
	__int64	lesiondatecree;	/*lesiondatecree*/
	__int64	lesiondatemaj;	/*lesiondatemaj*/
	__int64	lesiondatesuppr;	/*lesiondatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPELESION, *pstTYPELESION;
#pragma pack()

/* typemission */
#pragma pack(1)
typedef struct _stTYPEMISSION
{
	char	typemisisoncode[10+1];	/*typemisisoncode*/
	char	typemissionlibelle[50+1];	/*typemissionlibelle*/
	__int64	typemissiondatecree;	/*typemissiondatecree*/
	__int64	typemissiondatemaj;	/*typemissiondatemaj*/
	__int64	typemissiondatesuppr;	/*typemissiondatesuppr*/
	char	typemissionconvo[12+1];	/*typemissionconvo*/
	char	typemissionrapport[12+1];	/*typemissionrapport*/
	char	typemissionhono[12+1];	/*typemissionhono*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPEMISSION, *pstTYPEMISSION;
#pragma pack()

/* typemissionconvocation */
#pragma pack(1)
typedef struct _stTYPEMISSIONCONVOCATION
{
	__int64	collabnum;	/*collabnum*/
	char	docnom[30+1];	/*docnom*/
	char	typemisisoncode[10+1];	/*typemisisoncode*/
} stTYPEMISSIONCONVOCATION, *pstTYPEMISSIONCONVOCATION;
#pragma pack()

/* typemissionconvocation.PRIMARY */
#pragma pack(1)
typedef struct _st_TYPEMISSIONCONVOCATION_PRIMARY
{
	__int64	collabnum;	/*collabnum*/
	char	docnom[30];	/*docnom*/
	char	typemisisoncode[10];	/*typemisisoncode*/
} st_TYPEMISSIONCONVOCATION_PRIMARY, *pst_TYPEMISSIONCONVOCATION_PRIMARY;
#pragma pack()

/* typepersonne */
#pragma pack(1)
typedef struct _stTYPEPERSONNE
{
	char	typeperscode[10+1];	/*typeperscode*/
	char	typeperslibelle[32+1];	/*typeperslibelle*/
	char	typeperscategorie[40+1];	/*typeperscategorie*/
	__int64	typepersdatecree;	/*typepersdatecree*/
	__int64	typepersdatemaj;	/*typepersdatemaj*/
	__int64	typepersdatesuppr;	/*typepersdatesuppr*/
	char	typepersbloque;	/*typepersbloque*/
	__int64	typepersnum;	/*typepersnum*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	char	typepersmodifiable;	/*typepersmodifiable*/
} stTYPEPERSONNE, *pstTYPEPERSONNE;
#pragma pack()

/* typepi */
#pragma pack(1)
typedef struct _stTYPEPI
{
	__int64	typepinum;	/*typepinum*/
	char	typepilibelle[50+1];	/*typepilibelle*/
	__int64	typepidatecree;	/*typepidatecree*/
	__int64	typepidatemaj;	/*typepidatemaj*/
	__int64	typepidatesuppr;	/*typepidatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPEPI, *pstTYPEPI;
#pragma pack()

/* typepipar */
#pragma pack(1)
typedef struct _stTYPEPIPAR
{
	__int64	typepiparnum;	/*typepiparnum*/
	char	typepiparlibelle[50+1];	/*typepiparlibelle*/
	__int64	typepipardatecree;	/*typepipardatecree*/
	__int64	typepipardatemaj;	/*typepipardatemaj*/
	__int64	typepipardatesuppr;	/*typepipardatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPEPIPAR, *pstTYPEPIPAR;
#pragma pack()

/* typeprejudice */
#pragma pack(1)
typedef struct _stTYPEPREJUDICE
{
	__int64	typeprejunum;	/*typeprejunum*/
	char	typeprejucode[5+1];	/*typeprejucode*/
	char	typeprejulibelle[120+1];	/*typeprejulibelle*/
	char	typeprejueco;	/*typeprejueco*/
	char	typeprejutemp;	/*typeprejutemp*/
	__int64	typeprejudatecree;	/*typeprejudatecree*/
	__int64	typeprejudatemaj;	/*typeprejudatemaj*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
	__int64	typeprejudatesuppr;	/*typeprejudatesuppr*/
} stTYPEPREJUDICE, *pstTYPEPREJUDICE;
#pragma pack()

/* typeprejuest */
#pragma pack(1)
typedef struct _stTYPEPREJUEST
{
	__int64	prejuestnum;	/*prejuestnum*/
	char	prejuestlibelle[35+1];	/*prejuestlibelle*/
	__int64	prejuestdatecree;	/*prejuestdatecree*/
	__int64	prejuestdatemaj;	/*prejuestdatemaj*/
	__int64	prejuestdatesuppr;	/*prejuestdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPEPREJUEST, *pstTYPEPREJUEST;
#pragma pack()

/* typeprofession */
#pragma pack(1)
typedef struct _stTYPEPROFESSION
{
	__int64	professionnum;	/*professionnum*/
	char	professionlibelle[60+1];	/*professionlibelle*/
	__int64	professiondatecree;	/*professiondatecree*/
	__int64	professiondatemaj;	/*professiondatemaj*/
	__int64	professiondatesuppr;	/*professiondatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPEPROFESSION, *pstTYPEPROFESSION;
#pragma pack()

/* typequantumdol */
#pragma pack(1)
typedef struct _stTYPEQUANTUMDOL
{
	__int64	quantumnum;	/*quantumnum*/
	char	quantumlibelle[35+1];	/*quantumlibelle*/
	__int64	quantumdatecree;	/*quantumdatecree*/
	__int64	quantumdatemaj;	/*quantumdatemaj*/
	__int64	quantumdatesuppr;	/*quantumdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPEQUANTUMDOL, *pstTYPEQUANTUMDOL;
#pragma pack()

/* typereception */
#pragma pack(1)
typedef struct _stTYPERECEPTION
{
	char	typereceptcode[10+1];	/*typereceptcode*/
	char	typereceptlibelle[32+1];	/*typereceptlibelle*/
	__int64	typereceptdatecree;	/*typereceptdatecree*/
	__int64	typereceptdatemaj;	/*typereceptdatemaj*/
	__int64	typereceptdatesuppr;	/*typereceptdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPERECEPTION, *pstTYPERECEPTION;
#pragma pack()

/* typetelephone */
#pragma pack(1)
typedef struct _stTYPETELEPHONE
{
	char	typetelcode[2+1];	/*typetelcode*/
	char	typetellibelle[32+1];	/*typetellibelle*/
	__int64	typeteldatecree;	/*typeteldatecree*/
	__int64	typeteldatemaj;	/*typeteldatemaj*/
	__int64	typeteldatesuppr;	/*typeteldatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stTYPETELEPHONE, *pstTYPETELEPHONE;
#pragma pack()

/* usageconstruction */
#pragma pack(1)
typedef struct _stUSAGECONSTRUCTION
{
	char	usageconstcode[8+1];	/*usageconstcode*/
	char	usageconstlibelle[30+1];	/*usageconstlibelle*/
	__int64	usageconstdatecree;	/*usageconstdatecree*/
	__int64	usageconstdatemaj;	/*usageconstdatemaj*/
	__int64	usageconstdatesuppr;	/*usageconstdatesuppr*/
	__int64	paramutilnumsuppr;	/*paramutilnumsuppr*/
} stUSAGECONSTRUCTION, *pstUSAGECONSTRUCTION;
#pragma pack()


-- Script g�n�r� par WINDEV le 01/05/2017 15:33:09
-- Tables de l'analyse Mon_Projet.wda
-- pour SQL g�n�rique (ANSI 92)

-- Cr�ation de la table accueilardi
CREATE TABLE "accueilardi" (
    "cabinet" VARCHAR(20)  NOT NULL ,
    "cabinettype" VARCHAR(2)  NOT NULL ,
    "agence" VARCHAR(20)  NOT NULL ,
    "agencetype" VARCHAR(2)  NOT NULL ,
    "dossnum" VARCHAR(20)  NOT NULL ,
    "collabnum" NUMERIC(19,0) ,
    "collabcode" VARCHAR(10) ,
    "secretnum" NUMERIC(19,0) ,
    "secretcode" VARCHAR(10) ,
    "sinistrecode" VARCHAR(38) ,
    "sinistreadminnum" NUMERIC(19,0) ,
    "sinistre" VARCHAR(38) ,
    "datesinistre" DATE ,
    "assureadminnum" NUMERIC(19,0) ,
    "assure" VARCHAR(38) ,
    "mandantadminnum" NUMERIC(19,0) ,
    "mandant" VARCHAR(38) ,
    "compagnieadminnum" NUMERIC(19,0) ,
    "compagnie" VARCHAR(38) ,
    "datecloture" DATE ,
    "datemission" DATE ,
    "datereception" DATE ,
    "typereceptcode" VARCHAR(10) ,
    "lese" SMALLINT ,
    "responsable" SMALLINT ,
    "commentairesinistre" LONGVARBINARY ,
    "cbtcodelie" VARCHAR(32) ,
    "groupement" VARCHAR(32) ,
    "color" VARCHAR(7) );
CREATE INDEX "WDIDX_accueilardi_PRIMARY" ON "accueilardi" ("cabinet","cabinettype","agence","agencetype","dossnum");

-- Cr�ation de la table admin
CREATE TABLE "admin" (
    "oldpersnum" NUMERIC(19,0) ,
    "code" VARCHAR(38) ,
    "adminnum" NUMERIC(19,0)  PRIMARY KEY ,
    "civilitenum" NUMERIC(19,0) ,
    "codepostal" VARCHAR(10) ,
    "ville" VARCHAR(28) ,
    "payscode" VARCHAR(4) ,
    "nom" VARCHAR(38) ,
    "prenom" VARCHAR(38) ,
    "surnom" VARCHAR(38) ,
    "adresse1" VARCHAR(38) ,
    "adresse2" VARCHAR(38) ,
    "adresse3" VARCHAR(38) ,
    "commune" VARCHAR(38) ,
    "email" VARCHAR(60) ,
    "web" VARCHAR(60) ,
    "admindatecree" DATE ,
    "admindatemaj" DATE ,
    "admindatesuppr" DATE ,
    "datenom" DATE ,
    "datesurnom" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_admin_civilitenum" ON "admin" ("civilitenum");
CREATE INDEX "WDIDX_admin_payscode" ON "admin" ("payscode");
CREATE INDEX "WDIDX_admin_FK_admincp" ON "admin" ("codepostal","ville");

-- Cr�ation de la table adrcabinet
CREATE TABLE "adrcabinet" (
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "cbtcodelie" VARCHAR(20)  NOT NULL ,
    "cbttypelie" VARCHAR(2)  NOT NULL );
CREATE INDEX "WDIDX_adrcabinet_PRIMARY" ON "adrcabinet" ("cbtcode","cbttype","cbtcodelie","cbttypelie");
CREATE INDEX "WDIDX_adrcabinet_FK_adrcabinet2" ON "adrcabinet" ("cbtcodelie","cbttypelie");

-- Cr�ation de la table adrdossier
CREATE TABLE "adrdossier" (
    "adrdossnum" NUMERIC(19,0)  PRIMARY KEY ,
    "adrdossnumtiers" NUMERIC(19,0) ,
    "cbtcode3" VARCHAR(20) ,
    "cbttype3" VARCHAR(2) ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "persnum" NUMERIC(19,0) ,
    "typeperscode" VARCHAR(10) ,
    "collabcode" VARCHAR(10) ,
    "collabcode2" VARCHAR(10) ,
    "secretcode" VARCHAR(10) ,
    "qualitecode" VARCHAR(2) ,
    "statutcode" VARCHAR(2) ,
    "adrdossreference1" VARCHAR(50) ,
    "adrdossreference2" VARCHAR(50) ,
    "adrdossreference3" VARCHAR(50) ,
    "adrdossnumpolice" VARCHAR(50) ,
    "adrdosscommentaire" LONGVARBINARY ,
    "adrdossmttravaux" REAL ,
    "adrdossdatedebut" DATE ,
    "adrdossdatecree" DATE ,
    "adrdossdatemaj" DATE ,
    "adrdossdatefin" DATE ,
    "adrdossdatesuppr" DATE ,
    "adrdosslibelle" VARCHAR(100) ,
    "adrdossposition" NUMERIC(19,0) ,
    "adrdossmtdevis" REAL ,
    "adrdossdatedevis" DATE ,
    "adrdossmiseencause" SMALLINT ,
    "adrdossmtfranchise" REAL ,
    "adrdossmtclause" REAL ,
    "adrdosstypetiers" VARCHAR(10) ,
    "adrdosslese" SMALLINT ,
    "adrdossresponsable" SMALLINT ,
    "adrdossrar" SMALLINT ,
    "adrdossnumsinistre" VARCHAR(50) ,
    "persnumsoustraite" NUMERIC(19,0) );
CREATE INDEX "WDIDX_adrdossier_persnum" ON "adrdossier" ("persnum");
CREATE INDEX "WDIDX_adrdossier_typeperscode" ON "adrdossier" ("typeperscode");
CREATE INDEX "WDIDX_adrdossier_qualitecode" ON "adrdossier" ("qualitecode");
CREATE INDEX "WDIDX_adrdossier_statutcode" ON "adrdossier" ("statutcode");
CREATE INDEX "WDIDX_adrdossier_FK_adrdossier" ON "adrdossier" ("cbtcode3","cbttype3");
CREATE INDEX "WDIDX_adrdossier_FK_adrdossier2" ON "adrdossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table adrpersonne
CREATE TABLE "adrpersonne" (
    "adrpersonne_id" NUMERIC(19,0)  PRIMARY KEY ,
    "persnum" NUMERIC(19,0)  NOT NULL ,
    "persnumlie" NUMERIC(19,0)  NOT NULL ,
    "typeperscode" VARCHAR(10) ,
    "typeperscodelie" VARCHAR(10) ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "adrpersreference" VARCHAR(50) ,
    "datenom" DATE );
CREATE INDEX "WDIDX_adrpersonne_persnum" ON "adrpersonne" ("persnum");
CREATE INDEX "WDIDX_adrpersonne_persnumlie" ON "adrpersonne" ("persnumlie");
CREATE INDEX "WDIDX_adrpersonne_typeperscode" ON "adrpersonne" ("typeperscode");
CREATE INDEX "WDIDX_adrpersonne_typeperscodelie" ON "adrpersonne" ("typeperscodelie");
CREATE INDEX "WDIDX_adrpersonne_PRIMARY" ON "adrpersonne" ("adrpersonne_id","persnum","persnumlie");
CREATE INDEX "WDIDX_adrpersonne_FK_adrpersonne5" ON "adrpersonne" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table annexe
CREATE TABLE "annexe" (
    "annexeligne" NUMERIC(19,0)  NOT NULL ,
    "dossnum" VARCHAR(20)  NOT NULL ,
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "cbtcode2" VARCHAR(20)  NOT NULL ,
    "cbttype2" VARCHAR(2)  NOT NULL ,
    "annexetype" VARCHAR(20) ,
    "annexecheminfichier" VARCHAR(250) ,
    "annexenomfichier" VARCHAR(50) ,
    "annexelibellefichier" VARCHAR(50) ,
    "annexedatecree" DATE ,
    "annexedatemaj" DATE ,
    "annextedatesuppr" DATE  NOT NULL ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_annexe_PRIMARY" ON "annexe" ("annexeligne","dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
CREATE INDEX "WDIDX_annexe_FK_ajoutpiece" ON "annexe" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table cabinet
CREATE TABLE "cabinet" (
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "tvacode" VARCHAR(10) ,
    "procode" VARCHAR(32) ,
    "prochrono" NUMERIC(19,0) ,
    "juridiquestatut" VARCHAR(2) ,
    "natinscription" VARCHAR(1) ,
    "adminnum" NUMERIC(19,0) ,
    "cbtemail" VARCHAR(60) ,
    "cbtemail2" VARCHAR(60) ,
    "cbtweb" VARCHAR(60) ,
    "cbtcomment" LONGVARBINARY ,
    "cbtdatecree" DATE ,
    "cbtdatemaj" DATE ,
    "cbtdatesuppr" DATE ,
    "cbtchemincompta" VARCHAR(80) ,
    "cbtirdwebom" SMALLINT ,
    "cbtirdwebnh" SMALLINT ,
    "cbtirdwebre" SMALLINT ,
    "cbtirdwebsda" SMALLINT ,
    "cbtirdwebsde" SMALLINT ,
    "cbtirdwebsdr" SMALLINT ,
    "cbtirdwebah" SMALLINT ,
    "cbtirdwebdt" SMALLINT ,
    "cbtirdwebpdf" SMALLINT ,
    "cbtirdwebcoderecup" VARCHAR(60) ,
    "cbtirdwebmdprecup" VARCHAR(60) ,
    "cbtirdwebcodepdf" VARCHAR(60) ,
    "cbtirdwebmdppdf" VARCHAR(60) ,
    "cbtirdwebcodemodif" VARCHAR(60) ,
    "cbtirdwebmdpmodif" VARCHAR(60) ,
    "cbtirdwebcodeabonne" VARCHAR(60) ,
    "cbtcheminmultimedia" VARCHAR(80) ,
    "cbtcheminirdwebbase" VARCHAR(80) ,
    "cbtcheminirdwebrecup" VARCHAR(80) ,
    "cbtcheminmodeldoc" VARCHAR(80) ,
    "cbtchemindoctype" VARCHAR(80) ,
    "cbtcomptevente" VARCHAR(15) ,
    "cbtcodeanalytique" VARCHAR(20) ,
    "cbtjournalvente" VARCHAR(2) ,
    "cbtnumfolio" VARCHAR(3) ,
    "cbtplateforme" SMALLINT ,
    "cbtordremaif" INTEGER ,
    "cbtordregenerali" INTEGER ,
    "cbtcheminfichierfusion" VARCHAR(80) ,
    "cbtcheminbase" VARCHAR(80) ,
    "cbtcheminbaseinter" VARCHAR(80) ,
    "cbtchemincompactinter" VARCHAR(80) ,
    "cbtcheminbasearchive" VARCHAR(80) ,
    "cbtchemindocarchive" VARCHAR(80) ,
    "cbtcheminmultiarchive" VARCHAR(80) ,
    "cbtcodeape" VARCHAR(5) ,
    "cbtnumsiret" VARCHAR(17) ,
    "cbtrc" VARCHAR(10) ,
    "cbtmtcapital" REAL ,
    "cbtnumintra" VARCHAR(13) ,
    "cbtlocgerant" VARCHAR(1) ,
    "cbtcheminetat" VARCHAR(80) ,
    "cbtirdwebnumcertif" VARCHAR(14) ,
    "cbtcalculhono" VARCHAR(8) ,
    "cbtdelairelance" NUMERIC(19,0) ,
    "cbtarchivage" VARCHAR(8) ,
    "cbtdelaiarchivage" NUMERIC(19,0) ,
    "cbttypepointage" VARCHAR(8) ,
    "cbtversionbase" VARCHAR(5) ,
    "cbtnumparagence" SMALLINT ,
    "cbttypenumdossier" INTEGER ,
    "cbttypenumhono" INTEGER ,
    "cbttypenumrgl" INTEGER ,
    "cbtdevise" VARCHAR(10) ,
    "cbtcompourcentmt" VARCHAR(1) ,
    "cbtcombasecalcul" VARCHAR(2) ,
    "cbtcheminlogo" VARCHAR(80) ,
    "cbttypecompta" VARCHAR(1) ,
    "cbtnumcollectif" VARCHAR(30) ,
    "cbtrelance" INTEGER ,
    "cbtdateenvoistat" DATE ,
    "cbtcheminsysteme" VARCHAR(80) ,
    "cbtirdwebcodehono" VARCHAR(15) ,
    "cbtirdwebcodefrais" VARCHAR(15) ,
    "cbtstatemail" VARCHAR(60) ,
    "cbtstatcc" VARCHAR(60) ,
    "cbtirdweberreur" VARCHAR(60) ,
    "cbtregtel" SMALLINT ,
    "cbttabletpc" SMALLINT ,
    "cbttypeafficheplanning" VARCHAR(1) ,
    "cbttypeaffichedoc" VARCHAR(1) ,
    "cbtentreprise" SMALLINT ,
    "cbtchemintabletpc" VARCHAR(80) ,
    "cbtcrenhauto" SMALLINT ,
    "cbtdatenhdefaut" VARCHAR(5) ,
    "cbtsoumisetva" SMALLINT ,
    "cbtrepatnh" VARCHAR(3) ,
    "cbtcopieinter" SMALLINT ,
    "cbtcopiedoc" SMALLINT ,
    "cbtpjinter" SMALLINT ,
    "cbtpjdoc" SMALLINT ,
    "cbtreceptdossier" SMALLINT ,
    "cbtajoutprof" SMALLINT ,
    "cbtajoutsecu" SMALLINT ,
    "cbtajoutcp" SMALLINT ,
    "cbtajoutprejudice" SMALLINT ,
    "cbtajoutspecialite" SMALLINT ,
    "cbtajouttypepiece" SMALLINT ,
    "cbtajoutlieux" SMALLINT ,
    "cbtnbepuretablet" NUMERIC(19,0) ,
    "cbtenvoistatdelai" SMALLINT ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "cbtnumauto" SMALLINT ,
    "cbtinfosecu" LONGVARBINARY ,
    "groupement" VARCHAR(38) ,
    "cbtsmtplogin" VARCHAR(50) ,
    "cbtsmtppwd" VARCHAR(50) ,
    "cbtsmtphost" VARCHAR(50) ,
    "cbtword" SMALLINT );
CREATE INDEX "WDIDX_cabinet_tvacode" ON "cabinet" ("tvacode");
CREATE INDEX "WDIDX_cabinet_juridiquestatut" ON "cabinet" ("juridiquestatut");
CREATE INDEX "WDIDX_cabinet_natinscription" ON "cabinet" ("natinscription");
CREATE INDEX "WDIDX_cabinet_adminnum" ON "cabinet" ("adminnum");
CREATE INDEX "WDIDX_cabinet_PRIMARY" ON "cabinet" ("cbtcode","cbttype");
CREATE INDEX "WDIDX_cabinet_FK_chartecabinet" ON "cabinet" ("procode","prochrono");

-- Cr�ation de la table cbtcompetence
CREATE TABLE "cbtcompetence" (
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "secteurnum" NUMERIC(19,0)  NOT NULL ,
    "secteurcode" VARCHAR(5)  NOT NULL ,
    "sinistrecode" VARCHAR(38)  NOT NULL ,
    "persnum" NUMERIC(19,0)  NOT NULL ,
    "cbtcompetencenbdossier" NUMERIC(19,0) );
CREATE INDEX "WDIDX_cbtcompetence_secteurcode" ON "cbtcompetence" ("secteurcode");
CREATE INDEX "WDIDX_cbtcompetence_sinistrecode" ON "cbtcompetence" ("sinistrecode");
CREATE INDEX "WDIDX_cbtcompetence_persnum" ON "cbtcompetence" ("persnum");
CREATE INDEX "WDIDX_cbtcompetence_PRIMARY" ON "cbtcompetence" ("cbtcode","cbttype","secteurnum","secteurcode","sinistrecode","persnum");

-- Cr�ation de la table chantier
CREATE TABLE "chantier" (
    "chantiernum" VARCHAR(20)  NOT NULL  UNIQUE ,
    "adminnum" NUMERIC(19,0) ,
    "usageconstcode" VARCHAR(8) ,
    "chantierref" VARCHAR(15) ,
    "chantierdroc" DATE ,
    "chantierdaterecept1" DATE ,
    "chantierdaterecept2" DATE ,
    "chantierdaterecept3" DATE ,
    "chantierdaterecept4" DATE ,
    "chantierdaterecept5" DATE ,
    "chantierdaterecept6" DATE ,
    "chantierrecptlibelle1" VARCHAR(64) ,
    "chantierrecptlibelle2" VARCHAR(64) ,
    "chantierrecptlibelle3" VARCHAR(64) ,
    "chantierrecptlibelle4" VARCHAR(64) ,
    "chantierrecptlibelle5" VARCHAR(64) ,
    "chantierrecptlibelle6" VARCHAR(64) ,
    "chantierdateoccup" DATE ,
    "chantiermtprev" REAL ,
    "chantiermtdecl" REAL ,
    "chantiermtreel" REAL ,
    "chantierdescriptgene" LONGVARBINARY ,
    "chantierdescriptpart" LONGVARBINARY ,
    "chantiernumpolice" LONGVARCHAR ,
    "chantierdatemaj" DATE ,
    "chantierdatecree" DATE ,
    "chantierdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_chantier_adminnum" ON "chantier" ("adminnum");
CREATE INDEX "WDIDX_chantier_usageconstcode" ON "chantier" ("usageconstcode");

-- Cr�ation de la table charte
CREATE TABLE "charte" (
    "procode" VARCHAR(32)  NOT NULL ,
    "prochrono" NUMERIC(19,0)  NOT NULL ,
    "datedossiernum" NUMERIC(19,0) ,
    "typemisisoncode" VARCHAR(10) ,
    "docnom" VARCHAR(30) ,
    "sinistrecode" VARCHAR(38) ,
    "propolice" VARCHAR(50) ,
    "proordre" INTEGER ,
    "protype" VARCHAR(1) ,
    "proparticipant" VARCHAR(38) ,
    "prodatecree" DATE ,
    "prodatemaj" DATE ,
    "prodatesuppr" DATE ,
    "prolibelledoc" VARCHAR(50) ,
    "prodestinataire" VARCHAR(40) ,
    "prodelairelance" NUMERIC(19,0) ,
    "proapreslibelle" VARCHAR(20) ,
    "pronbexemplaire" NUMERIC(19,0) ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "prodelaiconserve" NUMERIC(19,0) );
CREATE INDEX "WDIDX_charte_datedossiernum" ON "charte" ("datedossiernum");
CREATE INDEX "WDIDX_charte_typemisisoncode" ON "charte" ("typemisisoncode");
CREATE INDEX "WDIDX_charte_docnom" ON "charte" ("docnom");
CREATE INDEX "WDIDX_charte_sinistrecode" ON "charte" ("sinistrecode");
CREATE INDEX "WDIDX_charte_PRIMARY" ON "charte" ("procode","prochrono");

-- Cr�ation de la table chrono
CREATE TABLE "chrono" (
    "chronotype" VARCHAR(5)  NOT NULL ,
    "chronoanneemois" VARCHAR(10)  NOT NULL ,
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "chronodernier" VARCHAR(20) ,
    "chronodatecree" DATE ,
    "chronodatemaj" DATE ,
    "chronodatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_chrono_PRIMARY" ON "chrono" ("chronotype","chronoanneemois","cbtcode","cbttype");
CREATE INDEX "WDIDX_chrono_FK_cbtchrono" ON "chrono" ("cbtcode","cbttype");

-- Cr�ation de la table civilite
CREATE TABLE "civilite" (
    "cbtcode" VARCHAR(20) ,
    "civilitenum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "civilitetitre" VARCHAR(30) ,
    "civilitelibelle" VARCHAR(40) ,
    "civiliteformule" VARCHAR(40) ,
    "civilitedatecree" DATE ,
    "civilitedatemaj" DATE ,
    "civilitedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table clause
CREATE TABLE "clause" (
    "clausenum" NUMERIC(19,0)  PRIMARY KEY ,
    "clausetype" VARCHAR(1) ,
    "clauselibelle" VARCHAR(30) ,
    "clausedatecree" DATE ,
    "clausedatemaj" DATE ,
    "clausedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table clausechantier
CREATE TABLE "clausechantier" (
    "chantiernum" VARCHAR(20)  NOT NULL  UNIQUE ,
    "clausenum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "clausechantiermt" REAL ,
    "clausechantierdatecree" DATE ,
    "clausechantierdatemaj" DATE ,
    "clausechantierdatesuppr" DATE );
CREATE INDEX "WDIDX_clausechantier_PRIMARY" ON "clausechantier" ("chantiernum","clausenum");

-- Cr�ation de la table clausecontratsouscr
CREATE TABLE "clausecontratsouscr" (
    "clausenum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "persnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "dossnum" VARCHAR(20)  NOT NULL ,
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "cbtcode2" VARCHAR(20)  NOT NULL ,
    "cbttype2" VARCHAR(2)  NOT NULL ,
    "clausemt" REAL );
CREATE INDEX "WDIDX_clausecontratsouscr_PRIMARY" ON "clausecontratsouscr" ("clausenum","persnum","dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
CREATE INDEX "WDIDX_clausecontratsouscr_FK_clausecontratsouscr3" ON "clausecontratsouscr" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table codepostal
CREATE TABLE "codepostal" (
    "codepostal" VARCHAR(10)  NOT NULL ,
    "ville" VARCHAR(28)  NOT NULL ,
    "cpdureetrajet" TIME ,
    "cpdistance" NUMERIC(19,0) ,
    "cpabscisse" VARCHAR(3) ,
    "cpordonnee" VARCHAR(3) ,
    "cpdatecree" DATE ,
    "cpdatemaj" DATE ,
    "cpdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_codepostal_PRIMARY" ON "codepostal" ("codepostal","ville");

-- Cr�ation de la table collabcommission
CREATE TABLE "collabcommission" (
    "collabcom_id" NUMERIC(19,0)  PRIMARY KEY ,
    "collabnum" NUMERIC(19,0) ,
    "hononumfacture" VARCHAR(20) ,
    "compourcent" REAL ,
    "commontant" REAL );
CREATE INDEX "WDIDX_collabcommission_collabnum" ON "collabcommission" ("collabnum");
CREATE INDEX "WDIDX_collabcommission_hononumfacture" ON "collabcommission" ("hononumfacture");

-- Cr�ation de la table collabcompetence
CREATE TABLE "collabcompetence" (
    "sinistrecode" VARCHAR(38)  NOT NULL  UNIQUE ,
    "collabnum" NUMERIC(19,0)  NOT NULL  UNIQUE );
CREATE INDEX "WDIDX_collabcompetence_PRIMARY" ON "collabcompetence" ("sinistrecode","collabnum");

-- Cr�ation de la table collabcompte
CREATE TABLE "collabcompte" (
    "collabnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "compteid" NUMERIC(19,0)  NOT NULL  UNIQUE );
CREATE INDEX "WDIDX_collabcompte_PRIMARY" ON "collabcompte" ("collabnum","compteid");

-- Cr�ation de la table collabmultibase
CREATE TABLE "collabmultibase" (
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "collabnum" NUMERIC(19,0)  NOT NULL ,
    "collabcheminbase" VARCHAR(80) ,
    "collabcorres" VARCHAR(10) ,
    "collabactif" SMALLINT ,
    "collabchemindoctype" VARCHAR(80) ,
    "collabchemindocword" VARCHAR(80) ,
    "collabchemindocdivers" VARCHAR(80) ,
    "collabchemintabletemis" VARCHAR(80) ,
    "collabchemintabletrecept" VARCHAR(80) ,
    "collabchemintabletsav" VARCHAR(80) ,
    "collabplanningmulti" SMALLINT ,
    "collabtypetablet" VARCHAR(10) );
CREATE INDEX "WDIDX_collabmultibase_collabnum" ON "collabmultibase" ("collabnum");
CREATE INDEX "WDIDX_collabmultibase_PRIMARY" ON "collabmultibase" ("cbtcode","cbttype","collabnum");

-- Cr�ation de la table collaborateur
CREATE TABLE "collaborateur" (
    "collabnum" NUMERIC(19,0)  PRIMARY KEY ,
    "procode" VARCHAR(32) ,
    "prochrono" NUMERIC(19,0) ,
    "idparametreutilisateur" NUMERIC(19,0) ,
    "collabcode" VARCHAR(10) ,
    "tvacode" VARCHAR(10) ,
    "adminnum" NUMERIC(19,0) ,
    "collabemail" VARCHAR(60) ,
    "collabsommeil" SMALLINT ,
    "collabtype" VARCHAR(3) ,
    "collabafaire" LONGVARBINARY ,
    "collabfait" LONGVARBINARY ,
    "collabordre" NUMERIC(19,0) ,
    "collabchemintablet" VARCHAR(80) ,
    "collabdatecree" DATE ,
    "collabdatemaj" DATE ,
    "collabdatesuppr" DATE ,
    "collabdateentree" DATE ,
    "collabdatesortie" DATE ,
    "collabdureeminirdv" TIME ,
    "collabdureedefrdv" TIME ,
    "collabindentationrdv" TIME ,
    "collabtauxhoraire" REAL ,
    "collabtvaintra" VARCHAR(15) ,
    "collabpourcent1" REAL ,
    "collabpourcent2" REAL ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "collabinfosecu" LONGVARBINARY ,
    "couleuraccueil" VARCHAR(7) ,
    "coordmaj" DATE ,
    "collablongi" VARCHAR(31) ,
    "collablatt" VARCHAR(31) );
CREATE INDEX "WDIDX_collaborateur_idparametreutilisateur" ON "collaborateur" ("idparametreutilisateur");
CREATE INDEX "WDIDX_collaborateur_tvacode" ON "collaborateur" ("tvacode");
CREATE INDEX "WDIDX_collaborateur_adminnum" ON "collaborateur" ("adminnum");
CREATE INDEX "WDIDX_collaborateur_FK_par_defaut" ON "collaborateur" ("procode","prochrono");

-- Cr�ation de la table collabpersonne
CREATE TABLE "collabpersonne" (
    "collabnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "persnum" NUMERIC(19,0)  NOT NULL  UNIQUE );
CREATE INDEX "WDIDX_collabpersonne_PRIMARY" ON "collabpersonne" ("collabnum","persnum");

-- Cr�ation de la table collabsecteur
CREATE TABLE "collabsecteur" (
    "collabnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "secteurnum" NUMERIC(19,0)  NOT NULL ,
    "secteurcode" VARCHAR(5)  NOT NULL  UNIQUE );
CREATE INDEX "WDIDX_collabsecteur_PRIMARY" ON "collabsecteur" ("collabnum","secteurnum","secteurcode");

-- Cr�ation de la table compta
CREATE TABLE "compta" (
    "numero" INTEGER ,
    "journal" VARCHAR(4) ,
    "dateecriture" DATE ,
    "dateecheance" DATE ,
    "numpiece" VARCHAR(12) ,
    "compte" VARCHAR(15) ,
    "libelle" VARCHAR(40) ,
    "debit" FLOAT ,
    "credit" FLOAT ,
    "pointage" VARCHAR(22) );

-- Cr�ation de la table compte
CREATE TABLE "compte" (
    "compteid" NUMERIC(19,0)  PRIMARY KEY ,
    "comptenum" VARCHAR(15) ,
    "adminnum" NUMERIC(19,0) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "ribnum" NUMERIC(19,0) ,
    "comptetype" VARCHAR(2) ,
    "compteintitule" VARCHAR(38) ,
    "comptedatemvt" DATE ,
    "comptedatecree" DATE ,
    "comptedatemaj" DATE ,
    "comptedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "comptedaterelance" DATE ,
    "comptetypelet" VARCHAR(1) ,
    "comptedatefacture" DATE ,
    "comptedernlettre" VARCHAR(2) ,
    "comptemodereglt" VARCHAR(20) ,
    "comptedelai" NUMERIC(19,0) );
CREATE INDEX "WDIDX_compte_adminnum" ON "compte" ("adminnum");
CREATE INDEX "WDIDX_compte_ribnum" ON "compte" ("ribnum");
CREATE INDEX "WDIDX_compte_comptenum2" ON "compte" ("comptenum","adminnum");
CREATE INDEX "WDIDX_compte_FK_cabinetcompte" ON "compte" ("cbtcode","cbttype");

-- Cr�ation de la table consultationutil
CREATE TABLE "consultationutil" (
    "idparametreutilisateur" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "persnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "typeadherent" VARCHAR(10)  NOT NULL ,
    "dateheuredebutconnection" DATE  NOT NULL ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "histoligne" NUMERIC(19,0) ,
    "reglementnum" VARCHAR(20) ,
    "compteid" NUMERIC(19,0) ,
    "type_piece" VARCHAR(2) ,
    "hononumfacture" VARCHAR(20) ,
    "dateheurefinconnection" DATE );
CREATE INDEX "WDIDX_consultationutil_hononumfacture" ON "consultationutil" ("hononumfacture");
CREATE INDEX "WDIDX_consultationutil_PRIMARY" ON "consultationutil" ("idparametreutilisateur","typeadherent","dateheuredebutconnection","persnum");
CREATE INDEX "WDIDX_consultationutil_FK_consultationutil" ON "consultationutil" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne");
CREATE INDEX "WDIDX_consultationutil_FK_consultationutil2" ON "consultationutil" ("reglementnum","compteid","type_piece");
CREATE INDEX "WDIDX_consultationutil_FK_consultationutil5" ON "consultationutil" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table contactcbt
CREATE TABLE "contactcbt" (
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "adminnum" NUMERIC(19,0)  NOT NULL ,
    "champvide" VARCHAR(5) );
CREATE INDEX "WDIDX_contactcbt_adminnum" ON "contactcbt" ("adminnum");
CREATE INDEX "WDIDX_contactcbt_PRIMARY" ON "contactcbt" ("cbtcode","cbttype","adminnum");
CREATE INDEX "WDIDX_contactcbt_FK_contactcbt" ON "contactcbt" ("cbtcode","cbttype");

-- Cr�ation de la table contratpersonne
CREATE TABLE "contratpersonne" (
    "persnuminter" NUMERIC(19,0)  NOT NULL ,
    "contratligne" NUMERIC(19,0)  NOT NULL ,
    "persnumassur" NUMERIC(19,0) ,
    "contratservsp" VARCHAR(30) ,
    "contratnumpolice" VARCHAR(50) ,
    "contratdatedebut" DATE ,
    "contratdatefin" DATE ,
    "contratmtfranchise" REAL ,
    "contratdatecree" DATE ,
    "contratdatemaj" DATE ,
    "contratdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_contratpersonne_persnuminter" ON "contratpersonne" ("persnuminter");
CREATE INDEX "WDIDX_contratpersonne_persnumassur" ON "contratpersonne" ("persnumassur");
CREATE INDEX "WDIDX_contratpersonne_PRIMARY" ON "contratpersonne" ("contratligne","persnuminter");

-- Cr�ation de la table convention
CREATE TABLE "convention" (
    "conventioncode" VARCHAR(6)  NOT NULL  UNIQUE ,
    "conventionlibelle" VARCHAR(50) ,
    "conventiondatecree" DATE ,
    "conventiondatemaj" DATE ,
    "conventiondatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table corresdarvaardi
CREATE TABLE "corresdarvaardi" (
    "codedarva" VARCHAR(2)  NOT NULL  UNIQUE ,
    "libelle" VARCHAR(164) ,
    "corresardi" VARCHAR(32) ,
    "darvadatecree" DATE ,
    "darvadatemaj" DATE  NOT NULL ,
    "paramsuppr" LONGVARCHAR );

-- Cr�ation de la table darva
CREATE TABLE "darva" (
    "code_darva" VARCHAR(12)  NOT NULL ,
    "code_gta" VARCHAR(4)  NOT NULL ,
    "sinistre" VARCHAR(6)  NOT NULL ,
    "type" VARCHAR(8) ,
    "base" VARCHAR(60) ,
    "login_acces" VARCHAR(30) ,
    "mdp_acces" VARCHAR(30) ,
    "login_pdf" VARCHAR(30) ,
    "mdp_pdf" VARCHAR(30) ,
    "login_recep" VARCHAR(30) ,
    "mdp_recep" VARCHAR(30) ,
    "mission" VARCHAR(1) ,
    "rapport" VARCHAR(1) ,
    "nh" VARCHAR(1) ,
    "entite" VARCHAR(1) ,
    "assureur" VARCHAR(1) ,
    "exp" VARCHAR(1) ,
    "collab" VARCHAR(6) ,
    "secret" VARCHAR(6) ,
    "agence" VARCHAR(6) ,
    "cabinet" VARCHAR(6) ,
    "utilisateur" VARCHAR(6) ,
    "nom" VARCHAR(32) ,
    "mail" VARCHAR(32) ,
    "actif" VARCHAR(1) ,
    "rapport_vide" VARCHAR(1)  NOT NULL );
CREATE INDEX "WDIDX_darva_PRIMARY" ON "darva" ("code_darva","code_gta","sinistre");

-- Cr�ation de la table datedossier
CREATE TABLE "datedossier" (
    "datedossiernum" NUMERIC(19,0)  PRIMARY KEY ,
    "datedossiercode" VARCHAR(30) );

-- Cr�ation de la table decision
CREATE TABLE "decision" (
    "decisioncode" VARCHAR(10)  NOT NULL  UNIQUE ,
    "decisionlibelle" VARCHAR(50) ,
    "decisiondatecree" DATE ,
    "decisiondatemaj" DATE ,
    "decisiondatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table decisiondossier
CREATE TABLE "decisiondossier" (
    "decisdossnum" NUMERIC(19,0)  PRIMARY KEY ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "decisioncode" VARCHAR(10) ,
    "decisdate" DATE ,
    "decisdatedebut" DATE ,
    "decisdatefin" DATE ,
    "decisappel" SMALLINT ,
    "decissurcis" SMALLINT ,
    "descispourvoi" SMALLINT ,
    "descisdepens" SMALLINT ,
    "deciscommentaire" LONGVARBINARY );
CREATE INDEX "WDIDX_decisiondossier_decisioncode" ON "decisiondossier" ("decisioncode");
CREATE INDEX "WDIDX_decisiondossier_FK_decisiondossier" ON "decisiondossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table delaicontractuel
CREATE TABLE "delaicontractuel" (
    "delaicontnum" NUMERIC(19,0)  PRIMARY KEY ,
    "delaicontlibelle" VARCHAR(30) ,
    "delaicontlegal" NUMERIC(19,0) ,
    "delaicontcbt" NUMERIC(19,0) ,
    "delaicontdatemaj" DATE ,
    "delaicontdatecree" DATE ,
    "delaicontdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table delaidossier
CREATE TABLE "delaidossier" (
    "dossnum" VARCHAR(20)  NOT NULL ,
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "cbtcode2" VARCHAR(20)  NOT NULL ,
    "cbttype2" VARCHAR(2)  NOT NULL ,
    "delaicontnum" NUMERIC(19,0)  NOT NULL ,
    "delaicbt" NUMERIC(19,0) ,
    "delailegal" NUMERIC(19,0) ,
    "delaidatecree" DATE ,
    "delaidatemaj" DATE ,
    "delaidatesuppr" DATE ,
    "delaidateecheance" DATE ,
    "delairetard" NUMERIC(19,0) );
CREATE INDEX "WDIDX_delaidossier_delaicontnum" ON "delaidossier" ("delaicontnum");
CREATE INDEX "WDIDX_delaidossier_PRIMARY" ON "delaidossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","delaicontnum");
CREATE INDEX "WDIDX_delaidossier_FK_delaidossier" ON "delaidossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table desordre
CREATE TABLE "desordre" (
    "desordrenum" INTEGER  PRIMARY KEY ,
    "adminnum" NUMERIC(19,0) ,
    "famdesordrecode" VARCHAR(8) ,
    "chantiernum" VARCHAR(20) ,
    "desordreligne" NUMERIC(19,0) ,
    "desordrelibellecourt" VARCHAR(100) ,
    "desordrelibellelong" LONGVARBINARY ,
    "desordrerefinterne" VARCHAR(12) ,
    "desordrereserve" VARCHAR(30) ,
    "desordredatelevee" DATE ,
    "desordredatedecl" DATE ,
    "desordredateapparit" DATE ,
    "desordreconsequence" LONGVARBINARY ,
    "desordreorigine" LONGVARBINARY ,
    "desordrelocal" VARCHAR(30) ,
    "desordrepartcomm" SMALLINT ,
    "desorordreremede" LONGVARBINARY ,
    "desordredatesuppr" DATE ,
    "desordredatecree" DATE ,
    "desordredatemaj" DATE ,
    "desordreprisecharge" SMALLINT ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_desordre_adminnum" ON "desordre" ("adminnum");
CREATE INDEX "WDIDX_desordre_famdesordrecode" ON "desordre" ("famdesordrecode");
CREATE INDEX "WDIDX_desordre_chantiernum" ON "desordre" ("chantiernum");

-- Cr�ation de la table destinataire
CREATE TABLE "destinataire" (
    "destiligne" NUMERIC(19,0)  PRIMARY KEY ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "histoligne" NUMERIC(19,0) ,
    "persnum" NUMERIC(19,0) ,
    "destitype" VARCHAR(15) ,
    "destitiersnum" NUMERIC(19,0) ,
    "destichoixtype" VARCHAR(1) ,
    "destitypelibelle" VARCHAR(20) ,
    "destidatecree" DATE ,
    "destidatemaj" DATE ,
    "destidatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "destinbexemplaire" INTEGER ,
    "destitypeperstiers" VARCHAR(15) ,
    "destilistdepend" VARCHAR(20) );
CREATE INDEX "WDIDX_destinataire_persnum" ON "destinataire" ("persnum");
CREATE INDEX "WDIDX_destinataire_FK_envoiea" ON "destinataire" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne");

-- Cr�ation de la table destinatairereunion
CREATE TABLE "destinatairereunion" (
    "reunionnum" VARCHAR(20)  NOT NULL  UNIQUE ,
    "persnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "dossnum" VARCHAR(20)  NOT NULL ,
    "reunionabsent" SMALLINT ,
    "reunionmotifabsence" VARCHAR(200) ,
    "destireunionconvoque" SMALLINT ,
    "destireuniondestconvo" SMALLINT ,
    "destireuniondestcr" SMALLINT ,
    "destireunionchoixtypelibelle" VARCHAR(20) ,
    "destireuniontierstype" VARCHAR(10) );
CREATE INDEX "WDIDX_destinatairereunion_PRIMARY" ON "destinatairereunion" ("reunionnum","persnum","dossnum");

-- Cr�ation de la table diversbati
CREATE TABLE "diversbati" (
    "diversbatinum" NUMERIC(19,0)  PRIMARY KEY ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "ticketannee" DATE ,
    "diverspourfranchise" VARCHAR(12) ,
    "diversconcernechantier" SMALLINT ,
    "diversnatureforfait" VARCHAR(1) ,
    "diversavenant" SMALLINT ,
    "diversprolongation" SMALLINT ,
    "diversdatejourj" DATE ,
    "diversdateenvoi" DATE ,
    "divershorsdelai" SMALLINT ,
    "diversreference" VARCHAR(50) ,
    "diversconcerne" VARCHAR(160) ,
    "diverspourcfranchise" VARCHAR(12) ,
    "diversfrchant" SMALLINT ,
    "diversdateinfdoss" DATE ,
    "diversbatidatecree" DATE ,
    "diversbatidatemaj" DATE ,
    "diversbatidatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "diversmtsinistre" REAL );
CREATE INDEX "WDIDX_diversbati_ticketannee" ON "diversbati" ("ticketannee");
CREATE INDEX "WDIDX_diversbati_FK_diversbaticard" ON "diversbati" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table diversird
CREATE TABLE "diversird" (
    "diversirdnum" NUMERIC(19,0)  PRIMARY KEY ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "typemisisoncode" VARCHAR(10) ,
    "conventioncode" VARCHAR(6) ,
    "motifcode" VARCHAR(6) ,
    "typeexpertisecode" VARCHAR(25) ,
    "diverscategoriestat" VARCHAR(6) ,
    "diverspresomptionfraude" SMALLINT ,
    "diversnonconforme" SMALLINT ,
    "diversrectifie" SMALLINT ,
    "diverselec" SMALLINT ,
    "diversaspm" SMALLINT ,
    "diversaspi" SMALLINT ,
    "diversmtreclame" REAL ,
    "diversmtattribue" REAL ,
    "diversmtdontmobilier" REAL ,
    "diversmtasubir" REAL ,
    "diversmtaexercer" REAL ,
    "diversdatesinistre" DATE ,
    "diversdatedebsinistre" DATE ,
    "diversdatefinsinistre" DATE ,
    "diversdate1ercontact" DATE ,
    "diversdate1erevisite" DATE ,
    "diversdatedervisite" DATE ,
    "diversdatemisecause" DATE ,
    "diversdatear" DATE ,
    "diversdate1ercourrier" DATE ,
    "diversdatederrelance" DATE ,
    "diversdatereceptdoc" DATE ,
    "diversdateirpv" DATE ,
    "diversdateremisefrappe" DATE ,
    "diversdatefrappe" DATE ,
    "diversdaterelecture" DATE ,
    "diversdaterapport" DATE ,
    "diversdatesignature" DATE ,
    "diversdateenvoila" DATE ,
    "diversdateretourla" DATE ,
    "diversdatedevisregtel" DATE ,
    "diversdatesigneprotocole" DATE ,
    "diverstec" SMALLINT ,
    "diversexamcause" SMALLINT ,
    "diversverifrisque" SMALLINT ,
    "diversformerapp" SMALLINT ,
    "diversmissioncomplete" SMALLINT ,
    "diversrecours" SMALLINT ,
    "diverssignesurplace" SMALLINT ,
    "diverstypeintervention" VARCHAR(5) ,
    "diversdatedebtravaux" DATE ,
    "diversdatefintravaux" DATE ,
    "diversdatesignepv" DATE ,
    "diversreparpar" VARCHAR(1) ,
    "diversmissionannule" SMALLINT ,
    "diversmttravaux" REAL ,
    "diverscausesuppr" SMALLINT ,
    "diversdossierpidi" SMALLINT ,
    "diversrespectdelai" SMALLINT ,
    "diversrefevenement" VARCHAR(50) ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_diversird_typemisisoncode" ON "diversird" ("typemisisoncode");
CREATE INDEX "WDIDX_diversird_conventioncode" ON "diversird" ("conventioncode");
CREATE INDEX "WDIDX_diversird_motifcode" ON "diversird" ("motifcode");
CREATE INDEX "WDIDX_diversird_typeexpertisecode" ON "diversird" ("typeexpertisecode");
CREATE INDEX "WDIDX_diversird_FK_diversird" ON "diversird" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table diversmedi
CREATE TABLE "diversmedi" (
    "diversmedinum" NUMERIC(19,0)  PRIMARY KEY ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "typepiparnum" NUMERIC(19,0) ,
    "prejuestnum" NUMERIC(19,0) ,
    "typepinum" NUMERIC(19,0) ,
    "secunum" NUMERIC(19,0) ,
    "quantumnum" NUMERIC(19,0) ,
    "professionnum" NUMERIC(19,0) ,
    "typ_professionnum" NUMERIC(19,0) ,
    "typeexpertisecode" VARCHAR(25) ,
    "typemisisoncode" VARCHAR(10) ,
    "diversdateaccident" DATE ,
    "diversemailinter" VARCHAR(60) ,
    "diversdatenaissinter" DATE ,
    "diversdatedeceinter" DATE ,
    "diversetatcivilinter" VARCHAR(25) ,
    "diversnbenfantinter" INTEGER ,
    "diversforminter" VARCHAR(50) ,
    "diverscontratinter" VARCHAR(50) ,
    "diversmodeexerinter" VARCHAR(50) ,
    "diverssexeinter" VARCHAR(1) ,
    "diversetiquette" VARCHAR(60) ,
    "diversdatedepot" DATE ,
    "diversdatejuge" DATE ,
    "diversnumjuge" VARCHAR(25) ,
    "diversnumrg" VARCHAR(10) ,
    "diverslieujuge" VARCHAR(50) ,
    "diversdateexp" DATE ,
    "diversippprov1" VARCHAR(20) ,
    "diversippprov2" VARCHAR(20) ,
    "diversippdef" INTEGER ,
    "diversprejest" VARCHAR(35) ,
    "diverscaissecomple" VARCHAR(32) ,
    "diversprejagre" SMALLINT ,
    "diversprejagreinfo" LONGVARBINARY ,
    "diversincidprof" SMALLINT ,
    "diversincidinfo" LONGVARBINARY ,
    "diversprejsexinfo" LONGVARBINARY ,
    "diversprejmoral" SMALLINT ,
    "diversprejmoralinfo" LONGVARBINARY ,
    "diversdatearretdebut" DATE ,
    "diversdatearretfin" DATE ,
    "diversdatereprisetravaux" DATE ,
    "diversinfotravaux" VARCHAR(150) ,
    "diversarevoir" SMALLINT ,
    "diversdateconsolide" DATE ,
    "diversdateconsolideprev" VARCHAR(25) ,
    "diversdatesouscrip" DATE ,
    "diversinfosouscrip" VARCHAR(80) ,
    "diversittdatedebut" DATE ,
    "diversittdatefin" DATE ,
    "diversittinfo" VARCHAR(150) ,
    "diversitp" VARCHAR(2) ,
    "diversitpdatedebut" DATE ,
    "diversitpdatefin" DATE ,
    "diversitpinfo" VARCHAR(150) ,
    "diversgtt" INTEGER ,
    "diversgttdatedebut" DATE ,
    "diversgttdatefin" DATE ,
    "diversgttinfo" VARCHAR(150) ,
    "diversgtp" INTEGER ,
    "diversgtpdatedebut" DATE ,
    "diversgtpdatefin" DATE ,
    "diversgtpinfo" VARCHAR(150) ,
    "diversgt" INTEGER ,
    "diversgtdatedebut" DATE ,
    "diversgtdatefin" DATE ,
    "diversgtinfo" VARCHAR(150) ,
    "diverssoinsdatedebut" DATE ,
    "diverssoinsdatefin" DATE ,
    "diverssoinsdatedebut2" DATE ,
    "diverssoinsdatefin2" DATE ,
    "divershospidatedebut" DATE ,
    "divershospidatefin" DATE ,
    "divershospidatedebut2" DATE ,
    "divershospidatefin2" DATE ,
    "diversfraisfutur" SMALLINT ,
    "diverstiercepersonne" SMALLINT ,
    "diversdoug" VARCHAR(1) ,
    "diverstaille" VARCHAR(4) ,
    "diverspoids" VARCHAR(7) ,
    "diversmascorp" NUMERIC(19,0) ,
    "diversmascorplibelle" VARCHAR(50) ,
    "diversservicemili" VARCHAR(50) ,
    "diversantecedentmed" LONGVARBINARY ,
    "diversantecedentchir" LONGVARBINARY ,
    "diversantecedenttrau" LONGVARBINARY ,
    "diversheureaccident" DATE ,
    "diverslieuaccident" VARCHAR(50) ,
    "diversmoyenloc" VARCHAR(15) ,
    "diversmoyenlocadverse" VARCHAR(15) ,
    "diversstatutaccident" VARCHAR(15) ,
    "diversportceinture" SMALLINT ,
    "diversusageappuietete" SMALLINT ,
    "diversusagecasque" SMALLINT ,
    "diverslieuimpact" VARCHAR(25) ,
    "diversmtmateriel" REAL ,
    "diversepave" SMALLINT ,
    "divershospitalise" SMALLINT ,
    "diversretourdomicile" SMALLINT ,
    "diversretourtravail" SMALLINT ,
    "diversdaterevision" DATE ,
    "diversdatedercontact" DATE ,
    "diversdatederrdv" DATE ,
    "diversdatederrapport" DATE ,
    "diversdatedercourrier" DATE ,
    "diversdatedercourhono" DATE ,
    "diversdatederrgl" DATE ,
    "diversdatederrglpartie" DATE ,
    "diverssamu" SMALLINT ,
    "diversurgence" SMALLINT ,
    "divershospilibelle1" VARCHAR(50) ,
    "divershospilibelle2" VARCHAR(50) ,
    "divershospilibelle3" VARCHAR(50) ,
    "diversdatehospi1debut" DATE ,
    "diversdatehospi2debut" DATE ,
    "diversdatehospi3debut" DATE ,
    "diversdatehospi1ifin" DATE ,
    "diversdatehospi3ifin" DATE ,
    "diversdatehospi2ifin" DATE ,
    "diversnbconsultext" INTEGER ,
    "diversnbconsulmed" INTEGER ,
    "diversreeduclibelle" VARCHAR(50) ,
    "diversreeducdatedebut" DATE ,
    "diversreeducdatefin" DATE ,
    "diversnbreedu" NUMERIC(19,0) ,
    "diverskinelibelle" VARCHAR(50) ,
    "diverskinedatedebut" DATE ,
    "diverskinedatefin" DATE ,
    "diversnbkine" INTEGER ,
    "diverspharmacie" LONGVARBINARY ,
    "diversexambio" LONGVARBINARY ,
    "diversautresoins" LONGVARBINARY ,
    "divershabitattype" VARCHAR(50) ,
    "divershabitatetage" SMALLINT ,
    "divershabitatascenseur" SMALLINT ,
    "divershabitatsurface" VARCHAR(50) ,
    "diversmedimtreclame" REAL ,
    "diversmedimtattribue" REAL ,
    "diverspitype" VARCHAR(50) ,
    "diverspdidatedelivre" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "diversmedidatecree" DATE ,
    "diversmedidatemaj" DATE ,
    "diversmedidatesuppr" DATE ,
    "diversreference1" VARCHAR(60) ,
    "diversreference2" VARCHAR(60) ,
    "diversreference3" VARCHAR(60) ,
    "diverslieunaissance" VARCHAR(38) ,
    "diversprejsex" SMALLINT ,
    "diversnumsecuinter" VARCHAR(31) ,
    "diversdateclotinstruct" DATE );
CREATE INDEX "WDIDX_diversmedi_typepiparnum" ON "diversmedi" ("typepiparnum");
CREATE INDEX "WDIDX_diversmedi_prejuestnum" ON "diversmedi" ("prejuestnum");
CREATE INDEX "WDIDX_diversmedi_typepinum" ON "diversmedi" ("typepinum");
CREATE INDEX "WDIDX_diversmedi_secunum" ON "diversmedi" ("secunum");
CREATE INDEX "WDIDX_diversmedi_quantumnum" ON "diversmedi" ("quantumnum");
CREATE INDEX "WDIDX_diversmedi_professionnum" ON "diversmedi" ("professionnum");
CREATE INDEX "WDIDX_diversmedi_typ_professionnum" ON "diversmedi" ("typ_professionnum");
CREATE INDEX "WDIDX_diversmedi_typeexpertisecode" ON "diversmedi" ("typeexpertisecode");
CREATE INDEX "WDIDX_diversmedi_typemisisoncode" ON "diversmedi" ("typemisisoncode");
CREATE INDEX "WDIDX_diversmedi_FK_diversmedicard" ON "diversmedi" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table document
CREATE TABLE "document" (
    "docnom" VARCHAR(30)  NOT NULL  UNIQUE ,
    "secretnum" NUMERIC(19,0) ,
    "groupecode" VARCHAR(38) ,
    "persnum" NUMERIC(19,0) ,
    "secretnummailto" NUMERIC(19,0) ,
    "groupecodemailto" VARCHAR(38) ,
    "collabnum" NUMERIC(19,0) ,
    "editeurcode" VARCHAR(20) ,
    "typedoccode" VARCHAR(6) ,
    "collabnummailto" NUMERIC(19,0) ,
    "doclibelle" VARCHAR(50) ,
    "docdelairelance" NUMERIC(19,0) ,
    "docdelaiarchivage" NUMERIC(19,0) ,
    "docnbexemplaire" NUMERIC(19,0) ,
    "docvisiblepar" VARCHAR(2) ,
    "docexiste" SMALLINT ,
    "doctabletpc" SMALLINT ,
    "docmailtopiece" SMALLINT ,
    "docmailto" SMALLINT ,
    "dochorsselect" SMALLINT ,
    "docfamillestat" VARCHAR(32) ,
    "docmajdate" VARCHAR(32) ,
    "docdatecree" DATE ,
    "docdatemaj" DATE ,
    "docdatesuppr" DATE ,
    "docconvoc" SMALLINT ,
    "doccrendu" SMALLINT ,
    "docmailtodestidoc" SMALLINT ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "docmailtodesti" VARCHAR(32) );
CREATE INDEX "WDIDX_document_secretnum" ON "document" ("secretnum");
CREATE INDEX "WDIDX_document_groupecode" ON "document" ("groupecode");
CREATE INDEX "WDIDX_document_persnum" ON "document" ("persnum");
CREATE INDEX "WDIDX_document_secretnummailto" ON "document" ("secretnummailto");
CREATE INDEX "WDIDX_document_groupecodemailto" ON "document" ("groupecodemailto");
CREATE INDEX "WDIDX_document_collabnum" ON "document" ("collabnum");
CREATE INDEX "WDIDX_document_editeurcode" ON "document" ("editeurcode");
CREATE INDEX "WDIDX_document_typedoccode" ON "document" ("typedoccode");
CREATE INDEX "WDIDX_document_collabnummailto" ON "document" ("collabnummailto");

-- Cr�ation de la table dossier
CREATE TABLE "dossier" (
    "dossnum" VARCHAR(20)  NOT NULL ,
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "cbtcode2" VARCHAR(20)  NOT NULL ,
    "cbttype2" VARCHAR(2)  NOT NULL ,
    "famillecode" VARCHAR(20) ,
    "collabnum" NUMERIC(19,0) ,
    "secretnum" NUMERIC(19,0) ,
    "secretnum2" NUMERIC(19,0) ,
    "typereceptcode" VARCHAR(10) ,
    "chantiernum" VARCHAR(20) ,
    "qualitecode" VARCHAR(2) ,
    "sinistrecode" VARCHAR(38) ,
    "metiercode" VARCHAR(20) ,
    "metierpwd" VARCHAR(20) ,
    "procode" VARCHAR(32) ,
    "prochrono" NUMERIC(19,0) ,
    "collabnum2" NUMERIC(19,0) ,
    "adminnum" NUMERIC(19,0) ,
    "dossnumom" VARCHAR(20) ,
    "dossnumcbt" VARCHAR(20) ,
    "dossnumsinistrerecu" VARCHAR(20) ,
    "dossidentifiantrecu" VARCHAR(20) ,
    "dosscodegta" VARCHAR(6) ,
    "dossdestiar" SMALLINT ,
    "dossconnexe" VARCHAR(50) ,
    "dossnumpolice" VARCHAR(50) ,
    "dosstypepolice" VARCHAR(20) ,
    "dossnumpoliceregie" VARCHAR(50) ,
    "dossresponsable" SMALLINT ,
    "dosslese" SMALLINT ,
    "dosscommentsinistre" LONGVARBINARY ,
    "dossdatemaj" DATE ,
    "dossunioucontre" VARCHAR(32) ,
    "dosscomment1" LONGVARBINARY ,
    "dosscomment2" LONGVARBINARY ,
    "dosstransfert" SMALLINT ,
    "dosshorsdelai" SMALLINT ,
    "dossdatereception" DATE ,
    "dossrefsinistre" VARCHAR(50) ,
    "dossdatearchive" DATE ,
    "dossarretprocedure" SMALLINT ,
    "dossdatecloture" DATE ,
    "dossdatedeclsinistre" DATE ,
    "dossdatederhono" DATE ,
    "dossdatemission" DATE ,
    "dossdatetransfert" DATE ,
    "dossmtindemntotal" REAL ,
    "dossdatecree" DATE ,
    "dossdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "codeportefeuille" VARCHAR(32) ,
    "utilisateur" VARCHAR(32) );
CREATE INDEX "WDIDX_dossier_famillecode" ON "dossier" ("famillecode");
CREATE INDEX "WDIDX_dossier_collabnum" ON "dossier" ("collabnum");
CREATE INDEX "WDIDX_dossier_secretnum" ON "dossier" ("secretnum");
CREATE INDEX "WDIDX_dossier_secretnum2" ON "dossier" ("secretnum2");
CREATE INDEX "WDIDX_dossier_typereceptcode" ON "dossier" ("typereceptcode");
CREATE INDEX "WDIDX_dossier_chantiernum" ON "dossier" ("chantiernum");
CREATE INDEX "WDIDX_dossier_sinistrecode" ON "dossier" ("sinistrecode");
CREATE INDEX "WDIDX_dossier_collabnum2" ON "dossier" ("collabnum2");
CREATE INDEX "WDIDX_dossier_adminnum" ON "dossier" ("adminnum");
CREATE INDEX "WDIDX_dossier_dossdatereception" ON "dossier" ("dossdatereception");
CREATE INDEX "WDIDX_dossier_dossdatecree" ON "dossier" ("dossdatecree");
CREATE INDEX "WDIDX_dossier_PRIMARY" ON "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
CREATE INDEX "WDIDX_dossier_FK_agence" ON "dossier" ("cbtcode","cbttype");
CREATE INDEX "WDIDX_dossier_FK_cabinet" ON "dossier" ("cbtcode2","cbttype2");
CREATE INDEX "WDIDX_dossier_FK_charte" ON "dossier" ("procode","prochrono");
CREATE INDEX "WDIDX_dossier_FK_metier" ON "dossier" ("metiercode","metierpwd");

-- Cr�ation de la table dossierlesionsequelle
CREATE TABLE "dossierlesionsequelle" (
    "lesionsequelledossnum" NUMERIC(19,0)  PRIMARY KEY ,
    "lesionsequelletype" VARCHAR(1) ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "lesionsequellecode" VARCHAR(6) );
CREATE INDEX "WDIDX_dossierlesionsequelle_FK_dossierlesionsequelle" ON "dossierlesionsequelle" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table dossiertauxhoraire
CREATE TABLE "dossiertauxhoraire" (
    "tauxhorairenum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "dossnum" VARCHAR(20)  NOT NULL  UNIQUE ,
    "cbtcode" VARCHAR(20)  NOT NULL  UNIQUE ,
    "cbttype" VARCHAR(2)  NOT NULL  UNIQUE ,
    "cbtcode2" VARCHAR(20)  NOT NULL  UNIQUE ,
    "cbttype2" VARCHAR(2)  NOT NULL  UNIQUE ,
    "dosstxtligne" NUMERIC(19,0) ,
    "dosstxttypetravaux" VARCHAR(6) ,
    "dosstxttypeligne" VARCHAR(5) ,
    "dosstxthoraire" FLOAT ,
    "dosstxttemps" TIME ,
    "dosstxtfacturable" SMALLINT ,
    "dosstxtafacturer" SMALLINT ,
    "dosstxudatecree" DATE ,
    "dosstxtdatemaj" DATE ,
    "dosstxtdatesuppr" DATE ,
    "dosstxtdatedeb" DATE ,
    "dosstxtdatefin" DATE );
CREATE INDEX "WDIDX_dossiertauxhoraire_PRIMARY" ON "dossiertauxhoraire" ("tauxhorairenum","dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
CREATE INDEX "WDIDX_dossiertauxhoraire_FK_dossiertauxhoraire2" ON "dossiertauxhoraire" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table droitaccesutilisateur
CREATE TABLE "droitaccesutilisateur" (
    "droitaccesnum" NUMERIC(19,0)  PRIMARY KEY ,
    "profilnum" NUMERIC(19,0) ,
    "formulairenum" NUMERIC(19,0) ,
    "droitacces" VARCHAR(15) );
CREATE INDEX "WDIDX_droitaccesutilisateur_profilnum" ON "droitaccesutilisateur" ("profilnum");
CREATE INDEX "WDIDX_droitaccesutilisateur_formulairenum" ON "droitaccesutilisateur" ("formulairenum");

-- Cr�ation de la table editeur
CREATE TABLE "editeur" (
    "editeurcode" VARCHAR(20)  NOT NULL  UNIQUE ,
    "editeurlibelle" VARCHAR(100) );

-- Cr�ation de la table electrodomestique
CREATE TABLE "electrodomestique" (
    "dossnum" VARCHAR(20)  NOT NULL ,
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "cbtcode2" VARCHAR(20)  NOT NULL ,
    "cbttype2" VARCHAR(2)  NOT NULL ,
    "electroligne" NUMERIC(19,0)  NOT NULL ,
    "electrotype" VARCHAR(6) ,
    "electroirreparable" SMALLINT ,
    "electroavisexpert" SMALLINT ,
    "electroavisreparateur" SMALLINT ,
    "electrodaterepare" DATE ,
    "electromtindemnite" REAL ,
    "typeindemncode" VARCHAR(6) ,
    "electrolibelle" VARCHAR(50) ,
    "electrodatecree" DATE ,
    "electrodatemaj" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_electrodomestique_PRIMARY" ON "electrodomestique" ("electroligne","dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
CREATE INDEX "WDIDX_electrodomestique_FK_electro" ON "electrodomestique" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table etatdb
CREATE TABLE "etatdb" (
    "CODECOLLAB" VARCHAR(1) ,
    "NBDOSSIERENCOURS" INTEGER ,
    "NBDOSSIERFACTURE" INTEGER ,
    "NOMCOLLAB" VARCHAR(1) ,
    "NUMERO" VARCHAR(1) ,
    "TOTALHT" FLOAT );

-- Cr�ation de la table famille
CREATE TABLE "famille" (
    "famillecode" VARCHAR(20)  NOT NULL  UNIQUE ,
    "famillelibelle" VARCHAR(50) ,
    "familledatecree" DATE ,
    "familledatemaj" DATE ,
    "familledatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table familledesordre
CREATE TABLE "familledesordre" (
    "famdesordrecode" VARCHAR(8)  NOT NULL  UNIQUE ,
    "famdesordrelibelle" VARCHAR(30) ,
    "famdesordredatecree" DATE ,
    "famdesordredatemaj" DATE ,
    "famdesordredatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table formulaire
CREATE TABLE "formulaire" (
    "cbtcode" VARCHAR(20) ,
    "formulairenum" NUMERIC(19,0)  PRIMARY KEY ,
    "formulairenom" VARCHAR(50) ,
    "formulairelibelle" VARCHAR(100) );

-- Cr�ation de la table fuseadrdoss
CREATE TABLE "fuseadrdoss" (
    "adrdossnum" NUMERIC(19,0)  NOT NULL ,
    "adrdossnumtiers" NUMERIC(19,0) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "dossnum" VARCHAR(20) ,
    "persnum" NUMERIC(19,0) ,
    "typeperscode" VARCHAR(10) ,
    "collabcode" VARCHAR(10) ,
    "collabcode2" VARCHAR(10) ,
    "secretcode" VARCHAR(10) ,
    "qualitecode" VARCHAR(2) ,
    "statutcode" VARCHAR(2) ,
    "adrdossreference1" VARCHAR(50) ,
    "adrdossreference2" VARCHAR(50) ,
    "adrdossreference3" VARCHAR(50) ,
    "adrdossnumpolice" VARCHAR(50) ,
    "adrdosscommentaire" LONGVARBINARY ,
    "adrdossmttravaux" REAL ,
    "adrdossdatedebut" DATE ,
    "adrdossdatecree" DATE ,
    "adrdossdatemaj" DATE ,
    "adrdossdatefin" DATE ,
    "adrdossdatesuppr" DATE ,
    "adrdosslibelle" VARCHAR(100) ,
    "adrdossposition" NUMERIC(19,0) ,
    "adrdossmtdevis" REAL ,
    "adrdossdatedevis" DATE ,
    "adrdossmiseencause" SMALLINT ,
    "adrdossmtfranchise" REAL ,
    "adrdossmtclause" REAL ,
    "adrdosstypetiers" VARCHAR(10) ,
    "adrdosslese" SMALLINT ,
    "adrdossresponsable" SMALLINT ,
    "adrdossrar" SMALLINT ,
    "adrdossnumsinistre" VARCHAR(50) ,
    "persnumsoustraite" NUMERIC(19,0) );

-- Cr�ation de la table fusionsdoc
CREATE TABLE "fusionsdoc" (
    "fusionnum" NUMERIC(19,0)  PRIMARY KEY ,
    "docnom" VARCHAR(30) ,
    "fusionquantite" NUMERIC(19,0) ,
    "fusionbloque" SMALLINT ,
    "fusiondatecree" DATE ,
    "fusiondatemaj" DATE ,
    "fusiondatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_fusionsdoc_docnom" ON "fusionsdoc" ("docnom");

-- Cr�ation de la table garantie
CREATE TABLE "garantie" (
    "dossnum" VARCHAR(20)  NOT NULL ,
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "cbtcode2" VARCHAR(20)  NOT NULL ,
    "cbttype2" VARCHAR(2)  NOT NULL ,
    "garantligne" NUMERIC(19,0)  NOT NULL ,
    "typecontratcode" VARCHAR(20) ,
    "garantdate" DATE ,
    "garantcode" VARCHAR(5) ,
    "garantlibelle" VARCHAR(50) ,
    "garantindicesouscript" FLOAT ,
    "garantindiceregle" FLOAT ,
    "garantindexation" SMALLINT ,
    "garantrisqueexcep" SMALLINT ,
    "garantmtimmobilier" REAL ,
    "garantmtmobilier" REAL ,
    "garantmtmarchandise" REAL ,
    "garantmtvaleurespece" REAL ,
    "garantmtbiensensible" REAL ,
    "garantmtequipement" REAL ,
    "garantmtautre" REAL ,
    "garantmtembellissement" REAL ,
    "garantmtmateriel" REAL ,
    "garantmtobjetprecieux" REAL ,
    "garantmtrecherchefuite" REAL ,
    "garantmtstock" REAL ,
    "garantmtvetuste" REAL ,
    "garantmtfranchise" REAL ,
    "garantpourcentfranchise" FLOAT ,
    "garantmtfranchiseabsolue" REAL ,
    "garantmtplancher" REAL ,
    "garantmtplafond" REAL ,
    "garantnbpiece" NUMERIC(19,0) ,
    "garantinfomobilier" VARCHAR(32) ,
    "garantinfoimmo" VARCHAR(32) ,
    "garantinfomarchand" VARCHAR(32) ,
    "garantinfovaleurespece" VARCHAR(32) ,
    "garantinfobiensensible" VARCHAR(32) ,
    "garantinfoequipement" VARCHAR(32) ,
    "garantinfoautre" VARCHAR(32) ,
    "garantinfoembellissement" VARCHAR(32) ,
    "garantinfomateriel" VARCHAR(32) ,
    "garantinfoobjet" VARCHAR(32) ,
    "garantinfofuite" VARCHAR(32) ,
    "garantinfostock" VARCHAR(32) ,
    "garantinfovetuste" VARCHAR(32) ,
    "garantcommentaire" LONGVARCHAR ,
    "garantsurfacerisque" FLOAT ,
    "garantsurfacebatiment" FLOAT ,
    "garantsurfacedespend" FLOAT ,
    "garantchiffreaffaire" REAL ,
    "garantquantite" FLOAT ,
    "garantdatecree" DATE ,
    "garantdatemaj" DATE ,
    "garantdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_garantie_typecontratcode" ON "garantie" ("typecontratcode");
CREATE INDEX "WDIDX_garantie_PRIMARY" ON "garantie" ("garantligne","dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
CREATE INDEX "WDIDX_garantie_FK_garantie" ON "garantie" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table grillehonoraire
CREATE TABLE "grillehonoraire" (
    "persnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "typemisisoncode" VARCHAR(10)  NOT NULL  UNIQUE ,
    "grillehonomt" REAL );
CREATE INDEX "WDIDX_grillehonoraire_PRIMARY" ON "grillehonoraire" ("persnum","typemisisoncode");

-- Cr�ation de la table groupersonne
CREATE TABLE "groupersonne" (
    "groupecode" VARCHAR(38)  NOT NULL  UNIQUE ,
    "adminnum" NUMERIC(19,0) ,
    "idparametreutilisateur" NUMERIC(19,0) ,
    "groupedatecree" DATE ,
    "groupedatemaj" DATE ,
    "groupedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_groupersonne_adminnum" ON "groupersonne" ("adminnum");
CREATE INDEX "WDIDX_groupersonne_idparametreutilisateur" ON "groupersonne" ("idparametreutilisateur");

-- Cr�ation de la table historique
CREATE TABLE "historique" (
    "dossnum" VARCHAR(20)  NOT NULL ,
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "cbtcode2" VARCHAR(20)  NOT NULL ,
    "cbttype2" VARCHAR(2)  NOT NULL ,
    "histoligne" NUMERIC(19,0)  NOT NULL ,
    "destiligne" NUMERIC(19,0) ,
    "secretnum" NUMERIC(19,0) ,
    "hononumfacture" VARCHAR(20) ,
    "planningnum" NUMERIC(19,0) ,
    "procode" VARCHAR(32) ,
    "prochrono" NUMERIC(19,0) ,
    "docnom" VARCHAR(30) ,
    "reunionnum" VARCHAR(20) ,
    "doclibelle" VARCHAR(50) ,
    "histodatecree" DATE ,
    "histodatemaj" DATE ,
    "histodatesuppr" DATE ,
    "histodateecheance" DATE ,
    "histodateeventeffect" DATE ,
    "histodelairelance" NUMERIC(19,0) ,
    "histoarretrelance" SMALLINT ,
    "histoeventeffectue" SMALLINT ,
    "histodelaiarchivage" NUMERIC(19,0) ,
    "histocollab" VARCHAR(10) ,
    "histocollab2" VARCHAR(10) ,
    "histosecretgere" VARCHAR(10) ,
    "histosecretsecfrappe" VARCHAR(10) ,
    "histomodecreation" VARCHAR(6) ,
    "histonomdocfusionne" VARCHAR(20) ,
    "histopourinfo" NVARCHAR(100) ,
    "histoenvoimail" SMALLINT ,
    "historertdoc" VARCHAR(10) ,
    "histoenvoimaileva" SMALLINT ,
    "histoenvoimailto" SMALLINT ,
    "histodateretourtablet" DATE ,
    "histoidentifiantirdweb" VARCHAR(20) ,
    "histosigne" SMALLINT ,
    "histotabletpc" SMALLINT ,
    "histodatemail" DATE ,
    "histodemandemaileva" DATE ,
    "histodatetranstabletpc" DATE ,
    "histodatesignetabletpc" DATE ,
    "histotypeirdweb" VARCHAR(5) ,
    "histosoustypeirdweb" VARCHAR(5) ,
    "historeunionouconvo" VARCHAR(2) ,
    "histodateimpr" DATE ,
    "histomultidest" SMALLINT ,
    "histodatecopie" DATE ,
    "histotypecopie" VARCHAR(1) ,
    "histodatefinale" DATE ,
    "histocheminmatrice" VARCHAR(100) ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "histoaediter" SMALLINT ,
    "histovisiblepar" VARCHAR(2) ,
    "histoemisrecu" VARCHAR(1) ,
    "histodatear" DATE ,
    "histonumar" VARCHAR(25) ,
    "datedossiernum" NUMERIC(19,0) );
CREATE INDEX "WDIDX_historique_secretnum" ON "historique" ("secretnum");
CREATE INDEX "WDIDX_historique_hononumfacture" ON "historique" ("hononumfacture");
CREATE INDEX "WDIDX_historique_planningnum" ON "historique" ("planningnum");
CREATE INDEX "WDIDX_historique_docnom" ON "historique" ("docnom");
CREATE INDEX "WDIDX_historique_reunionnum" ON "historique" ("reunionnum");
CREATE INDEX "WDIDX_historique_PRIMARY" ON "historique" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne");
CREATE INDEX "WDIDX_historique_FK_dossierhisto" ON "historique" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
CREATE INDEX "WDIDX_historique_FK_provient" ON "historique" ("procode","prochrono");

-- Cr�ation de la table histotransfert
CREATE TABLE "histotransfert" (
    "histotransnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "histoligne" NUMERIC(19,0) ,
    "histotransdate" DATE ,
    "histotranssens" VARCHAR(25) ,
    "histotransfichier" VARCHAR(50) ,
    "histotransresultat" VARCHAR(80) ,
    "histotransdatecree" DATE ,
    "histotransdatemaj" DATE ,
    "histotransdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_histotransfert_FK_transfert" ON "histotransfert" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne");

-- Cr�ation de la table honoraire
CREATE TABLE "honoraire" (
    "hononumfacture" VARCHAR(20)  NOT NULL  UNIQUE ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "histoligne" NUMERIC(19,0) ,
    "destiligne" NUMERIC(19,0) ,
    "tresorcode" VARCHAR(6) ,
    "hononumirdweb" VARCHAR(20) ,
    "honotype" VARCHAR(2) ,
    "honoclosdossier" SMALLINT ,
    "honoht" SMALLINT ,
    "honoidentifiantirdweb" VARCHAR(20) ,
    "honodatefacture" DATE ,
    "honodateimpr" DATE ,
    "honodateecheance" DATE ,
    "honodatecompta" DATE ,
    "honocodetva1" VARCHAR(10) ,
    "honototalfrais" REAL ,
    "honototalhonos" REAL ,
    "honotyperepart" VARCHAR(1) ,
    "honodatereglement" DATE ,
    "honosimpleoudetaille" VARCHAR(1) ,
    "honodatemaj" DATE ,
    "honodatesuppr" DATE ,
    "honoregle" SMALLINT ,
    "honorelicat" REAL ,
    "hononbrelance" INTEGER ,
    "honosoumistva" SMALLINT ,
    "honocomplexe" SMALLINT ,
    "hononbheure" REAL ,
    "honotauxhoraire" REAL ,
    "honomtheure" REAL ,
    "honoreglementencours" SMALLINT ,
    "honomtregle" REAL ,
    "honocodetva2" VARCHAR(10) ,
    "honocodetva3" VARCHAR(10) ,
    "honotauxtva1" FLOAT ,
    "honotauxtva2" FLOAT ,
    "honotauxtva3" FLOAT ,
    "honomtht1" REAL ,
    "honomtht2" REAL ,
    "honomtht3" REAL ,
    "honomttva1" REAL ,
    "honomttva2" REAL ,
    "honomttva3" REAL ,
    "honomtttc1" REAL ,
    "honomtttc2" REAL ,
    "honomtttc3" REAL ,
    "honototalht" REAL ,
    "honototaltva" REAL ,
    "honototalttc" REAL ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "nomdot" VARCHAR(20) ,
    "factsolde" SMALLINT ,
    "cabinet" VARCHAR(20) ,
    "agence" VARCHAR(20) ,
    "regsecrsecond" NUMERIC(19,0) ,
    "regsecretaire" NUMERIC(19,0) ,
    "collab1" VARCHAR(6) ,
    "collab2" VARCHAR(6) ,
    "collab3" VARCHAR(6) ,
    "collab4" VARCHAR(6) ,
    "collab5" VARCHAR(6) ,
    "mtcollab1" FLOAT ,
    "mtcollab2" FLOAT ,
    "mtcollab3" FLOAT ,
    "mtcollab4" FLOAT ,
    "mtcollab5" FLOAT ,
    "pourcentcollab1" FLOAT ,
    "pourcentcollab2" FLOAT ,
    "pourcentcollab3" FLOAT ,
    "pourcentcollab4" FLOAT ,
    "pourcentcollab5" FLOAT );
CREATE INDEX "WDIDX_honoraire_tresorcode" ON "honoraire" ("tresorcode");
CREATE INDEX "WDIDX_honoraire_honodatefacture" ON "honoraire" ("honodatefacture");
CREATE INDEX "WDIDX_honoraire_regsecrsecond" ON "honoraire" ("regsecrsecond");
CREATE INDEX "WDIDX_honoraire_regsecretaire" ON "honoraire" ("regsecretaire");
CREATE INDEX "WDIDX_honoraire_FK_dossierhonoTampon" ON "honoraire" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
CREATE INDEX "WDIDX_honoraire_FK_honoimprimeTampon" ON "honoraire" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne");

-- Cr�ation de la table honorairedefaut
CREATE TABLE "honorairedefaut" (
    "honodefautperscode" VARCHAR(38)  NOT NULL ,
    "honodefautcode" VARCHAR(15)  NOT NULL ,
    "honodefautligne" NUMERIC(19,0)  NOT NULL ,
    "docnom" VARCHAR(30)  NOT NULL ,
    "collabnum" NUMERIC(19,0) ,
    "payeurnum" NUMERIC(19,0) ,
    "tvacode" VARCHAR(10) ,
    "typemisisoncode" VARCHAR(10) ,
    "groupecode" VARCHAR(38) ,
    "honodefauttype" VARCHAR(6) ,
    "honodefautlibelle" VARCHAR(50) ,
    "honodefautpu" REAL ,
    "honodefautunite" VARCHAR(5) ,
    "honodefautcolonne" VARCHAR(6) ,
    "honodfautdatecree" DATE ,
    "honodafautdatemaj" DATE ,
    "honodefautdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_honorairedefaut_docnom" ON "honorairedefaut" ("docnom");
CREATE INDEX "WDIDX_honorairedefaut_collabnum" ON "honorairedefaut" ("collabnum");
CREATE INDEX "WDIDX_honorairedefaut_payeurnum" ON "honorairedefaut" ("payeurnum");
CREATE INDEX "WDIDX_honorairedefaut_tvacode" ON "honorairedefaut" ("tvacode");
CREATE INDEX "WDIDX_honorairedefaut_typemisisoncode" ON "honorairedefaut" ("typemisisoncode");
CREATE INDEX "WDIDX_honorairedefaut_groupecode" ON "honorairedefaut" ("groupecode");
CREATE INDEX "WDIDX_honorairedefaut_PRIMARY" ON "honorairedefaut" ("honodefautligne","docnom","honodefautcode","honodefautperscode");

-- Cr�ation de la table honoraireligne
CREATE TABLE "honoraireligne" (
    "honoligne" NUMERIC(19,0)  NOT NULL ,
    "hononumfacture" VARCHAR(20)  NOT NULL ,
    "tvacode" VARCHAR(10) ,
    "honolignetype" VARCHAR(5) ,
    "honolignelibelle" VARCHAR(80) ,
    "honoligneunite" VARCHAR(5) ,
    "honolignequantite" FLOAT ,
    "honolignepu" REAL ,
    "honolignemtht" REAL ,
    "honolignemttva" REAL ,
    "honolignemtttc" REAL ,
    "honolignedatecree" DATE ,
    "honolignedatemaj" DATE ,
    "honolignedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_honoraireligne_hononumfacture" ON "honoraireligne" ("hononumfacture");
CREATE INDEX "WDIDX_honoraireligne_tvacode" ON "honoraireligne" ("tvacode");
CREATE INDEX "WDIDX_honoraireligne_PRIMARY" ON "honoraireligne" ("honoligne","hononumfacture");

-- Cr�ation de la table impayeadrdossier
CREATE TABLE "impayeadrdossier" (
    "adrdossreference1" VARCHAR(50) ,
    "dossnum" VARCHAR(20) ,
    "typeperscode" VARCHAR(10) ,
    "persnum" NUMERIC(19,0) );

-- Cr�ation de la table impayemayma34
CREATE TABLE "impayemayma34" (
    "nompayeur" VARCHAR(38) ,
    "nomexpert" VARCHAR(38) ,
    "nomassure" VARCHAR(38) ,
    "honotype" VARCHAR(2) ,
    "dossnum" VARCHAR(20) ,
    "annees" INTEGER ,
    "compa" VARCHAR(50) ,
    "EXPER" VARCHAR(50) ,
    "ASSUR" VARCHAR(50) ,
    "datemission" DATE ,
    "honototalttc" FLOAT ,
    "honodatefacture" DATE ,
    "factsolde" SMALLINT ,
    "honodatereglement" DATE ,
    "nomassurance" VARCHAR(38) ,
    "reglementnum" VARCHAR(20) ,
    "numpolice" VARCHAR(32) ,
    "reste" FLOAT ,
    "reglementdatesaisie" DATE ,
    "hononumfacture" VARCHAR(38) );

-- Cr�ation de la table impayeviewmayma34
CREATE TABLE "impayeviewmayma34" (
    "nompayeur" VARCHAR(38) ,
    "nomexpert" VARCHAR(38) ,
    "nomassure" VARCHAR(38) ,
    "honotype" VARCHAR(2) ,
    "dossnum" VARCHAR(20) ,
    "annees" INTEGER ,
    "compa" VARCHAR(50) ,
    "EXPER" VARCHAR(50) ,
    "ASSUR" VARCHAR(50) ,
    "datemission" DATE ,
    "honototalttc" REAL ,
    "honodatefacture" DATE ,
    "factsolde" SMALLINT ,
    "honodatereglement" DATE ,
    "nomassurance" VARCHAR(38) ,
    "hononumfacture" VARCHAR(20)  NOT NULL );

-- Cr�ation de la table indemnitedossier
CREATE TABLE "indemnitedossier" (
    "diversirdnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "indemndossiernum" VARCHAR(20)  NOT NULL  UNIQUE ,
    "typeindemncode" VARCHAR(6) ,
    "indemnmtrectifie" REAL ,
    "indemnmtindemnimmediate" REAL ,
    "indemnmtindemndiffere" REAL ,
    "indemnum" NUMERIC(19,0)  PRIMARY KEY );
CREATE INDEX "WDIDX_indemnitedossier_typeindemncode" ON "indemnitedossier" ("typeindemncode");
CREATE INDEX "WDIDX_indemnitedossier_PRIMARY" ON "indemnitedossier" ("diversirdnum","indemndossiernum","indemnum");

-- Cr�ation de la table juridique
CREATE TABLE "juridique" (
    "juridiquestatut" VARCHAR(2)  NOT NULL  UNIQUE ,
    "juridiquelibcourt" VARCHAR(10) ,
    "juridiqueliblong" VARCHAR(50) ,
    "juridiquedatecree" DATE ,
    "juridiquedatemaj" DATE ,
    "juridiquedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table lesionsequelle
CREATE TABLE "lesionsequelle" (
    "pathologiecode" VARCHAR(2) ,
    "lesionsequelletype" VARCHAR(1)  NOT NULL ,
    "lesionsequellecode" VARCHAR(6)  NOT NULL ,
    "lesionsequellelibelle" VARCHAR(80) ,
    "lesionsequelledatecree" DATE ,
    "lesionsequelledatemaj" DATE ,
    "lesionsequelledatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_lesionsequelle_pathologiecode" ON "lesionsequelle" ("pathologiecode");
CREATE INDEX "WDIDX_lesionsequelle_lesionsequellecode" ON "lesionsequelle" ("lesionsequellecode");
CREATE INDEX "WDIDX_lesionsequelle_PRIMARY" ON "lesionsequelle" ("lesionsequelletype","lesionsequellecode");

-- Cr�ation de la table metier
CREATE TABLE "metier" (
    "metiercode" VARCHAR(20)  NOT NULL ,
    "metierpwd" VARCHAR(20)  NOT NULL ,
    "metierlibelle" VARCHAR(50) ,
    "metierdatecree" DATE ,
    "metierdatemaj" DATE ,
    "metierdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "profilnum" NUMERIC(19,0) );
CREATE INDEX "WDIDX_metier_profilnum" ON "metier" ("profilnum");
CREATE INDEX "WDIDX_metier_PRIMARY" ON "metier" ("metiercode","metierpwd");

-- Cr�ation de la table motif
CREATE TABLE "motif" (
    "motifcode" VARCHAR(6)  NOT NULL  UNIQUE ,
    "motiflibelle" VARCHAR(50) ,
    "motifdatecree" DATE ,
    "motifdatemaj" DATE ,
    "motifdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table natureinscription
CREATE TABLE "natureinscription" (
    "natinscription" VARCHAR(1)  NOT NULL  UNIQUE ,
    "natinscrlibelle" VARCHAR(50) ,
    "natdatecree" DATE ,
    "natdatemaj" DATE ,
    "natdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table objetformulaire
CREATE TABLE "objetformulaire" (
    "objetcode" VARCHAR(20)  NOT NULL ,
    "droitaccesnum" NUMERIC(19,0)  NOT NULL ,
    "objettype" VARCHAR(20) ,
    "objetlibelle" VARCHAR(50) ,
    "autorisation" VARCHAR(20) );
CREATE INDEX "WDIDX_objetformulaire_droitaccesnum" ON "objetformulaire" ("droitaccesnum");
CREATE INDEX "WDIDX_objetformulaire_PRIMARY" ON "objetformulaire" ("objetcode","droitaccesnum");

-- Cr�ation de la table parametreformulaire
CREATE TABLE "parametreformulaire" (
    "idparametreutilisateur" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "objetcode" VARCHAR(20)  NOT NULL  UNIQUE ,
    "droitaccesnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "champsvide" VARCHAR(38) );
CREATE INDEX "WDIDX_parametreformulaire_PRIMARY" ON "parametreformulaire" ("idparametreutilisateur","objetcode","droitaccesnum");
CREATE INDEX "WDIDX_parametreformulaire_FK_parametreformulaire2" ON "parametreformulaire" ("objetcode","droitaccesnum");

-- Cr�ation de la table parametreutilisateur
CREATE TABLE "parametreutilisateur" (
    "idparametreutilisateur" NUMERIC(19,0)  PRIMARY KEY ,
    "profilnum" NUMERIC(19,0) ,
    "codeacces" VARCHAR(10) ,
    "pwd1" VARCHAR(32) ,
    "datepwd1" DATE ,
    "pwd2" VARCHAR(32) ,
    "datepwd2" DATE ,
    "pwd3" VARCHAR(32) ,
    "datepwd3" DATE ,
    "pwd4" VARCHAR(32) ,
    "datepwd4" DATE ,
    "agencedefaut" VARCHAR(10) ,
    "collabdefaut" VARCHAR(10) ,
    "secretdefaut" VARCHAR(10) ,
    "dossierencours" SMALLINT ,
    "typeutilisateur" VARCHAR(5) ,
    "profilnum2" NUMERIC(19,0) ,
    "profilnum3" NUMERIC(19,0) ,
    "secretaire" VARCHAR(10) ,
    "collaborateur" VARCHAR(10) ,
    "agence" VARCHAR(10) ,
    "relance" SMALLINT ,
    "messirdweb" SMALLINT ,
    "utildatecree" DATE ,
    "utildatemaj" DATE ,
    "utildatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "langue" VARCHAR(4) ,
    "administrateur" SMALLINT ,
    "addrmac" VARCHAR(50) ,
    "addip" VARCHAR(50) ,
    "sessipmac" INTEGER );
CREATE INDEX "WDIDX_parametreutilisateur_profilnum" ON "parametreutilisateur" ("profilnum");

-- Cr�ation de la table pathologie
CREATE TABLE "pathologie" (
    "pathologiecode" VARCHAR(2)  NOT NULL  UNIQUE ,
    "pathologielibelle" VARCHAR(60) ,
    "pathologietype" VARCHAR(1) ,
    "pathologiedatecree" DATE ,
    "pathologiedatemaj" DATE ,
    "pathologiedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table payeur
CREATE TABLE "payeur" (
    "payeurnum" NUMERIC(19,0)  PRIMARY KEY ,
    "compteid" NUMERIC(19,0) ,
    "persnum" NUMERIC(19,0) ,
    "hononumfacture" VARCHAR(20) ,
    "payeurtype" VARCHAR(5) ,
    "oldpayeurnum" NUMERIC(19,0) );
CREATE INDEX "WDIDX_payeur_compteid" ON "payeur" ("compteid");
CREATE INDEX "WDIDX_payeur_persnum" ON "payeur" ("persnum");
CREATE INDEX "WDIDX_payeur_hononumfacture" ON "payeur" ("hononumfacture");

-- Cr�ation de la table pays
CREATE TABLE "pays" (
    "payscode" VARCHAR(4)  NOT NULL  UNIQUE ,
    "paysnom" VARCHAR(50) ,
    "paysdatecree" DATE ,
    "paysdatemaj" DATE ,
    "paysdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table personne
CREATE TABLE "personne" (
    "oldpersnum" NUMERIC(19,0) ,
    "srcpersnum" VARCHAR(5) ,
    "persnum" NUMERIC(19,0)  PRIMARY KEY ,
    "docnom" VARCHAR(30) ,
    "groupecode" VARCHAR(38) ,
    "conventioncode" VARCHAR(6) ,
    "situationjuricode" VARCHAR(10) ,
    "idparametreutilisateur" NUMERIC(19,0) ,
    "famillecode" VARCHAR(20) ,
    "juridiquestatut" VARCHAR(2) ,
    "adminnum" NUMERIC(19,0) ,
    "typeperscode" VARCHAR(10) ,
    "natinscription" VARCHAR(1) ,
    "compteid" NUMERIC(19,0) ,
    "compteidparagence" NUMERIC(19,0) ,
    "specialitecode" VARCHAR(10) ,
    "procode" VARCHAR(32) ,
    "prochrono" NUMERIC(19,0) ,
    "categentrcode" VARCHAR(6) ,
    "perscode" VARCHAR(38) ,
    "perscodegta" VARCHAR(5) ,
    "perscommentaire" LONGVARBINARY ,
    "persdatecree" DATE ,
    "persdatemaj" DATE ,
    "persdatesuppr" DATE ,
    "persprovenance" VARCHAR(10) ,
    "persemail" VARCHAR(60) ,
    "persemail2" VARCHAR(60) ,
    "persweb" VARCHAR(60) ,
    "perstauxvetuste" NUMERIC(19,0) ,
    "persmdpintranet" VARCHAR(32) ,
    "perscategorie" VARCHAR(40) ,
    "persloginintranet" VARCHAR(15) ,
    "persmtplafond1" REAL ,
    "persmtplafond2" REAL ,
    "perstypefacturation" VARCHAR(1) ,
    "perspourcenttravaux" REAL ,
    "perstvaintra" VARCHAR(32) ,
    "perspartenaire" SMALLINT ,
    "persdistance" NUMERIC(19,0) ,
    "persnumordre" VARCHAR(50) ,
    "persagree" SMALLINT ,
    "persaignorer" SMALLINT ,
    "persqualiteresp" VARCHAR(32) ,
    "perscodeape" VARCHAR(4) ,
    "perssiret" VARCHAR(18) ,
    "persrc" VARCHAR(10) ,
    "perscapital" REAL ,
    "perstrancheff" NUMERIC(19,0) ,
    "perseffectlocal" NUMERIC(19,0) ,
    "persdatesitujuri" DATE ,
    "perscodequalib1" VARCHAR(6) ,
    "perscodequalib2" VARCHAR(6) ,
    "perscodequalib3" VARCHAR(6) ,
    "perscodequalib4" VARCHAR(6) ,
    "perscodequalib5" VARCHAR(6) ,
    "perscodequalib6" VARCHAR(6) ,
    "perscodequalib7" VARCHAR(6) ,
    "perscodequalib8" VARCHAR(6) ,
    "perscodequalib9" VARCHAR(6) ,
    "perscodequalib10" VARCHAR(6) ,
    "persinterv1" SMALLINT ,
    "persinterv2" SMALLINT ,
    "persinterv3" SMALLINT ,
    "persinterv4" SMALLINT ,
    "persinterv5" SMALLINT ,
    "persinterv6" SMALLINT ,
    "persinterv7" SMALLINT ,
    "persinterv8" SMALLINT ,
    "persinterv9" SMALLINT ,
    "persinterv10" SMALLINT ,
    "persqualif1" SMALLINT ,
    "persqualif2" SMALLINT ,
    "persqualif3" SMALLINT ,
    "persqualif4" SMALLINT ,
    "persqualif5" SMALLINT ,
    "persqualif6" SMALLINT ,
    "persqualif7" SMALLINT ,
    "persqualif8" SMALLINT ,
    "persqualif9" SMALLINT ,
    "persqualif10" SMALLINT ,
    "persintervsucc" NUMERIC(19,0) ,
    "perssiren" VARCHAR(12) ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_personne_groupecode" ON "personne" ("groupecode");
CREATE INDEX "WDIDX_personne_conventioncode" ON "personne" ("conventioncode");
CREATE INDEX "WDIDX_personne_situationjuricode" ON "personne" ("situationjuricode");
CREATE INDEX "WDIDX_personne_idparametreutilisateur" ON "personne" ("idparametreutilisateur");
CREATE INDEX "WDIDX_personne_famillecode" ON "personne" ("famillecode");
CREATE INDEX "WDIDX_personne_juridiquestatut" ON "personne" ("juridiquestatut");
CREATE INDEX "WDIDX_personne_adminnum" ON "personne" ("adminnum");
CREATE INDEX "WDIDX_personne_typeperscode" ON "personne" ("typeperscode");
CREATE INDEX "WDIDX_personne_natinscription" ON "personne" ("natinscription");
CREATE INDEX "WDIDX_personne_compteid" ON "personne" ("compteid");
CREATE INDEX "WDIDX_personne_compteidparagence" ON "personne" ("compteidparagence");
CREATE INDEX "WDIDX_personne_specialitecode" ON "personne" ("specialitecode");
CREATE INDEX "WDIDX_personne_categentrcode" ON "personne" ("categentrcode");
CREATE INDEX "WDIDX_personne_perscode" ON "personne" ("perscode");
CREATE INDEX "WDIDX_personne_perscategorie" ON "personne" ("perscategorie");
CREATE INDEX "WDIDX_personne_FK_chartepersonne" ON "personne" ("procode","prochrono");

-- Cr�ation de la table planning
CREATE TABLE "planning" (
    "planningnum" NUMERIC(19,0)  PRIMARY KEY ,
    "planningcode" VARCHAR(10) ,
    "collabnum" NUMERIC(19,0) ,
    "typetelcode" VARCHAR(2) ,
    "payscode" VARCHAR(4) ,
    "collabnum2" NUMERIC(19,0) ,
    "secteurcode" VARCHAR(5) ,
    "persnum" NUMERIC(19,0) ,
    "typetelcode2" VARCHAR(2) ,
    "codepostal" VARCHAR(10) ,
    "ville" VARCHAR(28) ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "civilitenum" NUMERIC(19,0) ,
    "planningdatedebut" DATE ,
    "planningdatefin" DATE ,
    "planningnature" VARCHAR(1) ,
    "planningvalid" VARCHAR(1) ,
    "planningdatecree" DATE ,
    "planningdatemaj" DATE ,
    "planningdatesuppr" DATE ,
    "planningnom" VARCHAR(38) ,
    "planningadresse1" VARCHAR(38) ,
    "planningadresse2" VARCHAR(38) ,
    "planningadresse3" VARCHAR(38) ,
    "planningcommentaire" LONGVARBINARY ,
    "planningtel1" VARCHAR(20) ,
    "planningtel2" VARCHAR(20) ,
    "planningabsent" SMALLINT ,
    "planningraisonabsence" VARCHAR(100) ,
    "planningconfirmepar" VARCHAR(8) ,
    "planningpriorite" VARCHAR(1) ,
    "planninglibelle" VARCHAR(60) ,
    "planningtypetel1" VARCHAR(10) ,
    "planningtypetel2" VARCHAR(10) ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "planningduree" TIME ,
    "planningdureealler" TIME ,
    "planningdureeretour" TIME ,
    "planningcommentaire2" VARCHAR(50) ,
    "planningligne" INTEGER ,
    "planningexchange" VARCHAR(255) );
CREATE INDEX "WDIDX_planning_collabnum" ON "planning" ("collabnum");
CREATE INDEX "WDIDX_planning_typetelcode" ON "planning" ("typetelcode");
CREATE INDEX "WDIDX_planning_collabnum2" ON "planning" ("collabnum2");
CREATE INDEX "WDIDX_planning_secteurcode" ON "planning" ("secteurcode");
CREATE INDEX "WDIDX_planning_persnum" ON "planning" ("persnum");
CREATE INDEX "WDIDX_planning_typetelcode2" ON "planning" ("typetelcode2");
CREATE INDEX "WDIDX_planning_civilitenum" ON "planning" ("civilitenum");
CREATE INDEX "WDIDX_planning_planningexchange" ON "planning" ("planningexchange");
CREATE INDEX "WDIDX_planning_FK_dossierrdv" ON "planning" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
CREATE INDEX "WDIDX_planning_FK_rdvlocalisation" ON "planning" ("codepostal","ville");

-- Cr�ation de la table postit
CREATE TABLE "postit" (
    "postitnum" NUMERIC(19,0)  PRIMARY KEY ,
    "postitexpediteur" VARCHAR(3) ,
    "postittypeexp" VARCHAR(1) ,
    "postitdestinataire" VARCHAR(3) ,
    "postittypedest" VARCHAR(1) ,
    "postitobjet" VARCHAR(50) ,
    "postitmessage" LONGVARBINARY ,
    "postitpiecejointe" VARCHAR(100) ,
    "postitdatesuppr" DATE ,
    "postitdatemaj" DATE ,
    "postitdatecree" DATE ,
    "postitdateactive" DATE ,
    "postitdatefin" DATE ,
    "postitnbactive" NUMERIC(19,0) ,
    "postitperiodicite" VARCHAR(1) ,
    "postitperioderecur" NUMERIC(19,0) ,
    "postitfini" SMALLINT ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table postithisto
CREATE TABLE "postithisto" (
    "postitnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "postithistoligne" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "postithistodate" DATE ,
    "postithistodaterel" DATE ,
    "postithistofini" SMALLINT ,
    "postithistodatecree" DATE ,
    "postithistodatemaj" DATE ,
    "postithistodatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_postithisto_PRIMARY" ON "postithisto" ("postitnum","postithistoligne");

-- Cr�ation de la table prejudicedossier
CREATE TABLE "prejudicedossier" (
    "dossnum" VARCHAR(20)  NOT NULL ,
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "cbtcode2" VARCHAR(20)  NOT NULL ,
    "cbttype2" VARCHAR(2)  NOT NULL ,
    "typeprejunum" NUMERIC(19,0)  NOT NULL ,
    "dossprejunum" NUMERIC(19,0) ,
    "dossprejudatedebut" DATE ,
    "dossprejudatefin" DATE ,
    "dossprejucout" REAL ,
    "dossprejucommentaire" LONGVARBINARY );
CREATE INDEX "WDIDX_prejudicedossier_typeprejunum" ON "prejudicedossier" ("typeprejunum");
CREATE INDEX "WDIDX_prejudicedossier_PRIMARY" ON "prejudicedossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","typeprejunum");
CREATE INDEX "WDIDX_prejudicedossier_FK_prejudicedossier" ON "prejudicedossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table profil
CREATE TABLE "profil" (
    "profilnum" NUMERIC(19,0)  PRIMARY KEY ,
    "profilcode" VARCHAR(20) ,
    "profildatecree" DATE ,
    "profildatemaj" DATE ,
    "profildatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table qualite
CREATE TABLE "qualite" (
    "qualitecode" VARCHAR(2)  NOT NULL  UNIQUE ,
    "qualitelibelle" VARCHAR(50) ,
    "qualitedatecree" DATE ,
    "qualitedatemaj" DATE ,
    "qualitedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table reglement
CREATE TABLE "reglement" (
    "reglementnum" VARCHAR(20)  NOT NULL ,
    "compteid" NUMERIC(19,0)  NOT NULL ,
    "type_piece" VARCHAR(2)  NOT NULL ,
    "tresorcode" VARCHAR(6) ,
    "reglementdatesaisie" DATE ,
    "reglementdatemaj" DATE ,
    "reglementdatesuppr" DATE ,
    "reglementdatetransfert" DATE ,
    "reglementmoderegl" VARCHAR(20) ,
    "reglementreference" VARCHAR(30) ,
    "reglementlibelle" VARCHAR(60) ,
    "reglementtotalregle" REAL ,
    "reglementcodeecart" VARCHAR(2) ,
    "reglementcoderemise" VARCHAR(2) ,
    "reglementmtecart" REAL ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "agence" VARCHAR(6) ,
    "cabinet" VARCHAR(6) ,
    "payeurnum" NUMERIC(19,0) ,
    "codepayeur" VARCHAR(38) ,
    "nompayeur" LONGVARCHAR ,
    "numpersassur" NUMERIC(19,0) ,
    "codeassur" VARCHAR(38) ,
    "nomassur" LONGVARCHAR ,
    "dossier" VARCHAR(12) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "refcompta" VARCHAR(50) ,
    "police" VARCHAR(32) ,
    "sinistre" VARCHAR(50) ,
    "collab1" VARCHAR(6) ,
    "collab2" VARCHAR(6) ,
    "collab3" VARCHAR(6) ,
    "collab4" VARCHAR(6) ,
    "collab5" VARCHAR(6) ,
    "secretaire" VARCHAR(6) ,
    "secretaire2" VARCHAR(6) ,
    "mtcollab1" FLOAT ,
    "mtcollab2" FLOAT ,
    "mtcollab3" FLOAT ,
    "mtcollab4" FLOAT ,
    "mtcollab5" FLOAT ,
    "mtfraiscollab1" FLOAT ,
    "mtfraiscollab2" FLOAT ,
    "mtfraiscollab3" FLOAT ,
    "mtfraiscollab4" FLOAT ,
    "mtfraiscollab5" FLOAT ,
    "totalht" FLOAT ,
    "totalfraisht" FLOAT ,
    "totalhonoht" FLOAT ,
    "mtfacture" FLOAT ,
    "mtreste" FLOAT ,
    "datesinistre" DATE ,
    "datehono" DATE ,
    "datepiece" DATE ,
    "dateche" DATE ,
    "datlettrage" DATE ,
    "daterelanc" DATE ,
    "datecompta" DATE ,
    "lettre" VARCHAR(2) ,
    "numrelanc" VARCHAR(2) ,
    "factsolde" SMALLINT ,
    "pointtemp" SMALLINT ,
    "flagetat" VARCHAR(1) ,
    "flagtemp" VARCHAR(1) ,
    "operateur" VARCHAR(6) );
CREATE INDEX "WDIDX_reglement_compteid" ON "reglement" ("compteid");
CREATE INDEX "WDIDX_reglement_tresorcode" ON "reglement" ("tresorcode");
CREATE INDEX "WDIDX_reglement_reglementdatesaisie" ON "reglement" ("reglementdatesaisie");
CREATE INDEX "WDIDX_reglement_PRIMARY" ON "reglement" ("reglementnum","compteid","type_piece");
CREATE INDEX "WDIDX_reglement_FK_DOSSIER" ON "reglement" ("dossier","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table reglementligne
CREATE TABLE "reglementligne" (
    "reglementnum" VARCHAR(20)  NOT NULL ,
    "compteid" NUMERIC(19,0)  NOT NULL ,
    "type_piece" VARCHAR(2)  NOT NULL ,
    "hononumfacture" VARCHAR(20)  NOT NULL ,
    "reglementligne" NUMERIC(19,0) ,
    "regleentlignemtregle" REAL ,
    "reglementlignemtreste" REAL ,
    "reglementlignefactsolde" SMALLINT ,
    "reglementlignemtecart" REAL ,
    "reglementlignefactregle" SMALLINT );
CREATE INDEX "WDIDX_reglementligne_hononumfacture" ON "reglementligne" ("hononumfacture");
CREATE INDEX "WDIDX_reglementligne_PRIMARY" ON "reglementligne" ("reglementnum","compteid","type_piece","hononumfacture");
CREATE INDEX "WDIDX_reglementligne_FK_reglementligne2" ON "reglementligne" ("reglementnum","compteid","type_piece");

-- Cr�ation de la table reglementlignetamp
CREATE TABLE "reglementlignetamp" (
    "reglementnum" VARCHAR(20)  NOT NULL ,
    "compteid" NUMERIC(19,0)  NOT NULL ,
    "type_piece" VARCHAR(2)  NOT NULL ,
    "hononumfacture" VARCHAR(20)  NOT NULL ,
    "reglementligne" NUMERIC(19,0) ,
    "regleentlignemtregle" REAL ,
    "reglementlignemtreste" REAL ,
    "reglementlignefactsolde" SMALLINT ,
    "reglementlignemtecart" REAL ,
    "reglementlignefactregle" SMALLINT ,
    "reglementlignedatesaisie" DATE ,
    "codepayeur" VARCHAR(50) ,
    "mtfacture" FLOAT );
CREATE INDEX "WDIDX_reglementlignetamp_PRIMARY" ON "reglementlignetamp" ("reglementnum","compteid","type_piece","hononumfacture");

-- Cr�ation de la table reglementparam
CREATE TABLE "reglementparam" (
    "rglparamnum" NUMERIC(19,0)  PRIMARY KEY ,
    "tresorcode" VARCHAR(6) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "rglparamcomptautilisee" VARCHAR(20) ,
    "rglparammtdiff" REAL ,
    "rglparamcomptedebit" VARCHAR(15) ,
    "rglparamcomptecredit" VARCHAR(15) ,
    "rglparamdatedernmvt" DATE ,
    "rglparamdatedermaj" DATE ,
    "rglparamdernjournal" VARCHAR(6) ,
    "rglparamdernmontant" REAL ,
    "rglparammoderegle" VARCHAR(20) ,
    "rglparamdelairegle" NUMERIC(19,0) ,
    "rglparamdatecree" DATE ,
    "rglparamdatemaj" DATE ,
    "rglparamdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "rglparamgestdiff" VARCHAR(1) );
CREATE INDEX "WDIDX_reglementparam_tresorcode" ON "reglementparam" ("tresorcode");
CREATE INDEX "WDIDX_reglementparam_FK_par_cabinet" ON "reglementparam" ("cbtcode","cbttype");

-- Cr�ation de la table relance
CREATE TABLE "relance" (
    "hononumfacture" VARCHAR(20) ,
    "relanceligne" INTEGER  NOT NULL  UNIQUE ,
    "secretnum" NUMERIC(19,0) ,
    "relancetype" VARCHAR(10) ,
    "relancedate" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_relance_hononumfacture" ON "relance" ("hononumfacture");
CREATE INDEX "WDIDX_relance_secretnum" ON "relance" ("secretnum");

-- Cr�ation de la table repartitiontemp
CREATE TABLE "repartitiontemp" (
    "IDUTIL" INTEGER ,
    "CODESINISTRE" VARCHAR(50) ,
    "NUMASSUREUR" INTEGER ,
    "CODEPOSTAL" VARCHAR(50) ,
    "NOMSINISTRE" VARCHAR(50) ,
    "NOMASSUREUR" VARCHAR(50) ,
    "DATE" VARCHAR(50) ,
    "MTHONO" FLOAT ,
    "MTFRAIS" FLOAT ,
    "DOSSIER" VARCHAR(50) ,
    "CODEASSUREUR" VARCHAR(50) );

-- Cr�ation de la table reunion
CREATE TABLE "reunion" (
    "reunionnum" VARCHAR(20)  NOT NULL  UNIQUE ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "reuniondate" DATE ,
    "reunionheure" TIME ,
    "reunionsujet" LONGVARBINARY ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "reuniondatecree" DATE ,
    "reuniondatemaj" DATE ,
    "reuniondatesuppr" DATE );
CREATE INDEX "WDIDX_reunion_FK_convoque" ON "reunion" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table rib
CREATE TABLE "rib" (
    "cbtcode" VARCHAR(20) ,
    "ribnum" NUMERIC(19,0)  PRIMARY KEY ,
    "ribbanque" VARCHAR(6) ,
    "ribindicatif" VARCHAR(6) ,
    "ribnumcompte" VARCHAR(15) ,
    "ribcle" VARCHAR(2) ,
    "ribdomiciliation" VARCHAR(38) ,
    "ribiban" VARCHAR(35) ,
    "ribbic" VARCHAR(10) ,
    "ribdatecree" DATE ,
    "ribdatemaj" DATE ,
    "ribdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "tresorcode" VARCHAR(6) );
CREATE INDEX "WDIDX_rib_tresorcode" ON "rib" ("tresorcode");

-- Cr�ation de la table secretaire
CREATE TABLE "secretaire" (
    "secretnum" NUMERIC(19,0)  PRIMARY KEY ,
    "adminnum" NUMERIC(19,0) ,
    "idparametreutilisateur" NUMERIC(19,0) ,
    "secretcode" VARCHAR(10) ,
    "secretemail" VARCHAR(60) ,
    "secretdateentree" DATE ,
    "secretdatesortie" DATE ,
    "secretdatecree" DATE ,
    "secretdatemaj" DATE ,
    "secretdatesuppr" DATE ,
    "secretaffiche" SMALLINT ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "secretinfosecu" LONGVARBINARY );
CREATE INDEX "WDIDX_secretaire_adminnum" ON "secretaire" ("adminnum");
CREATE INDEX "WDIDX_secretaire_idparametreutilisateur" ON "secretaire" ("idparametreutilisateur");

-- Cr�ation de la table secretcommission
CREATE TABLE "secretcommission" (
    "secretcom_id" NUMERIC(19,0)  PRIMARY KEY ,
    "secretnum" NUMERIC(19,0) ,
    "hononumfacture" VARCHAR(20) ,
    "compourcent" REAL ,
    "commontant" REAL );
CREATE INDEX "WDIDX_secretcommission_secretnum" ON "secretcommission" ("secretnum");
CREATE INDEX "WDIDX_secretcommission_hononumfacture" ON "secretcommission" ("hononumfacture");

-- Cr�ation de la table sinistre
CREATE TABLE "sinistre" (
    "sinistrecode" VARCHAR(38)  NOT NULL  UNIQUE ,
    "metiercode" VARCHAR(20) ,
    "metierpwd" VARCHAR(20) ,
    "famillecode" VARCHAR(20) ,
    "sinistrelibelle" VARCHAR(50) ,
    "sinistreplafond" VARCHAR(1) ,
    "sinistredatecree" DATE ,
    "sinistredatemaj" DATE ,
    "sinistredatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_sinistre_famillecode" ON "sinistre" ("famillecode");
CREATE INDEX "WDIDX_sinistre_FK_sinistremetier" ON "sinistre" ("metiercode","metierpwd");

-- Cr�ation de la table sinistrepersonne
CREATE TABLE "sinistrepersonne" (
    "persnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "sinistrecode" VARCHAR(38)  NOT NULL  UNIQUE ,
    "sinistrecorres" VARCHAR(38) );
CREATE INDEX "WDIDX_sinistrepersonne_PRIMARY" ON "sinistrepersonne" ("persnum","sinistrecode");

-- Cr�ation de la table situationjuridique
CREATE TABLE "situationjuridique" (
    "situationjuricode" VARCHAR(10)  NOT NULL  UNIQUE ,
    "situationjurilibelle" VARCHAR(30) ,
    "situationdatemaj" DATE ,
    "situationdatecree" DATE ,
    "situationdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table specialite
CREATE TABLE "specialite" (
    "specialitecode" VARCHAR(10)  NOT NULL  UNIQUE ,
    "specialitelibelle" VARCHAR(50) ,
    "specialitedatecree" DATE ,
    "specialitedatemaj" DATE ,
    "specialitedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table statparammaif
CREATE TABLE "statparammaif" (
    "statparammaifcode" VARCHAR(38)  NOT NULL ,
    "statparammaifcolonne" NUMERIC(19,0)  NOT NULL ,
    "sinistrecode" VARCHAR(38)  NOT NULL ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "statparammaifdatecree" DATE ,
    "statparammaifdatesuppr" DATE );
CREATE INDEX "WDIDX_statparammaif_sinistrecode" ON "statparammaif" ("sinistrecode");
CREATE INDEX "WDIDX_statparammaif_PRIMARY" ON "statparammaif" ("statparammaifcode","statparammaifcolonne","sinistrecode");

-- Cr�ation de la table statut
CREATE TABLE "statut" (
    "statutcode" VARCHAR(2)  NOT NULL  UNIQUE ,
    "statutlibelle" VARCHAR(50) ,
    "statutdatecree" DATE ,
    "statutdatemaj" DATE ,
    "statutdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table tableaubord
CREATE TABLE "tableaubord" (
    "ASSUREUR" VARCHAR(50) ,
    "EXPERT" VARCHAR(50) ,
    "SINISTRE" VARCHAR(50) ,
    "NBENCOURSDEB" INTEGER ,
    "NBRECU" INTEGER ,
    "NBCLOS" INTEGER ,
    "NBENCOURSFIN" INTEGER ,
    "DELAIMOY" INTEGER ,
    "DELAITOT" INTEGER ,
    "HONO" FLOAT ,
    "FRAIS" FLOAT ,
    "DOMMAGEDEM" FLOAT ,
    "DOMMAGEATTRIBUER" FLOAT ,
    "INDEMNITE" FLOAT ,
    "DOMMDEM" INTEGER ,
    "DOMMATTRIB" INTEGER ,
    "NBINDEMNITE" INTEGER ,
    "IDUTIL" INTEGER ,
    "ASSUREURCODE" VARCHAR(50) ,
    "EXPERTCODE" VARCHAR(50) ,
    "KEYTABLE" INTEGER  PRIMARY KEY );

-- Cr�ation de la table tabledossiertableaubord
CREATE TABLE "tabledossiertableaubord" (
    "IDUTIL" INTEGER ,
    "DOSSIER" VARCHAR(50) );

-- Cr�ation de la table tauxhoraire
CREATE TABLE "tauxhoraire" (
    "tauxhorairenum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "collabnum" NUMERIC(19,0) ,
    "secretnum" NUMERIC(19,0) ,
    "tauxhoraireligne" NUMERIC(19,0) ,
    "tauxhorairetypetravaux" VARCHAR(100) ,
    "tauxhorairetypeligne" VARCHAR(5) ,
    "tauxhoraire" REAL ,
    "tauxhorairedatecree" DATE ,
    "tauxhorairedatemaj" DATE ,
    "tauxhorairedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_tauxhoraire_collabnum" ON "tauxhoraire" ("collabnum");
CREATE INDEX "WDIDX_tauxhoraire_secretnum" ON "tauxhoraire" ("secretnum");

-- Cr�ation de la table telbareme
CREATE TABLE "telbareme" (
    "baremecode" VARCHAR(15)  NOT NULL  UNIQUE ,
    "corpsetatcode" VARCHAR(6) ,
    "tvacode" VARCHAR(10) ,
    "typebaremecode" VARCHAR(5) ,
    "persnumassur" NUMERIC(19,0) ,
    "persnumentreprise" NUMERIC(19,0) ,
    "baremelibellecourt" VARCHAR(32) ,
    "baremelibellelong" VARCHAR(64) ,
    "baremepu" REAL ,
    "baremeunite" VARCHAR(3) ,
    "baremeannee" DATE ,
    "baremedatecree" DATE ,
    "baremedatemaj" DATE ,
    "baremedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_telbareme_corpsetatcode" ON "telbareme" ("corpsetatcode");
CREATE INDEX "WDIDX_telbareme_tvacode" ON "telbareme" ("tvacode");
CREATE INDEX "WDIDX_telbareme_typebaremecode" ON "telbareme" ("typebaremecode");
CREATE INDEX "WDIDX_telbareme_persnumassur" ON "telbareme" ("persnumassur");
CREATE INDEX "WDIDX_telbareme_persnumentreprise" ON "telbareme" ("persnumentreprise");

-- Cr�ation de la table telcategentr
CREATE TABLE "telcategentr" (
    "categentrcode" VARCHAR(6)  NOT NULL  UNIQUE ,
    "categlibellecourt" VARCHAR(32) ,
    "categlibellelong" VARCHAR(64) ,
    "categentrdatecree" DATE ,
    "categentrdatemaj" DATE ,
    "categentrdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table telcorpsetat
CREATE TABLE "telcorpsetat" (
    "corpsetatcode" VARCHAR(6)  NOT NULL  UNIQUE ,
    "corpsetatlibellecourt" VARCHAR(32) ,
    "corpsetatlibellelong" VARCHAR(64) ,
    "corpsetatdatecree" DATE ,
    "corpsetatdatemaj" DATE ,
    "corpsetatdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table telcorpsetatentr
CREATE TABLE "telcorpsetatentr" (
    "persnum" NUMERIC(19,0)  NOT NULL ,
    "corpsetatcode" VARCHAR(6)  NOT NULL );
CREATE INDEX "WDIDX_telcorpsetatentr_corpsetatcode" ON "telcorpsetatentr" ("corpsetatcode");
CREATE INDEX "WDIDX_telcorpsetatentr_PRIMARY" ON "telcorpsetatentr" ("persnum","corpsetatcode");

-- Cr�ation de la table teldevis
CREATE TABLE "teldevis" (
    "devisnum" VARCHAR(20)  NOT NULL  UNIQUE ,
    "dossnum" VARCHAR(20) ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "cbtcode2" VARCHAR(20) ,
    "cbttype2" VARCHAR(2) ,
    "histoligne" NUMERIC(19,0) ,
    "persnum" NUMERIC(19,0) ,
    "docnom" VARCHAR(30) ,
    "secteurcode" VARCHAR(5) ,
    "devistypecloture" VARCHAR(1) ,
    "devisreparpar" VARCHAR(1) ,
    "deviscidre" SMALLINT ,
    "devisconforme" SMALLINT ,
    "devistypeventil" VARCHAR(1) ,
    "devisdatecree" DATE ,
    "devisdatemaj" DATE ,
    "devisdatesuppr" DATE ,
    "devisdateimprime" DATE ,
    "deviscodetva1" NUMERIC(19,0) ,
    "deviscodetva2" NUMERIC(19,0) ,
    "deviscodetva3" NUMERIC(19,0) ,
    "devistauxtva1" FLOAT ,
    "devistauxtva2" FLOAT ,
    "devistauxtva3" FLOAT ,
    "devismtht1" REAL ,
    "devismtht2" REAL ,
    "devismtht3" REAL ,
    "devismttva1" REAL ,
    "devismttva2" REAL ,
    "devismttva3" REAL ,
    "devismtttc1" REAL ,
    "devismtttc2" REAL ,
    "devismtttc3" REAL ,
    "devistotalht" REAL ,
    "devistotaltva" REAL ,
    "devistotalttc" REAL ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "devistotalmo" REAL ,
    "devistotalmat" REAL ,
    "devistotalmob" REAL ,
    "devistotalembel" REAL ,
    "devistauxassure" FLOAT ,
    "devistauxvetuste" FLOAT );
CREATE INDEX "WDIDX_teldevis_persnum" ON "teldevis" ("persnum");
CREATE INDEX "WDIDX_teldevis_docnom" ON "teldevis" ("docnom");
CREATE INDEX "WDIDX_teldevis_secteurcode" ON "teldevis" ("secteurcode");
CREATE INDEX "WDIDX_teldevis_FK_dossierteldevis" ON "teldevis" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
CREATE INDEX "WDIDX_teldevis_FK_teldevishisto" ON "teldevis" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne");

-- Cr�ation de la table teldevisligne
CREATE TABLE "teldevisligne" (
    "devisnum" VARCHAR(20)  NOT NULL  UNIQUE ,
    "devisligne" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "tvacode" VARCHAR(10) ,
    "baremecode" VARCHAR(15) ,
    "typebaremecode" VARCHAR(5) ,
    "localisationcode" VARCHAR(5) ,
    "corpsetatcode" VARCHAR(6) ,
    "devislignelibellebareme" VARCHAR(32) ,
    "devislignelibelle" VARCHAR(60) ,
    "devisligneunite" VARCHAR(3) ,
    "devislignelongueur" FLOAT ,
    "devislignehauteur" FLOAT ,
    "devislignepu" REAL ,
    "devislignecondit" VARCHAR(25) ,
    "devislignemtht" REAL ,
    "devislignepupondere" REAL ,
    "devislignetauxpondere" FLOAT ,
    "devislignedatecree" DATE ,
    "devislignedatemaj" DATE ,
    "devislignedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_teldevisligne_tvacode" ON "teldevisligne" ("tvacode");
CREATE INDEX "WDIDX_teldevisligne_baremecode" ON "teldevisligne" ("baremecode");
CREATE INDEX "WDIDX_teldevisligne_typebaremecode" ON "teldevisligne" ("typebaremecode");
CREATE INDEX "WDIDX_teldevisligne_localisationcode" ON "teldevisligne" ("localisationcode");
CREATE INDEX "WDIDX_teldevisligne_corpsetatcode" ON "teldevisligne" ("corpsetatcode");
CREATE INDEX "WDIDX_teldevisligne_PRIMARY" ON "teldevisligne" ("devisnum","devisligne");

-- Cr�ation de la table telentrassureur
CREATE TABLE "telentrassureur" (
    "persnumentreprise" NUMERIC(19,0)  NOT NULL ,
    "persnumassur" NUMERIC(19,0)  NOT NULL );
CREATE INDEX "WDIDX_telentrassureur_persnumassur" ON "telentrassureur" ("persnumassur");
CREATE INDEX "WDIDX_telentrassureur_PRIMARY" ON "telentrassureur" ("persnumentreprise","persnumassur");

-- Cr�ation de la table telephone
CREATE TABLE "telephone" (
    "telnum" NUMERIC(19,0)  PRIMARY KEY ,
    "typetelcode" VARCHAR(2) ,
    "adminnum" NUMERIC(19,0) ,
    "telnumero" VARCHAR(20) ,
    "teldatecree" DATE ,
    "teldatemaj" DATE ,
    "teldatesuppr" DATE );
CREATE INDEX "WDIDX_telephone_typetelcode" ON "telephone" ("typetelcode");
CREATE INDEX "WDIDX_telephone_adminnum" ON "telephone" ("adminnum");

-- Cr�ation de la table tellocalisation
CREATE TABLE "tellocalisation" (
    "localisationcode" VARCHAR(5)  NOT NULL  UNIQUE ,
    "localisationlibelle" VARCHAR(50) ,
    "localisationdatesuppr" DATE ,
    "localisationdatemaj" DATE ,
    "localisationdatecree" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table telponderation
CREATE TABLE "telponderation" (
    "ponderationannee" DATE  NOT NULL ,
    "secteurnum" NUMERIC(19,0)  NOT NULL ,
    "secteurcode" VARCHAR(5)  NOT NULL ,
    "corpsetatcode" VARCHAR(6)  NOT NULL ,
    "persnum" NUMERIC(19,0)  NOT NULL ,
    "ponderationtaux" FLOAT ,
    "ponderationdatecree" DATE ,
    "ponderationdatemaj" DATE ,
    "ponderationdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_telponderation_secteurcode" ON "telponderation" ("secteurcode");
CREATE INDEX "WDIDX_telponderation_corpsetatcode" ON "telponderation" ("corpsetatcode");
CREATE INDEX "WDIDX_telponderation_persnum" ON "telponderation" ("persnum");
CREATE INDEX "WDIDX_telponderation_PRIMARY" ON "telponderation" ("ponderationannee","secteurnum","secteurcode","corpsetatcode","persnum");

-- Cr�ation de la table telsecteurentr
CREATE TABLE "telsecteurentr" (
    "secteurnum" NUMERIC(19,0)  NOT NULL ,
    "secteurcode" VARCHAR(5)  NOT NULL ,
    "persnum" NUMERIC(19,0)  NOT NULL );
CREATE INDEX "WDIDX_telsecteurentr_secteurcode" ON "telsecteurentr" ("secteurcode");
CREATE INDEX "WDIDX_telsecteurentr_persnum" ON "telsecteurentr" ("persnum");
CREATE INDEX "WDIDX_telsecteurentr_PRIMARY" ON "telsecteurentr" ("secteurnum","secteurcode","persnum");

-- Cr�ation de la table teltypebareme
CREATE TABLE "teltypebareme" (
    "typebaremecode" VARCHAR(5)  NOT NULL  UNIQUE ,
    "typebaremelibellecourt" VARCHAR(32) ,
    "typebaremelibellelong" VARCHAR(64) ,
    "typebaremedatecree" DATE ,
    "typebaremedatemaj" DATE ,
    "typebaremedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "typebaremecumul" INTEGER );

-- Cr�ation de la table temppartage
CREATE TABLE "temppartage" (
    "tempid" NUMERIC(19,0)  PRIMARY KEY ,
    "dossnum" VARCHAR(20)  NOT NULL ,
    "cbtcode" VARCHAR(20)  NOT NULL ,
    "cbttype" VARCHAR(2)  NOT NULL ,
    "cbtcode2" VARCHAR(20)  NOT NULL ,
    "cbttype2" VARCHAR(2)  NOT NULL ,
    "idparametreutilisateur" NUMERIC(19,0) ,
    "datetimedebut" DATE ,
    "datetimefin" DATE );
CREATE INDEX "WDIDX_temppartage_dossier_key" ON "temppartage" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");

-- Cr�ation de la table ticketmoderateur
CREATE TABLE "ticketmoderateur" (
    "ticketannee" DATE  NOT NULL  UNIQUE ,
    "ticketmt" REAL ,
    "ticketdatemaj" DATE ,
    "ticketdatecree" DATE ,
    "ticketdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table tresorerie
CREATE TABLE "tresorerie" (
    "tresorcode" VARCHAR(6)  NOT NULL  UNIQUE ,
    "collabnum" NUMERIC(19,0) ,
    "hononumfacture" VARCHAR(20) ,
    "tresorlibelle" VARCHAR(50) ,
    "tresordatedermvt" DATE ,
    "tresormodeventil" VARCHAR(10) ,
    "tresornumcompte" VARCHAR(15) ,
    "tresordatecree" DATE ,
    "tresordatemaj" DATE ,
    "tresordatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_tresorerie_collabnum" ON "tresorerie" ("collabnum");
CREATE INDEX "WDIDX_tresorerie_hononumfacture" ON "tresorerie" ("hononumfacture");

-- Cr�ation de la table tva
CREATE TABLE "tva" (
    "tvacode" VARCHAR(10)  NOT NULL  UNIQUE ,
    "cbtcode" VARCHAR(20) ,
    "cbttype" VARCHAR(2) ,
    "tvataux" REAL ,
    "tvalibelle" VARCHAR(32) ,
    "tvacompte" VARCHAR(15) ,
    "tvadatecree" DATE ,
    "tvadatemaj" DATE ,
    "tvadatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_tva_FK_cabinettva" ON "tva" ("cbtcode","cbttype");

-- Cr�ation de la table typecentresecu
CREATE TABLE "typecentresecu" (
    "secunum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "seculibelle" VARCHAR(32) ,
    "secudatecree" DATE ,
    "secudatemaj" DATE ,
    "secudatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table typecontrat
CREATE TABLE "typecontrat" (
    "typecontratcode" VARCHAR(20)  NOT NULL  UNIQUE ,
    "typecontratlibelle" VARCHAR(80) ,
    "typecontratdatecree" DATE ,
    "typecontratdatemaj" DATE ,
    "typecontratdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table typedocument
CREATE TABLE "typedocument" (
    "typedoccode" VARCHAR(6)  NOT NULL  UNIQUE ,
    "typedoclibelle" VARCHAR(32) ,
    "typedocdatecree" DATE ,
    "typedocdatemaj" DATE ,
    "typedocdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table typeexpertise
CREATE TABLE "typeexpertise" (
    "typeexpertisecode" VARCHAR(25)  NOT NULL  UNIQUE ,
    "metiercode" VARCHAR(20) ,
    "metierpwd" VARCHAR(20) ,
    "typeexpertiselibelle" VARCHAR(50) ,
    "typeexpertisedatecree" DATE ,
    "typeexpertisedatemaj" DATE ,
    "typeexpertisedatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
CREATE INDEX "WDIDX_typeexpertise_FK_metiertypeexpertise" ON "typeexpertise" ("metiercode","metierpwd");

-- Cr�ation de la table typegarantie
CREATE TABLE "typegarantie" (
    "garantcode" VARCHAR(5) ,
    "garantlibelle" VARCHAR(50) );

-- Cr�ation de la table typeindemnite
CREATE TABLE "typeindemnite" (
    "typeindemncode" VARCHAR(6)  NOT NULL  UNIQUE ,
    "typeindemnlibelle" VARCHAR(32) ,
    "typeindemndatecree" DATE ,
    "typeindemndatemaj" DATE ,
    "typeindemndatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table typelesion
CREATE TABLE "typelesion" (
    "lesioncode" VARCHAR(4)  NOT NULL  UNIQUE ,
    "lesionlibelle" VARCHAR(65) ,
    "lesiondatecree" DATE ,
    "lesiondatemaj" DATE ,
    "lesiondatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table typemission
CREATE TABLE "typemission" (
    "typemisisoncode" VARCHAR(10)  NOT NULL  UNIQUE ,
    "typemissionlibelle" VARCHAR(50) ,
    "typemissiondatecree" DATE ,
    "typemissiondatemaj" DATE  NOT NULL ,
    "typemissiondatesuppr" DATE ,
    "typemissionconvo" VARCHAR(12) ,
    "typemissionrapport" VARCHAR(12) ,
    "typemissionhono" VARCHAR(12) ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table typemissionconvocation
CREATE TABLE "typemissionconvocation" (
    "collabnum" NUMERIC(19,0)  NOT NULL  UNIQUE ,
    "docnom" VARCHAR(30)  NOT NULL  UNIQUE ,
    "typemisisoncode" VARCHAR(10)  NOT NULL  UNIQUE );
CREATE INDEX "WDIDX_typemissionconvocation_PRIMARY" ON "typemissionconvocation" ("collabnum","docnom","typemisisoncode");

-- Cr�ation de la table typepersonne
CREATE TABLE "typepersonne" (
    "typeperscode" VARCHAR(10)  NOT NULL  UNIQUE ,
    "typeperslibelle" VARCHAR(32) ,
    "typeperscategorie" VARCHAR(40) ,
    "typepersdatecree" DATE ,
    "typepersdatemaj" DATE ,
    "typepersdatesuppr" DATE ,
    "typepersbloque" SMALLINT ,
    "typepersnum" NUMERIC(19,0) ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "typepersmodifiable" SMALLINT );
CREATE INDEX "WDIDX_typepersonne_typeperscategorie" ON "typepersonne" ("typeperscategorie");

-- Cr�ation de la table typepi
CREATE TABLE "typepi" (
    "typepinum" NUMERIC(19,0)  PRIMARY KEY ,
    "typepilibelle" VARCHAR(50) ,
    "typepidatecree" DATE ,
    "typepidatemaj" DATE ,
    "typepidatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table typepipar
CREATE TABLE "typepipar" (
    "typepiparnum" NUMERIC(19,0)  PRIMARY KEY ,
    "typepiparlibelle" VARCHAR(50) ,
    "typepipardatecree" DATE ,
    "typepipardatemaj" DATE ,
    "typepipardatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table typeprejudice
CREATE TABLE "typeprejudice" (
    "typeprejunum" NUMERIC(19,0)  PRIMARY KEY ,
    "typeprejucode" VARCHAR(5) ,
    "typeprejulibelle" VARCHAR(120) ,
    "typeprejueco" SMALLINT ,
    "typeprejutemp" SMALLINT ,
    "typeprejudatecree" DATE ,
    "typeprejudatemaj" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) ,
    "typeprejudatesuppr" DATE );

-- Cr�ation de la table typeprejuest
CREATE TABLE "typeprejuest" (
    "prejuestnum" NUMERIC(19,0)  PRIMARY KEY ,
    "prejuestlibelle" VARCHAR(35) ,
    "prejuestdatecree" DATE ,
    "prejuestdatemaj" DATE ,
    "prejuestdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table typeprofession
CREATE TABLE "typeprofession" (
    "professionnum" NUMERIC(19,0)  PRIMARY KEY ,
    "professionlibelle" VARCHAR(60) ,
    "professiondatecree" DATE ,
    "professiondatemaj" DATE ,
    "professiondatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table typequantumdol
CREATE TABLE "typequantumdol" (
    "quantumnum" NUMERIC(19,0)  PRIMARY KEY ,
    "quantumlibelle" VARCHAR(35) ,
    "quantumdatecree" DATE ,
    "quantumdatemaj" DATE ,
    "quantumdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table typereception
CREATE TABLE "typereception" (
    "typereceptcode" VARCHAR(10)  NOT NULL  UNIQUE ,
    "typereceptlibelle" VARCHAR(32) ,
    "typereceptdatecree" DATE ,
    "typereceptdatemaj" DATE ,
    "typereceptdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table typetelephone
CREATE TABLE "typetelephone" (
    "typetelcode" VARCHAR(2)  NOT NULL  UNIQUE ,
    "typetellibelle" VARCHAR(32) ,
    "typeteldatecree" DATE ,
    "typeteldatemaj" DATE ,
    "typeteldatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );

-- Cr�ation de la table usageconstruction
CREATE TABLE "usageconstruction" (
    "usageconstcode" VARCHAR(8)  NOT NULL  UNIQUE ,
    "usageconstlibelle" VARCHAR(30) ,
    "usageconstdatecree" DATE ,
    "usageconstdatemaj" DATE ,
    "usageconstdatesuppr" DATE ,
    "paramutilnumsuppr" NUMERIC(19,0) );
-- Contraintes d'int�grit�
ALTER TABLE "admin" ADD FOREIGN KEY ("civilitenum") REFERENCES "civilite" ("civilitenum");
ALTER TABLE "adrcabinet" ADD FOREIGN KEY ("cbtcodelie","cbttypelie") REFERENCES "cabinet" ("cbtcode","cbttype");
ALTER TABLE "adrdossier" ADD FOREIGN KEY ("cbtcode3","cbttype3") REFERENCES "cabinet" ("cbtcode","cbttype");
ALTER TABLE "adrdossier" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "adrdossier" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "adrdossier" ADD FOREIGN KEY ("qualitecode") REFERENCES "qualite" ("qualitecode");
ALTER TABLE "adrdossier" ADD FOREIGN KEY ("statutcode") REFERENCES "statut" ("statutcode");
ALTER TABLE "adrpersonne" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "adrpersonne" ADD FOREIGN KEY ("persnumlie") REFERENCES "personne" ("persnum");
ALTER TABLE "adrpersonne" ADD FOREIGN KEY ("typeperscode") REFERENCES "typepersonne" ("typeperscode");
ALTER TABLE "adrpersonne" ADD FOREIGN KEY ("typeperscodelie") REFERENCES "typepersonne" ("typeperscode");
ALTER TABLE "adrpersonne" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "annexe" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "cabinet" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "cabinet" ADD FOREIGN KEY ("tvacode") REFERENCES "tva" ("tvacode");
ALTER TABLE "cabinet" ADD FOREIGN KEY ("procode","prochrono") REFERENCES "charte" ("procode","prochrono");
ALTER TABLE "cabinet" ADD FOREIGN KEY ("natinscription") REFERENCES "natureinscription" ("natinscription");
ALTER TABLE "cabinet" ADD FOREIGN KEY ("juridiquestatut") REFERENCES "juridique" ("juridiquestatut");
ALTER TABLE "cbtcompetence" ADD FOREIGN KEY ("sinistrecode") REFERENCES "sinistre" ("sinistrecode");
ALTER TABLE "chantier" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "chantier" ADD FOREIGN KEY ("usageconstcode") REFERENCES "usageconstruction" ("usageconstcode");
ALTER TABLE "charte" ADD FOREIGN KEY ("docnom") REFERENCES "document" ("docnom");
ALTER TABLE "charte" ADD FOREIGN KEY ("typemisisoncode") REFERENCES "typemission" ("typemisisoncode");
ALTER TABLE "charte" ADD FOREIGN KEY ("sinistrecode") REFERENCES "sinistre" ("sinistrecode");
ALTER TABLE "clausechantier" ADD FOREIGN KEY ("chantiernum") REFERENCES "chantier" ("chantiernum");
ALTER TABLE "clausechantier" ADD FOREIGN KEY ("clausenum") REFERENCES "clause" ("clausenum");
ALTER TABLE "clausecontratsouscr" ADD FOREIGN KEY ("clausenum") REFERENCES "clause" ("clausenum");
ALTER TABLE "clausecontratsouscr" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "clausecontratsouscr" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "collabcommission" ADD FOREIGN KEY ("hononumfacture") REFERENCES "honoraire" ("hononumfacture");
ALTER TABLE "collabcommission" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "collabcompetence" ADD FOREIGN KEY ("sinistrecode") REFERENCES "sinistre" ("sinistrecode");
ALTER TABLE "collabcompetence" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "collabcompte" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "collabcompte" ADD FOREIGN KEY ("compteid") REFERENCES "compte" ("compteid");
ALTER TABLE "collabmultibase" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "collaborateur" ADD FOREIGN KEY ("idparametreutilisateur") REFERENCES "parametreutilisateur" ("idparametreutilisateur");
ALTER TABLE "collaborateur" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "collaborateur" ADD FOREIGN KEY ("procode","prochrono") REFERENCES "charte" ("procode","prochrono");
ALTER TABLE "collaborateur" ADD FOREIGN KEY ("tvacode") REFERENCES "tva" ("tvacode");
ALTER TABLE "collabpersonne" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "collabpersonne" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "collabsecteur" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "compte" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "compte" ADD FOREIGN KEY ("cbtcode","cbttype") REFERENCES "cabinet" ("cbtcode","cbttype");
ALTER TABLE "compte" ADD FOREIGN KEY ("ribnum") REFERENCES "rib" ("ribnum");
ALTER TABLE "consultationutil" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne") REFERENCES "historique" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne");
ALTER TABLE "consultationutil" ADD FOREIGN KEY ("reglementnum","compteid","type_piece") REFERENCES "reglement" ("reglementnum","compteid","type_piece");
ALTER TABLE "consultationutil" ADD FOREIGN KEY ("hononumfacture") REFERENCES "honoraire" ("hononumfacture");
ALTER TABLE "consultationutil" ADD FOREIGN KEY ("idparametreutilisateur") REFERENCES "parametreutilisateur" ("idparametreutilisateur");
ALTER TABLE "consultationutil" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "consultationutil" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "contactcbt" ADD FOREIGN KEY ("cbtcode","cbttype") REFERENCES "cabinet" ("cbtcode","cbttype");
ALTER TABLE "contactcbt" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "contratpersonne" ADD FOREIGN KEY ("persnuminter") REFERENCES "personne" ("persnum");
ALTER TABLE "contratpersonne" ADD FOREIGN KEY ("persnumassur") REFERENCES "personne" ("persnum");
ALTER TABLE "decisiondossier" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "decisiondossier" ADD FOREIGN KEY ("decisioncode") REFERENCES "decision" ("decisioncode");
ALTER TABLE "delaidossier" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "delaidossier" ADD FOREIGN KEY ("delaicontnum") REFERENCES "delaicontractuel" ("delaicontnum");
ALTER TABLE "desordre" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "desordre" ADD FOREIGN KEY ("chantiernum") REFERENCES "chantier" ("chantiernum");
ALTER TABLE "desordre" ADD FOREIGN KEY ("famdesordrecode") REFERENCES "familledesordre" ("famdesordrecode");
ALTER TABLE "destinataire" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "destinataire" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne") REFERENCES "historique" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne");
ALTER TABLE "destinatairereunion" ADD FOREIGN KEY ("reunionnum") REFERENCES "reunion" ("reunionnum");
ALTER TABLE "destinatairereunion" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "diversbati" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "diversbati" ADD FOREIGN KEY ("ticketannee") REFERENCES "ticketmoderateur" ("ticketannee");
ALTER TABLE "diversird" ADD FOREIGN KEY ("conventioncode") REFERENCES "convention" ("conventioncode");
ALTER TABLE "diversird" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "diversird" ADD FOREIGN KEY ("typeexpertisecode") REFERENCES "typeexpertise" ("typeexpertisecode");
ALTER TABLE "diversird" ADD FOREIGN KEY ("typemisisoncode") REFERENCES "typemission" ("typemisisoncode");
ALTER TABLE "diversird" ADD FOREIGN KEY ("motifcode") REFERENCES "motif" ("motifcode");
ALTER TABLE "diversmedi" ADD FOREIGN KEY ("secunum") REFERENCES "typecentresecu" ("secunum");
ALTER TABLE "diversmedi" ADD FOREIGN KEY ("typepiparnum") REFERENCES "typepipar" ("typepiparnum");
ALTER TABLE "diversmedi" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "diversmedi" ADD FOREIGN KEY ("typemisisoncode") REFERENCES "typemission" ("typemisisoncode");
ALTER TABLE "diversmedi" ADD FOREIGN KEY ("typeexpertisecode") REFERENCES "typeexpertise" ("typeexpertisecode");
ALTER TABLE "diversmedi" ADD FOREIGN KEY ("typepinum") REFERENCES "typepi" ("typepinum");
ALTER TABLE "diversmedi" ADD FOREIGN KEY ("prejuestnum") REFERENCES "typeprejuest" ("prejuestnum");
ALTER TABLE "diversmedi" ADD FOREIGN KEY ("professionnum") REFERENCES "typeprofession" ("professionnum");
ALTER TABLE "diversmedi" ADD FOREIGN KEY ("typ_professionnum") REFERENCES "typeprofession" ("professionnum");
ALTER TABLE "diversmedi" ADD FOREIGN KEY ("quantumnum") REFERENCES "typequantumdol" ("quantumnum");
ALTER TABLE "document" ADD FOREIGN KEY ("collabnummailto") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "document" ADD FOREIGN KEY ("secretnummailto") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "document" ADD FOREIGN KEY ("editeurcode") REFERENCES "editeur" ("editeurcode");
ALTER TABLE "document" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "document" ADD FOREIGN KEY ("groupecode") REFERENCES "groupersonne" ("groupecode");
ALTER TABLE "document" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "document" ADD FOREIGN KEY ("secretnum") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "document" ADD FOREIGN KEY ("groupecodemailto") REFERENCES "groupersonne" ("groupecode");
ALTER TABLE "document" ADD FOREIGN KEY ("typedoccode") REFERENCES "typedocument" ("typedoccode");
ALTER TABLE "dossier" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "dossier" ADD FOREIGN KEY ("cbtcode","cbttype") REFERENCES "cabinet" ("cbtcode","cbttype");
ALTER TABLE "dossier" ADD FOREIGN KEY ("cbtcode2","cbttype2") REFERENCES "cabinet" ("cbtcode","cbttype");
ALTER TABLE "dossier" ADD FOREIGN KEY ("chantiernum") REFERENCES "chantier" ("chantiernum");
ALTER TABLE "dossier" ADD FOREIGN KEY ("procode","prochrono") REFERENCES "charte" ("procode","prochrono");
ALTER TABLE "dossier" ADD FOREIGN KEY ("collabnum2") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "dossier" ADD FOREIGN KEY ("sinistrecode") REFERENCES "sinistre" ("sinistrecode");
ALTER TABLE "dossier" ADD FOREIGN KEY ("secretnum2") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "dossier" ADD FOREIGN KEY ("secretnum") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "dossier" ADD FOREIGN KEY ("metiercode","metierpwd") REFERENCES "metier" ("metiercode","metierpwd");
ALTER TABLE "dossier" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "dossier" ADD FOREIGN KEY ("famillecode") REFERENCES "famille" ("famillecode");
ALTER TABLE "dossierlesionsequelle" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "dossiertauxhoraire" ADD FOREIGN KEY ("tauxhorairenum") REFERENCES "tauxhoraire" ("tauxhorairenum");
ALTER TABLE "dossiertauxhoraire" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "droitaccesutilisateur" ADD FOREIGN KEY ("profilnum") REFERENCES "profil" ("profilnum");
ALTER TABLE "droitaccesutilisateur" ADD FOREIGN KEY ("formulairenum") REFERENCES "formulaire" ("formulairenum");
ALTER TABLE "fusionsdoc" ADD FOREIGN KEY ("docnom") REFERENCES "document" ("docnom");
ALTER TABLE "garantie" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "garantie" ADD FOREIGN KEY ("typecontratcode") REFERENCES "typecontrat" ("typecontratcode");
ALTER TABLE "grillehonoraire" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "grillehonoraire" ADD FOREIGN KEY ("typemisisoncode") REFERENCES "typemission" ("typemisisoncode");
ALTER TABLE "groupersonne" ADD FOREIGN KEY ("idparametreutilisateur") REFERENCES "parametreutilisateur" ("idparametreutilisateur");
ALTER TABLE "groupersonne" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "historique" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "historique" ADD FOREIGN KEY ("planningnum") REFERENCES "planning" ("planningnum");
ALTER TABLE "historique" ADD FOREIGN KEY ("reunionnum") REFERENCES "reunion" ("reunionnum");
ALTER TABLE "historique" ADD FOREIGN KEY ("procode","prochrono") REFERENCES "charte" ("procode","prochrono");
ALTER TABLE "histotransfert" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne") REFERENCES "historique" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne");
ALTER TABLE "honoraire" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "honoraire" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne") REFERENCES "historique" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne");
ALTER TABLE "honoraire" ADD FOREIGN KEY ("tresorcode") REFERENCES "tresorerie" ("tresorcode");
ALTER TABLE "honoraire" ADD FOREIGN KEY ("regsecretaire") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "honoraire" ADD FOREIGN KEY ("regsecrsecond") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "honorairedefaut" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "honorairedefaut" ADD FOREIGN KEY ("groupecode") REFERENCES "groupersonne" ("groupecode");
ALTER TABLE "honorairedefaut" ADD FOREIGN KEY ("payeurnum") REFERENCES "payeur" ("payeurnum");
ALTER TABLE "honorairedefaut" ADD FOREIGN KEY ("tvacode") REFERENCES "tva" ("tvacode");
ALTER TABLE "honorairedefaut" ADD FOREIGN KEY ("typemisisoncode") REFERENCES "typemission" ("typemisisoncode");
ALTER TABLE "honorairedefaut" ADD FOREIGN KEY ("docnom") REFERENCES "document" ("docnom");
ALTER TABLE "honoraireligne" ADD FOREIGN KEY ("hononumfacture") REFERENCES "honoraire" ("hononumfacture");
ALTER TABLE "honoraireligne" ADD FOREIGN KEY ("tvacode") REFERENCES "tva" ("tvacode");
ALTER TABLE "indemnitedossier" ADD FOREIGN KEY ("typeindemncode") REFERENCES "typeindemnite" ("typeindemncode");
ALTER TABLE "metier" ADD FOREIGN KEY ("profilnum") REFERENCES "profil" ("profilnum");
ALTER TABLE "objetformulaire" ADD FOREIGN KEY ("droitaccesnum") REFERENCES "droitaccesutilisateur" ("droitaccesnum");
ALTER TABLE "parametreformulaire" ADD FOREIGN KEY ("idparametreutilisateur") REFERENCES "parametreutilisateur" ("idparametreutilisateur");
ALTER TABLE "parametreformulaire" ADD FOREIGN KEY ("objetcode","droitaccesnum") REFERENCES "objetformulaire" ("objetcode","droitaccesnum");
ALTER TABLE "parametreutilisateur" ADD FOREIGN KEY ("profilnum") REFERENCES "profil" ("profilnum");
ALTER TABLE "payeur" ADD FOREIGN KEY ("compteid") REFERENCES "compte" ("compteid");
ALTER TABLE "payeur" ADD FOREIGN KEY ("hononumfacture") REFERENCES "honoraire" ("hononumfacture");
ALTER TABLE "payeur" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "personne" ADD FOREIGN KEY ("idparametreutilisateur") REFERENCES "parametreutilisateur" ("idparametreutilisateur");
ALTER TABLE "personne" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "personne" ADD FOREIGN KEY ("categentrcode") REFERENCES "telcategentr" ("categentrcode");
ALTER TABLE "personne" ADD FOREIGN KEY ("procode","prochrono") REFERENCES "charte" ("procode","prochrono");
ALTER TABLE "personne" ADD FOREIGN KEY ("conventioncode") REFERENCES "convention" ("conventioncode");
ALTER TABLE "personne" ADD FOREIGN KEY ("famillecode") REFERENCES "famille" ("famillecode");
ALTER TABLE "personne" ADD FOREIGN KEY ("groupecode") REFERENCES "groupersonne" ("groupecode");
ALTER TABLE "personne" ADD FOREIGN KEY ("natinscription") REFERENCES "natureinscription" ("natinscription");
ALTER TABLE "personne" ADD FOREIGN KEY ("compteid") REFERENCES "compte" ("compteid");
ALTER TABLE "personne" ADD FOREIGN KEY ("compteidparagence") REFERENCES "compte" ("compteid");
ALTER TABLE "personne" ADD FOREIGN KEY ("situationjuricode") REFERENCES "situationjuridique" ("situationjuricode");
ALTER TABLE "personne" ADD FOREIGN KEY ("specialitecode") REFERENCES "specialite" ("specialitecode");
ALTER TABLE "personne" ADD FOREIGN KEY ("juridiquestatut") REFERENCES "juridique" ("juridiquestatut");
ALTER TABLE "personne" ADD FOREIGN KEY ("typeperscode") REFERENCES "typepersonne" ("typeperscode");
ALTER TABLE "planning" ADD FOREIGN KEY ("collabnum2") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "planning" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "planning" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "planning" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "planning" ADD FOREIGN KEY ("civilitenum") REFERENCES "civilite" ("civilitenum");
ALTER TABLE "planning" ADD FOREIGN KEY ("typetelcode") REFERENCES "typetelephone" ("typetelcode");
ALTER TABLE "planning" ADD FOREIGN KEY ("typetelcode2") REFERENCES "typetelephone" ("typetelcode");
ALTER TABLE "postithisto" ADD FOREIGN KEY ("postitnum") REFERENCES "postit" ("postitnum");
ALTER TABLE "prejudicedossier" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "prejudicedossier" ADD FOREIGN KEY ("typeprejunum") REFERENCES "typeprejudice" ("typeprejunum");
ALTER TABLE "reglementligne" ADD FOREIGN KEY ("reglementnum","compteid","type_piece") REFERENCES "reglement" ("reglementnum","compteid","type_piece");
ALTER TABLE "reglementparam" ADD FOREIGN KEY ("tresorcode") REFERENCES "tresorerie" ("tresorcode");
ALTER TABLE "reglementparam" ADD FOREIGN KEY ("cbtcode","cbttype") REFERENCES "cabinet" ("cbtcode","cbttype");
ALTER TABLE "relance" ADD FOREIGN KEY ("secretnum") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "relance" ADD FOREIGN KEY ("hononumfacture") REFERENCES "honoraire" ("hononumfacture");
ALTER TABLE "reunion" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "rib" ADD FOREIGN KEY ("tresorcode") REFERENCES "tresorerie" ("tresorcode");
ALTER TABLE "secretaire" ADD FOREIGN KEY ("idparametreutilisateur") REFERENCES "parametreutilisateur" ("idparametreutilisateur");
ALTER TABLE "secretaire" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "secretcommission" ADD FOREIGN KEY ("secretnum") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "secretcommission" ADD FOREIGN KEY ("hononumfacture") REFERENCES "honoraire" ("hononumfacture");
ALTER TABLE "sinistre" ADD FOREIGN KEY ("famillecode") REFERENCES "famille" ("famillecode");
ALTER TABLE "sinistre" ADD FOREIGN KEY ("metiercode","metierpwd") REFERENCES "metier" ("metiercode","metierpwd");
ALTER TABLE "sinistrepersonne" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "sinistrepersonne" ADD FOREIGN KEY ("sinistrecode") REFERENCES "sinistre" ("sinistrecode");
ALTER TABLE "statparammaif" ADD FOREIGN KEY ("sinistrecode") REFERENCES "sinistre" ("sinistrecode");
ALTER TABLE "tauxhoraire" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "tauxhoraire" ADD FOREIGN KEY ("secretnum") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "telbareme" ADD FOREIGN KEY ("corpsetatcode") REFERENCES "telcorpsetat" ("corpsetatcode");
ALTER TABLE "telbareme" ADD FOREIGN KEY ("typebaremecode") REFERENCES "teltypebareme" ("typebaremecode");
ALTER TABLE "telcorpsetatentr" ADD FOREIGN KEY ("corpsetatcode") REFERENCES "telcorpsetat" ("corpsetatcode");
ALTER TABLE "teldevisligne" ADD FOREIGN KEY ("baremecode") REFERENCES "telbareme" ("baremecode");
ALTER TABLE "teldevisligne" ADD FOREIGN KEY ("corpsetatcode") REFERENCES "telcorpsetat" ("corpsetatcode");
ALTER TABLE "teldevisligne" ADD FOREIGN KEY ("devisnum") REFERENCES "teldevis" ("devisnum");
ALTER TABLE "teldevisligne" ADD FOREIGN KEY ("localisationcode") REFERENCES "tellocalisation" ("localisationcode");
ALTER TABLE "teldevisligne" ADD FOREIGN KEY ("typebaremecode") REFERENCES "teltypebareme" ("typebaremecode");
ALTER TABLE "telephone" ADD FOREIGN KEY ("typetelcode") REFERENCES "typetelephone" ("typetelcode");
ALTER TABLE "telephone" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "telponderation" ADD FOREIGN KEY ("corpsetatcode") REFERENCES "telcorpsetat" ("corpsetatcode");
ALTER TABLE "tresorerie" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "tresorerie" ADD FOREIGN KEY ("hononumfacture") REFERENCES "honoraire" ("hononumfacture");
ALTER TABLE "tva" ADD FOREIGN KEY ("cbtcode","cbttype") REFERENCES "cabinet" ("cbtcode","cbttype");
ALTER TABLE "typeexpertise" ADD FOREIGN KEY ("metiercode","metierpwd") REFERENCES "metier" ("metiercode","metierpwd");
ALTER TABLE "typemissionconvocation" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "typemissionconvocation" ADD FOREIGN KEY ("docnom") REFERENCES "document" ("docnom");
ALTER TABLE "typemissionconvocation" ADD FOREIGN KEY ("typemisisoncode") REFERENCES "typemission" ("typemisisoncode");
ALTER TABLE "lesionsequelle" ADD FOREIGN KEY ("pathologiecode") REFERENCES "pathologie" ("pathologiecode");
"garantie" ADD FOREIGN KEY ("typecontratcode") REFERENCES "typecontrat" ("typecontratcode");
ALTER TABLE "grillehonoraire" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "grillehonoraire" ADD FOREIGN KEY ("typemisisoncode") REFERENCES "typemission" ("typemisisoncode");
ALTER TABLE "groupersonne" ADD FOREIGN KEY ("idparametreutilisateur") REFERENCES "parametreutilisateur" ("idparametreutilisateur");
ALTER TABLE "groupersonne" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "historique" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "historique" ADD FOREIGN KEY ("planningnum") REFERENCES "planning" ("planningnum");
ALTER TABLE "historique" ADD FOREIGN KEY ("reunionnum") REFERENCES "reunion" ("reunionnum");
ALTER TABLE "historique" ADD FOREIGN KEY ("procode","prochrono") REFERENCES "charte" ("procode","prochrono");
ALTER TABLE "histotransfert" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne") REFERENCES "historique" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne");
ALTER TABLE "honoraire" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "honoraire" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne") REFERENCES "historique" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2","histoligne");
ALTER TABLE "honoraire" ADD FOREIGN KEY ("tresorcode") REFERENCES "tresorerie" ("tresorcode");
ALTER TABLE "honoraire" ADD FOREIGN KEY ("regsecretaire") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "honoraire" ADD FOREIGN KEY ("regsecrsecond") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "honorairedefaut" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "honorairedefaut" ADD FOREIGN KEY ("groupecode") REFERENCES "groupersonne" ("groupecode");
ALTER TABLE "honorairedefaut" ADD FOREIGN KEY ("payeurnum") REFERENCES "payeur" ("payeurnum");
ALTER TABLE "honorairedefaut" ADD FOREIGN KEY ("tvacode") REFERENCES "tva" ("tvacode");
ALTER TABLE "honorairedefaut" ADD FOREIGN KEY ("typemisisoncode") REFERENCES "typemission" ("typemisisoncode");
ALTER TABLE "honorairedefaut" ADD FOREIGN KEY ("docnom") REFERENCES "document" ("docnom");
ALTER TABLE "honoraireligne" ADD FOREIGN KEY ("hononumfacture") REFERENCES "honoraire" ("hononumfacture");
ALTER TABLE "honoraireligne" ADD FOREIGN KEY ("tvacode") REFERENCES "tva" ("tvacode");
ALTER TABLE "indemnitedossier" ADD FOREIGN KEY ("typeindemncode") REFERENCES "typeindemnite" ("typeindemncode");
ALTER TABLE "metier" ADD FOREIGN KEY ("profilnum") REFERENCES "profil" ("profilnum");
ALTER TABLE "objetformulaire" ADD FOREIGN KEY ("droitaccesnum") REFERENCES "droitaccesutilisateur" ("droitaccesnum");
ALTER TABLE "parametreformulaire" ADD FOREIGN KEY ("idparametreutilisateur") REFERENCES "parametreutilisateur" ("idparametreutilisateur");
ALTER TABLE "parametreformulaire" ADD FOREIGN KEY ("objetcode","droitaccesnum") REFERENCES "objetformulaire" ("objetcode","droitaccesnum");
ALTER TABLE "parametreutilisateur" ADD FOREIGN KEY ("profilnum") REFERENCES "profil" ("profilnum");
ALTER TABLE "payeur" ADD FOREIGN KEY ("compteid") REFERENCES "compte" ("compteid");
ALTER TABLE "payeur" ADD FOREIGN KEY ("hononumfacture") REFERENCES "honoraire" ("hononumfacture");
ALTER TABLE "payeur" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "personne" ADD FOREIGN KEY ("idparametreutilisateur") REFERENCES "parametreutilisateur" ("idparametreutilisateur");
ALTER TABLE "personne" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "personne" ADD FOREIGN KEY ("categentrcode") REFERENCES "telcategentr" ("categentrcode");
ALTER TABLE "personne" ADD FOREIGN KEY ("procode","prochrono") REFERENCES "charte" ("procode","prochrono");
ALTER TABLE "personne" ADD FOREIGN KEY ("conventioncode") REFERENCES "convention" ("conventioncode");
ALTER TABLE "personne" ADD FOREIGN KEY ("famillecode") REFERENCES "famille" ("famillecode");
ALTER TABLE "personne" ADD FOREIGN KEY ("groupecode") REFERENCES "groupersonne" ("groupecode");
ALTER TABLE "personne" ADD FOREIGN KEY ("natinscription") REFERENCES "natureinscription" ("natinscription");
ALTER TABLE "personne" ADD FOREIGN KEY ("compteid") REFERENCES "compte" ("compteid");
ALTER TABLE "personne" ADD FOREIGN KEY ("compteidparagence") REFERENCES "compte" ("compteid");
ALTER TABLE "personne" ADD FOREIGN KEY ("situationjuricode") REFERENCES "situationjuridique" ("situationjuricode");
ALTER TABLE "personne" ADD FOREIGN KEY ("specialitecode") REFERENCES "specialite" ("specialitecode");
ALTER TABLE "personne" ADD FOREIGN KEY ("juridiquestatut") REFERENCES "juridique" ("juridiquestatut");
ALTER TABLE "personne" ADD FOREIGN KEY ("typeperscode") REFERENCES "typepersonne" ("typeperscode");
ALTER TABLE "planning" ADD FOREIGN KEY ("collabnum2") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "planning" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "planning" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "planning" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "planning" ADD FOREIGN KEY ("civilitenum") REFERENCES "civilite" ("civilitenum");
ALTER TABLE "planning" ADD FOREIGN KEY ("typetelcode") REFERENCES "typetelephone" ("typetelcode");
ALTER TABLE "planning" ADD FOREIGN KEY ("typetelcode2") REFERENCES "typetelephone" ("typetelcode");
ALTER TABLE "postithisto" ADD FOREIGN KEY ("postitnum") REFERENCES "postit" ("postitnum");
ALTER TABLE "prejudicedossier" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "prejudicedossier" ADD FOREIGN KEY ("typeprejunum") REFERENCES "typeprejudice" ("typeprejunum");
ALTER TABLE "reglementligne" ADD FOREIGN KEY ("reglementnum","compteid","type_piece") REFERENCES "reglement" ("reglementnum","compteid","type_piece");
ALTER TABLE "reglementparam" ADD FOREIGN KEY ("tresorcode") REFERENCES "tresorerie" ("tresorcode");
ALTER TABLE "reglementparam" ADD FOREIGN KEY ("cbtcode","cbttype") REFERENCES "cabinet" ("cbtcode","cbttype");
ALTER TABLE "relance" ADD FOREIGN KEY ("secretnum") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "relance" ADD FOREIGN KEY ("hononumfacture") REFERENCES "honoraire" ("hononumfacture");
ALTER TABLE "reunion" ADD FOREIGN KEY ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2") REFERENCES "dossier" ("dossnum","cbtcode","cbttype","cbtcode2","cbttype2");
ALTER TABLE "rib" ADD FOREIGN KEY ("tresorcode") REFERENCES "tresorerie" ("tresorcode");
ALTER TABLE "secretaire" ADD FOREIGN KEY ("idparametreutilisateur") REFERENCES "parametreutilisateur" ("idparametreutilisateur");
ALTER TABLE "secretaire" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "secretcommission" ADD FOREIGN KEY ("secretnum") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "secretcommission" ADD FOREIGN KEY ("hononumfacture") REFERENCES "honoraire" ("hononumfacture");
ALTER TABLE "sinistre" ADD FOREIGN KEY ("famillecode") REFERENCES "famille" ("famillecode");
ALTER TABLE "sinistre" ADD FOREIGN KEY ("metiercode","metierpwd") REFERENCES "metier" ("metiercode","metierpwd");
ALTER TABLE "sinistrepersonne" ADD FOREIGN KEY ("persnum") REFERENCES "personne" ("persnum");
ALTER TABLE "sinistrepersonne" ADD FOREIGN KEY ("sinistrecode") REFERENCES "sinistre" ("sinistrecode");
ALTER TABLE "statparammaif" ADD FOREIGN KEY ("sinistrecode") REFERENCES "sinistre" ("sinistrecode");
ALTER TABLE "tauxhoraire" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "tauxhoraire" ADD FOREIGN KEY ("secretnum") REFERENCES "secretaire" ("secretnum");
ALTER TABLE "telbareme" ADD FOREIGN KEY ("corpsetatcode") REFERENCES "telcorpsetat" ("corpsetatcode");
ALTER TABLE "telbareme" ADD FOREIGN KEY ("typebaremecode") REFERENCES "teltypebareme" ("typebaremecode");
ALTER TABLE "telcorpsetatentr" ADD FOREIGN KEY ("corpsetatcode") REFERENCES "telcorpsetat" ("corpsetatcode");
ALTER TABLE "teldevisligne" ADD FOREIGN KEY ("baremecode") REFERENCES "telbareme" ("baremecode");
ALTER TABLE "teldevisligne" ADD FOREIGN KEY ("corpsetatcode") REFERENCES "telcorpsetat" ("corpsetatcode");
ALTER TABLE "teldevisligne" ADD FOREIGN KEY ("devisnum") REFERENCES "teldevis" ("devisnum");
ALTER TABLE "teldevisligne" ADD FOREIGN KEY ("localisationcode") REFERENCES "tellocalisation" ("localisationcode");
ALTER TABLE "teldevisligne" ADD FOREIGN KEY ("typebaremecode") REFERENCES "teltypebareme" ("typebaremecode");
ALTER TABLE "telephone" ADD FOREIGN KEY ("typetelcode") REFERENCES "typetelephone" ("typetelcode");
ALTER TABLE "telephone" ADD FOREIGN KEY ("adminnum") REFERENCES "admin" ("adminnum");
ALTER TABLE "telponderation" ADD FOREIGN KEY ("corpsetatcode") REFERENCES "telcorpsetat" ("corpsetatcode");
ALTER TABLE "tresorerie" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "tresorerie" ADD FOREIGN KEY ("hononumfacture") REFERENCES "honoraire" ("hononumfacture");
ALTER TABLE "tva" ADD FOREIGN KEY ("cbtcode","cbttype") REFERENCES "cabinet" ("cbtcode","cbttype");
ALTER TABLE "typeexpertise" ADD FOREIGN KEY ("metiercode","metierpwd") REFERENCES "metier" ("metiercode","metierpwd");
ALTER TABLE "typemissionconvocation" ADD FOREIGN KEY ("collabnum") REFERENCES "collaborateur" ("collabnum");
ALTER TABLE "typemissionconvocation" ADD FOREIGN KEY ("docnom") REFERENCES "document" ("docnom");
ALTER TABLE "typemissionconvocation" ADD FOREIGN KEY ("typemisisoncode") REFERENCES "typemission" ("typemisisoncode");
ALTER TABLE "lesionsequelle" ADD FOREIGN KEY ("pathologiecode") REFERENCES "pathologie" ("pathologiecode");
ALTER TABLE "lesionsequelle" ADD FOREIGN KEY ("lesionsequellecode") REFERENCES "typesequelle" ("sequellecode");
